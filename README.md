# dtrans-standalone
This repository contains all code for the dtrans-router which is required for receiving documents via dtrans transmission standard.

## RestAPI documentation
The documentation to the restapi can be found under docs/restapi.yaml. This file is valid a OpenAPI 3.0 definition and can be displayed with a proper viewer or any text editor.
All other documentations are replaced by this one.

## Setup and installation
### Prerequisites
Make dtrans.php available under /.well_known/dtrans

Ensure incoming/, processed/ and processing/ are writable for the web server.

Replace @@PATH_DKIM_PRIVATE_KEY@@, @@SELECTOR@@, @@SERVER_FQDN@@, @@DB_USERNAME@@ and @@DB_PASSWORD@@ in config.php.

Ensure the DKIM public key is available according to DTRANS standard.

### Run composer:
composer --no-progress --apcu-autoloader --optimize-autoloader -n update

### Generate php classes from xsd:
./deploy/generate-classes.sh