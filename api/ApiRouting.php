<?php

namespace Dtrans\api;

use Dtrans\api\controllers\AbstractController;
use Dtrans\api\controllers\DocumentsChain;
use Dtrans\api\controllers\DocumentsDelete;
use Dtrans\api\controllers\DocumentsEditChain;
use Dtrans\api\controllers\DocumentsEditChainState;
use Dtrans\api\controllers\DocumentsEditNote;
use Dtrans\api\controllers\DocumentsEinLieferschein;
use Dtrans\api\controllers\DocumentsGet;
use Dtrans\api\controllers\DocumentsList;
use Dtrans\api\controllers\DocumentsPDF;
use Dtrans\api\controllers\DocumentsShareCreate;
use Dtrans\api\controllers\DocumentsShareDelete;
use Dtrans\api\controllers\DocumentsShareGet;
use Dtrans\api\controllers\DocumentsUBL;
use Dtrans\api\controllers\DtransDocumentState;
use Dtrans\api\controllers\PDFGenerationAvailable;
use Dtrans\api\controllers\PDFGenerator;
use Dtrans\api\controllers\UsersGet;
use Dtrans\api\login\ApiUser;
use Dtrans\api\login\AuthController;
use Dtrans\api\login\LoginLDAP;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\PathResolver;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

abstract class ApiRouting
{
    // pre authentication routing. all this functionality can be accessed WITHOUT session token
    public static function handle_auth(AbstractSerializer $serializer): ?ApiUser
    {
        $path = PathResolver::resolve();
        if (empty($path)) {
            http_response_code(HttpStatusCodesEnum::BAD_REQUEST);
            exit();
        }

        // routing for not logged in case
        switch ($path) {
            case "auth/token/login": // attempt to create new token for user
            LoginLDAP::handle_login($serializer);
                break;
            case "auth/token/logout": // invalidate existing session token
                AuthController::handle_logout_token();
                break;
            case "documents/share/get": // for accessing shared document without token
                $code = DocumentsShareGet::process_not_logged_in();
                http_response_code($code);
                break;
            default:
                // validate token and send to default routing below on success
                return AuthController::validate_token();
        }

        // die after no login operation
        exit();
    }

    // default routing to controller AFTER authentication was successful
    public static function handle(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): ?AbstractController
    {
        switch ($request->get_path()) {
            case "documents/list":
                return new DocumentsList($request, $user, $serializer);
            case "documents/get":
                return new DocumentsGet($request, $user, $serializer);
            case "documents/chain":
                return new DocumentsChain($request, $user, $serializer);
            case "documents/share/create":
                return new DocumentsShareCreate($request, $user, $serializer);
            case "documents/share/delete":
                return new DocumentsShareDelete($request, $user, $serializer);
            case "documents/share/get":
                return new DocumentsShareGet($request, $user, $serializer);
            case "documents/edit/archived_state":
            case "documents/edit/handling_state":
                return new DocumentsEditChainState($request, $user, $serializer);
            case "documents/edit/chain":
                return new DocumentsEditChain($request, $user, $serializer);
            case "documents/edit/note":
                return new DocumentsEditNote($request, $user, $serializer);
            case "documents/delete/document":
            case "documents/delete/chain":
                return new DocumentsDelete($request, $user, $serializer);
            case "documents/new/ubl":
                return new DocumentsUBL($request, $user, $serializer);
            case "documents/new/1lieferschein":
                return new DocumentsEinLieferschein($request, $user, $serializer);
            case "documents/new/pdf":
                return new DocumentsPDF($request, $user, $serializer);
            case "dtrans/document_state":
                return new DtransDocumentState($request, $user, $serializer);
            case "users/get":
                return new UsersGet($request, $user, $serializer);
            case "pdf/create":
                return new PDFGenerator($request, $user, $serializer);
            case "pdf/features":
                return new PDFGenerationAvailable($request, $user, $serializer);
            default;
                DtransLogger::debug("Requested api function not found.", ["path" => $request->get_path()]);
                echo 'Error: Function not found.';
                http_response_code(HttpStatusCodesEnum::NOT_FOUND);
                return null;
        }
    }
}