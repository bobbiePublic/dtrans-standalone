<?php

namespace Dtrans\api\controllers;

use Dtrans\api\login\ApiUser;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;


abstract class AbstractController {
    public function __construct(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer)
    {
        $code = $this->process($request, $user, $serializer);
        http_response_code($code);

        if(UserFeedback::has_data()) {
           echo $serializer->serialize([]);
        }
    }

    protected abstract function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int;
}