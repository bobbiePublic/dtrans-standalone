<?php

namespace Dtrans\api\controllers;

use Dtrans\api\login\ApiUser;
use Dtrans\core\constants\ConstsApi;
use Dtrans\core\constants\ConstsJson;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\models\ModelDocumentsChains;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableDocuments;
use Dtrans\core\database\tables\DBTableDocumentsChains;
use Dtrans\core\enums\ArchivedStatusEnum;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\SanitizeHelper;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\permissions\PermissionFlags;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class DocumentsChain extends AbstractController
{

    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        // only allow get
        if (strcmp($request->get_request_method(), RequestMethodsEnum::GET) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }

        if (!$user->has_permission_flag(PermissionFlags::ACCESS_METADATA_DOCUMENTS)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        $chain = $request->get_param(ConstsApi::PARAM_GET_CHAIN);
        $uuid = $request->get_param(ConstsApi::PARAM_GET_UUID);
        $full = (bool)SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_DETAILED), false, ParameterTypesEnum::BOOLEAN);

        if (empty($chain) && empty($uuid)) {
            UserFeedback::error(ConstsStrings::CODE_PARAMETER_MISSING, 'chain or uuid');
            return HttpStatusCodesEnum::BAD_REQUEST;
        }

        if (!empty($chain) && !empty($uuid))
            UserFeedback::warning_custom('Parameter {0} was ignored, because {1} is also set to a value.', ConstsApi::PARAM_GET_UUID, ConstsApi::PARAM_GET_CHAIN);

        if (empty($chain)) {
            $chain = ModelDocuments::get_document_info($uuid);
            if (empty($chain) || empty($chain[DBTableDocuments::CHAIN_ID])) {
                UserFeedback::error(ConstsStrings::CODE_HTTP_NOT_FOUND);
                return HttpStatusCodesEnum::NOT_FOUND;
            }
            $chain = $chain[DBTableDocuments::CHAIN_ID];
        }

        $info = ModelDocumentsChains::get_document_chain_info($chain);
        if ($info === null) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_NOT_FOUND);
            return HttpStatusCodesEnum::NOT_FOUND;
        }

        if (strcmp($info[DBTableDocumentsChains::ARCHIVED_STATUS], ArchivedStatusEnum::ARCHIVED) === 0 && !$user->has_permission_flag(PermissionFlags::ACCESS_ARCHIVED_DOCUMENTS)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        // fill json
        $response[ConstsJson::JSON_CHAIN_ID] = $info[DBTableDocumentsChains::CHAINS_ID];
        $response[ConstsJson::JSON_CHAIN_ARCHIVED_STATUS] = $info[DBTableDocumentsChains::ARCHIVED_STATUS];
        $response[ConstsJson::JSON_CHAIN_HANDLING_STATUS] = $info[DBTableDocumentsChains::HANDLING_STATUS];
        $response[ConstsJson::JSON_CHAIN_DOCUMENT_COUNT] = $info[DBTableDocumentsChains::DOCUMENT_COUNT];
        $response[ConstsJson::JSON_CHAIN_UPDATED_AT] = $info[DBTable::UPDATED_AT];
        $response[ConstsJson::JSON_CHAIN_CREATED_AT] = $info[DBTable::CREATED_AT];

        if ($full) {
            $docs = ModelDocuments::get_all_documents_by_chain($chain);
            $response[ConstsJson::JSON_DOCUMENTS_LIST] = $docs;
        }

        // return json
        echo $serializer->serialize($response);
        return HttpStatusCodesEnum::OK;
    }
}