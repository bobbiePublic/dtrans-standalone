<?php

namespace Dtrans\api\controllers;

use Dtrans\api\login\ApiUser;
use Dtrans\core\constants\ConstsApi;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\models\ModelDocumentsChains;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\SanitizeHelper;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\permissions\PermissionFlags;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class DocumentsDelete extends AbstractController
{
    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        // only allow get
        if (strcmp($request->get_request_method(), RequestMethodsEnum::DELETE) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }

        if (!$user->has_permission_flag(PermissionFlags::IS_ADMIN)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        if (strtolower($request->get_path()) == "documents/delete/document") {
            $uuid = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_UUID), null, ParameterTypesEnum::UUID);

            // something went wrong while parsing the id parameter
            if (empty($uuid)) {
                UserFeedback::error(ConstsStrings::CODE_PARAMETER_MISSING, ConstsApi::PARAM_GET_UUID);
                return HttpStatusCodesEnum::BAD_REQUEST;
            }

            return ModelDocuments::delete_document($uuid) ? HttpStatusCodesEnum::OK : HttpStatusCodesEnum::NOT_FOUND;
        } else {
            $id = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_CHAIN), null, ParameterTypesEnum::CHAIN_ID);

            // something went wrong while parsing the id parameter
            if (empty($id)) {
                UserFeedback::error(ConstsStrings::CODE_PARAMETER_MISSING, ConstsApi::PARAM_GET_CHAIN);
                return HttpStatusCodesEnum::BAD_REQUEST;
            }

            return ModelDocumentsChains::delete_chain($id) ? HttpStatusCodesEnum::OK : HttpStatusCodesEnum::NOT_FOUND;
        }
    }
}
