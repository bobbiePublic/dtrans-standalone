<?php

namespace Dtrans\api\controllers;

use Dtrans\api\login\ApiUser;
use Dtrans\core\constants\ConstsJson;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\database\models\ModelDocumentsChains;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\SanitizeHelper;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\permissions\PermissionFlags;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class DocumentsEditChainState extends AbstractController
{
    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        // only allow get
        if (strcmp($request->get_request_method(), RequestMethodsEnum::PUT) !== 0 &&
            strcmp($request->get_request_method(), RequestMethodsEnum::POST) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }

        $archived_state = $request->get_path() == 'documents/edit/archived_state';

        if (!$user->has_permission_flag(PermissionFlags::MODIFY_STATUS_DOCUMENTS)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        // check if json is in body
        if (empty($request->get_data())) {
            UserFeedback::error(ConstsStrings::CODE_BODY_JSON_MISSING);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }
        $json = $serializer->deserialize($request->get_data());
        if (empty($json)) {
            UserFeedback::error(ConstsStrings::CODE_BODY_JSON_MISSING);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }
        if (empty($json[ConstsJson::JSON_STATE_NEW_STATE])) {
            UserFeedback::error(ConstsStrings::CODE_JSON_ATTRIBUTE_MISSING, ConstsJson::JSON_STATE_NEW_STATE);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }
        if (empty($json[ConstsJson::JSON_STATE_CHAIN_ID])) {
            UserFeedback::error(ConstsStrings::CODE_JSON_ATTRIBUTE_MISSING, ConstsJson::JSON_STATE_CHAIN_ID);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }

        // parse new state
        $new_state = SanitizeHelper::sanitize($json[ConstsJson::JSON_STATE_NEW_STATE], null, $archived_state ? ParameterTypesEnum::ARCHIVED_STATUS : ParameterTypesEnum::HANDLING_STATE);
        if (empty($new_state) || count($new_state) > 1) {
            UserFeedback::error(ConstsStrings::CODE_JSON_ATTRIBUTE_INVALID, ConstsJson::JSON_STATE_NEW_STATE);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }
        $new_state = $new_state[0];

        // parse document chain
        $chain = SanitizeHelper::sanitize($json[ConstsJson::JSON_STATE_CHAIN_ID], null, ParameterTypesEnum::CHAIN_ID);
        if (empty($chain)) {
            UserFeedback::error(ConstsStrings::CODE_JSON_ATTRIBUTE_INVALID, ConstsJson::JSON_STATE_CHAIN_ID);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }

        if ($archived_state)
            $suc = ModelDocumentsChains::set_document_chain_archive_state($chain, $new_state);
        else
            $suc = ModelDocumentsChains::set_document_chain_handling_state($chain, $new_state, true);

        return $suc ? HttpStatusCodesEnum::OK : HttpStatusCodesEnum::NOT_FOUND;
    }
}
