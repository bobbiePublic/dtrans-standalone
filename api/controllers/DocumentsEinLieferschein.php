<?php

namespace Dtrans\api\controllers;

use Dtrans\api\login\ApiUser;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\enums\DtransFormatsEnum;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\IncomingInterfaceEnum;
use Dtrans\core\enums\ProcessingReturnCodesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\permissions\PermissionFlags;
use Dtrans\core\providers\einlieferschein\LieferscheinProcessor;
use Dtrans\core\providers\IncomingProcessor;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class DocumentsEinLieferschein extends AbstractController
{

    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        // only allow POST
        if(strcmp($request->get_request_method(), RequestMethodsEnum::POST) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }

        if (!$user->has_permission_flag(PermissionFlags::SUBMIT_DOCUMENTS)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        $xml = $request->get_data();
        if(empty($xml))
        {
            UserFeedback::error(ConstsStrings::CODE_BODY_EMPTY);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }

        $file_name = IncomingProcessor::create_file($xml, DtransFormatsEnum::EINLIEFERSCHEIN, IncomingInterfaceEnum::API, $user->get_reference_string());
        if(is_null($file_name))
            return HttpStatusCodesEnum::CONFLICT;

        $res = LieferscheinProcessor::process($xml, $file_name);
        switch ($res) {
            case ProcessingReturnCodesEnum::PROCESSED:      return HttpStatusCodesEnum::CREATED;
            case ProcessingReturnCodesEnum::DUPLICATE:      return HttpStatusCodesEnum::CONFLICT;
            case ProcessingReturnCodesEnum::ERROR:          return HttpStatusCodesEnum::BAD_REQUEST;
            default:
                DtransLogger::error(__FILE__ . ": Unknown result '" . $res . "'");
                return HttpStatusCodesEnum::INTERNAL_SERVER_ERROR;
        }
    }
}
