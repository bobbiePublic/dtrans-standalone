<?php

namespace Dtrans\api\controllers;

use DOMDocument;
use Dtrans\api\login\ApiUser;
use Dtrans\core\constants\ConstsApi;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\tables\DBTableDocuments;
use Dtrans\core\database\tables\DBTableDocumentsChains;
use Dtrans\core\enums\ArchivedStatusEnum;
use Dtrans\core\enums\DocumentTypeEnum;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\DocumentReader;
use Dtrans\core\helpers\SanitizeHelper;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\permissions\PermissionFlags;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class DocumentsGet extends AbstractController
{
    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        // only allow get
        if(strcmp($request->get_request_method(), RequestMethodsEnum::GET) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }

        if (!$user->has_permission_flag(PermissionFlags::ACCESS_CONTENT_DOCUMENTS)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        $uuid = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_UUID), null, ParameterTypesEnum::UUID);
        $prettify = (bool)SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_PRETTIFY), false, ParameterTypesEnum::BOOLEAN);

        // something went wrong while parsing the id parameter
        if(empty($uuid))
        {
            UserFeedback::error(ConstsStrings::CODE_PARAMETER_MISSING, ConstsApi::PARAM_GET_UUID);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }

        // get document
        $doc = ModelDocuments::get_document_info($uuid);
        if(empty($doc))
        {
            UserFeedback::error(ConstsStrings::CODE_HTTP_NOT_FOUND);
            return HttpStatusCodesEnum::NOT_FOUND;
        }

        // only allow access to archived documents if allowed
        $archived = strcmp($doc[DBTableDocumentsChains::ARCHIVED_STATUS], ArchivedStatusEnum::ARCHIVED) === 0;
        if ($archived && !$user->has_permission_flag(PermissionFlags::ACCESS_ARCHIVED_DOCUMENTS)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        switch ($doc[DBTableDocuments::TYPE]) {
            case DocumentTypeEnum::PDF:
            case DocumentTypeEnum::PDF_HYBRID:
                header('Content-Type: ' . 'application/pdf');
                break;
            default:
                header('Content-Type: ' . 'text/xml; charset=UTF-8');
                break;
        }

        $xml = DocumentReader::read_document($doc[DBTableDocuments::FILE_NAME], $doc[DBTableDocuments::TYPE]);
        if ($prettify && in_array($doc[DBTableDocuments::TYPE], [DocumentTypeEnum::UBL_DESPATCH_ADVICE, DocumentTypeEnum::UBL_RECEIPT_ADVICE, DocumentTypeEnum::EINLIEFERSCHEIN_DESPATCH_ADVICE, DocumentTypeEnum::EINLIEFERSCHEIN_RECEIPT_ADVICE])) {
            $dom = new DOMDocument();
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->loadXML($xml);
            $xml = $dom->saveXML();
        }

        // output xml
        echo $xml;
        return HttpStatusCodesEnum::OK;
    }
}
