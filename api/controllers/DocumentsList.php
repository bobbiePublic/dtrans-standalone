<?php

namespace Dtrans\api\controllers;

use Dtrans\api\login\ApiUser;
use Dtrans\core\constants\ConstsApi;
use Dtrans\core\constants\ConstsJson;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\database\models\ModelDocumentsList;
use Dtrans\core\database\tables\DBTableDocuments;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\ListviewColumnEnum;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\SanitizeHelper;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\permissions\PermissionFlags;
use Dtrans\core\sanitization\SanitizeJsonDocumentAttribute;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class DocumentsList extends AbstractController {

    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        // only allow get
        if(strcmp($request->get_request_method(), RequestMethodsEnum::GET) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }

        if (!$user->has_permission_flag(PermissionFlags::ACCESS_METADATA_DOCUMENTS)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        $columns = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_LISTVIEW_COLUMNS), null, ParameterTypesEnum::LISTVIEW_COLUMNS);
        if(empty($columns))
            $columns = ListviewColumnEnum::cases();

        // extract get parameters from request
        $page = (int)SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_PAGE), ConstsApi::DEFAULT_LISTVIEW_PAGE, ParameterTypesEnum::INTEGER);
        $page_size = (int)SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_PAGE_SIZE), ConstsApi::DEFAULT_LISTVIEW_PAGE_SIZE, ParameterTypesEnum::INTEGER);
        $offset = $page * $page_size;

        $filter_rejected = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_REJECTED), NULL, ParameterTypesEnum::BOOLEAN);

        $sort_asc = (bool)SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_SORT_ASC), ConstsApi::DEFAULT_LISTVIEW_SORT_ASC, ParameterTypesEnum::BOOLEAN);
        $sort_by = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_SORT_BY), ConstsApi::DEFAULT_LISTVIEW_SORT_BY, ParameterTypesEnum::JSON_DOCUMENT_ATTRIBUTE);
        if(!SanitizeJsonDocumentAttribute::is_column_active($sort_by, $columns))
            $sort_by = null;

        $filter_created_after = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_CREATED_AFTER), NULL, ParameterTypesEnum::FILTER_TIMESTAMP);
        $filter_chain = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_CHAIN), NULL, ParameterTypesEnum::FILTER_STRING);
        $filter_uuid = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_UUID), NULL, ParameterTypesEnum::FILTER_STRING);
        $filter_initial_version = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_INITIAL_VERSION), NULL, ParameterTypesEnum::FILTER_STRING);
        $filter_created_at = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_CREATED_AT), NULL, ParameterTypesEnum::FILTER_TIMESTAMP_INTERVAL);
        $filter_updated_at = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_UPDATED_AT), NULL, ParameterTypesEnum::FILTER_TIMESTAMP_INTERVAL);
        $filter_document_date = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_DOCUMENT_DATE), NULL, ParameterTypesEnum::FILTER_TIMESTAMP_INTERVAL);
        $filter_actual_shipping_date = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_ACTUAL_SHIPPING_DATE), NULL, ParameterTypesEnum::FILTER_TIMESTAMP_INTERVAL);
        $filter_estimated_shipping_date = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_ESTIMATED_SHIPPING_DATE), NULL, ParameterTypesEnum::FILTER_DATE);
        $filter_search = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_SEARCH), NULL, ParameterTypesEnum::FILTER_STRING);
        $filter_references = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_REFERENCES), NULL, ParameterTypesEnum::FILTER_STRING);
        $filter_despatch_supplier_party = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_DESPATCH_SUPPLIER_PARTY), NULL, ParameterTypesEnum::FILTER_STRING);
        $filter_delivery_costumer_party = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_DELIVERY_CUSTOMER_PARTY), NULL, ParameterTypesEnum::FILTER_STRING);
        $filter_buyer_costumer_party = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_BUYER_COSTUMER_PARTY), NULL, ParameterTypesEnum::FILTER_STRING);
        $filter_seller_supplier_party = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_SELLER_SUPPLIER_PARTY), NULL, ParameterTypesEnum::FILTER_STRING);
        $filter_originator_costumer_party = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_ORIGINATOR_COSTUMER_PARTY), NULL, ParameterTypesEnum::FILTER_STRING);
        $filter_pdf_embedded_document_uuid = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_PDF_EMBEDDED_DOCUMENT_UUID), NULL, ParameterTypesEnum::FILTER_STRING);
        $filter_note = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_NOTE), NULL, ParameterTypesEnum::FILTER_STRING);
        $filter_chainless = (bool)SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_CHAINLESS), false, ParameterTypesEnum::BOOLEAN);
        // enums
        $filter_document_type = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_DOCUMENT_TYPE), NULL, ParameterTypesEnum::DOCUMENT_TYPE);
        $filter_handling_status = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_HANDLING_STATUS), NULL, ParameterTypesEnum::HANDLING_STATE);
        $filter_archived_status = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_ARCHIVED_STATUS), NULL, ParameterTypesEnum::ARCHIVED_STATUS);
        $filter_version_status = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_FILTER_VERSION_STATUS), NULL, ParameterTypesEnum::VERSION_STATUS);

        $filters = ModelDocumentsList::create_filter($filter_search, $filter_created_after, $filter_chain, $filter_uuid, $filter_initial_version, $filter_version_status, $filter_created_at,
            $filter_updated_at, $filter_document_date, $filter_actual_shipping_date, $filter_estimated_shipping_date, $filter_references, $filter_despatch_supplier_party, $filter_delivery_costumer_party,
            $filter_buyer_costumer_party, $filter_seller_supplier_party, $filter_originator_costumer_party, $filter_rejected, $filter_handling_status, $filter_archived_status, $filter_document_type,
            $filter_pdf_embedded_document_uuid, $filter_note, $filter_chainless);

        // retrieve documents count
        $availableDocuments = ModelDocumentsList::count_available_documents($filters);
        // if no documents available send empty response
        if($availableDocuments === 0 || $offset > $availableDocuments) {
            $response[ConstsJson::JSON_DOCUMENTS_TOTAL] = $availableDocuments;
            $response[ConstsJson::JSON_DOCUMENTS_COUNT] = 0;
            $response[ConstsJson::JSON_DOCUMENTS_LIST] = array();
            echo $serializer->serialize($response);
            return HttpStatusCodesEnum::OK;
        }

        $docs = ModelDocumentsList::query_document_list($offset, $page_size, $sort_asc, $sort_by, $filters, $columns);
        if($docs === null) // something went wrong
            return HttpStatusCodesEnum::INTERNAL_SERVER_ERROR;

        if(in_array(ListviewColumnEnum::UBL_REFERENCES, $columns))
            foreach ($docs as $id => $doc) {
                if(array_key_exists(ConstsJson::JSON_DOCUMENT_REFERENCES, $doc)
                    && !empty($doc[ConstsJson::JSON_DOCUMENT_REFERENCES])) {
                    $object = json_decode($doc[ConstsJson::JSON_DOCUMENT_REFERENCES]);
                    unset($docs[$id][ConstsJson::JSON_DOCUMENT_REFERENCES]);
                    $docs[$id][ConstsJson::JSON_DOCUMENT_REFERENCES] = $object;
                }
            }

        // return json
        $response[ConstsJson::JSON_DOCUMENTS_TOTAL] = $availableDocuments;
        $response[ConstsJson::JSON_DOCUMENTS_COUNT] = sizeof($docs);
        $response[ConstsJson::JSON_DOCUMENTS_LIST] = $docs;
        echo $serializer->serialize($response);
        return HttpStatusCodesEnum::OK;
    }
}