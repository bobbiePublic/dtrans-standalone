<?php

namespace Dtrans\api\controllers;

use Dtrans\api\login\ApiUser;
use Dtrans\core\constants\ConstsApi;
use Dtrans\core\constants\ConstsJson;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\models\ModelDocumentsShared;
use Dtrans\core\database\tables\DBTableDocumentsShared;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\SanitizeHelper;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\permissions\PermissionFlags;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class DocumentsShareCreate extends AbstractController
{
    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        // only allow get
        if (strcmp($request->get_request_method(), RequestMethodsEnum::POST) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }

        if (!$user->has_permission_flag(PermissionFlags::SHARE_DOCUMENTS)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        $uuid = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_UUID), null, ParameterTypesEnum::UUID);

        // something went wrong while parsing the id parameter
        if (empty($uuid)) {
            UserFeedback::error(ConstsStrings::CODE_PARAMETER_MISSING, ConstsApi::PARAM_GET_UUID);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }

        // get document info
        $doc = ModelDocuments::get_document_info($uuid);
        if (empty($doc)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_NOT_FOUND);
            return HttpStatusCodesEnum::NOT_FOUND;
        }

        $key = ModelDocumentsShared::create_document_key($uuid, $user->get_reference_string());
        if (empty($key)) {
            UserFeedback::error(ConstsStrings::CODE_UNKNOWN_ERROR);
            DtransLogger::warning('Failed to create document share token.', ['checkpoint' => 1, 'uuid' => $uuid, 'user' => $user->get_reference_string()]);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }

        $details = ModelDocumentsShared::get_key_info($key);
        if (empty($details)) {
            UserFeedback::error(ConstsStrings::CODE_UNKNOWN_ERROR);
            DtransLogger::warning('Failed to create document share token.', ['checkpoint' => 2, 'uuid' => $uuid, 'user' => $user->get_reference_string()]);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }

        $json[ConstsJson::JSON_SHARE_KEY] = $key;
        $json[ConstsJson::JSON_SHARE_EXPIRES_AT] = $details[DBTableDocumentsShared::EXPIRES_AT];
        echo $serializer->serialize($json);
        return HttpStatusCodesEnum::CREATED;
    }
}
