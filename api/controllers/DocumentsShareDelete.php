<?php

namespace Dtrans\api\controllers;

use Dtrans\api\login\ApiUser;
use Dtrans\core\constants\ConstsApi;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\database\models\ModelDocumentsShared;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\SanitizeHelper;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\permissions\PermissionFlags;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class DocumentsShareDelete extends AbstractController
{

    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        // only allow post
        if (strcmp($request->get_request_method(), RequestMethodsEnum::DELETE) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }

        if (!$user->has_permission_flag(PermissionFlags::UNSHARE_DOCUMENTS)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        $key = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_SHARE_KEY), null, ParameterTypesEnum::FILTER_STRING);
        if (empty($key)) {
            UserFeedback::error(ConstsStrings::CODE_PARAMETER_MISSING, ConstsApi::PARAM_GET_SHARE_KEY);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }

        return ModelDocumentsShared::delete_document_key($key) ? HttpStatusCodesEnum::OK : HttpStatusCodesEnum::NOT_FOUND;
    }
}