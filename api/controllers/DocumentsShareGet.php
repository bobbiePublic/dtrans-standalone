<?php

namespace Dtrans\api\controllers;

use DOMDocument;
use Dtrans\api\login\ApiUser;
use Dtrans\core\constants\ConstsApi;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\models\ModelDocumentsShared;
use Dtrans\core\database\tables\DBTableDocuments;
use Dtrans\core\enums\DocumentTypeEnum;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\DocumentReader;
use Dtrans\core\helpers\SanitizeHelper;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class DocumentsShareGet extends AbstractController
{
    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        // only allow get
        if (strcmp($request->get_request_method(), RequestMethodsEnum::GET) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }

        $key = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_SHARE_KEY), null, ParameterTypesEnum::FILTER_STRING);
        $prettify = (bool)SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_PRETTIFY), false, ParameterTypesEnum::BOOLEAN);
        $bodiless = (bool)SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_BODILESS), false, ParameterTypesEnum::BOOLEAN);


        if (empty($key)) {
            UserFeedback::error(ConstsStrings::CODE_PARAMETER_MISSING, ConstsApi::PARAM_GET_SHARE_KEY);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }

        $uuid = ModelDocumentsShared::get_document_uuid_from_key($key);
        if (empty($uuid)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_NOT_FOUND);
            return HttpStatusCodesEnum::NOT_FOUND;
        }

        // get document
        $doc = ModelDocuments::get_document_info($uuid);
        if (empty($doc)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_NOT_FOUND);
            return HttpStatusCodesEnum::NOT_FOUND;
        }

        switch ($doc[DBTableDocuments::TYPE]) {
            case DocumentTypeEnum::PDF:
            case DocumentTypeEnum::PDF_HYBRID:
                header('Content-Type: ' . 'application/pdf');
                break;
            default:
                header('Content-Type: ' . 'text/xml; charset=UTF-8');
                break;
        }

        if ($bodiless)
            return HttpStatusCodesEnum::OK;

        $xml = DocumentReader::read_document($doc[DBTableDocuments::FILE_NAME], $doc[DBTableDocuments::TYPE]);
        if ($prettify && in_array($doc[DBTableDocuments::TYPE], [DocumentTypeEnum::UBL_DESPATCH_ADVICE, DocumentTypeEnum::UBL_RECEIPT_ADVICE, DocumentTypeEnum::EINLIEFERSCHEIN_DESPATCH_ADVICE, DocumentTypeEnum::EINLIEFERSCHEIN_RECEIPT_ADVICE])) {
            $dom = new DOMDocument();
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->loadXML($xml);
            $xml = $dom->saveXML();
        }

        echo $xml;
        return HttpStatusCodesEnum::OK;
    }

    public static function process_not_logged_in(): int
    {
        // only allow get
        if (strcmp(RequestMethodsEnum::parse($_SERVER['REQUEST_METHOD']), RequestMethodsEnum::GET) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }


        $key = SanitizeHelper::sanitize($_GET[ConstsApi::PARAM_GET_SHARE_KEY] ?? null, null, ParameterTypesEnum::FILTER_STRING);
        $prettify = (bool)SanitizeHelper::sanitize($_GET[ConstsApi::PARAM_GET_PRETTIFY] ?? false, false, ParameterTypesEnum::BOOLEAN);
        $bodiless = (bool)SanitizeHelper::sanitize($_GET[ConstsApi::PARAM_GET_BODILESS] ?? false, false, ParameterTypesEnum::BOOLEAN);

        if (empty($key)) {
            UserFeedback::error(ConstsStrings::CODE_PARAMETER_MISSING, ConstsApi::PARAM_GET_SHARE_KEY);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }

        $uuid = ModelDocumentsShared::get_document_uuid_from_key($key);
        if (empty($uuid)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_NOT_FOUND);
            return HttpStatusCodesEnum::NOT_FOUND;
        }

        // get document
        $doc = ModelDocuments::get_document_info($uuid);
        if (empty($doc)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_NOT_FOUND);
            return HttpStatusCodesEnum::NOT_FOUND;
        }

        switch ($doc[DBTableDocuments::TYPE]) {
            case DocumentTypeEnum::PDF:
            case DocumentTypeEnum::PDF_HYBRID:
                header('Content-Type: ' . 'application/pdf');
                break;
            default:
                header('Content-Type: ' . 'text/xml; charset=UTF-8');
                break;
        }

        if ($bodiless)
            return HttpStatusCodesEnum::OK;

        $xml = DocumentReader::read_document($doc[DBTableDocuments::FILE_NAME], $doc[DBTableDocuments::TYPE]);
        if ($prettify && in_array($doc[DBTableDocuments::TYPE], [DocumentTypeEnum::UBL_DESPATCH_ADVICE, DocumentTypeEnum::UBL_RECEIPT_ADVICE, DocumentTypeEnum::EINLIEFERSCHEIN_DESPATCH_ADVICE, DocumentTypeEnum::EINLIEFERSCHEIN_RECEIPT_ADVICE])) {
            $dom = new DOMDocument();
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->loadXML($xml);
            $xml = $dom->saveXML();
        }

        echo $xml;
        return HttpStatusCodesEnum::OK;
    }
}
