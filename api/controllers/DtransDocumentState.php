<?php

namespace Dtrans\api\controllers;

use Dtrans\api\login\ApiUser;
use Dtrans\core\constants\ConstsApi;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\models\ModelSend;
use Dtrans\core\database\models\ModelSending;
use Dtrans\core\database\tables\DBTableSendingQueue;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\permissions\PermissionFlags;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class DtransDocumentState extends AbstractController
{

    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        // only allow get
        if (strcmp($request->get_request_method(), RequestMethodsEnum::GET) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }

        if (!$user->has_permission_flag(PermissionFlags::ACCESS_METADATA_DOCUMENTS)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        $uuid = $request->get_param(ConstsApi::PARAM_GET_UUID);

        if (empty($uuid)) {
            UserFeedback::error(ConstsStrings::CODE_PARAMETER_MISSING, 'uuid');
            return HttpStatusCodesEnum::BAD_REQUEST;
        }


        $id = ModelDocuments::get_document_id_by_uuid($uuid);
        if ($id === null) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_NOT_FOUND, 'uuid');
            return HttpStatusCodesEnum::NOT_FOUND;
        }

        $sending = ModelSending::get_sending_result($id);
        $send = ModelSend::get_sending_result($id);
        $receivers = 0;
        // clean results
        if (!empty($sending))
            foreach ($sending as $entryIndex => $s) {
                unset($s[DBTableSendingQueue::ID]);
                $sending[$entryIndex] = $s;
                $receivers++;
            }
        else
            $sending = array();

        if (!empty($send))
            foreach ($send as $entryIndex => $s) {
                unset($s[DBTableSendingQueue::ID]);
                $send[$entryIndex] = $s;
                $receivers++;
            }
        else
            $send = array();

        // fill json
        $response['sendingDone'] = empty($sending);
        $response['receivers'] = $receivers;
        $response['done'] = $send;
        $response['queue'] = $sending;

        // return json
        echo $serializer->serialize($response);
        return HttpStatusCodesEnum::OK;
    }
}