<?php

namespace Dtrans\api\controllers;

use Dtrans\api\login\ApiUser;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\helpers\DtransConfig;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class PDFGenerationAvailable extends AbstractController
{
    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        $available = DtransConfig::get_bool(DtransConfig::CFG_API, 'pdf', 'enable')
            && !empty(DtransConfig::get_string(DtransConfig::CFG_API, 'pdf', 'pdf_builder_path'));
        $documents = array('pdfGenerationEnabled' => $available);
        echo json_encode($documents);
        return HttpStatusCodesEnum::OK;
    }
}
