<?php

namespace Dtrans\api\controllers;

use Dtrans\api\login\ApiUser;
use Dtrans\core\constants\ConstsApi;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\models\ModelDocumentsShared;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableDocuments;
use Dtrans\core\database\tables\DBTableDocumentsChains;
use Dtrans\core\enums\ArchivedStatusEnum;
use Dtrans\core\enums\DocumentTypeEnum;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\DtransConfig;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\SanitizeHelper;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\permissions\PermissionFlags;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class PDFGenerator extends AbstractController
{
    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        // check if module is enabled
        if (!DtransConfig::get_bool(DtransConfig::CFG_API, 'pdf', 'enable')
            || empty(DtransConfig::get_string(DtransConfig::CFG_API, 'pdf', 'pdf_builder_path'))) {
            UserFeedback::error_custom('PDF generation module is not enabled on this server.');
            return HttpStatusCodesEnum::CONFLICT;
        }

        // check if module can be loaded
        if (str_starts_with('/', DtransConfig::get_string(DtransConfig::CFG_API, 'pdf', 'pdf_builder_path')))
            @include DtransConfig::get_string(DtransConfig::CFG_API, 'pdf', 'pdf_builder_path');
        else
            @include __DIR__ . '/../../' . DtransConfig::get_string(DtransConfig::CFG_API, 'pdf', 'pdf_builder_path');
        if (!class_exists('DtransXML2PDF\\PDFBuilder'))
            return HttpStatusCodesEnum::NOT_IMPLEMENTED;

        // only allow get
        if (strcmp($request->get_request_method(), RequestMethodsEnum::GET) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }

        if (!$user->has_permission_flag(PermissionFlags::ACCESS_CONTENT_DOCUMENTS)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        $lang = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_LANG), null, ParameterTypesEnum::FILTER_STRING);
        $uuid = SanitizeHelper::sanitize($request->get_param(ConstsApi::PARAM_GET_UUID), null, ParameterTypesEnum::UUID);
        // something went wrong while parsing the $uuid parameter
        if (empty($uuid)) {
            UserFeedback::error(ConstsStrings::CODE_PARAMETER_MISSING, ConstsApi::PARAM_GET_UUID);
            return HttpStatusCodesEnum::BAD_REQUEST;
        }

        // get document
        $doc = ModelDocuments::get_document_info($uuid);
        if (empty($doc)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_NOT_FOUND);
            return HttpStatusCodesEnum::NOT_FOUND;
        }

        // only allow access to archived documents if allowed
        $archived = strcmp($doc[DBTableDocumentsChains::ARCHIVED_STATUS], ArchivedStatusEnum::ARCHIVED) === 0;
        if ($archived && !$user->has_permission_flag(PermissionFlags::ACCESS_ARCHIVED_DOCUMENTS)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_FORBIDDEN);
            return HttpStatusCodesEnum::FORBIDDEN;
        }

        switch ($doc[DBTableDocuments::TYPE]) {
            // allowed for pdf rendering
            case DocumentTypeEnum::EINLIEFERSCHEIN_DESPATCH_ADVICE:
            case DocumentTypeEnum::EINLIEFERSCHEIN_RECEIPT_ADVICE:
            case DocumentTypeEnum::UBL_DESPATCH_ADVICE:
            case DocumentTypeEnum::UBL_RECEIPT_ADVICE:
                break;

            // disallowed by default
            default:
                UserFeedback::error(ConstsStrings::CODE_HTTP_BAD_REQUEST);
                return HttpStatusCodesEnum::BAD_REQUEST;
        }

        // create sharing key
        $expires_at = DBTable::TIME_NOW . ' + INTERVAL ' . '20' . ' MINUTE';
        $key = ModelDocumentsShared::create_document_key($uuid, "Created for PDF_GENERATION for " . $user->get_reference_string(), $expires_at);
        if (empty($key)) {
            UserFeedback::error(ConstsStrings::CODE_UNKNOWN_ERROR);
            DtransLogger::warning('Failed to create document share token.', ['checkpoint' => 1, 'uuid' => $uuid, 'user' => $user->get_reference_string()]);
            return HttpStatusCodesEnum::INTERNAL_SERVER_ERROR;
        }

        $details = ModelDocumentsShared::get_key_info($key);
        if (empty($details)) {
            UserFeedback::error(ConstsStrings::CODE_UNKNOWN_ERROR);
            DtransLogger::warning('Failed to create document share token.', ['checkpoint' => 2, 'uuid' => $uuid, 'user' => $user->get_reference_string()]);
            return HttpStatusCodesEnum::INTERNAL_SERVER_ERROR;
        }

        // now provide this key to pdf renderer
        try {
            header('Content-Type: ' . 'application/pdf');
            $embed_xml = DtransConfig::get_bool(DtransConfig::CFG_API, 'pdf', 'pdf_builder_embed_xml', true);
            $frontend_uri = DtransConfig::get_string(DtransConfig::CFG_DTRANS, "general", "frontend_uri");
            $builder = new \DtransXML2PDF\PDFBuilder($key, $frontend_uri, $lang, $embed_xml);
            echo $builder->create_pdf();
        } catch (\Throwable $e) {
            DtransLogger::warning('Failed to render pdf', ['exception' => $e->getMessage()]);
            // module was likely not loaded
            return HttpStatusCodesEnum::NOT_IMPLEMENTED;
        }

        // output xml
        return HttpStatusCodesEnum::OK;
    }
}
