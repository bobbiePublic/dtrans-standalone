<?php

namespace Dtrans\api\controllers;

use Dtrans\api\login\ApiUser;
use Dtrans\core\constants\ConstsJson;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\ApiRequest;

class UsersGet extends AbstractController
{
    protected function process(ApiRequest $request, ApiUser $user, AbstractSerializer $serializer): int
    {
        // only allow get
        if (strcmp($request->get_request_method(), RequestMethodsEnum::GET) !== 0) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_METHOD_NOT_ALLOWED);
            return HttpStatusCodesEnum::NOT_ALLOWED;
        }

        $u = $user->get_user_data();
        if (is_null($u)) {
            UserFeedback::error(ConstsStrings::CODE_HTTP_NOT_FOUND);
            return HttpStatusCodesEnum::NOT_FOUND;
        }


        $address = array();
        if(!empty($u->addressFull))
            $address = explode('$', $u->addressFull);
        else {
            if(!empty($u->companyName))
                $address[] =  $u->companyName;
            if(!empty($u->addressStreet))
                $address[] = $u->addressStreet;
            $c = "";
            if(!empty($u->addressZip))
                $c = $u->addressZip;
            if(!empty($u->addressCity))
                $c .= (strlen($c) > 0 ? ' ' : '') . $u->addressCity;
            if(!empty($c))
                $address[] = $c;
            if(!empty($u->addressRegion))
                $address[] = $u->addressRegion;
            if(!empty($u->addressCountry))
                $address[] = $u->addressCountry;
        }

        $user_data = [
            ConstsJson::JSON_USER_FIRST_NAME => $u->firstName,
            ConstsJson::JSON_USER_LAST_NAME => $u->lastName,
            ConstsJson::JSON_USER_COMPANY_NAME => $u->companyName,
            ConstsJson::JSON_USER_COMPANY_POSITION => $u->companyPosition,
            ConstsJson::JSON_USER_PHONE => $u->phone,
            ConstsJson::JSON_USER_MOBILE => $u->mobile,
            ConstsJson::JSON_USER_FAX => $u->fax,
            ConstsJson::JSON_USER_MAIL => $u->mail,
            ConstsJson::JSON_USER_ADDRESS => $address,
            ConstsJson::JSON_USER_ROLE => $user->get_app_role(),
            ConstsJson::JSON_SESSION_CREATED_AT => $u->sessionCreatedAt,
            ConstsJson::JSON_SESSION_EXPIRES_AT => $u->sessionExpiresAt,
        ];



        $json = $serializer->serialize($user_data);
        echo $json;
        return HttpStatusCodesEnum::OK;
    }
}
