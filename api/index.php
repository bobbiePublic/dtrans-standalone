<?php

namespace Dtrans;

require_once __DIR__ . '/../config.php';

use Dtrans\api\ApiRouting;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\serializer\JsonSerializer;
use Dtrans\core\types\ApiRequest;

// create serializer
$serializer = new JsonSerializer();

$user = ApiRouting::handle_auth($serializer);
if(is_null($user)) {
    http_response_code(HttpStatusCodesEnum::UNAUTHORIZED);
    exit();
}

// try to parse request attributes
$request = new ApiRequest();

// abort if path is faulty
if($request->get_path() === null) {
    echo "Error: Resolving path.";
    http_response_code(HttpStatusCodesEnum::BAD_REQUEST);
    return;
}

if(!$request->request_method_is_valid()) {
    echo "Error: Unsupported Request Method.";
    http_response_code(HttpStatusCodesEnum::NOT_ALLOWED);
    return;
}

DtransLogger::debug("Request to api function.", ["path" => $request->get_path(), 'user' => $user->get_reference_string()]);
ApiRouting::handle($request, $user, $serializer);