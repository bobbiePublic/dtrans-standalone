<?php

namespace Dtrans\api\login;

use Dtrans\core\permissions\PermissionFlags;
use Dtrans\core\permissions\PermissionInterface;
use Dtrans\core\types\UserData;

class ApiUser
{
    protected string $token;
    protected string $token_expire_at;
    protected ?string $ldap_dn;
    protected ?string $username;

    public function __construct(string $token, string $token_expire_at, string $username, ?string $ldap_dn = null)
    {
        $this->token = $token;
        $this->token_expire_at = $token_expire_at;
        $this->ldap_dn = $ldap_dn;
        $this->username = $username;
    }

    protected ?UserData $data = null;

    public function get_user_data(): ?UserData
    {
        if (is_null($this->data))
            $this->data = LoginModel::get_user_data_from_token($this->token);

        return $this->data;
    }

    public function get_token_expire_at(): string
    {
        return $this->token_expire_at;
    }

    public function get_reference_string(): string
    {
        return 'ldap_dn=' . $this->ldap_dn;
    }

    protected ?bool $is_admin = null;

    public function has_permission_flag(string $permission_flag): bool
    {
        // query and cache is_admin
        if (is_null($this->is_admin))
            $this->is_admin = PermissionInterface::query_permission_flag(PermissionFlags::IS_ADMIN, $this->username, $this->ldap_dn);
        if ($this->is_admin === true)
            return true;

        return PermissionInterface::query_permission_flag($permission_flag, $this->username, $this->ldap_dn);
    }

    public function get_app_role(): ?string
    {
        if (PermissionInterface::query_permission_flag(PermissionFlags::AUTH_USE_APP_RECEIVER, $this->username, $this->ldap_dn))
            return 'receiver';
        if (PermissionInterface::query_permission_flag(PermissionFlags::AUTH_USE_APP_DRIVER, $this->username, $this->ldap_dn))
            return 'driver';
        if (PermissionInterface::query_permission_flag(PermissionFlags::IS_ADMIN, $this->username, $this->ldap_dn))
            return 'administrator';

        return null;
    }
}