<?php

namespace Dtrans\api\login;

use Dtrans\core\constants\ConstsLogin;
use Dtrans\core\enums\HttpStatusCodesEnum;
use JetBrains\PhpStorm\NoReturn;

abstract class AuthController
{
    // responsible for logout
    #[NoReturn] public static function handle_logout_token(): void
    {
        $token = isset($_GET[ConstsLogin::LOGIN_DELETE_TOKEN]) ? trim($_GET[ConstsLogin::LOGIN_DELETE_TOKEN]) : null;
        if (empty($token)) {
            http_response_code(HttpStatusCodesEnum::BAD_REQUEST);
            exit();
        }

        LoginModel::delete_token($token);
        http_response_code(HttpStatusCodesEnum::OK);
        exit();
    }

    // responsible for login via token
    public static function validate_token(): ?ApiUser
    {
        if (empty($_SERVER['HTTP_AUTHORIZATION'])) {
            http_response_code(HttpStatusCodesEnum::UNAUTHORIZED);
            exit();
        }

        $auth = trim($_SERVER['HTTP_AUTHORIZATION']);
        $prefix = 'Bearer ';

        // check if prefix exists
        if (!str_starts_with($auth, $prefix))
            return null;

        // remove prefix
        $token = substr($auth, strlen($prefix));
        return LoginModel::get_user_from_token($token); // success
    }
}