<?php

namespace Dtrans\api\login;

use Dtrans\core\constants\ConstsLogin;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\DtransConfig;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\LDAPHelper;
use Dtrans\core\permissions\PermissionFlags;
use Dtrans\core\permissions\PermissionInterface;
use Dtrans\core\serializer\AbstractSerializer;
use Dtrans\core\types\UserData;
use JetBrains\PhpStorm\NoReturn;

abstract class LoginLDAP
{
    #[NoReturn] public static function handle_login(AbstractSerializer $serializer): void
    {
        if (strcmp($_SERVER['REQUEST_METHOD'], RequestMethodsEnum::POST) !== 0) {
            http_response_code(HttpStatusCodesEnum::NOT_ALLOWED);
            exit();
        }

        $json = file_get_contents('php://input');
        if (empty($json)) {
            http_response_code(HttpStatusCodesEnum::BAD_REQUEST);
            exit();
        }

        $jsonArray = $serializer->deserialize($json);
        if (!is_array($jsonArray) || !array_key_exists(ConstsLogin::LOGIN_JSON_USERNAME, $jsonArray) || !array_key_exists(ConstsLogin::LOGIN_JSON_PASSWORD, $jsonArray)) {
            http_response_code(HttpStatusCodesEnum::BAD_REQUEST);
            exit();
        }

        // check login credentials
        $username = $jsonArray[ConstsLogin::LOGIN_JSON_USERNAME];
        $password = $jsonArray[ConstsLogin::LOGIN_JSON_PASSWORD];
        $user_data = self::check_login($username, $password);
        if ($user_data === false) {
            DtransLogger::info("Login via LDAP failed. Invalid credentials.", ["username" => $username]);
            http_response_code(HttpStatusCodesEnum::UNAUTHORIZED);
            exit();
        }

        $validity_minutes = null;
        if (array_key_exists(ConstsLogin::LOGIN_JSON_VALIDITY_MINUTES, $jsonArray))
            $validity_minutes = $jsonArray[ConstsLogin::LOGIN_JSON_VALIDITY_MINUTES];

        if (!is_null($validity_minutes) && !filter_var($validity_minutes, FILTER_VALIDATE_INT)) {
            http_response_code(HttpStatusCodesEnum::BAD_REQUEST);
            exit();
        }

        if(empty($validity_minutes))
            $validity_minutes = DtransConfig::get_int(DtransConfig::CFG_API, 'auth', 'default_token_validity_minutes', 240);

        // generate token for user
        DtransLogger::info("Login via LDAP successful.", ["username" => $username, "dn" => $user_data->dn, "validity_minutes" => $validity_minutes]);
        $token = LoginModel::create_token($user_data, $validity_minutes);
        echo $serializer->serialize([ConstsLogin::LOGIN_JSON_TOKEN => $token]);
        http_response_code(HttpStatusCodesEnum::OK);
        exit();
    }

    private static function check_login(?string $username, ?string $password) /* :false|string */
    {
        // avoid anonymous bind in case LDAP service allows it
        if (empty($username) || empty($password))
            return false;

        $lc = LDAPHelper::get_host_manager();

        $search_base = DtransConfig::get_string(DtransConfig::CFG_API, 'ldap', 'search_base');
        $search_filter = DtransConfig::get_string(DtransConfig::CFG_API, 'ldap', 'search_filter');
        $search_scope = DtransConfig::get_string(DtransConfig::CFG_API, 'ldap', 'search_scope');

        $escaped_username = ldap_escape($username, '', LDAP_ESCAPE_FILTER);
        $filter = str_replace('%user', $escaped_username, $search_filter);

        // list of attributes fetched from node
        $attributes = self::get_ldap_build_attr_list();

        $r = LDAPHelper::search($lc, $attributes, $search_base, $filter, $search_scope);
        $entries = ldap_get_entries($lc, $r);

        // check if account was found
        if (empty($entries) || $entries['count'] == 0 || empty($entries[0]['dn']))
            return false;

        $dn = $entries[0]['dn'];

        if (!PermissionInterface::query_permission_flag(PermissionFlags::AUTH_USE_API, $username, $dn)) {
            DtransLogger::debug('LDAP found entry but user does not have sufficient permission to use api. Login failed.', ['username' => $username, 'dn' => $dn]);
            return false;
        }

        // what will be returned to controller if bind was successful
        // process queried attributes here
        $user = new UserData();
        $user->dn = $dn;
        $user->phone = self::ldap_get_attr_value($entries, self::$queried_user_phone, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_PHONE));
        $user->mobile = self::ldap_get_attr_value($entries, self::$queried_user_mobile, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_MOBILE));
        $user->fax = self::ldap_get_attr_value($entries, self::$queried_user_fax, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_FAX));
        $user->firstName = self::ldap_get_attr_value($entries, self::$queried_user_firstname, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_FIRSTNAME));
        $user->lastName = self::ldap_get_attr_value($entries, self::$queried_user_lastname, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_LASTNAME));
        $user->companyPosition = self::ldap_get_attr_value($entries, self::$queried_user_company_position, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_COMPANY_POSITION));
        $user->companyName = self::ldap_get_attr_value($entries, self::$queried_user_company_name, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_COMPANY_NAME));
        $user->addressStreet = self::ldap_get_attr_value($entries, self::$queried_user_address_street, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_ADDRESS_STREET));
        $user->addressCity = self::ldap_get_attr_value($entries, self::$queried_user_address_city, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_ADDRESS_CITY));
        $user->addressZip = self::ldap_get_attr_value($entries, self::$queried_user_address_zip, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_ADDRESS_ZIP));
        $user->addressRegion = self::ldap_get_attr_value($entries, self::$queried_user_address_region, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_ADDRESS_REGION));
        $user->addressCountry = self::ldap_get_attr_value($entries, self::$queried_user_address_country, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_ADDRESS_COUNTRY));
        $user->addressFull = self::ldap_get_attr_value($entries, self::$queried_user_address_full, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_ADDRESS_FULL));

        // workaround for multiple mails per user. Try to use the mail which was used for login
        foreach (self::$queried_user_mail as $key) {
            $key = strtolower($key);
            if (!array_key_exists($key, $entries[0]))
                continue;

            $count = $entries[0][$key]['count'];
            for ($i = 0; $i < $count; $i++) {
                $mail = $entries[0][$key][$i];

                if (strcmp(strtolower(trim($mail)), strtolower(trim($username))) === 0) {
                    $user->mail = $mail;
                    break;
                }
            }
        }
        // fallback mail if no match was found
        if (empty($user->mail))
            $user->mail = self::ldap_get_attr_value($entries, self::$queried_user_mail, DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR_DEFAULTS, ConstsLogin::CFG_LDAP_ATTR_MAIL));

        // reset bind to prepare for user bind
        ldap_unbind($lc);
        $lc = LDAPHelper::get_host();

        DtransLogger::debug("LDAP found entry and attempting bind for user.", ['username' => $username, 'dn' => $dn]);
        if (ldap_bind($lc, $dn, $password))
            return $user;
        else
            return false;
    }


    // parse a parameter name string and add it query to list
    private static function ldap_attr_add_list(string $property, array &$list = []): array
    {
        $value = DtransConfig::get_string(DtransConfig::CFG_API, ConstsLogin::CFG_LDAP_ATTR, $property);
        if (empty($value)) return [];

        $local_list = [];
        $attrs = explode(',', $value);
        foreach ($attrs as $attr) {
            $attr = trim($attr);
            if (empty($attr))
                continue;

            if (!in_array($attr, $list))
                $list[] = $attr;

            if (!in_array($attr, $local_list))
                $local_list[] = $attr;
        }

        return $local_list;
    }


    // attribute names
    private static array $queried_user_mail = array();
    private static array $queried_user_phone = array();
    private static array $queried_user_mobile = array();
    private static array $queried_user_fax = array();
    private static array $queried_user_firstname = array();
    private static array $queried_user_lastname = array();
    private static array $queried_user_company_name = array();
    private static array $queried_user_company_position = array();
    private static array $queried_user_address_street = array();
    private static array $queried_user_address_city = array();
    private static array $queried_user_address_zip = array();
    private static array $queried_user_address_region = array();
    private static array $queried_user_address_country = array();
    private static array $queried_user_address_full = array();

    // build a list of all node attributes which need to be fetched
    private static function get_ldap_build_attr_list(): array
    {
        $list = array('dn'); // always query dn

        self::$queried_user_mail = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_MAIL, $list);
        self::$queried_user_phone = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_PHONE, $list);
        self::$queried_user_mobile = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_MOBILE, $list);
        self::$queried_user_fax = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_FAX, $list);
        self::$queried_user_firstname = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_FIRSTNAME, $list);
        self::$queried_user_lastname = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_LASTNAME, $list);
        self::$queried_user_company_name = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_COMPANY_NAME, $list);
        self::$queried_user_company_position = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_COMPANY_POSITION, $list);
        self::$queried_user_address_street = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_ADDRESS_STREET, $list);
        self::$queried_user_address_city = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_ADDRESS_CITY, $list);
        self::$queried_user_address_zip = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_ADDRESS_ZIP, $list);
        self::$queried_user_address_region = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_ADDRESS_REGION, $list);
        self::$queried_user_address_country = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_ADDRESS_COUNTRY, $list);
        self::$queried_user_address_full = self::ldap_attr_add_list(ConstsLogin::CFG_LDAP_ATTR_ADDRESS_FULL, $list);

        return $list;
    }

    private static function ldap_get_attr_value(array $entries, array $keys, ?string $default = null): ?string
    {
        foreach ($keys as $key) {
            // ldap returns the keys in lowercase
            $key = strtolower($key);
            if (!array_key_exists($key, $entries[0]))
                continue;

            $count = $entries[0][$key]['count'];
            if ($count == 0)
                continue;

            return $entries[0][$key][0];
        }

        if (empty($default))
            return null;
        else
            return $default;
    }
}