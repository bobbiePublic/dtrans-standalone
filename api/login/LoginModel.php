<?php

namespace Dtrans\api\login;

use Dtrans\core\constants\ConstsLogin;
use Dtrans\core\database\abstraction\SQLCondition;
use Dtrans\core\database\abstraction\SQLDelete;
use Dtrans\core\database\abstraction\SQLExpression;
use Dtrans\core\database\abstraction\SQLInsert;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableSessionTokens;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\helpers\DtransConfig;
use Dtrans\core\helpers\Random;
use Dtrans\core\helpers\TimestampHelper;
use Dtrans\core\types\UserData;

abstract class LoginModel
{
    public static function get_user_from_token(string $token): ?ApiUser
    {
        $token = trim($token);
        if (empty($token))
            return null;

        $columns = [DBTableSessionTokens::TOKEN, DBTableSessionTokens::LDAP_DN, DBTableSessionTokens::EXPIRES_AT, DBTableSessionTokens::USER_MAIL];

        $where = (new SQLCondition())->equal(DBTableSessionTokens::TOKEN, $token);
        $where->AND()->greater_than(DBTableSessionTokens::EXPIRES_AT, DBTable::TIME_NOW);
        $u = SQLSelect::select_one(DBTableSessionTokens::TABLE_NAME, $columns, $where);

        // failed
        if (empty($u))
            return null;

        // login successful
        return new ApiUser($u[DBTableSessionTokens::TOKEN], $u[DBTableSessionTokens::EXPIRES_AT], $u[DBTableSessionTokens::USER_MAIL], $u[DBTableSessionTokens::LDAP_DN]);
    }

    public static function get_user_data_from_token(string $token): ?UserData
    {
        $token = trim($token);
        if (empty($token)) return null;

        $columns = [
            DBTableSessionTokens::LDAP_DN,
            DBTableSessionTokens::USER_FIRSTNAME,
            DBTableSessionTokens::USER_LASTNAME,
            DBTableSessionTokens::USER_COMPANY_NAME,
            DBTableSessionTokens::USER_COMPANY_POSITION,
            DBTableSessionTokens::USER_PHONE,
            DBTableSessionTokens::USER_MOBILE,
            DBTableSessionTokens::USER_FAX,
            DBTableSessionTokens::USER_MAIL,
            DBTableSessionTokens::USER_ADDRESS_STREET,
            DBTableSessionTokens::USER_ADDRESS_CITY,
            DBTableSessionTokens::USER_ADDRESS_ZIP,
            DBTableSessionTokens::USER_ADDRESS_REGION,
            DBTableSessionTokens::USER_ADDRESS_COUNTRY,
            DBTableSessionTokens::USER_ADDRESS_FULL,
            DBTableSessionTokens::CREATED_AT => TimestampHelper::query_timestamp_as_iso8601(DBTableSessionTokens::CREATED_AT),
            DBTableSessionTokens::EXPIRES_AT => TimestampHelper::query_timestamp_as_iso8601(DBTableSessionTokens::EXPIRES_AT)
        ];

        $where = (new SQLCondition())->equal(DBTableSessionTokens::TOKEN, $token);
        $u = SQLSelect::select_one(DBTableSessionTokens::TABLE_NAME, $columns, $where);

        // failed
        if (empty($u))
            return null;

        // convert here
        $user = new UserData();
        $user->dn = $u[DBTableSessionTokens::LDAP_DN];
        $user->firstName = $u[DBTableSessionTokens::USER_FIRSTNAME];
        $user->lastName = $u[DBTableSessionTokens::USER_LASTNAME];
        $user->companyName = $u[DBTableSessionTokens::USER_COMPANY_NAME];
        $user->companyPosition = $u[DBTableSessionTokens::USER_COMPANY_POSITION];
        $user->phone = $u[DBTableSessionTokens::USER_PHONE];
        $user->mobile = $u[DBTableSessionTokens::USER_MOBILE];
        $user->fax = $u[DBTableSessionTokens::USER_FAX];
        $user->mail = $u[DBTableSessionTokens::USER_MAIL];
        $user->addressStreet = $u[DBTableSessionTokens::USER_ADDRESS_STREET];
        $user->addressCity = $u[DBTableSessionTokens::USER_ADDRESS_CITY];
        $user->addressZip = $u[DBTableSessionTokens::USER_ADDRESS_ZIP];
        $user->addressRegion = $u[DBTableSessionTokens::USER_ADDRESS_REGION];
        $user->addressCountry = $u[DBTableSessionTokens::USER_ADDRESS_COUNTRY];
        $user->addressFull = $u[DBTableSessionTokens::USER_ADDRESS_FULL];
        $user->sessionCreatedAt = $u[DBTableSessionTokens::CREATED_AT];
        $user->sessionExpiresAt = $u[DBTableSessionTokens::EXPIRES_AT];
        return $user;
    }

    public static function delete_token(string $token)
    {
        $where = (new SQLCondition())->equal(DBTableSessionTokens::TOKEN, $token);
        $where->OR()->less_than(DBTableSessionTokens::EXPIRES_AT, DBTable::TIME_NOW);
        SQLDelete::delete(DBTableSessionTokens::TABLE_NAME, $where);
    }

    public static function create_token(UserData $user, int $validity_minutes)
    {
        // calculate expiration timestamp
        $expires_at = DBTable::TIME_NOW;
        $MAX = 1000 * 365 * 24 * 60; // 1000 years
        if ($validity_minutes == 0 || $validity_minutes > $MAX) // unlimited means just very long
            $validity_minutes = $MAX; // token length is very long
        else if ($validity_minutes < 0) // fallback
            $validity_minutes = 240;

        $expires_at .= ' + INTERVAL ' . $validity_minutes . ' MINUTE';

        $token = Random::string_cryptographic(ConstsLogin::LOGIN_TOKEN_LENGTH);
        $values = [
            DBTableSessionTokens::TOKEN => $token,
            DBTableSessionTokens::EXPIRES_AT => new SQLExpression($expires_at),

            // user data
            DBTableSessionTokens::LDAP_DN => $user->dn,
            DBTableSessionTokens::USER_FIRSTNAME => $user->firstName,
            DBTableSessionTokens::USER_LASTNAME => $user->lastName,
            DBTableSessionTokens::USER_COMPANY_NAME => $user->companyName,
            DBTableSessionTokens::USER_COMPANY_POSITION => $user->companyPosition,
            DBTableSessionTokens::USER_PHONE => $user->phone,
            DBTableSessionTokens::USER_MOBILE => $user->mobile,
            DBTableSessionTokens::USER_FAX => $user->fax,
            DBTableSessionTokens::USER_MAIL => $user->mail,
            DBTableSessionTokens::USER_ADDRESS_STREET => $user->addressStreet,
            DBTableSessionTokens::USER_ADDRESS_CITY => $user->addressCity,
            DBTableSessionTokens::USER_ADDRESS_ZIP => $user->addressZip,
            DBTableSessionTokens::USER_ADDRESS_REGION => $user->addressRegion,
            DBTableSessionTokens::USER_ADDRESS_COUNTRY => $user->addressCountry,
            DBTableSessionTokens::USER_ADDRESS_FULL => $user->addressFull,
        ];

        $result = SQLInsert::insert(DBTableSessionTokens::TABLE_NAME, $values);

        // failed to insert token into table
        if (!$result) {
            http_response_code(HttpStatusCodesEnum::INTERNAL_SERVER_ERROR);
            exit();
        }

        return $token;
    }
}