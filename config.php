<?php
// composer autoloader
require __DIR__ . '/vendor/autoload.php';

// important: set timezone to the timezone which should be used in the api
const DB_TIMEZONE = '+00:00';

const API_PREFIX = '/dtrans/v1/';

const PATH_FILE_PROCESSOR_INCOMING = 'incoming/';
const PATH_DOCUMENTS = 'documents/';
const SUB_DIR_ERROR = 'error/';
const SUB_DIR_DUPLICATE = 'duplicate/';
const PATH_LOGS = 'logs/';
// subdirectory where crash reports are saved to (if enabled)
const SUB_DIR_CRASH_REPORTS = 'crash';

// a boolean used to show exceptions and debug info to user
// useful for debugging but dangerous in a production system
// do not touch this if you do not know what you are doing
const IS_PRODUCTION = true;

// UBL XSDs used in validation process
const XSD_PATH = "vendor/bobbie/module-ubl/schemas/ubl/maindoc/";
const XSD_DESPATCH_ADVICE = "UBL-DespatchAdvice-2.4.xsd";
const XSD_RECEIPT_ADVICE = "UBL-ReceiptAdvice-2.4.xsd";


if (!defined('IS_DTRANS_TEST_PHP_UNIT_ENVIRONMENT') || IS_DTRANS_TEST_PHP_UNIT_ENVIRONMENT === false) {
    // register error handler as first action everywhere
    require_once 'core/helpers/ErrorHandler.php';
    // perform startup checks
    require_once 'core/StartupChecks.php';
    // load configuration files
    \Dtrans\core\helpers\DtransConfig::load_configs();

    if (!defined('SKIP_DTRANS_DATABASE_CONNECT'))
        \Dtrans\core\database\abstraction\SQLCore::init();
}