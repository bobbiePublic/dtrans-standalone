<?php

$dir = __DIR__ . '/../';
$error = false;
$chcode = 0775;

if (!is_writable($dir . PATH_FILE_PROCESSOR_INCOMING)) {
    echo "Error: Folder '" . $dir . PATH_FILE_PROCESSOR_INCOMING . "' does not exist or is not writable.\n";
    $error = true;
} else {
    foreach (\Dtrans\core\enums\DtransFormatsEnum::_values as $format) {
        $sub = $dir . PATH_FILE_PROCESSOR_INCOMING . '/' . $format;

        // do not create sub folders for unknown files
        if (strcmp($format, \Dtrans\core\enums\DtransFormatsEnum::UNKNOWN) === 0)
            continue;

        if (!is_dir($sub)) {
            if(mkdir($sub, $chcode, true) === false) {
                echo "Error: Failed to create dir '" . $sub . "'. Maybe the parent folder is not writable.\n";
                $error = true;
            } else
                chmod($sub, $chcode);
        }
    }
}

if (!is_writable($dir . PATH_LOGS)) {
    echo "Error: Folder '" . $dir . PATH_LOGS . "' does not exist or is not writable.\n";
    $error = true;
} else if (!is_null(SUB_DIR_CRASH_REPORTS)) {
    $sub = $dir . PATH_LOGS . '/' . SUB_DIR_CRASH_REPORTS;

    if (!is_dir($sub)) {
        if (mkdir($sub, $chcode, true) === false) {
            echo "Error: Failed to create dir '" . $sub . "'. Maybe the parent folder is not writable.\n";
            $error = true;
        } else
            chmod($sub, $chcode);
    }
}

if (!is_writable($dir . PATH_DOCUMENTS)) {
    echo "Error: Folder '" . $dir . PATH_DOCUMENTS . "' does not exist or is not writable.\n";
    $error = true;
} else {
    foreach (\Dtrans\core\enums\DtransFormatsEnum::_values as $format) {
        $sub = $dir . PATH_DOCUMENTS . '/' . $format;
        if(!is_dir($sub)) {
            if(mkdir($sub, $chcode, true) === false) {
                echo "Error: Failed to create dir '" . $sub . "'. Maybe the parent folder is not writable.\n";
                $error = true;
            } else
                chmod($sub, $chcode);
        }

        // do not create sub folders for unknown files
        if(strcmp($format, \Dtrans\core\enums\DtransFormatsEnum::UNKNOWN) === 0)
            continue;

        $sub = $dir . PATH_DOCUMENTS . '/' . $format . '/' . SUB_DIR_ERROR;
        if(!is_dir($sub)) {
            if(mkdir($sub, $chcode, true) === false) {
                echo "Error: Failed to create dir '" . $sub . "'. Maybe the parent folder is not writable.\n";
                $error = true;
            } else
                chmod($sub, $chcode);
        }

        $sub = $dir . PATH_DOCUMENTS . '/' . $format . '/' . SUB_DIR_DUPLICATE;
        if(!is_dir($sub)) {
            if(mkdir($sub, $chcode, true) === false) {
                echo "Error: Failed to create dir '" . $sub . "'. Maybe the parent folder is not writable.\n";
                $error = true;
            } else
                chmod($sub, $chcode);
        }
    }
}

if(!extension_loaded('dom')) {
    echo "Error: Required PHP extension 'ext-dom' is not loaded. Please adjust PHP configuration.\n";
    $error = true;
}

if(!extension_loaded('json')) {
    echo "Error: Required PHP extension 'ext-json' is not loaded. Please adjust PHP configuration.\n";
    $error = true;
}

if(!extension_loaded('curl')) {
    echo "Error: Required PHP extension 'ext-curl' is not loaded. Please adjust PHP configuration.\n";
    $error = true;
}

if(!extension_loaded('openssl')) {
    echo "Error: Required PHP extension 'ext-openssl' is not loaded. Please adjust PHP configuration.\n";
    $error = true;
}

if(!extension_loaded('mysqli')) {
    echo "Error: Required PHP extension 'ext-mysqli' is not loaded. Please adjust PHP configuration.\n";
    $error = true;
}

if(!extension_loaded('ldap')) {
    echo "Error: Required PHP extension 'ext-ldap' is not loaded. Please adjust PHP configuration.\n";
    $error = true;
}

if (!extension_loaded('pdo')) {
    echo "Error: Required PHP extension 'pdo' is not loaded. Please adjust PHP configuration.\n";
    $error = true;
}

if (!extension_loaded('pdo_mysql')) {
    echo "Error: Required PHP extension 'pdo_mysql  ' is not loaded. Please adjust PHP configuration.\n";
    $error = true;
}

if($error)  {
    echo "Service is not available as above problems need to be resolved manually. Please contact service administrator.\n";
    http_response_code(503);
    exit();
}