<?php

namespace Dtrans\core\api;

use Dtrans\core\database\models\ModelCacheGeocoding;
use Dtrans\core\helpers\DtransConfig;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\types\UBLLocation;

abstract class ApiGeocoding
{
    /* returns null on error / no result */
    /* returns array on success
    0 => results→formatted_address
    1 => results→geometry→location->lat,results→geometry→location->lng
    */
    public static function request(string $query): ?UBLLocation
    {
        $cache_lifetime_hours = DtransConfig::get_int(DtransConfig::CFG_GENERAL, 'geocode', 'api_cache_lifetime_hours', 0);
        if (!empty($cache_lifetime_hours)) {
            $cache_hit = ModelCacheGeocoding::get_from_cache($query);
            if (!is_null($cache_hit)) {
                DtransLogger::debug('Geocode api cache hit!', ['query' => $query, 'result' => var_export($cache_hit, true)]);
                return $cache_hit;
            } else
                DtransLogger::debug('Geocode not cached!', ['query' => $query]);
        }

        if (self::is_google_available())
            return self::request_google($query);

        // use open street map as fallback
        if (!empty(self::get_open_street_map()))
            return self::request_open_street_map($query);

        // no api available
        DtransLogger::notice('Geocoding not available! Neither google nor openstreetmap is set up!', ['query' => $query]);
        return null;
    }

    public static function request_open_street_map(string $query): ?UBLLocation
    {
        $uri = self::get_open_street_map();
        if (0 !== substr_compare($uri, '/', -1))
            $uri .= '/';

        $uri .= 'search?q=' . urlencode($query);
        $uri .= '&limit=1&format=json';

        $client = new \GuzzleHttp\Client();
        $request = $client->request('GET', $uri);

        if ($request->getStatusCode() != 200) {
            DtransLogger::warning('Geocode api returned non okay status code.', ['code' => $request->getStatusCode(), 'query_address' => $query]);
            return null;
        }
        $body = $request->getBody()->getContents();
        DtransLogger::debug('Geocode api call!', ['url' => $uri, 'code' => $request->getStatusCode(), 'body' => $body]);
        $body = json_decode($body, true);

        $cache_lifetime_hours = DtransConfig::get_int(DtransConfig::CFG_GENERAL, 'geocode', 'api_cache_lifetime_hours', 0);
        if (empty($body)) {
            if (!empty($cache_lifetime_hours))
                ModelCacheGeocoding::refresh_cache($query, null, null, null);
            return null; // request no result
        }

        $ubl_location = new UBLLocation();
        $ubl_location->setAddress($body['display_name']);
        $ubl_location->setLocationCoords($body['lat'] . ',' . $body['lon']);

        if (!empty($cache_lifetime_hours))
            ModelCacheGeocoding::refresh_cache($query, $body['display_name'], $body['lat'], $body['lon']);

        return $ubl_location;
    }


    public static function request_google(string $query): ?UBLLocation
    {
        if (!self::is_google_available())
            return null;

        $uri = 'https://maps.google.com/maps/api/geocode/json';
        $uri .= '?address=' . urlencode($query);
        $uri .= '&key=' . urlencode(self::get_key());

        $client = new \GuzzleHttp\Client();
        $request = $client->request('GET', $uri);

        if ($request->getStatusCode() != 200) {
            DtransLogger::warning('Geocode api returned non okay status code.', ['code' => $request->getStatusCode(), 'query_address' => $query]);
            return null;
        }
        $body = $request->getBody()->getContents();
        DtransLogger::debug('Geocode api call!', ['url' => explode('&key', $uri)[0], 'code' => $request->getStatusCode(), 'body' => $body]);
        $body = json_decode($body, true);

        $cache_lifetime_hours = DtransConfig::get_int(DtransConfig::CFG_GENERAL, 'geocode', 'api_cache_lifetime_hours', 0);
        if (empty($body) || empty($body['status']) || $body['status'] == 'ZERO_RESULTS' || empty($body['results'])) {
            if (!empty($cache_lifetime_hours))
                ModelCacheGeocoding::refresh_cache($query, null, null, null);
            return null; // request no result
        }

        $result = $body['results'][0];
        $l = $result['geometry']['location'];

        $ubl_location = new UBLLocation();
        $ubl_location->setAddress($result['formatted_address']);
        $ubl_location->setLocationCoords($l['lat'] . ',' . $l['lng']);

        if (!empty($cache_lifetime_hours))
            ModelCacheGeocoding::refresh_cache($query, $result['formatted_address'], $l['lat'], $l['lng']);

        return $ubl_location;
    }

    public static function is_google_available(): bool
    {
        $key = self::get_key();
        return !empty($key);
    }

    private static function get_key(): ?string
    {
        return DtransConfig::get_string(DtransConfig::CFG_GENERAL, 'geocode', 'google_api_key');
    }

    private static function get_open_street_map(): ?string
    {
        return DtransConfig::get_string(DtransConfig::CFG_GENERAL, 'geocode', 'openstreetmap_api_url');
    }
}