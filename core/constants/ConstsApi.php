<?php

namespace Dtrans\core\constants;

use Dtrans\core\enums\ListviewColumnEnum;

abstract class ConstsApi
{
    const PARAM_GET_PAGE = "page";
    const PARAM_GET_PAGE_SIZE = "page_size";
    const PARAM_GET_SORT_ASC = "sort_asc";
    const PARAM_GET_SORT_BY = "sort_by";
    const PARAM_GET_UUID = "uuid";
    const PARAM_GET_LANG = "lang";
    const PARAM_GET_SHARE_KEY = "key";
    const PARAM_GET_CHAIN = "chain";
    const PARAM_GET_DETAILED = "detailed";
    const PARAM_GET_PRETTIFY = "prettify";
    const PARAM_GET_BODILESS = "bodiless";
    const PARAM_GET_FILTER_SEARCH = "search";
    const PARAM_GET_FILTER_CREATED_AFTER = "filter_created_after";
    const PARAM_GET_FILTER_CHAIN = "filter_chain";
    const PARAM_GET_FILTER_UUID = "filter_uuid";
    const PARAM_GET_FILTER_INITIAL_VERSION = "filter_initial_version";
    const PARAM_GET_FILTER_VERSION_STATUS = "filter_version_status";
    const PARAM_GET_FILTER_CREATED_AT = "filter_created_at";
    const PARAM_GET_FILTER_UPDATED_AT = "filter_updated_at";
    const PARAM_GET_FILTER_DOCUMENT_DATE = "filter_document_date";
    const PARAM_GET_FILTER_DOCUMENT_TYPE = "filter_document_type";
    const PARAM_GET_FILTER_ACTUAL_SHIPPING_DATE = "filter_shipping_actual";
    const PARAM_GET_FILTER_ESTIMATED_SHIPPING_DATE = "filter_shipping_estimated";
    const PARAM_GET_FILTER_REFERENCES = "filter_references";
    const PARAM_GET_FILTER_DESPATCH_SUPPLIER_PARTY = "filter_despatch_supplier_party";
    const PARAM_GET_FILTER_DELIVERY_CUSTOMER_PARTY = "filter_delivery_customer_party";
    const PARAM_GET_FILTER_BUYER_COSTUMER_PARTY = "filter_buyer_customer_party";
    const PARAM_GET_FILTER_SELLER_SUPPLIER_PARTY = "filter_seller_supplier_party";
    const PARAM_GET_FILTER_ORIGINATOR_COSTUMER_PARTY = "filter_originator_customer_party";
    const PARAM_GET_FILTER_PDF_EMBEDDED_DOCUMENT_UUID = "filter_pdf_embedded_document_uuid";
    const PARAM_GET_FILTER_NOTE = "filter_note";
    const PARAM_GET_FILTER_REJECTED = "filter_rejected";
    const PARAM_GET_FILTER_HANDLING_STATUS = "filter_handling_status";
    const PARAM_GET_FILTER_ARCHIVED_STATUS = "filter_archived_status";
    const PARAM_GET_FILTER_CHAINLESS = "filter_chainless";
    const PARAM_LISTVIEW_COLUMNS = "columns";

    const DEFAULT_LISTVIEW_COLUMNS_ALTERNATIVE = array(ListviewColumnEnum::UUID,
        ListviewColumnEnum::CHAIN_ID, ListviewColumnEnum::DOCUMENT_TYPE, ListviewColumnEnum::VERSION_STATUS,
        ListviewColumnEnum::CREATED_AT, ListviewColumnEnum::UPDATED_AT, ListviewColumnEnum::DOCUMENT_PREDECESSOR,
        ListviewColumnEnum::HANDLING_STATUS,
    );

    const DEFAULT_LISTVIEW_PAGE = 0;
    const DEFAULT_LISTVIEW_PAGE_SIZE = 25;
    const DEFAULT_LISTVIEW_SORT_ASC = false;
    const DEFAULT_LISTVIEW_SORT_BY = ConstsJson::JSON_DOCUMENT_CREATED_AT;

    const MULTI_VALUE_SEPARATOR = ';';
}