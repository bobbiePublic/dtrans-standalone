<?php
namespace Dtrans\core\constants;

// documents
abstract class ConstsJson
{
    const JSON_DOCUMENTS_TOTAL = 'totalDocuments';
    const JSON_DOCUMENTS_COUNT = 'count';
    const JSON_DOCUMENTS_LIST = 'documents';
    const JSON_DOCUMENT_CHAIN_ID = 'chain';
    const JSON_DOCUMENT_ID = 'id';
    const JSON_DOCUMENT_UUID = 'uuid';
    const JSON_DOCUMENT_TYPE = 'type';

    const JSON_DOCUMENT_PREVIOUS_VERSION_UUID = 'previousVersionUUID';
    const JSON_DOCUMENT_VERSION_STATUS = 'versionStatus';
    const JSON_DOCUMENT_NOTE = 'note';
    const JSON_DOCUMENT_FILE_SIZE = 'fileSize';
    const JSON_DOCUMENT_CREATED_AT = 'createdAt';
    const JSON_DOCUMENT_UPDATED_AT = 'updatedAt';
    const JSON_DOCUMENT_REFERENCES = 'otherReferences';
    const JSON_DOCUMENT_DATE = 'dateDocument';
    const JSON_DOCUMENT_DATE_SHIPPING_ESTIMATED_START = 'dateShippingEstimatedStart';
    const JSON_DOCUMENT_DATE_SHIPPING_ESTIMATED_END = 'dateShippingEstimatedEnd';
    const JSON_DOCUMENT_DATE_SHIPPING_ACTUAL = 'dateShippingActual';
    const JSON_DOCUMENT_DESPATCH_SUPPLIER_PARTY = 'despatchSupplierParty';
    const JSON_DOCUMENT_DELIVERY_CUSTOMER_PARTY = 'deliveryCustomerParty';
    const JSON_DOCUMENT_BUYER_CUSTOMER_PARTY = 'buyerCustomerParty';
    const JSON_DOCUMENT_SELLER_SUPPLIER_PARTY = 'sellerSupplierParty';
    const JSON_DOCUMENT_ORIGINATOR_COSTUMER_PARTY = 'originatorCustomerParty';
    const JSON_DOCUMENT_LICENSE_PLATE = 'licensePlate';
    const JSON_DOCUMENT_CARRIER_PARTY_MAIN = 'carrierPartyMain';
    const JSON_DOCUMENT_CARRIER_PARTY_SUB = 'carrierPartySub';
    const JSON_DOCUMENT_CARRIER_DRIVER_NAME = 'carrierDriverName';
    const JSON_DOCUMENT_DELIVERY_LOCATION = 'deliveryLocation';
    const JSON_DOCUMENT_ALTERNATIVE_DELIVERY_LOCATION = 'alternativeDeliveryLocation';
    const JSON_DOCUMENT_ITEM_DESCRIPTION = 'itemDescription';
    const JSON_DOCUMENT_ITEM_WEIGHTS = 'itemWeights';
    const JSON_DOCUMENT_HANDLING_STATUS = 'handlingStatus';
    const JSON_DOCUMENT_ARCHIVED_STATUS = 'archivedStatus';
    const JSON_DOCUMENT_PDF_EMBEDDED_DOCUMENT_UUID = 'pdfEmbeddedDocumentUUID';

    const JSON_USER_FIRST_NAME = 'firstName';
    const JSON_USER_LAST_NAME = 'lastName';
    const JSON_USER_COMPANY_NAME = 'companyName';
    const JSON_USER_COMPANY_POSITION = 'companyPosition';
    const JSON_USER_PHONE = 'phone';
    const JSON_USER_FAX = 'fax';
    const JSON_USER_MOBILE = 'mobile';
    const JSON_USER_MAIL = 'mail';
    const JSON_USER_ADDRESS = 'address';
    const JSON_USER_ROLE = 'role';
    const JSON_SESSION_CREATED_AT = 'sessionCreatedAt';
    const JSON_SESSION_EXPIRES_AT = 'sessionExpiresAt';

    const JSON_STATE_NEW_STATE = 'state';
    const JSON_STATE_CHAIN_ID = 'chain';

    const JSON_CHAIN_ID = 'chain';
    const JSON_CHAIN_UPDATED_AT = 'updatedAt';
    const JSON_CHAIN_CREATED_AT = 'createdAt';
    const JSON_CHAIN_HANDLING_STATUS = 'handlingStatus';
    const JSON_CHAIN_ARCHIVED_STATUS = 'archivedStatus';
    const JSON_CHAIN_DOCUMENT_COUNT = 'documentCount';

    const JSON_SHARE_KEY = 'key';
    const JSON_SHARE_EXPIRES_AT = 'expiresAt';
}