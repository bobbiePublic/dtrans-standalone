<?php

namespace Dtrans\core\constants;

abstract class ConstsLogin
{

// the length of the generated token. muse be less than 256 which is the table column limit
    const LOGIN_TOKEN_LENGTH = 64;
    const LOGIN_JSON_USERNAME = 'username';
    const LOGIN_JSON_PASSWORD = 'password';
    const LOGIN_JSON_VALIDITY_MINUTES = 'validity_minutes';
    const LOGIN_JSON_TOKEN = 'token';

    const LOGIN_DELETE_TOKEN = 'token';

    // config about ldap
    const CFG_LDAP_ATTR = 'ldap_attributes';
    const CFG_LDAP_ATTR_DEFAULTS = 'ldap_attributes_defaults';

    const CFG_LDAP_ATTR_MAIL = 'user_mail';
    const CFG_LDAP_ATTR_PHONE = 'user_phone';
    const CFG_LDAP_ATTR_MOBILE = 'user_mobile';
    const CFG_LDAP_ATTR_FAX = 'user_fax';
    const CFG_LDAP_ATTR_FIRSTNAME = 'user_firstname';
    const CFG_LDAP_ATTR_LASTNAME = 'user_lastname';
    const CFG_LDAP_ATTR_COMPANY_NAME = 'user_company_name';
    const CFG_LDAP_ATTR_COMPANY_POSITION = 'user_company_position';
    const CFG_LDAP_ATTR_ADDRESS_STREET = 'user_address_street';
    const CFG_LDAP_ATTR_ADDRESS_CITY = 'user_address_city';
    const CFG_LDAP_ATTR_ADDRESS_ZIP = 'user_address_zip';
    const CFG_LDAP_ATTR_ADDRESS_REGION = 'user_address_region';
    const CFG_LDAP_ATTR_ADDRESS_COUNTRY = 'user_address_country';
    const CFG_LDAP_ATTR_ADDRESS_FULL = 'user_address_full';
}