<?php

namespace Dtrans\core\constants;

use Dtrans\core\helpers\DtransLogger;

abstract class ConstsStrings {

    // 100-599 reserved for HTTP codes
    const CODE_HTTP_BAD_REQUEST = 400;
    const CODE_HTTP_FORBIDDEN= 403;
    const CODE_HTTP_NOT_FOUND = 404;
    const CODE_HTTP_METHOD_NOT_ALLOWED = 405;
    // 600-999 reserved
    // 999-1000 used for custom messages
    const CODE_CUSTOM_MESSAGE = 1000;
    // 1001-4000 api codes
    const CODE_BODY_JSON_MISSING = 1001;
    const CODE_BODY_EMPTY = 1002;
    const CODE_PARAMETER_MISSING = 1009;
    const CODE_JSON_ATTRIBUTE_MISSING = 1010;
    const CODE_JSON_ATTRIBUTE_INVALID = 1011;
    const CODE_JSON_NO_CHANGES = 1015;
    const CODE_UNKNOWN_ERROR = 1048;
    const CODE_UNKNOWN_ERROR_DOCUMENT_PROCESSING = 1049;

    const CODE_XML_UBL_INVALID = 1050;
    const CODE_XML_UBL_IS_1LIEFERSCHEIN= 1051;
    const CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE = 1071;
    const CODE_XML_1LIEFERSCHEIN_ATTRIBUTE_NOT_SET_TO_DEFAULT = 1072;
    const CODE_XML_1LIEFERSCHEIN_ATTRIBUTE_NOT_SUPPORTED = 1073;
    const CODE_XML_1LIEFERSCHEIN_SIGNATURE_MULTIPLE = 1075;
    const CODE_XML_1LIEFERSCHEIN_SIGNATURE_METHOD_UNSUPPORTED = 1076;
    const CODE_API_PARAMETER_FAULTY_AND_IGNORED = 1100;
    const CODE_API_TIMESTAMP_FAULTY_AND_IGNORED = 1101;
    const CODE_API_MAIL_FAULTY_AND_IGNORED = 1102;

    const _codes = [
        self::CODE_HTTP_BAD_REQUEST => "Bad Request.",
        self::CODE_HTTP_FORBIDDEN => "Forbidden. User does not have permission to access this functionality. If you believe this is an error, contact the server administrator.",
        self::CODE_HTTP_NOT_FOUND => "Target not found.",
        self::CODE_HTTP_METHOD_NOT_ALLOWED => "Method not allowed.",
        self::CODE_UNKNOWN_ERROR => "An unknown error occurred. Please try again.",
        self::CODE_UNKNOWN_ERROR_DOCUMENT_PROCESSING => "An unknown error occurred. Does submitted document has content?",
        self::CODE_BODY_JSON_MISSING => "No valid json in body provided.",
        self::CODE_BODY_EMPTY => "No file in body provided.",
        self::CODE_PARAMETER_MISSING => "Required parameter '{0}' missing in request.",
        self::CODE_JSON_ATTRIBUTE_MISSING => "Required json attribute '{0}' missing in request.",
        self::CODE_JSON_ATTRIBUTE_INVALID => "Invalid value for json attribute '{0}' in request.",
        self::CODE_XML_UBL_INVALID => "XSD-validation error: {0}",
        self::CODE_XML_UBL_IS_1LIEFERSCHEIN => "Given document has CustomizationID set to a 1Lieferschein identifier. Please submit this document as a 1Lieferschein document as it is not a plain UBL document.",
        self::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE => "1Lieferschein required attribute '{0}' is missing.",
        self::CODE_XML_1LIEFERSCHEIN_SIGNATURE_METHOD_UNSUPPORTED => "1Lieferschein required attribute 'SignatureMethod' under 'Signature' is missing or not a 1Lieferschein supported signature method.",
        self::CODE_XML_1LIEFERSCHEIN_ATTRIBUTE_NOT_SET_TO_DEFAULT => "1Lieferschein required attribute '{0}' is not set to required value '{1}'.",
        self::CODE_XML_1LIEFERSCHEIN_ATTRIBUTE_NOT_SUPPORTED => "1Lieferschein defined {0} '{1}' is not support.",
        self::CODE_JSON_NO_CHANGES => "No changes provided. Are you using the correct json property names?",
        self::CODE_API_PARAMETER_FAULTY_AND_IGNORED => "The given parameter '{0}' could not be processed and was ignored. For more information please consult the documentation.",
        self::CODE_API_TIMESTAMP_FAULTY_AND_IGNORED => "The given parameter '{0}' could not be processed and was ignored. For more information please consult the documentation about timestamps.",
        self::CODE_API_MAIL_FAULTY_AND_IGNORED => "The given parameter '{0}' could not be processed and was ignored. The given value in not a valid mail address.",
    ];

    // helper functions to replace placeholders
    public static function format_code(int $code, array $args) : ?string{
        if(!array_key_exists($code, self::_codes))
            return null;

        $length = count($args);
        $message = self::_codes[$code];
        for($x = 0; $x < $length; $x++)
            $message = str_replace('{' . $x . '}', $args[$x], $message);

        if (str_contains($message, '{' . $length . '}'))
            DtransLogger::warning(__FILE__ . ": Too less arguments for formatting string provided. Code '" . $code . "'");

        return $message;
    }

    public static function format_string(string $string, array $args) : string{
        $length = count($args);
        for($x = 0; $x < $length; $x++)
            $string = str_replace('{' . $x . '}', $args[$x], $string);

        if (str_contains($string, '{' . $length . '}'))
            DtransLogger::warning(__FILE__ . ": Too less arguments for formatting custom string provided. String: '" . $string . "'");

        return $string;
    }

    // helper function to generate list for documentation
    public static function _dump_string_table() {
        $codes = self::_codes;
        ksort($codes);

        foreach ($codes as $code => $message) {
            echo $code . ': ' . $message . "\n";
        }
    }
}

// php ./core/constants/ConstsStrings.php
// ConstsStrings::_dump_string_table();