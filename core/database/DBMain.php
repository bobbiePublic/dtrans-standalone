<?php

namespace Dtrans\core\database;

use Dtrans\core\database\tables\DBTableCacheErrorLog;
use Dtrans\core\database\tables\DBTableCacheGeocoding;
use Dtrans\core\database\tables\DBTableDocuments;
use Dtrans\core\database\tables\DBTableDocumentsChains;
use Dtrans\core\database\tables\DBTableDocumentsReferences;
use Dtrans\core\database\tables\DBTableDocumentsShared;
use Dtrans\core\database\tables\DBTableIncoming;
use Dtrans\core\database\tables\DBTableIncomingRejected;
use Dtrans\core\database\tables\DBTableSendingDone;
use Dtrans\core\database\tables\DBTableSendingQueue;
use Dtrans\core\database\tables\DBTableSessionTokens;

abstract class DBMain
{
    const DB_VERSION_REQUIRED = 4;

    public static DBTableDocuments $documents;
    public static DBTableDocumentsChains $documents_chain;
    public static DBTableDocumentsShared $documents_shared;
    public static DBTableDocumentsReferences $documents_reference;
    public static DBTableSendingQueue $sending_queue;
    public static DBTableSendingDone $sending_done;
    public static DBTableSessionTokens $session_tokens;
    public static DBTableIncoming $incoming;
    public static DBTableIncomingRejected $incoming_rejected;
    public static DBTableCacheGeocoding $cache_geocoding;
    public static DBTableCacheErrorLog $cache_error_log;

    private static bool $initialized = false;

    public static function init()
    {
        if (self::$initialized) return;
        self::$documents = new DBTableDocuments();
        self::$documents_chain = new DBTableDocumentsChains();
        self::$documents_shared = new DBTableDocumentsShared();
        self::$documents_reference = new DBTableDocumentsReferences();
        self::$sending_queue = new DBTableSendingQueue();
        self::$sending_done = new DBTableSendingDone();
        self::$session_tokens = new DBTableSessionTokens();
        self::$incoming = new DBTableIncoming();
        self::$incoming_rejected = new DBTableIncomingRejected();
        self::$cache_geocoding = new DBTableCacheGeocoding();
        self::$cache_error_log = new DBTableCacheErrorLog();
        self::$initialized = true;
    }
}

// initialize data
DBMain::init();