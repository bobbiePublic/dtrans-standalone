<?php

namespace Dtrans\core\database\abstraction;

class SQLCondition
{

    // structure for child array:
    // length=3: (COMBINATOR, ESCAPED_SQL, UNSAFE_VARS)
    protected array $raw = array();
    protected string $active_combinator = SQLOperators::COMBINE_AND;

    // base function
    protected function add_operation($value_left, $value_right, string $operator, string $table_left, ?string $table_right): SQLCondition
    {
        // booleans behave weird so lets convert them two tinyint before they cause any problems
        if (is_bool($value_left))
            $value_left = $value_left === true ? '1' : '0';
        if (is_bool($value_right))
            $value_right = $value_right === true ? '1' : '0';

        if (is_null($table_right))
            $table_right = $table_left;

        $variables = array();
        if (SQLSanitizer::escape_required($value_left))
            $variables[] = $value_left;
        if (SQLSanitizer::escape_required($value_right))
            $variables[] = $value_right;

        $this->raw[] = array($this->active_combinator, SQLSanitizer::escape($value_left, $table_left) . $operator . SQLSanitizer::escape($value_right, $table_right), $variables);
        return $this;
    }

    protected function add_like_operation($value_left, $value_right, $mask_left, $mask_right, string $table_left, ?string $table_right): SQLCondition
    {
        if (is_null($table_right))
            $table_right = $table_left;

        $variables = array();
        if (SQLSanitizer::escape_required($value_left)) {
            $value_left = SQLSanitizer::escape_like($value_left);

            if (empty($mask_left))
                $variables[] = $value_left;
            else
                $variables[] = str_replace('@', $value_left, $mask_left);
            $mask_left = '?';
        } else {
            $value_left = SQLSanitizer::escape($value_left, $table_left);
            if (empty($mask_left))
                $mask_left = $value_left;
            else
                $mask_left = str_replace('@', $value_left, $mask_left);
        }

        if (SQLSanitizer::escape_required($value_right)) {
            $value_right = SQLSanitizer::escape_like($value_right);

            if (empty($mask_right))
                $variables[] = $value_right;
            else
                $variables[] = str_replace('@', $value_right, $mask_right);
            $mask_right = '?';
        } else {
            $value_right = SQLSanitizer::escape($value_right, $table_right);
            if (empty($mask_right))
                $mask_right = $value_right;
            else
                $mask_right = str_replace('@', $value_right, $mask_right);
        }


        $ec = SQLOperators::ESCAPE_CHARACTER;
        $this->raw[] = array($this->active_combinator, $mask_left . ' LIKE ' . $mask_right . " ESCAPE '$ec'", $variables);
        return $this;
    }

    // custom like function to compare two strings in database
    public function match($value_left, $value_right, string $table_left = '', ?string $table_right = null): SQLCondition
    {
        if (is_null($table_right))
            $table_right = $table_left;

        $variables = array();
        if (SQLSanitizer::escape_required($value_left)) {
            $value_left = SQLSanitizer::escape_like($value_left);
            $variables[] = str_replace('@', $value_left, '%@%');
            $mask_left = 'UPPER(?)';
        } else {
            $value_left = SQLSanitizer::escape($value_left, $table_left);
            $mask_left = "UPPER($value_left)";
        }

        if (SQLSanitizer::escape_required($value_right)) {
            $value_right = SQLSanitizer::escape_like($value_right);
            $variables[] = str_replace('@', $value_right, '%@%');
            $mask_right = 'UPPER(?)';
        } else {
            $value_right = SQLSanitizer::escape($value_right, $table_right);
            $mask_right = "UPPER($value_right)";
        }


        $ec = SQLOperators::ESCAPE_CHARACTER;
        $this->raw[] = array($this->active_combinator, $mask_left . ' LIKE ' . $mask_right . " ESCAPE '$ec'", $variables);
        return $this;
    }

    public function nest(SQLCondition $condition, ?string $combinator_overwrite = null)
    {
        $content = $condition->to_sql($sql, $variables);
        $comb = !is_null($combinator_overwrite) ? $combinator_overwrite : $this->active_combinator;
        if ($content)
            $this->raw[] = array($comb, '(' . $sql . ')', $variables);
    }

    // conditions
    public function between(string $column, $value_lower, $value_upper, string $table = ''): SQLCondition
    {
        $variables = array();
        if (SQLSanitizer::escape_required($value_lower))
            $variables[] = $value_lower;
        if (SQLSanitizer::escape_required($value_upper))
            $variables[] = $value_upper;

        if (!empty($table))
            $table .= '.';

        $this->raw[] = array($this->active_combinator, $table . SQLSanitizer::escape($column) . ' BETWEEN '
            . SQLSanitizer::escape($value_lower) . ' AND ' . SQLSanitizer::escape($value_upper), $variables);
        return $this;
    }

    public function in(string $column, array $values, string $table = ''): SQLCondition
    {
        $variables = array();
        $set = '(';
        foreach ($values as $value) {
            if (SQLSanitizer::escape_required($value))
                $variables[] = $value;
            $set .= SQLSanitizer::escape($value) . ',';
        }

        if (strlen($set) == 1) // empty set
            return $this;

        $set = substr_replace($set, ")", -1);

        if (!empty($table))
            $table .= '.';

        $this->raw[] = array($this->active_combinator, $table . SQLSanitizer::escape($column) . ' IN ' . $set, $variables);
        return $this;
    }

    public function is_null(string $column, string $table = ''): SQLCondition
    {
        if (!empty($table))
            $table .= '.';
        $this->raw[] = array($this->active_combinator, $table . SQLSanitizer::escape($column) . ' IS NULL', []);
        return $this;
    }

    public function is_not_null(string $column, string $table = ''): SQLCondition
    {
        if (!empty($table))
            $table .= '.';
        $this->raw[] = array($this->active_combinator, $table . SQLSanitizer::escape($column) . ' IS NOT NULL', []);
        return $this;
    }

    public function like($value_left, $value_right, string $mask_left = '', string $mask_right = '', string $table_left = '', ?string $table_right = null): SQLCondition
    {
        return $this->add_like_operation($value_left, $value_right, $mask_left, $mask_right, $table_left, $table_right);
    }

    public function equal($value_left, $value_right, string $table_left = '', ?string $table_right = null): SQLCondition
    {
        return $this->add_operation($value_left, $value_right, ' = ', $table_left, $table_right);
    }

    public function unequal($value_left, $value_right, string $table_left = '', ?string $table_right = null): SQLCondition
    {
        return $this->add_operation($value_left, $value_right, ' <> ', $table_left, $table_right);
    }

    public function less_than($value_left, $value_right, string $table_left = '', ?string $table_right = null): SQLCondition
    {
        return $this->add_operation($value_left, $value_right, ' < ', $table_left, $table_right);
    }

    public function greater_than($value_left, $value_right, string $table_left = '', ?string $table_right = null): SQLCondition
    {
        return $this->add_operation($value_left, $value_right, ' > ', $table_left, $table_right);
    }

    public function less_than_or_equal($value_left, $value_right, string $table_left = '', ?string $table_right = null): SQLCondition
    {
        return $this->add_operation($value_left, $value_right, ' <= ', $table_left, $table_right);
    }

    public function greater_than_or_equal($value_left, $value_right, string $table_left = '', ?string $table_right = null): SQLCondition
    {
        return $this->add_operation($value_left, $value_right, ' >= ', $table_left, $table_right);
    }

    // combinators
    public function AND(): SQLCondition
    {
        $this->active_combinator = SQLOperators::COMBINE_AND;
        return $this;
    }

    public function OR(): SQLCondition
    {
        $this->active_combinator = SQLOperators::COMBINE_OR;
        return $this;
    }

    // export function for running
    public function to_sql(?string &$safe_sql, ?array &$values): bool
    {
        if (empty($this->raw))
            return false;

        $safe_sql = '';
        $values = array();

        // length=3: (string COMBINATOR, string ESCAPED_SQL, array UNSAFE_VARS)
        foreach ($this->raw as $r) {
            // add to SQL
            if (empty($safe_sql))
                $safe_sql = $r[1];
            else
                $safe_sql .= $r[0] . ' ' . $r[1];

            // add values
            if (!empty($r[2]))
                array_push($values, ...$r[2]);
        }

        return strlen($safe_sql) > 0;
    }
}