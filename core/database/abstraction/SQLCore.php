<?php

namespace Dtrans\core\database\abstraction;

use Dtrans\core\database\DBMain;
use Dtrans\core\database\migration\DBStartupCheck;
use Dtrans\core\helpers\DtransConfig;
use Dtrans\core\helpers\DtransLogger;
use PDO;
use PDOException;

abstract class SQLCore
{
    // log toggle for debugging.
    const DEBUGGING_SQL_ENABLED = false;

    // initializer fields
    private static bool $initialized = false;
    private static ?PDO $adapter = null;

    // setup function to establish connection to DB
    // called only once
    public static function init()
    {
        if (self::$initialized)
            return;

        self::$adapter = self::create_pdo();
        self::$initialized = true;
        DBMain::init();
    }

    // get function to access SQL capabilities
    public static function get_adapter(): ?PDO
    {
        if (is_null(self::$adapter))
            self::init();

        return self::$adapter;
    }


    private static ?string $connected_db = null;

    public static function getDatabaseName(): ?string
    {
        return self::$connected_db;
    }

    private static function create_pdo(): ?PDO
    {
        // prepare dsn
        $database = DtransConfig::get_string(DtransConfig::CFG_GENERAL, "database", "database");
        $host = DtransConfig::get_string(DtransConfig::CFG_GENERAL, "database", "host");
        $port = DtransConfig::get_int(DtransConfig::CFG_GENERAL, "database", "port");
        if (empty($host))
            $host = '127.0.0.1';
        $dsn = 'mysql:';
        $dsn .= "host=" . $host . ';';
        if (!empty($port) && is_numeric($port))
            $dsn .= "port=" . $port . ';';
        $dsn .= "dbname=$database;";
        $dsn .= "charset=UTF8";

        self::$connected_db = $database;

        // try connecting to sql database
        try {
            $username = DtransConfig::get_string(DtransConfig::CFG_GENERAL, "database", "username", '');
            $password = DtransConfig::get_string(DtransConfig::CFG_GENERAL, "database", "password", '');

            $pdo = new PDO($dsn, $username, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // set timezone for session
            if (!empty(DB_TIMEZONE))
                $pdo->query("SET time_zone = '" . DB_TIMEZONE . "';");

            // startup check
            if (!defined('DTRANS_SQL_STANDALONE_MODE'))
                DBStartupCheck::perform_startup_check($pdo, $database);
            return $pdo;
        } catch (PDOException $e) {
            if (defined('DTRANS_SQL_STANDALONE_MODE'))
                echo $e->getMessage();
            DtransLogger::critical('Database connection failed.', ['error' => $e->getMessage(), 'host' => $host, 'db' => $database]);
            if (!defined('DTRANS_SQL_STANDALONE_MODE'))
                DBStartupCheck::show_maintenance_message();
            return null;
        }
    }
}