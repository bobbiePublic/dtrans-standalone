<?php

namespace Dtrans\core\database\abstraction;

use Dtrans\core\helpers\DtransLogger;

abstract class SQLCount
{
    public static function count(string $table, ?SQLCondition $condition = null, ?string $count = '*'): ?int
    {
        $count = SQLSanitizer::quote_keywords($count);
        return self::call_function($table, $condition, "COUNT($count)");
    }

    public static function count_join(string $table_left, string $table_right, string $on_column_left, string $on_column_right,
                                      string $join_type = SQLOperators::JOIN_INNER, ?SQLCondition $condition = null, ?string $count = '*'): ?int
    {
        $count = SQLSanitizer::quote_keywords($count);
        return self::call_function_join($table_left, $table_right, $on_column_left, $on_column_right, $join_type, $condition, "COUNT($count)");
    }

    private static function call_function_join(string        $table_left, string $table_right, string $on_column_left, string $on_column_right, string $join_type,
                                               ?SQLCondition $condition, ?string $function): ?int
    {
        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug("SQL $function join operation started.", ['table_left' => $table_left, 'table_right' => $table_right]);

        // serialize column names
        $on_column_left = SQLSanitizer::quote_keywords($on_column_left);
        $on_column_right = SQLSanitizer::quote_keywords($on_column_right);

        $str = "SELECT $function FROM $table_left $join_type $table_right";
        $str .= " ON $table_left.$on_column_left = $table_right.$on_column_right";

        $unsafe_values = array();
        if (!is_null($condition) && $condition->to_sql($where_sql, $unsafe_values))
            $str .= " WHERE $where_sql";

        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug("SQL $function join operation created.", ['sql' => $str, 'values' => var_export($unsafe_values, true)]);

        $sql = SQLCore::get_adapter();
        $statement = $sql->prepare($str);
        $suc = $statement->execute($unsafe_values);
        if (!$suc)
            return null;

        $result = (int)$statement->fetchColumn();
        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug("SQL $function operation completed.", ['table_left' => $table_left, 'table_right' => $table_right, 'result' => $result]);

        return $result;
    }

    private static function call_function(string $table, ?SQLCondition $condition, ?string $function): ?int
    {
        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug("SQL $function operation started.", ['table' => $table]);

        // generate sql string
        $unsafe_values = null;
        $str = "SELECT $function FROM $table";
        if (!is_null($condition) && $condition->to_sql($where_sql, $unsafe_values))
            $str .= " WHERE $where_sql";

        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug("SQL $function operation created.", ['sql' => $str, 'values' => var_export($unsafe_values, true)]);

        $sql = SQLCore::get_adapter();
        $statement = $sql->prepare($str);
        $suc = $statement->execute($unsafe_values);
        if (!$suc)
            return null;

        $result = (int)$statement->fetchColumn();
        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug("SQL $function operation completed.", ['table' => $table, 'result' => $result]);

        return $result;
    }
}