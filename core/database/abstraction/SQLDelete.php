<?php

namespace Dtrans\core\database\abstraction;

use Dtrans\core\helpers\DtransLogger;

abstract class SQLDelete
{
    public static function delete(string $table, ?SQLCondition $condition): int
    {
        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL delete operation started.', ['table' => $table]);

        // generate sql string
        $unsafe_values = null;
        $str = "DELETE FROM $table";

        if (!is_null($condition) && $condition->to_sql($where_sql, $unsafe_values))
            $str .= " WHERE $where_sql";

        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL delete operation created.', ['sql' => $str, 'values' => var_export($unsafe_values, true)]);

        $sql = SQLCore::get_adapter();
        $statement = $sql->prepare($str);
        $suc = $statement->execute($unsafe_values);
        if (!$suc)
            return -1;

        $rows = $statement->rowCount();

        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL delete operation completed.', ['table' => $table, 'rows' => $rows]);

        return $rows;
    }
}