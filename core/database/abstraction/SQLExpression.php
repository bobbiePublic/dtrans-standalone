<?php

namespace Dtrans\core\database\abstraction;

class SQLExpression
{
    protected string $expression = '';

    public function __construct(string $expression)
    {
        $this->expression = $expression;
    }

    /**
     * @return string
     */
    public function get_expression(): string
    {
        return $this->expression;
    }

    /**
     * @param string $expression
     */
    public function set_expression(string $expression): void
    {
        $this->expression = $expression;
    }
}