<?php

namespace Dtrans\core\database\abstraction;

use Dtrans\core\helpers\DtransLogger;

abstract class SQLInsert
{
    public static function insert(string $table, array $values): bool
    {
        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL insert operation started.', ['table' => $table, 'values' => $values]);

        // generate sql string
        $col = array();
        $val = array();
        $unsafe_values = array();
        foreach ($values as $column => $value) {
            $col[] = SQLSanitizer::quote_keywords($column);
            if ($value instanceof SQLExpression) {
                $val[] = $value->get_expression();
                continue;
            }

            $val[] = '?';
            $unsafe_values[] = $value;
        }
        $col = '(' . implode(',', $col) . ')';
        $val = '(' . implode(',', $val) . ')';
        $str = "INSERT INTO $table $col VALUES $val";
        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL insert operation created.', ['sql' => $str, 'values' => var_export($unsafe_values, true)]);

        $sql = SQLCore::get_adapter();
        $statement = $sql->prepare($str);
        $suc = $statement->execute($unsafe_values);

        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL insert operation completed.', ['table' => $table, 'rows' => $statement->rowCount()]);

        return $suc;
    }

    public static function last_insert_id(): ?int
    {
        $sql = SQLCore::get_adapter();
        $last_id = (int)$sql->query("SELECT LAST_INSERT_ID()")->fetchColumn();
        if (empty($last_id))
            return null;

        return $last_id;
    }
}