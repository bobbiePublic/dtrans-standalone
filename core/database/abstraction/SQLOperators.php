<?php

namespace Dtrans\core\database\abstraction;

abstract class SQLOperators
{
    const ESCAPE_CHARACTER = '~';
    const COMBINE_AND = ' AND ';
    const COMBINE_OR = ' OR ';


    const JOIN_INNER = ' INNER JOIN ';
    const JOIN_LEFT = ' LEFT JOIN ';
    const JOIN_RIGHT = ' RIGHT JOIN ';
    const JOIN_FULL = ' FULL JOIN ';
}