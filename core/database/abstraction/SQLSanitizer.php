<?php

namespace Dtrans\core\database\abstraction;

abstract class SQLSanitizer
{
    private static array $allowed_symbols = array();

    public static function escape_required(/* string|int|SQLExpression */ $var): bool
    {
        if ($var instanceof SQLExpression)
            return false;

        return !in_array($var, self::$allowed_symbols);
    }

    public static function escape(/* string|int|SQLExpression */ $var, string $table = ''): string
    {
        if ($var instanceof SQLExpression)
            return $var->get_expression();

        if (!empty($table))
            $table .= '.';

        if (in_array($var, self::$allowed_symbols))
            return $table . self::quote_keywords($var);

        return '?';
    }

    public static function register_symbol(string $symbol)
    {
        if (!is_numeric($symbol) && !in_array($symbol, self::$allowed_symbols))
            self::$allowed_symbols[] = $symbol;
    }

    public static function register_symbols(array $symbols)
    {
        foreach ($symbols as $symbol)
            self::register_symbol($symbol);
    }

    public static function quote_keywords($column)
    {
        if ($column instanceof SQLExpression)
            return $column;

        $sql_reserved_keywords = array('KEY');
        if (in_array(strtoupper($column), $sql_reserved_keywords))
            return "`$column`";

        return $column;
    }

    public static function escape_like(string $value): string
    {
        $ec = SQLOperators::ESCAPE_CHARACTER;
        $value = str_replace('%', $ec . '%', $value);
        $value = str_replace($ec, $ec . $ec, $value);
        return str_replace('_', $ec . '_', $value);
    }
}