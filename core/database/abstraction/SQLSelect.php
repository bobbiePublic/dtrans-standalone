<?php

namespace Dtrans\core\database\abstraction;

use Dtrans\core\helpers\DtransLogger;
use PDO;

abstract class SQLSelect
{
    public static function select_one(string $table, array $columns, ?SQLCondition $condition, ?string $order_by = null, bool $order_asc = true, ?int $offset = null): ?array
    {
        $res = self::select($table, $columns, $condition, $order_by, $order_asc, 1, $offset);
        return empty($res) ? null : $res[0];
    }

    public static function select(string $table, array $columns, ?SQLCondition $condition, ?string $order_by = null, bool $order_asc = true, ?int $limit = null, ?int $offset = null): ?array
    {
        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL select operation started.', ['table' => $table]);

        // generate sql string
        $unsafe_values = null;
        if (!empty($columns)) {
            $col = array();
            foreach ($columns as $alias => $column) {
                if ($column instanceof SQLExpression)
                    $c = $column->get_expression();
                else
                    $c = SQLSanitizer::quote_keywords($column);
                if (!is_int($alias)) {
                    if ($alias instanceof SQLExpression)
                        $c .= ' AS ' . $alias->get_expression();
                    else
                        $c .= ' AS ' . $alias;
                }
                $col[] = $c;
            }
            $col = implode(',', $col);
        } else
            $col = '*';

        $str = "SELECT $col FROM $table";

        if (!is_null($condition) && $condition->to_sql($where_sql, $unsafe_values))
            $str .= " WHERE $where_sql";

        if (!is_null($order_by))
            $str .= " ORDER BY $order_by " . ($order_asc ? 'ASC' : 'DESC');
        if (!is_null($limit))
            $str .= " LIMIT $limit";
        if (!is_null($offset))
            $str .= " OFFSET $offset";

        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL select operation created.', ['sql' => $str, 'values' => var_export($unsafe_values, true)]);

        $sql = SQLCore::get_adapter();
        $statement = $sql->prepare($str);
        $suc = $statement->execute($unsafe_values);
        if (!$suc)
            return null;

        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL select operation completed.', ['table' => $table, 'rows' => $statement->rowCount()]);

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function select_join_one(string        $table_left, array $columns_left, string $table_right, array $columns_right,
                                           string        $on_column_left, string $on_column_right, string $join_type = SQLOperators::JOIN_INNER,
                                           ?SQLCondition $condition = null, ?string $order_by = null, bool $order_asc = true, ?int $offset = null): ?array

    {
        $res = self::select_join($table_left, $columns_left, $table_right, $columns_right, $on_column_left, $on_column_right, $join_type,
            $condition, $order_by, $order_asc, 1, $offset);
        return empty($res) ? null : $res[0];
    }

    public static function select_join(string        $table_left, array $columns_left, string $table_right, array $columns_right,
                                       string        $on_column_left, string $on_column_right, string $join_type = SQLOperators::JOIN_INNER,
                                       ?SQLCondition $condition = null, ?string $order_by = null, bool $order_asc = true, ?int $limit = null, ?int $offset = null): ?array
    {
        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL select join operation started.', ['table_left' => $table_left, 'table_right' => $table_right]);

        // generate sql string
        $unsafe_values = null;
        if (!empty($columns_left)) {
            $cols_left = array();
            foreach ($columns_left as $alias => $column) {
                if ($column instanceof SQLExpression)
                    $c = $column->get_expression();
                else
                    $c = "$table_left." . SQLSanitizer::quote_keywords($column);
                if (!is_int($alias)) {
                    if ($alias instanceof SQLExpression)
                        $c .= ' AS ' . $alias->get_expression();
                    else
                        $c .= ' AS ' . $alias;
                }
                $cols_left[] = $c;
            }
            $cols_left = implode(',', $cols_left);
        } else
            $cols_left = "$table_left.*";

        if (!empty($columns_right)) {
            $cols_right = array();
            foreach ($columns_right as $alias => $column) {
                if ($column instanceof SQLExpression)
                    $c = $column->get_expression();
                else
                    $c = "$table_right." . SQLSanitizer::quote_keywords($column);
                if (!is_int($alias)) {
                    if ($alias instanceof SQLExpression)
                        $c .= ' AS ' . $alias->get_expression();
                    else
                        $c .= ' AS ' . $alias;
                }
                $cols_right[] = $c;
            }
            $cols_right = implode(',', $cols_right);
        } else
            $cols_right = "$table_right.*";

        // serialize column names
        $on_column_left = SQLSanitizer::quote_keywords($on_column_left);
        $on_column_right = SQLSanitizer::quote_keywords($on_column_right);

        $str = "SELECT $cols_left, $cols_right FROM $table_left $join_type $table_right";
        $str .= " ON $table_left.$on_column_left = $table_right.$on_column_right";

        if (!is_null($condition) && $condition->to_sql($where_sql, $unsafe_values))
            $str .= " WHERE $where_sql";

        if (!is_null($order_by))
            $str .= " ORDER BY $order_by " . ($order_asc ? 'ASC' : 'DESC');
        if (!is_null($limit))
            $str .= " LIMIT $limit";
        if (!is_null($offset))
            $str .= " OFFSET $offset";

        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL select join operation created.', ['sql' => $str, 'values' => var_export($unsafe_values, true)]);

        $sql = SQLCore::get_adapter();
        $statement = $sql->prepare($str);
        $suc = $statement->execute($unsafe_values);
        if (!$suc)
            return null;

        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL select join operation completed.', ['table_left' => $table_left, 'table_right' => $table_right]);

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}