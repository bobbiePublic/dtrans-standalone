<?php

namespace Dtrans\core\database\abstraction;

abstract class SQLTransaction
{
    private static bool $in_transaction = false;

    public static function start()
    {
        self::$in_transaction = SQLCore::get_adapter()->beginTransaction();
    }

    public static function restart()
    {
        if (self::$in_transaction)
            self::commit();

        self::$in_transaction = SQLCore::get_adapter()->beginTransaction();
    }


    public static function commit()
    {
        self::$in_transaction = !SQLCore::get_adapter()->commit();
    }

    public static function rollback()
    {
        self::$in_transaction = !SQLCore::get_adapter()->rollBack();
    }

    public static function in_transaction(): bool
    {
        return self::$in_transaction;
    }
}