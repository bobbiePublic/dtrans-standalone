<?php

namespace Dtrans\core\database\abstraction;

use Dtrans\core\helpers\DtransLogger;

abstract class SQLUpdate
{
    public static function update(string $table, array $values, ?SQLCondition $condition): int
    {
        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL update operation started.', ['table' => $table]);

        // generate sql string
        $val = array();
        $unsafe_values = array();
        foreach ($values as $column => $value) {
            if ($value instanceof SQLExpression) {
                $val[] = SQLSanitizer::quote_keywords($column) . ' = ' . $value->get_expression();
                continue;
            }

            $val[] = SQLSanitizer::quote_keywords($column) . '=?';
            $unsafe_values[] = $value;
        }
        $val = implode(',', $val);
        $str = "UPDATE $table SET $val";

        if (!is_null($condition) && $condition->to_sql($where_sql, $where_values)) {
            $str .= " WHERE $where_sql";
            array_push($unsafe_values, ...$where_values);
        }

        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL update operation created.', ['sql' => $str, 'values' => var_export($unsafe_values, true)]);

        $sql = SQLCore::get_adapter();
        $statement = $sql->prepare($str);
        $suc = $statement->execute($unsafe_values);
        if (!$suc)
            return -1;

        $rows = $statement->rowCount();

        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug('SQL update operation completed.', ['table' => $table, 'rows' => $rows]);

        return $rows;
    }
}