<?php

namespace Dtrans\core\database\abstraction\creation;

abstract class SQLConsts
{
    const COLUMN_VARCHAR = 'VARCHAR';
    const COLUMN_TEXT = 'TEXT';
    const COLUMN_BOOLEAN = 'TINYINT';
    const COLUMN_INT = 'INT';
    const COLUMN_DOUBLE = 'DOUBLE';
    const COLUMN_DATETIME = 'DATETIME';
    const COLUMN_DATE = 'DATE';

    const SET_NULL = 'SET NULL';
    const NO_ACTION = 'NO ACTION';
    const CASCADE = 'CASCADE';
    const RESTRICT = 'RESTRICT';

    const DEFAULT_VARCHAR_ENUM_SIZE = 64;
    const DEFAULT_VARCHAR_SIZE = 768;
    const DEFAULT_INTEGER_SIZE = 11;
    const DEFAULT_DOUBLE_SIZE = null;
    const DEFAULT_DATETIME_PRECISION = 6;
    const TIME_NOW = 'CURRENT_TIMESTAMP(' . self::DEFAULT_DATETIME_PRECISION . ')';
}