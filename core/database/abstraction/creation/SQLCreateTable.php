<?php

namespace Dtrans\core\database\abstraction\creation;

use Dtrans\core\database\abstraction\SQLCore;
use Dtrans\core\helpers\DtransLogger;

abstract class SQLCreateTable
{

    public static function create_table(string $table_name, string $sql)
    {
        DtransLogger::debug('Trying to create table', ['table' => $table_name]);

        if (SQLCore::DEBUGGING_SQL_ENABLED)
            DtransLogger::debug($sql);

        $adapter = SQLCore::get_adapter();
        $statement = $adapter->query($sql);
        $statement->execute();

        DtransLogger::debug('Table creation completed', ['table' => $table_name]);
    }
}