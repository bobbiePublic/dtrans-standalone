<?php

namespace Dtrans\core\database\abstraction\creation;

use Dtrans\core\database\abstraction\SQLSanitizer;
use Dtrans\core\database\tables\DBTable;

class SQLTableCreation
{

    protected string $table_name;
    protected array $columns = array();

    public function __construct(string $name)
    {
        $this->table_name = $name;
    }

    protected function add_column(string $type, ?string $length, string $name, bool $nullable = false, $default = null): SQLTableCreationColumn
    {
        $column = new SQLTableCreationColumn($type, $length, $name, $nullable, $default);
        $this->columns[] = $column;
        return $column;
    }

    public function add_varchar(string $name, bool $nullable = false, $default = null, ?int $length = SQLConsts::DEFAULT_VARCHAR_SIZE): SQLTableCreationColumn
    {
        if (!is_null($default))
            $default = "'$default'";

        return self::add_column(SQLConsts::COLUMN_VARCHAR, $length, $name, $nullable, $default);
    }

    public function add_text(string $name, bool $nullable = false, $default = null, ?int $length = SQLConsts::DEFAULT_VARCHAR_SIZE): SQLTableCreationColumn
    {
        if (!is_null($default))
            $default = "'$default'";

        return self::add_column(SQLConsts::COLUMN_TEXT, $length, $name, $nullable, $default);
    }

    public function add_varchar_enum(string $name, bool $nullable = false, $default = null, ?int $length = SQLConsts::DEFAULT_VARCHAR_ENUM_SIZE): SQLTableCreationColumn
    {
        if (!is_null($default))
            $default = "'$default'";

        return self::add_column(SQLConsts::COLUMN_VARCHAR, $length, $name, $nullable, $default);
    }

    public function add_integer(string $name, bool $nullable = false, $default = null, ?int $length = SQLConsts::DEFAULT_INTEGER_SIZE): SQLTableCreationColumn
    {
        return self::add_column(SQLConsts::COLUMN_INT, $length, $name, $nullable, $default);
    }

    public function add_double(string $name, bool $nullable = false, $default = null, ?string $length = SQLConsts::DEFAULT_DOUBLE_SIZE): SQLTableCreationColumn
    {
        return self::add_column(SQLConsts::COLUMN_DOUBLE, $length, $name, $nullable, $default);
    }

    public function add_boolean(string $name, bool $nullable = false, ?bool $default = null): SQLTableCreationColumn
    {
        if (!is_null($default))
            $default = $default === true ? '1' : '0';

        return self::add_column(SQLConsts::COLUMN_BOOLEAN, '1', $name, $nullable, $default);
    }

    public function add_updated_at(string $name = DBTable::UPDATED_AT): SQLTableCreationColumn
    {
        $column = self::add_column(SQLConsts::COLUMN_DATETIME, SQLConsts::DEFAULT_DATETIME_PRECISION, $name, false, SQLConsts::TIME_NOW);
        $column->set_on_update(SQLConsts::TIME_NOW);
        $column->set_comment('Timestamp last time this row was altered.');
        return $column;
    }

    public function add_created_at(string $name = DBTable::CREATED_AT): SQLTableCreationColumn
    {
        $column = self::add_column(SQLConsts::COLUMN_DATETIME, SQLConsts::DEFAULT_DATETIME_PRECISION, $name, false, SQLConsts::TIME_NOW);
        $column->set_comment('Timestamp when row was inserted.');
        return $column;
    }

    public function add_datetime(string $name, bool $nullable = false, $default = null): SQLTableCreationColumn
    {
        return self::add_column(SQLConsts::COLUMN_DATETIME, SQLConsts::DEFAULT_DATETIME_PRECISION, $name, $nullable, $default);
    }

    public function add_date(string $name, bool $nullable = false, $default = null): SQLTableCreationColumn
    {
        if (!is_null($default))
            $default = "'$default'";

        return self::add_column(SQLConsts::COLUMN_DATE, null, $name, $nullable, $default);
    }

    public function create()
    {
        if (empty($this->columns)) return;
        $sql = "CREATE TABLE IF NOT EXISTS $this->table_name";
        $col = array();
        foreach ($this->columns as $column)
            $col[] = $column->get_sql_definition();

        $col = implode(', ', $col);
        $sql .= " ($col)";

        SQLCreateTable::create_table($this->table_name, $sql);
    }
}

class SQLTableCreationColumn
{
    protected string $name;
    protected string $type;
    protected bool $nullable;
    protected $default;

    protected ?string $comment = null;
    protected ?string $on_update = null;
    protected ?string $fk_references = null;

    protected bool $extra_auto_increment = false;
    protected bool $extra_unique = false;
    protected bool $extra_primary_key = false;

    public function __construct(string $type, ?string $length, string $name, bool $nullable, $default)
    {
        $this->type = $type;
        if (!is_null($length))
            $this->type .= "($length)";

        $this->name = SQLSanitizer::quote_keywords($name);
        $this->nullable = $nullable;
        $this->default = $default;
    }

    public function set_type($type)
    {
        $this->type = $type;
    }

    public function set_comment(string $comment): SQLTableCreationColumn
    {
        $this->comment = $comment;
        return $this;
    }

    public function set_on_update(?string $on_update): SQLTableCreationColumn
    {
        $this->on_update = $on_update;
        return $this;
    }

    public function references(string $table, string $column, ?string $on_delete = null, ?string $on_update = null): SQLTableCreationColumn
    {
        $this->fk_references = "$table($column)";
        if (!is_null($on_delete))
            $this->fk_references .= ' ON DELETE ' . $on_delete;
        if (!is_null($on_update))
            $this->fk_references .= ' ON UPDATE ' . $on_update;

        return $this;
    }

    public function auto_increment(): SQLTableCreationColumn
    {
        $this->extra_auto_increment = true;
        return $this;
    }

    public function primary_key(): SQLTableCreationColumn
    {
        $this->extra_primary_key = true;
        return $this;
    }

    public function unique_key(): SQLTableCreationColumn
    {
        $this->extra_unique = true;
        return $this;
    }

    public function get_sql_definition(): string
    {
        $sql = "$this->name $this->type";
        if ($this->nullable && !$this->extra_primary_key)
            $sql .= ' NULL';
        else
            $sql .= ' NOT NULL';
        if (!is_null($this->default))
            $sql .= " DEFAULT " . $this->default;
        if (!is_null($this->on_update))
            $sql .= " ON UPDATE $this->on_update";

        // extras
        if ($this->extra_auto_increment)
            $sql .= ' AUTO_INCREMENT';
        if ($this->extra_unique && !$this->extra_primary_key)
            $sql .= ' UNIQUE KEY';
        if ($this->extra_primary_key)
            $sql .= ' PRIMARY KEY';

        // documentation
        if (!empty($this->comment))
            $sql .= " COMMENT '$this->comment'";
        // foreign key
        if (!is_null($this->fk_references))
            $sql .= " REFERENCES $this->fk_references";

        return $sql;
    }
}