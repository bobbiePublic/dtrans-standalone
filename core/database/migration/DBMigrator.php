<?php

namespace Dtrans\core\database\migration;

use Dtrans\core\database\abstraction\SQLCore;
use Dtrans\core\database\DBMain;
use Dtrans\core\database\tables\DBInfo;
use Dtrans\core\helpers\DtransLogger;

abstract class DBMigrator
{
    public static function migrate(int $target_version)
    {
        $current_version = DBStartupCheck::get_db_version(SQLCore::get_adapter());

        if ($current_version == $target_version) {
            DtransLogger::notice('DBMigrate: Current version matches required version. No upgrade was performed.', ['current' => $current_version, 'target' => $target_version]);
            return;
        }

        // create initial db version
        if ($current_version < 1) {
            DtransLogger::notice('DBMigrate: Creating initial version. 0=>1');
            self::create_initial();
        }

        // apply patches to patch level
        $suc = DBPatcher::patch($target_version);

        if ($suc) {
            DtransLogger::notice('DBMigrate: Migrating process was successful!', ['new_version' => $target_version]);
        } else {
            DtransLogger::critical('DBMigrate: Migrating process failed. Please check log files for more information.');
        }
    }

    private static function create_initial()
    {
        // document tables
        DBMain::$documents_reference->create_initial();
        DBMain::$documents_chain->create_initial();
        DBMain::$documents->create_initial();
        DBMain::$documents_shared->create_initial();

        DBMain::$sending_queue->create_initial();
        DBMain::$sending_done->create_initial();

        DBMain::$incoming->create_initial();
        DBMain::$incoming_rejected->create_initial();

        DBMain::$session_tokens->create_initial();
        DBMain::$cache_geocoding->create_initial();
        DBMain::$cache_error_log->create_initial();

        $info = new DBInfo();
        $info->create_initial();
        $info->set_version(1);
    }
}