<?php

namespace Dtrans\core\database\migration;

use Dtrans\core\database\patches\UBLAdditionalAttributesPatch;
use Dtrans\core\database\patches\UBLAdditionalCarrierInfoPatch;
use Dtrans\core\database\patches\UBLDocumentsReferencesPatch;
use Dtrans\core\database\tables\DBInfo;
use Dtrans\core\helpers\DtransLogger;
use Throwable;

abstract class DBPatcher
{

    protected static array $PATCHES = [];

    private static function init_patches(): void
    {
        if (!empty(self::$PATCHES)) return;

        // 0-1 are placeholders
        self::$PATCHES[2] = new UBLAdditionalAttributesPatch();
        self::$PATCHES[3] = new UBLDocumentsReferencesPatch();
        self::$PATCHES[4] = new UBLAdditionalCarrierInfoPatch();
        // REGISTER NEW PATCHES HERE
    }

    public static function patch(int $target): bool
    {
        self::init_patches();

        $info = new DBInfo();
        try {
            $current = $info->get_version();
        } catch (Throwable $e) {
            DtransLogger::critical('DBPatcher: Failed to fetch current database version. Patching process failed!');
            return false;
        }

        if ($current < 1) {
            DtransLogger::critical('DBPatcher: Fetched an invalid database version. Database is likely corrupted. Patching process failed!');
            return false;
        }

        if ($current == $target) {
            DtransLogger::notice('DBPatcher: Version already matches requested patch version. No patches were applied', ['version' => $current]);
            return true;
        }

        while ($current < $target) {
            $p = $current + 1;
            if (!array_key_exists($p, self::$PATCHES)) {
                DtransLogger::critical('No patch was found to patch to version. Patching process failed!', ['missing_patch' => $p, 'current_version' => $current]);
                return false;
            }

            // try to patch in transaction so on error we can roll back
            DtransLogger::notice('Applying patch...', ['patch_version' => $p]);
            try {
                $patch = self::$PATCHES[$p];
                $patch->run();
                // update version in database table
                $info->set_version($p);
                DtransLogger::notice('Patch successfully applied!', ['patch_version' => $p]);
            } catch (Throwable $e) {
                DtransLogger::critical('Patch ran into an unrecoverable error. Patching process failed!', ['patch_version' => $p, 'current_version' => $current, 'exception' => $e->__toString()]);
                return false;
            }

            $current++;
        }

        DtransLogger::notice('All patches were successfully applied!', ['patch_version' => $target]);
        // migration was successful
        return true;
    }
}