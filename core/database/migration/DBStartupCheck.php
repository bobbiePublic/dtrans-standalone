<?php

namespace Dtrans\core\database\migration;

use Dtrans\core\database\DBMain;
use Dtrans\core\database\tables\DBInfo;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\helpers\DtransLogger;
use JetBrains\PhpStorm\NoReturn;
use PDO;
use Throwable;

abstract class DBStartupCheck
{
    public static function perform_startup_check(PDO $pdo, string $database, bool $embedded = true): bool
    {
        try {
            $query = "SELECT COUNT(*) as c FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$database';";
            $query = $pdo->query($query);
            $query->execute();
            $tables = (int)$query->fetchColumn();
            if ($tables == 0) {
                if (!$embedded) return false;
                DtransLogger::critical('Running on empty database. Please run setup script!');
                self::show_maintenance_message();
            }
        } catch (Throwable $e) {
            if (!$embedded) return false;
            DtransLogger::critical('Failed to count database tables. Can not access database!', ['exception' => $e->__toString()]);
            self::show_maintenance_message();
        }

        $version = self::get_db_version($pdo);
        if ($version == 0) {
            if (!$embedded) return false;
            DtransLogger::critical('DB is not setup. Please ensure to run upgrade script to initialize database!', ['current_version' => $version, 'required_version' => DBMain::DB_VERSION_REQUIRED]);
            self::show_maintenance_message();
        }

        if ($version != DBMain::DB_VERSION_REQUIRED) {
            if (!$embedded) return false;
            DtransLogger::critical('DB version mismatches with required version. Please ensure to run upgrade script after an update!', ['current_version' => $version, 'required_version' => DBMain::DB_VERSION_REQUIRED]);
            self::show_maintenance_message();
        }

        return true;
    }

    public static function get_db_version(PDO $pdo): int
    {
        try {
            $table = DBInfo::TABLE_NAME;
            $column = DBInfo::DB_VERSION;
            $query = "SELECT $column FROM $table LIMIT 1;";
            $query = $pdo->query($query);
            $query->execute();
            return (int)$query->fetchColumn();
        } catch (Throwable $e) {
            return 0;
        }
    }

    #[NoReturn] public static function show_maintenance_message(): void
    {
        echo '<b>Service is currently under maintenance</b> Please try again later!' . "<br>";
        echo 'If this error persist please contact service administrator.' . "<br>";
        http_response_code(HttpStatusCodesEnum::SERVICE_UNAVAILABLE);
        exit(1);
    }

}