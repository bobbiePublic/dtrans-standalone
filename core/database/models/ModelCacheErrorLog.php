<?php

namespace Dtrans\core\database\models;

use Dtrans\core\database\abstraction\creation\SQLConsts;
use Dtrans\core\database\abstraction\SQLCondition;
use Dtrans\core\database\abstraction\SQLDelete;
use Dtrans\core\database\abstraction\SQLExpression;
use Dtrans\core\database\abstraction\SQLInsert;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\abstraction\SQLTransaction;
use Dtrans\core\database\abstraction\SQLUpdate;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableCacheErrorLog;
use Dtrans\core\helpers\DtransLogger;

abstract class ModelCacheErrorLog
{
    public static function report_cron_error(string $message): bool
    {
        SQLTransaction::start();
        SQLInsert::insert(DBTableCacheErrorLog::TABLE_NAME, [DBTableCacheErrorLog::MESSAGE => $message]);
        if (self::should_flush()) {
            $where = (new SQLCondition())->greater_than(DBTableCacheErrorLog::ID, '-1');
            $messages = SQLSelect::select(DBTableCacheErrorLog::TABLE_NAME,
                [DBTableCacheErrorLog::MESSAGE, DBTable::CREATED_AT], $where, DBTable::CREATED_AT);
            SQLDelete::delete(DBTableCacheErrorLog::TABLE_NAME, $where);
            $where = (new SQLCondition())->equal(DBTableCacheErrorLog::ID, '-1');
            SQLUpdate::update(DBTableCacheErrorLog::TABLE_NAME, [DBTable::CREATED_AT => new SQLExpression(SQLConsts::TIME_NOW)], $where);
        }

        SQLTransaction::commit();

        if (isset($messages)) {
            $reported = array();
            $report = '';

            foreach ($messages as $entry) {
                $message = $entry[DBTableCacheErrorLog::MESSAGE];
                $timestamp = $entry[DBTable::CREATED_AT];

                if (in_array($message, $reported))
                    continue;

                $reported[] = $message;
                $report .= "'$message' occurred at $timestamp\n";
            }

            echo $report;
            DtransLogger::error('Flushed error cached.', ['report' => str_replace("\n", ", ", $report)]);
            return true;
        }

        return false;
    }

    private static function should_flush(): bool
    {
        $where = (new SQLCondition())->equal(DBTableCacheErrorLog::ID, '-1');
        $now = SQLConsts::TIME_NOW;
        $minutes_between_reports = 60;
        $where->AND()->less_than(DBTable::CREATED_AT, new SQLExpression("DATE_SUB($now, INTERVAL $minutes_between_reports MINUTE)"));
        $result = SQLSelect::select_one(DBTableCacheErrorLog::TABLE_NAME, [DBTableCacheErrorLog::MESSAGE], $where);
        return !empty($result);
    }
}