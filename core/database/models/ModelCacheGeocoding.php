<?php

namespace Dtrans\core\database\models;

use Dtrans\core\database\abstraction\SQLCondition;
use Dtrans\core\database\abstraction\SQLCount;
use Dtrans\core\database\abstraction\SQLDelete;
use Dtrans\core\database\abstraction\SQLExpression;
use Dtrans\core\database\abstraction\SQLInsert;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\abstraction\SQLTransaction;
use Dtrans\core\database\abstraction\SQLUpdate;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableCacheGeocoding;
use Dtrans\core\helpers\DtransConfig;
use Dtrans\core\types\UBLLocation;

abstract class ModelCacheGeocoding
{
    public static function get_from_cache(?string $query): ?UBLLocation
    {
        $cache_lifetime_hours = DtransConfig::get_int(DtransConfig::CFG_GENERAL, 'geocode', 'api_cache_lifetime_hours', 0);
        if (empty($query) || empty($cache_lifetime_hours))
            return null;

        $columns = [DBTable::UPDATED_AT, DBTableCacheGeocoding::RESULT_ADDRESS, DBTableCacheGeocoding::RESULT_GPS_LAT, DBTableCacheGeocoding::RESULT_GPS_LNG];
        $condition = (new SQLCondition())->equal(DBTableCacheGeocoding::QUERY, $query);
        $condition->AND()->greater_than_or_equal(DBTable::UPDATED_AT, new SQLExpression('SUBTIME(' . DBTable::TIME_NOW . ', \'' . $cache_lifetime_hours . ':0:0\')'));
        $result = SQLSelect::select_one(DBTableCacheGeocoding::TABLE_NAME, $columns, $condition, DBTable::UPDATED_AT, false);
        if (empty($result)) // no result
            return null;

        // pack and return cache hit
        $res = new UBLLocation();
        $lat = $result[DBTableCacheGeocoding::RESULT_GPS_LAT];
        $lng = $result[DBTableCacheGeocoding::RESULT_GPS_LNG];
        $address = $result[DBTableCacheGeocoding::RESULT_ADDRESS];
        if (!is_null($lat) && !is_null($lng)) {
            $gps = $lat . ',' . $lng;
            $res->setLocationCoords($gps);
        }
        if (!empty($address))
            $res->setAddress($result[DBTableCacheGeocoding::RESULT_ADDRESS]);
        return $res;
    }

    public static function refresh_cache(string $query, ?string $address, ?string $gps_latitude, ?string $gps_longitude): void
    {
        $cache_lifetime_hours = DtransConfig::get_int(DtransConfig::CFG_GENERAL, 'geocode', 'api_cache_lifetime_hours', 0);
        if (empty($query) || empty($cache_lifetime_hours))
            return;

        SQLTransaction::start();

        // remove previous results for query
        $where = (new SQLCondition())->equal(DBTableCacheGeocoding::QUERY, $query);
        $exists = 0 < SQLCount::count(DBTableCacheGeocoding::TABLE_NAME, $where);

        $values = [
            DBTableCacheGeocoding::QUERY => $query,
            DBTableCacheGeocoding::RESULT_ADDRESS => $address,
            DBTableCacheGeocoding::RESULT_GPS_LAT => $gps_latitude,
            DBTableCacheGeocoding::RESULT_GPS_LNG => $gps_longitude,
        ];

        if (!$exists)
            SQLInsert::insert(DBTableCacheGeocoding::TABLE_NAME, $values);
        else
            SQLUpdate::update(DBTableCacheGeocoding::TABLE_NAME, $values, $where);

        SQLTransaction::commit();
    }

    public static function flush_cache(?string $query = NULL, bool $full = false): int
    {
        $condition = new SQLCondition();
        $cache_lifetime_hours = DtransConfig::get_int(DtransConfig::CFG_GENERAL, 'geocode', 'api_cache_lifetime_hours', 0);
        if (!$full) {
            if (!is_null($query))
                $condition->equal(DBTableCacheGeocoding::QUERY, $query);
            $condition->OR()->less_than(DBTable::UPDATED_AT,
                new SQLExpression('SUBTIME(' . DBTable::TIME_NOW . ', \'' . $cache_lifetime_hours . ':0:0\')'));
        }
        return SQLDelete::delete(DBTableCacheGeocoding::TABLE_NAME, $condition);
    }
}