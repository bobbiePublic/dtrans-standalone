<?php

namespace Dtrans\core\database\models;

use Dtrans\core\constants\ConstsJson;
use Dtrans\core\database\abstraction\SQLCondition;
use Dtrans\core\database\abstraction\SQLCount;
use Dtrans\core\database\abstraction\SQLDelete;
use Dtrans\core\database\abstraction\SQLOperators;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\abstraction\SQLUpdate;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableDocuments;
use Dtrans\core\database\tables\DBTableDocumentsChains;
use Dtrans\core\enums\DocumentTypeEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\FileCreator;
use Dtrans\core\helpers\TimestampHelper;

abstract class ModelDocuments {
    public static function get_all_documents_by_chain(string $chain_id): ?array
    {
        $columns = [
            ConstsJson::JSON_DOCUMENT_UUID => DBTableDocuments::UUID,
            ConstsJson::JSON_DOCUMENT_TYPE => DBTableDocuments::TYPE,
            ConstsJson::JSON_DOCUMENT_VERSION_STATUS => DBTableDocuments::VERSION_STATUS,
            ConstsJson::JSON_DOCUMENT_UPDATED_AT => TimestampHelper::query_timestamp_as_iso8601(DBTableDocuments::TABLE_NAME . '.' . DBTable::UPDATED_AT),
            ConstsJson::JSON_DOCUMENT_CREATED_AT => TimestampHelper::query_timestamp_as_iso8601(DBTableDocuments::TABLE_NAME . '.' . DBTable::CREATED_AT),
            ConstsJson::JSON_DOCUMENT_PREVIOUS_VERSION_UUID => DBTableDocuments::UBL_PREVIOUS_VERSION_UUID,
            ConstsJson::JSON_DOCUMENT_PDF_EMBEDDED_DOCUMENT_UUID => DBTableDocuments::PDF_EMBEDDED_UUID,
        ];
        $where = (new SQLCondition())->equal(DBTableDocuments::CHAIN_ID, $chain_id);
        return SQLSelect::select(DBTableDocuments::TABLE_NAME, $columns, $where, DBTable::CREATED_AT);
    }

    public static function get_document_info(string $uuid): ?array
    {
        $columns_left = [
            DBTableDocuments::CHAIN_ID,
            DBTableDocuments::FILE_NAME,
            DBTableDocuments::TYPE,
            DBTableDocuments::IN_DTRANS_QUEUE,
            DBTableDocuments::VERSION_STATUS,
            DBTableDocuments::ID
        ];

        $columns_right = [
            DBTableDocumentsChains::HANDLING_STATUS,
            DBTableDocumentsChains::ARCHIVED_STATUS
        ];

        $where = (new SQLCondition())->equal(DBTableDocuments::UUID, $uuid, DBTableDocuments::TABLE_NAME);
        return SQLSelect::select_join_one(DBTableDocuments::TABLE_NAME, $columns_left, DBTableDocumentsChains::TABLE_NAME, $columns_right,
            DBTableDocuments::CHAIN_ID, DBTableDocumentsChains::CHAINS_ID, SQLOperators::JOIN_LEFT, $where);
    }

    public static function delete_document(string $uuid): bool
    {
        DtransLogger::notice("Deleting document.", ['$uuid' => $uuid]);

        $data = self::get_document_info($uuid);
        if (empty($data)) return false;

        $type = DocumentTypeEnum::get_mapping($data[DBTableDocuments::TYPE]);
        FileCreator::delete_file($type, $data[DBTableDocuments::FILE_NAME]);

        $chain_id = $data[DBTableDocuments::CHAIN_ID];
        $where = (new SQLCondition())->equal(DBTableDocuments::UUID, $uuid, DBTableDocuments::TABLE_NAME);
        $okay = SQLDelete::delete(DBTableDocuments::TABLE_NAME, $where) > 0;

        if ($okay)
            ModelDocumentsChains::increment_document_chain_document_count($chain_id, -1);
        return $okay;
    }

    // helper function used to fetch some data which is included in the fallback mail
    public static function get_document_info_mail(string $id): ?array
    {
        $columns = [
            DBTableDocuments::CHAIN_ID,
            DBTableDocuments::UUID,
            DBTableDocuments::TYPE,
            DBTableDocuments::FILE_NAME,
            DBTableDocuments::UBL_DATE_DOCUMENT,
            DBTable::CREATED_AT => TimestampHelper::query_timestamp_as_iso8601(DBTableDocuments::TABLE_NAME . '.' . DBTable::CREATED_AT)
        ];
        $where = (new SQLCondition)->equal(DBTableDocuments::ID, $id);
        return SQLSelect::select_one(DBTableDocuments::TABLE_NAME, $columns, $where);
    }

    public static function get_document_id_by_filename(string $file_name): ?int
    {
        $result = SQLSelect::select_one(DBTableDocuments::TABLE_NAME, [DBTableDocuments::ID],
            (new SQLCondition())->equal(DBTableDocuments::FILE_NAME, $file_name));

        if (empty($result)) return null;
        else return (int)$result[DBTableDocuments::ID];
    }

    public static function get_document_id_by_uuid(string $uuid): ?int
    {
        $result = SQLSelect::select_one(DBTableDocuments::TABLE_NAME, [DBTableDocuments::ID],
            (new SQLCondition())->equal(DBTableDocuments::UUID, $uuid));

        if (empty($result)) return null;
        else return (int)$result[DBTableDocuments::ID];
    }

    public static function set_in_dtrans_queue_by_internal_id(int $document_id, bool $new_state)
    {
        $set = [DBTableDocuments::IN_DTRANS_QUEUE => $new_state == 'true' ? 1 : 0];
        $where = (new SQLCondition())->equal(DBTableDocuments::ID, $document_id);
        SQLUpdate::update(DBTableDocuments::TABLE_NAME, $set, $where);
    }

    private static function update_by_uuid(string $uuid, array $set): bool
    {
        $where = (new SQLCondition())->equal(DBTableDocuments::UUID, $uuid);
        return SQLUpdate::update(DBTableDocuments::TABLE_NAME, $set, $where) > 0;
    }

    public static function set_chain_by_uuid(string $uuid, ?string $new_chain): bool
    {
        return self::update_by_uuid($uuid, [DBTableDocuments::CHAIN_ID => $new_chain]);
    }

    public static function set_note_by_uuid(string $uuid, ?string $note): bool
    {
        return self::update_by_uuid($uuid, [DBTableDocuments::NOTE => $note]);
    }

    public static function is_duplicate(string $uuid): bool
    {
        $where = (new SQLCondition())->equal(DBTableDocuments::UUID, $uuid);
        return 0 < SQLCount::count(DBTableDocuments::TABLE_NAME, $where);
    }
}