<?php

namespace Dtrans\core\database\models;

use Dtrans\core\database\abstraction\SQLCondition;
use Dtrans\core\database\abstraction\SQLDelete;
use Dtrans\core\database\abstraction\SQLExpression;
use Dtrans\core\database\abstraction\SQLInsert;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\abstraction\SQLTransaction;
use Dtrans\core\database\abstraction\SQLUpdate;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableDocuments;
use Dtrans\core\database\tables\DBTableDocumentsChains;
use Dtrans\core\enums\HandlingStatusEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\TimestampHelper;

abstract class ModelDocumentsChains
{

    public static function create_document_chain(string $file_dtrans_id, string $handling_state, string $archived_state, int $document_count, ?int $reference_id)
    {
        $values = [
            DBTableDocumentsChains::CHAINS_ID => $file_dtrans_id,
            DBTableDocumentsChains::HANDLING_STATUS => $handling_state,
            DBTableDocumentsChains::ARCHIVED_STATUS => $archived_state,
            DBTableDocumentsChains::DOCUMENT_COUNT => $document_count,
            DBTableDocumentsChains::REFERENCE_ID => $reference_id
        ];

        SQLInsert::insert(DBTableDocumentsChains::TABLE_NAME, $values);
    }

    public static function get_document_chain_state(string $dtrans_id): ?array
    {
        $columns = [DBTableDocumentsChains::ARCHIVED_STATUS, DBTableDocumentsChains::HANDLING_STATUS];
        $condition = (new SQLCondition())->equal(DBTableDocumentsChains::CHAINS_ID, $dtrans_id);
        return SQLSelect::select_one(DBTableDocumentsChains::TABLE_NAME, $columns, $condition);
    }

    public static function set_document_chain_handling_state(string $chain_id, string $new_handling_state, bool $skip_test = false): bool
    {
        $states = self::get_document_chain_state($chain_id);
        if (!$skip_test && !empty($states)) {
            $current = $states[DBTableDocumentsChains::HANDLING_STATUS];
            $evaluated = HandlingStatusEnum::evaluate_state($current, $new_handling_state);
            if (strcmp($evaluated, $current) === 0) return false;
            else $new_handling_state = $evaluated;
        }

        // set state
        $set = [DBTableDocumentsChains::HANDLING_STATUS => $new_handling_state];
        $condition = (new SQLCondition())->equal(DBTableDocumentsChains::CHAINS_ID, $chain_id);
        return SQLUpdate::update(DBTableDocumentsChains::TABLE_NAME, $set, $condition) > 0;
    }

    public static function get_document_chain_info(string $chain_id): ?array
    {
        $columns = [
            DBTableDocumentsChains::CHAINS_ID,
            DBTableDocumentsChains::ARCHIVED_STATUS,
            DBTableDocumentsChains::HANDLING_STATUS,
            DBTableDocumentsChains::DOCUMENT_COUNT,
            DBTable::UPDATED_AT => TimestampHelper::query_timestamp_as_iso8601(DBTable::UPDATED_AT),
            DBTable::CREATED_AT => TimestampHelper::query_timestamp_as_iso8601(DBTable::CREATED_AT),
        ];
        $condition = (new SQLCondition())->equal(DBTableDocumentsChains::CHAINS_ID, $chain_id);
        return SQLSelect::select_one(DBTableDocumentsChains::TABLE_NAME, $columns, $condition);
    }

    public static function increment_document_chain_document_count(string $chain_id, int $add = 1): bool
    {
        $set = [DBTableDocumentsChains::DOCUMENT_COUNT => new SQLExpression(DBTableDocumentsChains::DOCUMENT_COUNT . ' + ' . $add)];
        $condition = (new SQLCondition())->equal(DBTableDocumentsChains::CHAINS_ID, $chain_id);
        return SQLUpdate::update(DBTableDocumentsChains::TABLE_NAME, $set, $condition) === 1;
    }

    public static function set_document_chain_archive_state(string $chain_id, string $new_status): bool
    {
        $set = [DBTableDocumentsChains::ARCHIVED_STATUS => $new_status];
        $condition = (new SQLCondition())->equal(DBTableDocumentsChains::CHAINS_ID, $chain_id);
        return SQLUpdate::update(DBTableDocumentsChains::TABLE_NAME, $set, $condition) === 1;
    }

    public static function delete_chain(string $id): bool
    {
        DtransLogger::notice("Deleting chain.", ['$id' => $id]);

        $documents = ModelDocuments::get_all_documents_by_chain($id);
        if (empty($documents)) return false;

        SQLTransaction::start();

        // delete individual files
        foreach ($documents as $document) {
            ModelDocuments::delete_document($document[DBTableDocuments::UUID]);
        }

        // delete leftover metadata
        $where = (new SQLCondition())->equal(DBTableDocumentsChains::CHAINS_ID, $id, DBTableDocumentsChains::TABLE_NAME);
        $okay = SQLDelete::delete(DBTableDocumentsChains::TABLE_NAME, $where) > 0;

        SQLTransaction::commit();
        return $okay;
    }
}