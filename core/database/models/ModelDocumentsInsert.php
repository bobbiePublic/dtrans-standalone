<?php

namespace Dtrans\core\database\models;

use Dtrans\core\database\abstraction\SQLCondition;
use Dtrans\core\database\abstraction\SQLExpression;
use Dtrans\core\database\abstraction\SQLInsert;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\abstraction\SQLTransaction;
use Dtrans\core\database\abstraction\SQLUpdate;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableDocuments;
use Dtrans\core\enums\ArchivedStatusEnum;
use Dtrans\core\enums\VersionStatusEnum;
use Dtrans\core\types\DtransDocument;

abstract class ModelDocumentsInsert
{
    public static function insert_new_document(?DtransDocument $document): bool
    {
        if (is_null($document))
            return false;

        SQLTransaction::start();

        // query previous documents in chain and skip if no chain was found in document
        $version_status = VersionStatusEnum::CURRENT;
        if (!is_null($document->get_dtrans_id())) {
            $columns = [DBTableDocuments::UUID, DBTableDocuments::VERSION_STATUS, DBTableDocuments::UBL_PREVIOUS_VERSION_UUID];
            $where = (new SQLCondition())->equal(DBTableDocuments::CHAIN_ID, $document->get_dtrans_id());
            $result = SQLSelect::select(DBTableDocuments::TABLE_NAME, $columns, $where);

            // if first document in chain create order entry
            if (empty($result)) {
                $referenceID = NULL;
                if (!empty($document->getReference()) && !$document->getReference()->referenceAlreadyExists())
                    $referenceID = ModelDocumentsReference::create_new_reference($document->getReference());
                else if (!empty($document->getReference()) && $document->getReference()->referenceAlreadyExists())
                    $referenceID = $document->getReference()->getExistingReferenceID();
                ModelDocumentsChains::create_document_chain($document->get_dtrans_id(), $document->get_handling_status(), ArchivedStatusEnum::ACTIVE, 1, $referenceID);
            } else {
                ModelDocumentsChains::set_document_chain_handling_state($document->get_dtrans_id(), $document->get_handling_status());
                ModelDocumentsChains::increment_document_chain_document_count($document->get_dtrans_id());

                // search for document which references this document as previous
                // this could be the case if one document is send over dtrans delayed and is already outdated
                foreach ($result as $row)
                    if (!is_null($row[DBTableDocuments::UBL_PREVIOUS_VERSION_UUID]) && strcmp($row[DBTableDocuments::UBL_PREVIOUS_VERSION_UUID], $document->get_uuid())) {
                        $version_status = VersionStatusEnum::PREVIOUS;
                        break;
                    }
            }
        }

        // set previous document to replaced
        if (!is_null($document->get_previous_version_uuid())) {
            $set = [DBTable::UPDATED_AT => new SQLExpression(DBTable::TIME_NOW),
                DBTableDocuments::VERSION_STATUS => VersionStatusEnum::PREVIOUS];
            $where = (new SQLCondition())->equal(DBTableDocuments::UUID, $document->get_previous_version_uuid());
            SQLUpdate::update(DBTableDocuments::TABLE_NAME, $set, $where);
        }

        // documents table
        $values = [
            DBTableDocuments::TYPE => $document->get_type(),
            DBTableDocuments::CHAIN_ID => $document->get_dtrans_id(),
            DBTableDocuments::FILE_NAME => $document->get_file_name(),
            DBTableDocuments::FILE_SIZE => $document->get_file_size(),
            DBTableDocuments::UUID => $document->get_uuid(),
            DBTableDocuments::VERSION_STATUS => $version_status,
            DBTableDocuments::IN_DTRANS_QUEUE => ($document->get_dtrans_queue_status() == 'true' ? 1 : 0),
            DBTable::CREATED_AT => $document->get_created_at() ?? new SQLExpression(DBTable::TIME_NOW),
            DBTable::UPDATED_AT => $document->get_updated_at() ?? new SQLExpression(DBTable::TIME_NOW),

            // UBL
            DBTableDocuments::UBL_ID => $document->get_dtrans_id(),
            DBTableDocuments::UBL_PREVIOUS_VERSION_UUID => $document->get_previous_version_uuid(),
            DBTableDocuments::UBL_DOCUMENT_REFERENCES => $document->get_document_references(),
            DBTableDocuments::UBL_DATE_DOCUMENT => $document->get_date_document(),
            DBTableDocuments::UBL_DATE_SHIPPING_ESTIMATED_START => $document->get_shipping_estimated_start(),
            DBTableDocuments::UBL_DATE_SHIPPING_ESTIMATED_END => $document->get_shipping_estimated_end(),
            DBTableDocuments::UBL_DATE_SHIPPING_ACTUAL => $document->get_shipping_actual(),
            DBTableDocuments::UBL_PARTY_DESPATCH_SUPPLIER => $document->get_despatch_supplier_party(),
            DBTableDocuments::UBL_PARTY_DELIVERY_CUSTOMER => $document->get_delivery_customer_party(),
            DBTableDocuments::UBL_PARTY_BUYER_CUSTOMER => $document->get_buyer_customer_party(),
            DBTableDocuments::UBL_PARTY_SELLER_SUPPLIER => $document->get_seller_supplier_party(),
            DBTableDocuments::UBL_PARTY_ORIGINATOR_CUSTOMER => $document->get_originator_customer_party(),
            DBTableDocuments::UBL_LICENSE_PLATE => $document->getLicensePlate(),
            DBTableDocuments::UBL_DRIVER_NAME => $document->getDriverName(),
            DBTableDocuments::UBL_CARRIER_PARTY_SUB => $document->get_sub_carrier_party(),
            DBTableDocuments::UBL_CARRIER_PARTY_MAIN => $document->get_main_carrier_party(),
            DBTableDocuments::UBL_DELIVERY_LOCATION => $document->get_delivery_location(),
            DBTableDocuments::UBL_ALTERNATIVE_DELIVERY_LOCATION => $document->get_alternative_delivery_location(),
            DBTableDocuments::UBL_ITEM_DESCRIPTIONS => $document->getItemDescription(),
            DBTableDocuments::UBL_ITEM_WEIGHTS => $document->getItemWeights(),
            DBTableDocuments::UBL_HAS_REJECTED_ITEMS => ($document->get_rejected() == 'true' ? 1 : 0),

            // PDF
            DBTableDocuments::PDF_EMBEDDED_ID => $document->get_pdf_embedded_id(),
            DBTableDocuments::PDF_EMBEDDED_UUID => $document->get_pdf_embedded_uuid(),
        ];

        $result = SQLInsert::insert(DBTableDocuments::TABLE_NAME, $values);
        $document->set_internal_id(SQLInsert::last_insert_id());

        SQLTransaction::commit();

        return $result;
    }
}