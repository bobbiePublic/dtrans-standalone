<?php

namespace Dtrans\core\database\models;

use Dtrans\core\constants\ConstsJson;
use Dtrans\core\database\abstraction\SQLCondition;
use Dtrans\core\database\abstraction\SQLCount;
use Dtrans\core\database\abstraction\SQLOperators;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableDocuments;
use Dtrans\core\database\tables\DBTableDocumentsChains;
use Dtrans\core\enums\ArchivedStatusEnum;
use Dtrans\core\enums\ListviewColumnEnum;
use Dtrans\core\helpers\TimestampHelper;

abstract class ModelDocumentsList
{
    // counts available documents
    public static function count_available_documents(?SQLCondition $filters): int
    {
        $where = new SQLCondition();

        if (!is_null($filters))
            $where->nest($filters, SQLOperators::COMBINE_AND);

        return SQLCount::count_join(DBTableDocuments::TABLE_NAME, DBTableDocumentsChains::TABLE_NAME,
            DBTableDocuments::CHAIN_ID, DBTableDocumentsChains::CHAINS_ID,
            SQLOperators::JOIN_LEFT, $where);
    }

    // returns list of documents
    public static function query_document_list(int $offset, int $limit, bool $sort_asc, ?string $sort_by, ?SQLCondition $filters, array $columns): ?array
    {
        $columns_left = [
            // alias => queried_column from documents
            ConstsJson::JSON_DOCUMENT_UUID => DBTableDocuments::UUID,
        ];

        if(in_array(ListviewColumnEnum::CHAIN_ID, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_CHAIN_ID] = DBTableDocuments::CHAIN_ID;
        if(in_array(ListviewColumnEnum::DOCUMENT_TYPE, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_TYPE] = DBTableDocuments::TYPE;
        //if(in_array(ListColumnEnum::CREATED_AT, $columns))
        $columns_left[ConstsJson::JSON_DOCUMENT_CREATED_AT] = TimestampHelper::query_timestamp_as_iso8601(DBTableDocuments::TABLE_NAME . '.' . DBTable::CREATED_AT);
        //if(in_array(ListColumnEnum::UPDATED_AT, $columns))
        $columns_left[ConstsJson::JSON_DOCUMENT_UPDATED_AT] = TimestampHelper::query_timestamp_as_iso8601(DBTableDocuments::TABLE_NAME . '.' . DBTable::UPDATED_AT);
        if(in_array(ListviewColumnEnum::DOCUMENT_PREDECESSOR, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_PREVIOUS_VERSION_UUID] = DBTableDocuments::UBL_PREVIOUS_VERSION_UUID;
        if(in_array(ListviewColumnEnum::DOCUMENT_PDF_EMBEDDED_UUID, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_PDF_EMBEDDED_DOCUMENT_UUID] = DBTableDocuments::PDF_EMBEDDED_UUID;
        if(in_array(ListviewColumnEnum::VERSION_STATUS, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_VERSION_STATUS] = DBTableDocuments::VERSION_STATUS;
        if(in_array(ListviewColumnEnum::FILE_SIZE, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_FILE_SIZE] = DBTableDocuments::FILE_SIZE;
        if(in_array(ListviewColumnEnum::NOTE, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_NOTE] = DBTableDocuments::NOTE;
        if(in_array(ListviewColumnEnum::UBL_ID, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_ID] = DBTableDocuments::UBL_ID;
        if(in_array(ListviewColumnEnum::UBL_REFERENCES, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_REFERENCES] = DBTableDocuments::UBL_DOCUMENT_REFERENCES;
        if(in_array(ListviewColumnEnum::UBL_DATE_DOCUMENT, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_DATE] = DBTableDocuments::UBL_DATE_DOCUMENT;
        if(in_array(ListviewColumnEnum::UBL_DATE_SHIPPING_ESTIMATED_START, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_DATE_SHIPPING_ESTIMATED_START] = DBTableDocuments::UBL_DATE_SHIPPING_ESTIMATED_START;
        if(in_array(ListviewColumnEnum::UBL_DATE_SHIPPING_ESTIMATED_END, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_DATE_SHIPPING_ESTIMATED_END] = DBTableDocuments::UBL_DATE_SHIPPING_ESTIMATED_END;
        if(in_array(ListviewColumnEnum::UBL_DATE_SHIPPING_ACTUAL, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_DATE_SHIPPING_ACTUAL] = DBTableDocuments::UBL_DATE_SHIPPING_ACTUAL;
        if(in_array(ListviewColumnEnum::UBL_PARTY_DESPATCH_SUPPLIER, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_DESPATCH_SUPPLIER_PARTY] = DBTableDocuments::UBL_PARTY_DESPATCH_SUPPLIER;
        if(in_array(ListviewColumnEnum::UBL_PARTY_DELIVERY_CUSTOMER, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_DELIVERY_CUSTOMER_PARTY] = DBTableDocuments::UBL_PARTY_DELIVERY_CUSTOMER;
        if(in_array(ListviewColumnEnum::UBL_PARTY_BUYER_CUSTOMER, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_BUYER_CUSTOMER_PARTY] = DBTableDocuments::UBL_PARTY_BUYER_CUSTOMER;
        if(in_array(ListviewColumnEnum::UBL_PARTY_SELLER_SUPPLIER, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_SELLER_SUPPLIER_PARTY] = DBTableDocuments::UBL_PARTY_SELLER_SUPPLIER;
        if(in_array(ListviewColumnEnum::UBL_PARTY_ORIGINATOR_CUSTOMER, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_ORIGINATOR_COSTUMER_PARTY] = DBTableDocuments::UBL_PARTY_ORIGINATOR_CUSTOMER;
        if(in_array(ListviewColumnEnum::UBL_ITEM_DESCRIPTION, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_ITEM_DESCRIPTION] = DBTableDocuments::UBL_ITEM_DESCRIPTIONS;
        if(in_array(ListviewColumnEnum::UBL_ITEM_WEIGHTS, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_ITEM_WEIGHTS] = DBTableDocuments::UBL_ITEM_WEIGHTS;
        if(in_array(ListviewColumnEnum::UBL_CARRIER_LICENSE_PLATE, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_LICENSE_PLATE] = DBTableDocuments::UBL_LICENSE_PLATE;
        if(in_array(ListviewColumnEnum::UBL_PARTY_CARRIER_MAIN, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_CARRIER_PARTY_MAIN] = DBTableDocuments::UBL_CARRIER_PARTY_MAIN;
        if(in_array(ListviewColumnEnum::UBL_PARTY_CARRIER_SUB, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_CARRIER_PARTY_SUB] = DBTableDocuments::UBL_CARRIER_PARTY_SUB;
        if(in_array(ListviewColumnEnum::UBL_CARRIER_DRIVER_NAME, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_CARRIER_DRIVER_NAME] = DBTableDocuments::UBL_DRIVER_NAME;
        if(in_array(ListviewColumnEnum::UBL_DELIVERY_LOCATION, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_DELIVERY_LOCATION] = DBTableDocuments::UBL_DELIVERY_LOCATION;
        if(in_array(ListviewColumnEnum::UBL_ALTERNATIVE_DELIVERY_LOCATION, $columns))
            $columns_left[ConstsJson::JSON_DOCUMENT_ALTERNATIVE_DELIVERY_LOCATION] = DBTableDocuments::UBL_ALTERNATIVE_DELIVERY_LOCATION;

        $columns_right = [];
        $columns_right[ConstsJson::JSON_DOCUMENT_HANDLING_STATUS] = DBTableDocumentsChains::HANDLING_STATUS;
        if(in_array(ListviewColumnEnum::ARCHIVED_STATUS, $columns))
            $columns_right[ConstsJson::JSON_DOCUMENT_ARCHIVED_STATUS] = DBTableDocumentsChains::ARCHIVED_STATUS;

        /* WTF IS THIS CODE HERE? $filter_chain is never declared at this point
        if (!empty($filter_chain) && 'NULL' == strtoupper($filter_chain))
            $w->and->isNull(DBTableDocuments::TABLE_NAME . '.' . DBTableDocuments::CHAIN_ID);
        else if (!empty($filter_chain)) {
            $w->and->like(DBTableDocuments::TABLE_NAME . '.' . DBTableDocuments::CHAIN_ID, '%' . $filter_chain . '%');
        }*/


        return SQLSelect::select_join(DBTableDocuments::TABLE_NAME, $columns_left,
            DBTableDocumentsChains::TABLE_NAME, $columns_right,
            DBTableDocuments::CHAIN_ID, DBTableDocumentsChains::CHAINS_ID, SQLOperators::JOIN_LEFT,
            $filters, $sort_by, $sort_asc, $limit, $offset
        );
    }

    public static function create_filter($search_string, $filter_created_after, $filter_chain, $filter_uuid, $filter_initial_version, $filter_version_status, $filter_created_at,
                                         $filter_updated_at, $filter_document_date, $filter_actual_shipping_date, $filter_estimated_shipping_date, $filter_references, $filter_despatch_supplier_party, $filter_delivery_costumer_party,
                                         $filter_buyer_costumer_party, $filter_seller_supplier_party, $filter_originator_costumer_party, $filter_rejected, $filter_handling_status, $filter_archived_status, $filter_document_type, $filter_pdf_embedded_document_uuid,
                                         $filter_note, bool $filter_chainless): ?SQLCondition
    {
        $where = new SQLCondition();
        $where->AND();

        if (!empty($search_string))
            self::add_search_string($where, $search_string);

        if (!empty($filter_uuid))
            $where->match(DBTableDocuments::UUID, $filter_uuid, DBTableDocuments::TABLE_NAME);
        if (!empty($filter_initial_version))
            $where->match(DBTableDocuments::UBL_PREVIOUS_VERSION_UUID, $filter_initial_version, DBTableDocuments::TABLE_NAME);
        //if (!empty($filter_references))
            //$where->match(DBTableDocuments::UBL_DOCUMENT_REFERENCES, $filter_references, DBTableDocuments::TABLE_NAME);
        if (!empty($filter_despatch_supplier_party))
            $where->match(DBTableDocuments::UBL_PARTY_DESPATCH_SUPPLIER, $filter_despatch_supplier_party, DBTableDocuments::TABLE_NAME);
        if (!empty($filter_delivery_costumer_party))
            $where->match(DBTableDocuments::UBL_PARTY_DELIVERY_CUSTOMER, $filter_delivery_costumer_party, DBTableDocuments::TABLE_NAME);
        if (!empty($filter_buyer_costumer_party))
            $where->match(DBTableDocuments::UBL_PARTY_BUYER_CUSTOMER, $filter_buyer_costumer_party, DBTableDocuments::TABLE_NAME);
        if (!empty($filter_seller_supplier_party))
            $where->match(DBTableDocuments::UBL_PARTY_SELLER_SUPPLIER, $filter_seller_supplier_party, DBTableDocuments::TABLE_NAME);
        if (!empty($filter_originator_costumer_party))
            $where->match(DBTableDocuments::UBL_PARTY_ORIGINATOR_CUSTOMER, $filter_originator_costumer_party, DBTableDocuments::TABLE_NAME);
        if (!empty($filter_pdf_embedded_document_uuid))
            $where->match(DBTableDocuments::PDF_EMBEDDED_UUID, $filter_pdf_embedded_document_uuid, DBTableDocuments::TABLE_NAME);
        if (!empty($filter_note))
            $where->match(DBTableDocuments::NOTE, $filter_note, DBTableDocuments::TABLE_NAME);

        // special
        if (!empty($filter_created_after))
            $where->greater_than(DBTable::CREATED_AT, $filter_created_after, DBTableDocuments::TABLE_NAME);

        // TODO $filter_estimated_shipping_date is a mess
        if (is_string($filter_estimated_shipping_date)) {
            $where->is_not_null(DBTableDocuments::UBL_DATE_SHIPPING_ESTIMATED_START, DBTableDocuments::TABLE_NAME);
            $where->is_not_null(DBTableDocuments::UBL_DATE_SHIPPING_ESTIMATED_END, DBTableDocuments::TABLE_NAME);
            $where->less_than_or_equal(DBTableDocuments::UBL_DATE_SHIPPING_ESTIMATED_START, $filter_estimated_shipping_date, DBTableDocuments::TABLE_NAME);
            $where->greater_than_or_equal(DBTableDocuments::UBL_DATE_SHIPPING_ESTIMATED_END, $filter_estimated_shipping_date, DBTableDocuments::TABLE_NAME);
        } /*else if(is_array($filter_estimated_shipping_date)) { // interval search on interval is not supported. What would be the expected result?
            $filters[DBTableDocuments::TABLE_NAME . '.' . DBTableDocuments::UBL_DATE_SHIPPING_ESTIMATED_START . " IS NOT NULL AND " . DBTableDocuments::TABLE_NAME . '.' . DBTableDocuments::UBL_DATE_SHIPPING_ESTIMATED_START . " <= ?"] = array_pop($filter_estimated_shipping_date);
            $filters[DBTableDocuments::TABLE_NAME . '.' . DBTableDocuments::UBL_DATE_SHIPPING_ESTIMATED_END . " IS NOT NULL AND " . DBTableDocuments::TABLE_NAME . '.' . DBTableDocuments::UBL_DATE_SHIPPING_ESTIMATED_END . " >= ?"] = array_pop($filter_estimated_shipping_date);
        } */

        // time stamps
        self::add_timestamp_to_filter($where, $filter_created_at, DBTable::CREATED_AT);
        self::add_timestamp_to_filter($where, $filter_updated_at, DBTable::UPDATED_AT);
        self::add_timestamp_to_filter($where, $filter_document_date, DBTableDocuments::UBL_DATE_DOCUMENT);
        self::add_timestamp_to_filter($where, $filter_actual_shipping_date, DBTableDocuments::UBL_DATE_SHIPPING_ACTUAL);

        // enums
        if (!empty($filter_version_status))
            $where->in(DBTableDocuments::VERSION_STATUS, $filter_version_status, DBTableDocuments::TABLE_NAME);
        if (!empty($filter_document_type))
            $where->in(DBTableDocuments::TYPE, $filter_document_type, DBTableDocuments::TABLE_NAME);

        if ($filter_chainless)
            $where->is_null(DBTableDocuments::CHAIN_ID, DBTableDocuments::TABLE_NAME);
        else { // filter_chainless overwrites all attribute filters which are related to chain
            if (empty($filter_archived_status)) // hide archived documents by default
                $where->equal(DBTableDocumentsChains::ARCHIVED_STATUS, ArchivedStatusEnum::ACTIVE, DBTableDocumentsChains::TABLE_NAME);
            else
                $where->in(DBTableDocumentsChains::ARCHIVED_STATUS, $filter_archived_status, DBTableDocumentsChains::TABLE_NAME);

            if (!empty($filter_handling_status))
                $where->in(DBTableDocumentsChains::HANDLING_STATUS, $filter_handling_status, DBTableDocumentsChains::TABLE_NAME);

            if (!empty($filter_chain)) // file id exists in both tables so table name is required
                $where->match(DBTableDocuments::CHAIN_ID, $filter_chain, DBTableDocuments::TABLE_NAME);
        }

        // booleans
        if (!empty($filter_rejected))
            $where->equal(DBTableDocuments::UBL_HAS_REJECTED_ITEMS, $filter_rejected, DBTableDocuments::TABLE_NAME);

        return $where;
    }

    // sub function which is used to add a timestamp filter to where condition
    private static function add_timestamp_to_filter(SQLCondition $condition, /*string|array|null*/ $timestamp, string $column, string $table = DBTableDocuments::TABLE_NAME)
    {
        if (empty($timestamp)) return;

        if (is_string($timestamp))
            $condition->equal($column, $timestamp, $table);
        else if (is_array($timestamp)) {
            $condition->less_than_or_equal($column, array_pop($timestamp), $table);
            $condition->greater_than_or_equal($column, array_pop($timestamp), $table);
        }
    }

    // sub function which is used to add the generic search string to where condition
    private static function add_search_string(SQLCondition $condition, ?string $search_string)
    {
        $where = (new SQLCondition())->OR();

        $where->match(DBTableDocuments::CHAIN_ID, $search_string, DBTableDocuments::TABLE_NAME);
        $where->match(DBTableDocuments::UUID, $search_string, DBTableDocuments::TABLE_NAME);
        $where->match(DBTableDocuments::NOTE, $search_string, DBTableDocuments::TABLE_NAME);
        $where->match(DBTableDocuments::UBL_DOCUMENT_REFERENCES, $search_string, DBTableDocuments::TABLE_NAME);
        $where->match(DBTableDocuments::UBL_PARTY_DESPATCH_SUPPLIER, $search_string, DBTableDocuments::TABLE_NAME);
        $where->match(DBTableDocuments::UBL_PARTY_DELIVERY_CUSTOMER, $search_string, DBTableDocuments::TABLE_NAME);
        $where->match(DBTableDocuments::UBL_PARTY_BUYER_CUSTOMER, $search_string, DBTableDocuments::TABLE_NAME);
        $where->match(DBTableDocuments::UBL_PARTY_SELLER_SUPPLIER, $search_string, DBTableDocuments::TABLE_NAME);
        $where->match(DBTableDocuments::UBL_PARTY_ORIGINATOR_CUSTOMER, $search_string, DBTableDocuments::TABLE_NAME);

        $condition->nest($where, SQLOperators::COMBINE_AND);
    }

}