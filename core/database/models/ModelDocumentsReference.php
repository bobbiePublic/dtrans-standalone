<?php

namespace Dtrans\core\database\models;

use Dtrans\core\database\abstraction\SQLCondition;
use Dtrans\core\database\abstraction\SQLInsert;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\tables\DBTableDocumentsReferences;
use Dtrans\core\types\UBLProjectReference;

abstract class ModelDocumentsReference
{
    public static function create_new_reference(UBLProjectReference $reference): ?int
    {
        $gps_latitude = NULL;
        $gps_longitude = NULL;

        if (!empty($reference->getLocationCoords()) && str_contains($reference->getLocationCoords(), ',')) {
            $location_gps = explode(',', $reference->getLocationCoords());
            $gps_latitude = $location_gps[0];
            $gps_longitude = $location_gps[1];
        }

        $values = [
            DBTableDocumentsReferences::LOCATION_SOURCE => $reference->getLocationSource(),
            DBTableDocumentsReferences::LOCATION_ADDRESS => $reference->getAddress(),
            DBTableDocumentsReferences::LOCATION_GPS_LAT => $gps_latitude,
            DBTableDocumentsReferences::LOCATION_GPS_LNG => $gps_longitude,
            DBTableDocumentsReferences::SALES_ID => $reference->getSalesID(),
            DBTableDocumentsReferences::NAME => $reference->getName()
        ];
        if (!SQLInsert::insert(DBTableDocumentsReferences::TABLE_NAME, $values))
            return null;
        return SQLInsert::last_insert_id();
    }

    public static function search_reference(?array $sales_ids, ?string $sourceAddress, ?string $locationAddress, ?string $locationCoordinate): ?UBLProjectReference
    {
        $gps_latitude = NULL;
        $gps_longitude = NULL;

        if (!empty($locationCoordinate) && str_contains($locationCoordinate, ',')) {
            $location_gps = explode(',', $locationCoordinate);
            $gps_latitude = $location_gps[0];
            $gps_longitude = $location_gps[1];
        }

        if (!empty($sourceAddress) || !empty($sales_ids)) { // first check
            $columns = [DBTableDocumentsReferences::ID, DBTableDocumentsReferences::LOCATION_SOURCE,
                DBTableDocumentsReferences::LOCATION_ADDRESS, DBTableDocumentsReferences::LOCATION_GPS_LAT,
                DBTableDocumentsReferences::LOCATION_GPS_LNG, DBTableDocumentsReferences::SALES_ID, DBTableDocumentsReferences::NAME];

            $where = (new SQLCondition());
            if (!empty($sourceAddress))
                $where->equal(DBTableDocumentsReferences::LOCATION_SOURCE, $sourceAddress);
            if (!empty($sales_ids))
                $where->OR()->in(DBTableDocumentsReferences::SALES_ID, $sales_ids);
            $result = SQLSelect::select_one(DBTableDocumentsReferences::TABLE_NAME, $columns, $where);
            if (!empty($result)) {
                $ref = new UBLProjectReference();
                $ref->setExistingReferenceID($result[DBTableDocumentsReferences::ID]);
                $ref->setName($result[DBTableDocumentsReferences::NAME]);
                $ref->setSalesID($result[DBTableDocumentsReferences::SALES_ID]);
                $ref->setAddress($result[DBTableDocumentsReferences::LOCATION_ADDRESS]);
                $ref->setLocationSource($result[DBTableDocumentsReferences::LOCATION_SOURCE]);
                if (!empty($result[DBTableDocumentsReferences::LOCATION_GPS_LAT]) && !empty($result[DBTableDocumentsReferences::LOCATION_GPS_LNG]))
                    $ref->setLocationCoords($result[DBTableDocumentsReferences::LOCATION_GPS_LAT] . ',' . $result[DBTableDocumentsReferences::LOCATION_GPS_LNG]);
                return $ref;
            }
        }

        if ((!empty($gps_longitude) && !empty($gps_latitude)) || !empty($locationAddress)) {
            $columns = [DBTableDocumentsReferences::ID, DBTableDocumentsReferences::LOCATION_SOURCE,
                DBTableDocumentsReferences::LOCATION_ADDRESS, DBTableDocumentsReferences::LOCATION_GPS_LAT,
                DBTableDocumentsReferences::LOCATION_GPS_LNG, DBTableDocumentsReferences::SALES_ID, DBTableDocumentsReferences::NAME];
            $where = new SQLCondition();
            if (!empty($gps_longitude) && !empty($gps_latitude)) {
                $where->equal(DBTableDocumentsReferences::LOCATION_GPS_LAT, $gps_latitude);
                $where->AND()->equal(DBTableDocumentsReferences::LOCATION_GPS_LNG, $gps_longitude);
            }

            if (!empty($locationAddress)) // AND has precedence over OR in SQL
                $where->OR()->equal(DBTableDocumentsReferences::LOCATION_ADDRESS, $locationAddress);
            $result = SQLSelect::select_one(DBTableDocumentsReferences::TABLE_NAME, $columns, $where);
            if (!empty($result)) {
                $ref = new UBLProjectReference();
                $ref->setExistingReferenceID($result[DBTableDocumentsReferences::ID]);
                $ref->setName($result[DBTableDocumentsReferences::NAME]);
                $ref->setSalesID($result[DBTableDocumentsReferences::SALES_ID]);
                $ref->setAddress($result[DBTableDocumentsReferences::LOCATION_ADDRESS]);
                $ref->setLocationSource($result[DBTableDocumentsReferences::LOCATION_SOURCE]);
                if (!empty($result[DBTableDocumentsReferences::LOCATION_GPS_LAT]) && !empty($result[DBTableDocumentsReferences::LOCATION_GPS_LNG]))
                    $ref->setLocationCoords($result[DBTableDocumentsReferences::LOCATION_GPS_LAT] . ',' . $result[DBTableDocumentsReferences::LOCATION_GPS_LNG]);
                return $ref;
            }
        }

        return null;
    }
}