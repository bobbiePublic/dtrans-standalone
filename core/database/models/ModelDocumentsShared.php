<?php

namespace Dtrans\core\database\models;

use Dtrans\core\database\abstraction\SQLCondition;
use Dtrans\core\database\abstraction\SQLExpression;
use Dtrans\core\database\abstraction\SQLInsert;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\abstraction\SQLUpdate;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableDocumentsShared;
use Dtrans\core\helpers\Random;
use Dtrans\core\helpers\TimestampHelper;

abstract class ModelDocumentsShared
{
    public static function increase_key_view_count(string $key): bool
    {
        $where = (new SQLCondition())->equal(DBTableDocumentsShared::KEY, strtolower($key));
        $set = [DBTableDocumentsShared::VIEW_COUNT => new SQLExpression(DBTableDocumentsShared::VIEW_COUNT . ' + 1')];
        return 0 < SQLUpdate::update(DBTableDocumentsShared::TABLE_NAME, $set, $where);
    }

    public static function get_document_uuid_from_key(string $key, bool $increase_counter = true): ?string
    {
        // token does never expire
        $where_nested = (new SQLCondition())->is_null(DBTableDocumentsShared::EXPIRES_AT);
        // token is not expired
        $where_nested->OR()->greater_than(DBTableDocumentsShared::EXPIRES_AT, new SQLExpression(DBTable::TIME_NOW));
        $where = (new SQLCondition())->equal(DBTableDocumentsShared::KEY, strtolower($key));
        $where->AND()->nest($where_nested);
        $result = SQLSelect::select_one(DBTableDocumentsShared::TABLE_NAME, [DBTableDocumentsShared::UUID], $where);

        // not found
        if (empty($result))
            return null;

        // found
        if ($increase_counter)
            self::increase_key_view_count($key);
        return $result[DBTableDocumentsShared::UUID];
    }

    public static function get_key_info(string $key): ?array
    {
        $columns = [DBTableDocumentsShared::UUID,
            DBTableDocumentsShared::CREATED_BY,
            DBTableDocumentsShared::CREATED_AT => TimestampHelper::query_timestamp_as_iso8601(DBTableDocumentsShared::CREATED_AT),
            DBTableDocumentsShared::EXPIRES_AT => TimestampHelper::query_timestamp_as_iso8601(DBTableDocumentsShared::EXPIRES_AT),
            DBTableDocumentsShared::VIEW_COUNT,
            DBTableDocumentsShared::ACCESSED_AT];

        $where = (new SQLCondition())->equal(DBTableDocumentsShared::KEY, strtolower($key));
        $result = SQLSelect::select_one(DBTableDocumentsShared::TABLE_NAME, $columns, $where);

        // not found
        if (empty($result))
            return null;
        return $result;
    }

    public static function delete_document_key(string $key): bool
    {
        $set = [
            DBTableDocumentsShared::EXPIRES_AT => new SQLExpression(DBTable::TIME_NOW)
        ];
        $where = (new SQLCondition())->equal(DBTableDocumentsShared::KEY, strtolower($key));
        return SQLUpdate::update(DBTableDocumentsShared::TABLE_NAME, $set, $where);
    }

    public static function create_document_key(string $uuid, string $creator_reference, string $expires_at_sql = ''): ?string
    {
        $creator_reference = substr($creator_reference, 0, 255);
        $key = self::generate_key($uuid);

        // expiration time
        if (empty($expires_at_sql)) {
            $expires_at_sql = DBTable::TIME_NOW;
            // $expires_at .= ' + INTERVAL '. h . ' HOUR';
            // $expires_at .= ' + INTERVAL '. h . ' MINUTE';
            $expires_at_sql .= ' + INTERVAL ' . '90' . ' DAY';
        }

        $values = [
            DBTableDocumentsShared::KEY => $key,
            DBTableDocumentsShared::UUID => $uuid,
            DBTableDocumentsShared::CREATED_BY => $creator_reference,
            DBTableDocumentsShared::EXPIRES_AT => new SQLExpression($expires_at_sql),
        ];

        return SQLInsert::insert(DBTableDocumentsShared::TABLE_NAME, $values) ? $key : null;
    }

    private static function generate_key(string $uuid): string
    {
        $hash = gmdate("Y-m-d H:i:s") . $uuid . Random::string_cryptographic(256);
        return strtolower(hash('SHA512', $hash));
    }
}