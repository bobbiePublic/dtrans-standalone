<?php

namespace Dtrans\core\database\models;

use Dtrans\core\database\abstraction\SQLCondition;
use Dtrans\core\database\abstraction\SQLInsert;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\tables\DBTableIncoming;
use Dtrans\core\enums\DtransFormatsEnum;

abstract class ModelIncoming
{
    public static function get_file_name_by_hash(string $hash, string $format = null): ?string
    {
        $where = (new SQLCondition())->equal(DBTableIncoming::HASH, $hash);
        if (!is_null($format))
            $where->equal(DBTableIncoming::FORMAT, $format);

        $entry = SQLSelect::select_one(DBTableIncoming::TABLE_NAME, [DBTableIncoming::FILE_NAME], $where);
        if (empty($entry))
            return null;
        return $entry[DBTableIncoming::FILE_NAME];
    }

    public static function create_entry(string $file_name, string $hash, string $format, string $origin_interface, ?string $origin_ip, ?string $origin_user): int
    {
        $format = DtransFormatsEnum::parse($format);
        $values = [
            DBTableIncoming::FORMAT => $format,
            DBTableIncoming::FILE_NAME => $file_name,
            DBTableIncoming::HASH => $hash,
            DBTableIncoming::ORIGIN_INTERFACE => $origin_interface,
            DBTableIncoming::ORIGIN_IP => $origin_ip,
            DBTableIncoming::ORIGIN_USER => $origin_user
        ];
        SQLInsert::insert(DBTableIncoming::TABLE_NAME, $values);
        return SQLInsert::last_insert_id();
    }
}