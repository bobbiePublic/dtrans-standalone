<?php

namespace Dtrans\core\database\models;

use Dtrans\core\database\abstraction\SQLInsert;
use Dtrans\core\database\tables\DBTableIncomingRejected;

abstract class ModelRejected
{
    public static function create_entry(int $id, string $code, ?string $reason, ?int $duplicate_ref = null): bool
    {
        $values = [
            DBTableIncomingRejected::ID => $id,
            DBTableIncomingRejected::CODE => $code,
            DBTableIncomingRejected::REASON => $reason,
            DBTableIncomingRejected::DUPLICATE_REF => $duplicate_ref
        ];
        return SQLInsert::insert(DBTableIncomingRejected::TABLE_NAME, $values);
    }

}