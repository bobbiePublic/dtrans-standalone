<?php

namespace Dtrans\core\database\models;

use Dtrans\core\database\abstraction\SQLCondition;
use Dtrans\core\database\abstraction\SQLInsert;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableSendingDone;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\TimestampHelper;

abstract class ModelSend
{
    public static function create_sending_result(int $document_id, string $document_receiver, string $result_code, string $result_reason): bool
    {
        // cap reason to limit
        if (strlen($result_reason) > DBTable::DTRANS_SIGNATURE_LENGTH) {
            DtransLogger::warning('Capping result_reason to column_limit.', ['column_limit' => DBTable::DTRANS_SIGNATURE_LENGTH, 'result_reason' => $result_reason]);
            $result_reason = substr($result_reason, 0, DBTable::DTRANS_SIGNATURE_LENGTH);
        }

        $values = [
            DBTableSendingDone::DOCUMENT_ID => $document_id,
            DBTableSendingDone::DOCUMENT_RECEIVER => $document_receiver,
            DBTableSendingDone::RESULT_CODE => $result_code,
            DBTableSendingDone::RESULT_REASON => $result_reason,
        ];
        DtransLogger::info('Completed sending a document.', ['document_id' => $document_id, 'document_receiver' => $document_receiver, 'code' => $result_code]);
        return SQLInsert::insert(DBTableSendingDone::TABLE_NAME, $values);
    }

    public static function get_sending_result(int $document_id): ?array
    {
        $columns = [
            DBTableSendingDone::ID,
            DBTableSendingDone::RESULT_CODE,
            DBTableSendingDone::RESULT_REASON,
            DBTableSendingDone::DOCUMENT_RECEIVER,
            DBTable::CREATED_AT => TimestampHelper::query_timestamp_as_iso8601(DBTableSendingDone::TABLE_NAME . '.' . DBTable::CREATED_AT)
        ];

        $where = (new SQLCondition)->equal(DBTableSendingDone::DOCUMENT_ID, $document_id);
        return SQLSelect::select(DBTableSendingDone::TABLE_NAME, $columns, $where);
    }
}