<?php

namespace Dtrans\core\database\models;

use Dtrans\core\database\abstraction\SQLCondition;
use Dtrans\core\database\abstraction\SQLCount;
use Dtrans\core\database\abstraction\SQLDelete;
use Dtrans\core\database\abstraction\SQLExpression;
use Dtrans\core\database\abstraction\SQLInsert;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\abstraction\SQLUpdate;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableSendingQueue;
use Dtrans\core\helpers\DtransConfig;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\TimestampHelper;

abstract class ModelSending
{
    public static function create_sending_entry(int $document_id, string $file_path, string $dtrans_server, ?string $fallback_mail, string $document_format): void
    {
        // check if document is already queued for this server
        $condition = (new SQLCondition())->equal(DBTableSendingQueue::DESTINATION, $dtrans_server)
            ->AND()->equal(DBTableSendingQueue::DOCUMENT_ID, $document_id);
        if (0 < SQLCount::count(DBTableSendingQueue::TABLE_NAME, $condition)) {
            DtransLogger::info("Skipping already queued document for sending.", ['document_id' => $document_id, 'receiver' => $dtrans_server]);
            return;
        }

        // insert entry
        $values = [
            DBTableSendingQueue::DOCUMENT_FORMAT => $document_format,
            DBTableSendingQueue::DOCUMENT_ID => $document_id,
            DBTableSendingQueue::DOCUMENT_PATH => $file_path,
            DBTableSendingQueue::DESTINATION => $dtrans_server,
            DBTableSendingQueue::FALLBACK_MAIL => $fallback_mail,
            DBTable::UPDATED_AT => '1970-01-01' // set to a long time ago so first attempt is made immediately after insertion
        ];
        SQLInsert::insert(DBTableSendingQueue::TABLE_NAME, $values);
        DtransLogger::info('Added new document to sending queue.', ['document_id' => $document_id, 'receiver' => $dtrans_server]);
    }

    public static function get_sending_entries(): ?array
    {
        $minutes_between_attempts = DtransConfig::get_int(DtransConfig::CFG_DTRANS, 'router', 'minutes_between_attempts', 10);
        $columns = [DBTableSendingQueue::ID, DBTableSendingQueue::DOCUMENT_ID, DBTableSendingQueue::DOCUMENT_PATH, DBTableSendingQueue::DESTINATION, DBTableSendingQueue::FALLBACK_MAIL, DBTableSendingQueue::ATTEMPT_COUNT, DBTableSendingQueue::DOCUMENT_FORMAT];
        $where = (new SQLCondition())->greater_than(DBTable::TIME_NOW,
            new SQLExpression('DATE_ADD(' . DBTable::UPDATED_AT . ', INTERVAL (' . $minutes_between_attempts . ' * ' . DBTableSendingQueue::ATTEMPT_COUNT . ') MINUTE)'));
        return SQLSelect::select(DBTableSendingQueue::TABLE_NAME, $columns, $where);
    }

    // checks if completed entry was the final entry for document. So after row was deleted the document is processed and can be flagged as send.
    public static function check_sending_document_completed(int $document_id): bool
    {
        $where = (new SQLCondition())->equal(DBTableSendingQueue::DOCUMENT_ID, $document_id);
        return SQLCount::count(DBTableSendingQueue::TABLE_NAME, $where, DBTableSendingQueue::ID) === 0;
    }

    public static function increment_attempt_count(array $id_failed)
    {
        if (empty($id_failed)) return;

        $values = [DBTableSendingQueue::ATTEMPT_COUNT => new SQLExpression(DBTableSendingQueue::ATTEMPT_COUNT . ' + 1')];
        $where = (new SQLCondition())->in(DBTableSendingQueue::ID, $id_failed);
        SQLUpdate::update(DBTableSendingQueue::TABLE_NAME, $values, $where);
    }

    public static function delete_completed_entries(array $id_done)
    {
        if (empty($id_done)) return;
        $where = (new SQLCondition())->in(DBTableSendingQueue::ID, $id_done);
        SQLDelete::delete(DBTableSendingQueue::TABLE_NAME, $where);
    }

    public static function get_sending_result(int $document_id): ?array
    {
        $columns = [
            DBTableSendingQueue::ID,
            DBTableSendingQueue::DESTINATION,
            DBTableSendingQueue::FALLBACK_MAIL,
            DBTableSendingQueue::ATTEMPT_COUNT,
            DBTable::CREATED_AT => TimestampHelper::query_timestamp_as_iso8601(DBTableSendingQueue::TABLE_NAME . '.' . DBTable::CREATED_AT),
            DBTable::UPDATED_AT => TimestampHelper::query_timestamp_as_iso8601(DBTableSendingQueue::TABLE_NAME . '.' . DBTable::UPDATED_AT)
        ];

        $where = (new SQLCondition)->equal(DBTableSendingQueue::DOCUMENT_ID, $document_id);
        return SQLSelect::select(DBTableSendingQueue::TABLE_NAME, $columns, $where);
    }
}