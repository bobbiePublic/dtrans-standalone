<?php

namespace Dtrans\core\database\patches;

abstract class DBPatch
{
    public abstract function run();
}