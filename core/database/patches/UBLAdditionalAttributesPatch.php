<?php

namespace Dtrans\core\database\patches;


use Dtrans\core\database\abstraction\SQLCore;
use Dtrans\core\database\abstraction\SQLExpression;
use Dtrans\core\database\tables\DBTableDocuments;

class UBLAdditionalAttributesPatch extends DBPatch {

    public function run(): void
    {
        $db = SQLCore::get_adapter();

        $documents = DBTableDocuments::TABLE_NAME;
        $ubl_originator = DBTableDocuments::UBL_PARTY_ORIGINATOR_CUSTOMER;
        $db->exec("alter table $documents add " . DBTableDocuments::UBL_ITEM_WEIGHTS . " float null after $ubl_originator;");
        $db->exec("alter table $documents add "  . DBTableDocuments::UBL_ITEM_DESCRIPTIONS . " varchar(2048) null after $ubl_originator;");
        $db->exec("alter table $documents add " . DBTableDocuments::UBL_LICENSE_PLATE . " varchar(2048) null after $ubl_originator;");
    }
}