<?php

namespace Dtrans\core\database\patches;


use Dtrans\core\database\abstraction\SQLCore;
use Dtrans\core\database\abstraction\SQLExpression;
use Dtrans\core\database\tables\DBTableDocuments;

class UBLAdditionalCarrierInfoPatch extends DBPatch {

    public function run(): void
    {
        $db = SQLCore::get_adapter();

        $documents = DBTableDocuments::TABLE_NAME;
        $column = DBTableDocuments::UBL_LICENSE_PLATE;
        $db->exec("alter table $documents add " . DBTableDocuments::UBL_ALTERNATIVE_DELIVERY_LOCATION . " varchar(128) null after $column;");
        $db->exec("alter table $documents add " . DBTableDocuments::UBL_DELIVERY_LOCATION . " varchar(128) null after $column;");
        $db->exec("alter table $documents add " . DBTableDocuments::UBL_CARRIER_PARTY_MAIN . " varchar(128) null after $column;");
        $db->exec("alter table $documents add "  . DBTableDocuments::UBL_CARRIER_PARTY_SUB . " varchar(128) null after $column;");
        $db->exec("alter table $documents add " . DBTableDocuments::UBL_DRIVER_NAME . " varchar(128) null after $column;");
    }
}