<?php

namespace Dtrans\core\database\patches;


use Dtrans\core\database\abstraction\SQLCore;
use Dtrans\core\database\tables\DBTableDocuments;
use PDO;

class UBLDocumentsReferencesPatch extends DBPatch {

    public function run(): void
    {
        $db = SQLCore::get_adapter();
        $documents = DBTableDocuments::TABLE_NAME;
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);

        // shrinken uuid to varchar(96)
        // requires recreation of all foreign key checks
        $db->exec("
            ALTER TABLE documents DROP CONSTRAINT documents_ibfk_3;
            ALTER TABLE documents_shared DROP CONSTRAINT documents_shared_ibfk_1;
            
            ALTER TABLE documents MODIFY uuid VARCHAR(96) NOT NULL;
            ALTER TABLE documents MODIFY ubl_previous_version_uuid VARCHAR(96) NULL;
            ALTER TABLE documents MODIFY pdf_embedded_uuid VARCHAR(96) NULL;
            ALTER TABLE documents_shared MODIFY uuid VARCHAR(96) NOT NULL;
            
            ALTER TABLE documents ADD CONSTRAINT documents_ibfk_3 FOREIGN KEY (pdf_embedded_uuid) REFERENCES documents(uuid) ON DELETE SET NULL ON UPDATE CASCADE;
            ALTER TABLE documents_shared ADD CONSTRAINT documents_shared_ibfk_1 FOREIGN KEY (uuid) REFERENCES documents (uuid) ON DELETE CASCADE ON UPDATE CASCADE;
        ");

        // shrink ubl id and chain id to 128
        $db->exec("
            ALTER TABLE documents DROP CONSTRAINT documents_ibfk_1;

            ALTER TABLE documents MODIFY chain_id VARCHAR(128) NOT NULL;
            ALTER TABLE documents MODIFY ubl_id VARCHAR(128) NOT NULL;
            ALTER TABLE documents_chains MODIFY id VARCHAR(128) NOT NULL;
            
            ALTER TABLE documents ADD CONSTRAINT documents_ibfk_1 FOREIGN KEY (chain_id) REFERENCES documents_chains (id) ON DELETE CASCADE ON UPDATE CASCADE;
        ");


        // shrink other columns
        $db->exec("ALTER TABLE $documents MODIFY ubl_date_document VARCHAR(32) NULL;");
        $db->exec("ALTER TABLE $documents MODIFY ubl_date_shipping_actual VARCHAR(32) NULL;");
        $db->exec("ALTER TABLE $documents MODIFY ubl_despatch_supplier_party VARCHAR(128) NULL;");
        $db->exec("ALTER TABLE $documents MODIFY ubl_delivery_customer_party VARCHAR(128) NULL;");
        $db->exec("ALTER TABLE $documents MODIFY ubl_buyer_customer_party VARCHAR(128) NULL;");
        $db->exec("ALTER TABLE $documents MODIFY ubl_seller_supplier_party VARCHAR(128) NULL;");
        $db->exec("ALTER TABLE $documents MODIFY ubl_originator_customer_party VARCHAR(128) NULL;");
        $db->exec("ALTER TABLE $documents MODIFY ubl_license_plate VARCHAR(32) NULL;");
        $db->exec("ALTER TABLE $documents MODIFY ubl_item_descriptions TEXT NULL;");
        $db->exec("ALTER TABLE $documents MODIFY file_name VARCHAR(32) NOT NULL;");
        $db->exec("ALTER TABLE $documents MODIFY note TEXT NULL;");

        // drop old customer_reference column
        $db->exec("IF EXISTS(SELECT NULL FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '". $documents . "' AND table_schema = '" . SQLCore::getDatabaseName() ."' AND column_name = '" . DBTableDocuments::_OLD_UBL_CUSTOMER_REFERENCE . "')  THEN " .
            "alter table $documents drop column " . DBTableDocuments::_OLD_UBL_CUSTOMER_REFERENCE . ";" .
        "END IF;");

        // add new ubl document references
        $db->exec("IF NOT EXISTS(SELECT NULL FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '". $documents . "' AND table_schema = '" . SQLCore::getDatabaseName() ."' AND column_name = '" . DBTableDocuments::UBL_DOCUMENT_REFERENCES . "')  THEN " .
            "alter table $documents add " . DBTableDocuments::UBL_DOCUMENT_REFERENCES . " TEXT DEFAULT NULL;" .
            "END IF;");
    }
}