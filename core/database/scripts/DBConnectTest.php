<?php

use Dtrans\core\database\abstraction\SQLCore;

// this definition turns off all checks otherwise the process would die when trying to connect to sql database
const DTRANS_SQL_STANDALONE_MODE = true;
echo "Trying to connect to database...\n";
require_once __DIR__ . '/../../../config.php';
$pdo = SQLCore::get_adapter();
if (is_null($pdo)) {
    echo "\nConnection failed. Check logs for more details!\n";
    exit(1);
} else
    echo "Done. Everything looks good!\n";