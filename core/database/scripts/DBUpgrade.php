<?php

use Dtrans\core\database\DBMain;
use Dtrans\core\database\migration\DBMigrator;
use Dtrans\core\helpers\DtransLogger;

// this definition turns off all checks otherwise the process would die when trying to connect to sql database
const DTRANS_SQL_STANDALONE_MODE = true;
require_once __DIR__ . '/../../../config.php';

const REQUIRED_DB_VERSION = DBMain::DB_VERSION_REQUIRED;

DtransLogger::notice('--------- DBUpgrade.php SCRIPT START ---------');
DBMigrator::migrate(REQUIRED_DB_VERSION);
DtransLogger::notice('--------- DBUpgrade.php SCRIPT END ---------');
