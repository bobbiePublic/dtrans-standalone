<?php

// STATIC LDAP STUFF needs to be adjusted for successful bind
// MUST BE LDAPMANAGER AS LDAPQUERY HAS ONLY READ PERMISSIONS
// MUST USE LDAPS as unencrypted connection do not password hashing for new users!
const LDAP_HOST = 'ldaps://' . 'niklasr.bobbie.de' . ':636';
const LDAP_BIND_DN = "cn=ldapmanager,ou=services,dc=niklasr,dc=bobbie,dc=de";
const LDAP_BIND_PASSWORD = "@PASSWORD_GOES_HERE@";

// leaving this to null will result in a DN generated from the BIND_DN
// or set it to a value to override the default location
const LDAP_USER_LOCATION_DN = null;

// PERMISSION LEVELS FROM LOWEST TO HIGHEST
const DTRANS_ROLE_EXTERN = 'extern';
const DTRANS_ROLE_INTERN = 'intern';

/* // these are currently not usable as permissions.ini does not handle them
const DTRANS_ROLE_BOOKKEEPER = 'bookkeeper';
const DTRANS_ROLE_ADMIN = 'admin';
*/

// USERS WHICH WILL BE ADDED
$users = [
    array(
        // REQUIRED fields
        'firstName' => 'John',
        'lastName' => 'Doe',
        'mail' => array('john.doe@bobbie.de', "john.doe@niklasr.bobbie.de", "john.doe@fantasy-land.gg"),
        'password' => 'verysecretpassword',
        // field used for permission
        'employeeType' => DTRANS_ROLE_INTERN, // use presets!!!!!

        // OPTIONAL attributes are send unchecked to server. use array for multi values
        'optional' => array(
            'o' => "Bobbie Deutschland Vertriebs GmbH", // organization
            'title' => "Superbuchhaltermann", // job type
            'mobile' => '+49 3126731', // mobile phone number
            'facsimileTelephoneNumber' => '+49 3126732', // fax number
            'telephoneNumber' => '+49 3126731', // phone number
            'street' => "Hauptstraße 4",
            'postalCode' => "52353",
            'l' => "Düren", // city
            'st' => "Nordrhein Westfalen", // subCountry/state/Bundesland
            'postalAddress' => implode("$", array("Hauptstraße 4", "52353 Düren", "Nordrhein Westfalen", "Deutschland")), // address fallback
        )
    ),
];


// ACTUAL CODE
// part one: connect to LDAP
$con = ldap_connect(LDAP_HOST);
if ($con === false) {
    echo "Failed to connect to LDAP server.\n";
    die();
}

// important do not change!
ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);
ldap_set_option($con, LDAP_OPT_REFERRALS, 0);

// try to bind
if (!ldap_bind($con, LDAP_BIND_DN, LDAP_BIND_PASSWORD)) {
    echo "Failed to bind as user on LDAP server.\n";
    die();
}

echo 'Successfully bound to ' . LDAP_HOST . ' as ' . LDAP_BIND_DN . "\n";

// generate DN for users location
$dcs = array_reverse(explode(',', LDAP_BIND_DN));
$user_dn = '';

if (empty(LDAP_USER_LOCATION_DN)) {
    foreach ($dcs as $dc) {
        $dc = trim($dc);
        if (str_starts_with($dc, 'dc=')) {
            if ($user_dn != '')
                $user_dn = $dc . ',' . $user_dn;
            else
                $user_dn = $dc;
        } else { // reached end of dc
            $user_dn = "ou=users," . $user_dn;
            break;
        }
    }
} else
    $user_dn = LDAP_USER_LOCATION_DN;

echo "Creating users in: $user_dn\n";

foreach ($users as $user) {
    if (!array_key_exists('firstName', $user)) {
        echo "Missing firstName. Skipping " . var_export($user, true) . "\”";
        continue;
    }
    if (!array_key_exists('lastName', $user)) {
        echo "Missing lastName. Skipping " . var_export($user, true) . "\”";
        continue;
    }
    if (!array_key_exists('mail', $user) || empty($user['mail'])) {
        echo "Missing mail. Skipping " . var_export($user, true) . "\”";
        continue;
    }
    if (!array_key_exists('employeeType', $user)) {
        echo "Missing employeeType. Skipping " . var_export($user, true) . "\”";
        continue;
    }
    if (!array_key_exists('password', $user)) {
        echo "Missing password. Skipping " . var_export($user, true) . "\”";
        continue;
    }


    $new_cn = array();
    $new_cn["objectClass"] = array('inetOrgPerson', 'organizationalPerson', 'person', 'top');

    $new_cn['cn'] = $cn = $user['firstName'] . ' ' . $user['lastName'];
    $new_cn['sn'] = $user['lastName'];
    $new_cn['givenName'] = $user['firstName'];
    $new_cn['uid'] = $user['lastName'] . random_int(12345, 99999);
    $new_cn['employeeType'] = $user['employeeType'];
    $new_cn['userPassword'] = $user['password'];
    $new_cn['mail'] = $user['mail'];

    if (array_key_exists('optional', $user) && !empty($user['optional']))
        foreach ($user['optional'] as $key => $value)
            $new_cn[$key] = $value;

    // for debugging
    //echo 'entry ' . var_export($new_cn, true) . "\n";

    echo "Sending $cn\n";
    $r = ldap_add($con, "cn=$cn," . $user_dn, $new_cn);
    echo "Result was " . ($r == 1 ? "positive" : "negative") . "\n";
}


// close connection and terminate
ldap_close($con);
echo "Connection closed!\n";