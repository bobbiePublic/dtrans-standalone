<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\database\abstraction\creation\SQLTableCreation;
use Dtrans\core\database\abstraction\SQLInsert;
use Dtrans\core\database\abstraction\SQLSelect;
use Dtrans\core\database\abstraction\SQLUpdate;

class DBInfo extends DBTable
{
    const TABLE_NAME = '_db_info';

    const DB_VERSION = 'version';

    public function register_symbols()
    {
        // DO NOTHING as there are no important symbols to register
    }

    public function create_initial()
    {
        $table = new SQLTableCreation(self::TABLE_NAME);
        $table->add_integer(self::DB_VERSION)->primary_key();
        $table->add_created_at();
        $table->add_updated_at();
        $table->create();

        // add row
        $initial_version = 0;
        SQLInsert::insert(self::TABLE_NAME, [self::DB_VERSION => $initial_version]);
    }

    public function set_version(int $version)
    {
        SQLUpdate::update(self::TABLE_NAME, [self::DB_VERSION => $version], null);
    }

    public function get_version(): int
    {
        $result = SQLSelect::select_one(self::TABLE_NAME, [self::DB_VERSION], null);
        return $result[self::DB_VERSION];
    }
}