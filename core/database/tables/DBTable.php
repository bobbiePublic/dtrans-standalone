<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\database\abstraction\SQLSanitizer;
use ReflectionClass;

abstract class DBTable
{
    const UPDATED_AT = 'updated_at';
    const CREATED_AT = 'created_at';
    const TIME_PRECISION = 6;
    const TIME_NOW = 'CURRENT_TIMESTAMP(' . self::TIME_PRECISION . ')';

    const DTRANS_SIGNATURE_LENGTH = 2048;

    public function __construct()
    {
        $this::register_symbols();
    }

    // abstract functionality
    public abstract function create_initial();

    public function register_symbols()
    {
        SQLSanitizer::register_symbols($this->get_rows());
    }

    protected function get_rows(): array
    {
        $qt = new ReflectionClass($this);
        $types = $qt->getConstants();
        return array_values($types);
    }
}
