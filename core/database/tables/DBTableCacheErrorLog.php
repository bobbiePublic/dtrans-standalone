<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\database\abstraction\creation\SQLTableCreation;
use Dtrans\core\database\abstraction\SQLInsert;

class DBTableCacheErrorLog extends DBTable
{
    const TABLE_NAME = 'cache_' . 'errorlog';
    const ID = 'ID';
    const MESSAGE = 'msg';

    public function create_initial()
    {
        $table = new SQLTableCreation(self::TABLE_NAME);
        $table->add_integer(self::ID)->primary_key()->auto_increment();
        $table->add_varchar(self::MESSAGE, false, null, 2048);
        $table->add_created_at();
        $table->create();

        self::add_control_entry(true);
    }

    public function add_control_entry(bool $initial = false)
    {
        $values = [
            self::ID => -1,
            self::MESSAGE => 'CONTROL MESSAGE. DO NOT ALTER OR REMOVE THIS ROW!',
        ];

        if ($initial)
            $values[self::CREATED_AT] = '1970-01-01';


        SQLInsert::insert(self::TABLE_NAME, $values);
    }
}