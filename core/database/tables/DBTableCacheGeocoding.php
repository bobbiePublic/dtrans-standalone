<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\database\abstraction\creation\SQLTableCreation;

class DBTableCacheGeocoding extends DBTable
{
    const TABLE_NAME = 'cache_' . 'geocoding';
    const QUERY = 'query';
    const RESULT_ADDRESS = 'result_address';
    const RESULT_GPS_LAT = 'result_gps_lat';
    const RESULT_GPS_LNG = 'result_gps_lng';

    public function create_initial()
    {
        $table = new SQLTableCreation(self::TABLE_NAME);
        $table->add_varchar(self::QUERY, false, null, 512)->primary_key();
        $table->add_varchar(self::RESULT_ADDRESS, true);
        $table->add_double(self::RESULT_GPS_LAT, true);
        $table->add_double(self::RESULT_GPS_LNG, true);
        $table->add_updated_at();
        $table->create();
    }
}