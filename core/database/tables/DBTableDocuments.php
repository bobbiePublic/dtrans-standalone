<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\database\abstraction\creation\SQLConsts;
use Dtrans\core\database\abstraction\creation\SQLTableCreation;

class DBTableDocuments extends DBTable
{
    const TABLE_NAME = 'documents';

    const ID = 'id';
    const UUID = 'uuid';
    const CHAIN_ID = 'chain_id';
    const FILE_NAME = 'file_name';
    const FILE_SIZE = 'file_size';
    const TYPE = 'type';
    const IN_DTRANS_QUEUE = 'in_dtrans_queue';
    const VERSION_STATUS = 'version_status';
    const NOTE = 'note';

    /* UBL fields */
    const UBL_ID = 'ubl_id';
    const UBL_PREVIOUS_VERSION_UUID = 'ubl_previous_version_uuid';
    const UBL_DOCUMENT_REFERENCES = 'ubl_document_references';
    const _OLD_UBL_CUSTOMER_REFERENCE = 'ubl_customer_reference';
    const UBL_DATE_DOCUMENT = 'ubl_date_document';
    const UBL_DATE_SHIPPING_ESTIMATED_START = 'ubl_date_shipping_estimated_start';
    const UBL_DATE_SHIPPING_ESTIMATED_END = 'ubl_date_shipping_estimated_end';
    const UBL_DATE_SHIPPING_ACTUAL = 'ubl_date_shipping_actual';
    const UBL_PARTY_DESPATCH_SUPPLIER = 'ubl_despatch_supplier_party';
    const UBL_PARTY_DELIVERY_CUSTOMER = 'ubl_delivery_customer_party';
    const UBL_PARTY_BUYER_CUSTOMER = 'ubl_buyer_customer_party';
    const UBL_PARTY_SELLER_SUPPLIER = 'ubl_seller_supplier_party';
    const UBL_PARTY_ORIGINATOR_CUSTOMER = 'ubl_originator_customer_party';
    const UBL_HAS_REJECTED_ITEMS = 'ubl_has_rejected_items';
    const UBL_LICENSE_PLATE = 'ubl_license_plate';
    const UBL_DRIVER_NAME = 'ubl_carrier_driver_name';
    const UBL_CARRIER_PARTY_MAIN = 'ubl_carrier_party_main';
    const UBL_CARRIER_PARTY_SUB = 'ubl_carrier_party_sub';
    const UBL_DELIVERY_LOCATION = 'ubl_delivery_location';
    const UBL_ALTERNATIVE_DELIVERY_LOCATION = 'ubl_alternative_delivery_location';

    const UBL_ITEM_WEIGHTS = 'ubl_item_weights';
    const UBL_ITEM_DESCRIPTIONS = 'ubl_item_descriptions';
    const PDF_EMBEDDED_ID = 'pdf_embedded_id';
    const PDF_EMBEDDED_UUID = 'pdf_embedded_uuid';

    public function create_initial()
    {
        $table = new SQLTableCreation(self::TABLE_NAME);
        $table->add_integer(self::ID)->primary_key()->auto_increment();
        $table->add_varchar_enum(self::TYPE);
        $table->add_varchar(self::UUID)->unique_key();
        $table->add_varchar(self::CHAIN_ID, true)->references(DBTableDocumentsChains::TABLE_NAME,
            DBTableDocumentsChains::CHAINS_ID, SQLConsts::CASCADE, SQLConsts::CASCADE);
        $table->add_varchar(self::FILE_NAME)->unique_key();
        $table->add_integer(self::FILE_SIZE, false, 0);
        $table->add_boolean(self::IN_DTRANS_QUEUE, false, false);
        $table->add_varchar_enum(self::VERSION_STATUS);
        $table->add_varchar(self::NOTE, true);
        $table->add_created_at();
        $table->add_updated_at();

        // pdf
        $table->add_integer(self::PDF_EMBEDDED_ID, true)->references(self::TABLE_NAME, self::ID,
            SQLConsts::SET_NULL, SQLConsts::CASCADE);
        $table->add_varchar(self::PDF_EMBEDDED_UUID, true)->references(self::TABLE_NAME, self::UUID,
            SQLConsts::SET_NULL, SQLConsts::CASCADE);

        // ubl
        $table->add_varchar(self::UBL_ID, true);
        $table->add_varchar(self::UBL_PREVIOUS_VERSION_UUID, true);
        $table->add_varchar(self::_OLD_UBL_CUSTOMER_REFERENCE, true);
        $table->add_varchar(self::UBL_DATE_DOCUMENT, true);
        $table->add_date(self::UBL_DATE_SHIPPING_ESTIMATED_START, true);
        $table->add_date(self::UBL_DATE_SHIPPING_ESTIMATED_END, true);
        $table->add_varchar(self::UBL_DATE_SHIPPING_ACTUAL, true);
        $table->add_varchar(self::UBL_PARTY_DESPATCH_SUPPLIER, true);
        $table->add_varchar(self::UBL_PARTY_DELIVERY_CUSTOMER, true);
        $table->add_varchar(self::UBL_PARTY_BUYER_CUSTOMER, true);
        $table->add_varchar(self::UBL_PARTY_SELLER_SUPPLIER, true);
        $table->add_varchar(self::UBL_PARTY_ORIGINATOR_CUSTOMER, true);
        //$table->add_varchar(self::UBL_LICENSE_PLATE, true);
        //$table->add_varchar(self::UBL_ITEM_DESCRIPTIONS, true);
        //$table->add_double(self::UBL_ITEM_WEIGHTS, true);
        $table->add_boolean(self::UBL_HAS_REJECTED_ITEMS, true);

        $table->create();
    }
}