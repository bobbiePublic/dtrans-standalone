<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\database\abstraction\creation\SQLConsts;
use Dtrans\core\database\abstraction\creation\SQLTableCreation;

class DBTableDocumentsChains extends DBTable
{
    const TABLE_NAME = 'documents' . '_chains';
    const CHAINS_ID = 'id';
    const REFERENCE_ID = 'reference_id';
    const HANDLING_STATUS = 'handling_status';
    const ARCHIVED_STATUS = 'archived_status';
    const DOCUMENT_COUNT = 'document_count';

    public function create_initial()
    {
        $table = new SQLTableCreation(self::TABLE_NAME);
        $table->add_varchar(self::CHAINS_ID)->primary_key();
        $table->add_varchar_enum(self::HANDLING_STATUS);
        $table->add_varchar_enum(self::ARCHIVED_STATUS);
        $table->add_integer(self::REFERENCE_ID, true)
            ->references(DBTableDocumentsReferences::TABLE_NAME, DBTableDocumentsReferences::ID,
                SQLConsts::SET_NULL, SQLConsts::CASCADE);
        $table->add_integer(self::DOCUMENT_COUNT);
        $table->add_created_at();
        $table->add_updated_at();
        $table->create();
    }
}