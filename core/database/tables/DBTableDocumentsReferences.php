<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\database\abstraction\creation\SQLTableCreation;

class DBTableDocumentsReferences extends DBTable
{
    const TABLE_NAME = 'documents' . '_references';
    const ID = 'id';
    const NAME = 'name';
    const SALES_ID = 'sales_id';
    const LOCATION_SOURCE = 'location_source';
    const LOCATION_ADDRESS = 'location_address';
    const LOCATION_GPS_LAT = 'location_gps_lat';
    const LOCATION_GPS_LNG = 'location_gps_lng';

    public function create_initial()
    {
        $table = new SQLTableCreation(self::TABLE_NAME);
        $table->add_integer(self::ID)->auto_increment()->primary_key();
        $table->add_varchar(self::NAME, true);
        $table->add_varchar(self::SALES_ID, true);
        $table->add_varchar(self::LOCATION_SOURCE, true);
        $table->add_varchar(self::LOCATION_ADDRESS, true);
        $table->add_double(self::LOCATION_GPS_LAT, true);
        $table->add_double(self::LOCATION_GPS_LNG, true);
        $table->add_created_at();
        $table->add_updated_at();

        $table->create();
    }
}