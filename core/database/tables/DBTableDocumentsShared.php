<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\database\abstraction\creation\SQLConsts;
use Dtrans\core\database\abstraction\creation\SQLTableCreation;

class DBTableDocumentsShared extends DBTable
{
    const TABLE_NAME = 'documents' . '_shared';

    const KEY = 'key';
    const KEY_LENGTH = 128;
    const UUID = 'uuid';
    const CREATED_BY = 'created_by';
    const CREATED_AT = 'created_at';
    const EXPIRES_AT = 'expires_at';
    const ACCESSED_AT = 'accessed_at';
    const VIEW_COUNT = 'view_count';


    public function create_initial()
    {
        $table = new SQLTableCreation(self::TABLE_NAME);
        $table->add_varchar(self::KEY, false, null, self::KEY_LENGTH)->primary_key();
        $table->add_varchar(self::UUID)->references(DBTableDocuments::TABLE_NAME, DBTableDocuments::UUID,
            SQLConsts::CASCADE, SQLConsts::CASCADE);
        $table->add_varchar(self::CREATED_BY, true);
        $table->add_created_at(self::CREATED_AT);
        $table->add_datetime(self::EXPIRES_AT, true, SQLConsts::TIME_NOW);
        $table->add_updated_at(self::ACCESSED_AT);
        $table->add_integer(self::VIEW_COUNT, false, 0);
        $table->create();
    }
}