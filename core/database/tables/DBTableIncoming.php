<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\database\abstraction\creation\SQLTableCreation;

class DBTableIncoming extends DBTable
{
    const TABLE_NAME = 'incoming';
    const ID = 'id';
    const FILE_NAME = 'file_name';
    const FORMAT = 'format';
    const HASH = 'hash';
    const HASH_ALGO = 'hash_algo';
    const ORIGIN_INTERFACE = 'origin_interface';
    const ORIGIN_IP = 'origin_ip';
    const ORIGIN_USER = 'origin_user';

    public function create_initial()
    {
        $table = new SQLTableCreation(self::TABLE_NAME);
        $table->add_integer(self::ID)->primary_key()->auto_increment();
        $table->add_varchar(self::FORMAT);
        $table->add_varchar(self::FILE_NAME, false, null, 32);
        $table->add_varchar(self::HASH, false, null, 64);
        $table->add_varchar(self::HASH_ALGO, false, 'SHA256', 8);
        $table->add_varchar_enum(self::ORIGIN_INTERFACE);
        $table->add_varchar(self::ORIGIN_IP, true, null, 64);
        $table->add_varchar(self::ORIGIN_USER, true);
        $table->add_created_at();
        $table->create();
    }
}