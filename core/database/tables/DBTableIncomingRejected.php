<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\database\abstraction\creation\SQLConsts;
use Dtrans\core\database\abstraction\creation\SQLTableCreation;

class DBTableIncomingRejected extends DBTable
{
    const TABLE_NAME = 'incoming_rejected';
    const ID = 'id';
    const CODE = 'code';
    const REASON = 'reason';
    const DUPLICATE_REF = 'duplicate_ref';

    public function create_initial()
    {
        $table = new SQLTableCreation(self::TABLE_NAME);
        $table->add_integer(self::ID)->primary_key()->references(DBTableIncoming::TABLE_NAME,
            DBTableIncoming::ID, SQLConsts::CASCADE, SQLConsts::CASCADE);
        $table->add_varchar_enum(self::CODE);
        $table->add_text(self::REASON, true, null, 2048);
        $table->add_integer(self::DUPLICATE_REF, true)->references(DBTableDocuments::TABLE_NAME,
            DBTableDocuments::ID, SQLConsts::SET_NULL, SQLConsts::CASCADE);
        $table->add_created_at();
        $table->create();
    }
}