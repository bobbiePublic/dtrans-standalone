<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\database\abstraction\creation\SQLConsts;
use Dtrans\core\database\abstraction\creation\SQLTableCreation;

class DBTableSendingDone extends DBTable
{
    const TABLE_NAME = 'sending_done';

    const ID = 'id';
    const DOCUMENT_ID = 'document_id';
    const DOCUMENT_RECEIVER = 'document_receiver';
    const RESULT_CODE = 'result_code';
    const RESULT_REASON = 'result_reason';

    public function create_initial()
    {
        $table = new SQLTableCreation(self::TABLE_NAME);
        $table->add_integer(self::ID)->auto_increment()->primary_key();
        $table->add_integer(self::DOCUMENT_ID, true)->references(DBTableDocuments::TABLE_NAME, DBTableDocuments::ID,
            SQLConsts::SET_NULL, SQLConsts::CASCADE);
        $table->add_varchar_enum(self::RESULT_CODE);
        $table->add_varchar(self::DOCUMENT_RECEIVER);
        $table->add_text(self::RESULT_REASON, true, null, self::DTRANS_SIGNATURE_LENGTH);
        $table->add_created_at();
        $table->create();
    }
}
