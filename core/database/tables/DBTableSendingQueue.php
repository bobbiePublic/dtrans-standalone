<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\database\abstraction\creation\SQLConsts;
use Dtrans\core\database\abstraction\creation\SQLTableCreation;
use Dtrans\core\enums\DtransFormatsEnum;

class DBTableSendingQueue extends DBTable
{
    const TABLE_NAME = 'sending_queue';

    const ID = 'id';
    const DOCUMENT_ID = 'document_id';
    const DOCUMENT_PATH = 'document_path';
    const DOCUMENT_FORMAT = 'document_format';
    const DESTINATION = 'destination';
    const FALLBACK_MAIL = 'fallback_mail';
    const ATTEMPT_COUNT = 'attempt_count';

    public function create_initial()
    {
        $table = new SQLTableCreation(self::TABLE_NAME);
        $table->add_integer(self::ID)->primary_key()->auto_increment();
        $table->add_integer(self::DOCUMENT_ID)->references(DBTableDocuments::TABLE_NAME, DBTableDocuments::ID,
            SQLConsts::CASCADE, SQLConsts::CASCADE);
        $table->add_varchar_enum(self::DOCUMENT_FORMAT, false, DtransFormatsEnum::UNKNOWN);
        $table->add_varchar(self::DOCUMENT_PATH);
        $table->add_varchar(self::DESTINATION);
        $table->add_varchar(self::FALLBACK_MAIL, true);
        $table->add_integer(self::ATTEMPT_COUNT, false, 0);
        $table->add_created_at();
        $table->add_updated_at();
        $table->create();
    }
}
