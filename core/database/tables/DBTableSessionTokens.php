<?php

namespace Dtrans\core\database\tables;

use Dtrans\core\constants\ConstsLogin;
use Dtrans\core\database\abstraction\creation\SQLTableCreation;

class DBTableSessionTokens extends DBTable
{
    const TABLE_NAME = 'sessions';

    const TOKEN = 'token';
    const AUTH = 'auth';
    const CREATED_AT = 'created_at';
    const EXPIRES_AT = 'expires_at';
    const LDAP_DN = 'ldap_dn';
    const USER_FIRSTNAME = 'user_first_name';
    const USER_LASTNAME = 'user_last_name';
    const USER_COMPANY_NAME = 'user_company_name';
    const USER_COMPANY_POSITION = 'user_company_position';
    const USER_PHONE = 'user_phone';
    const USER_MOBILE = 'user_mobile';
    const USER_FAX = 'user_fax';
    const USER_MAIL = 'user_mail';
    const USER_ADDRESS_STREET = 'user_address_street';
    const USER_ADDRESS_CITY = 'user_address_city';
    const USER_ADDRESS_ZIP = 'user_address_zip';
    const USER_ADDRESS_REGION = 'user_address_region';
    const USER_ADDRESS_COUNTRY = 'user_address_country';
    const USER_ADDRESS_FULL = 'user_address_full';

    public function create_initial()
    {
        $table = new SQLTableCreation(self::TABLE_NAME);
        $table->add_varchar(self::TOKEN, false, null, ConstsLogin::LOGIN_TOKEN_LENGTH)->primary_key();
        $table->add_varchar_enum(self::AUTH, false, 'LDAP');
        $table->add_varchar(self::LDAP_DN, true, null, 1024);
        $table->add_created_at();
        $table->add_updated_at(self::EXPIRES_AT);

        $default_size = 255;
        $table->add_varchar(self::USER_FIRSTNAME, true, null, $default_size);
        $table->add_varchar(self::USER_LASTNAME, true, null, $default_size);
        $table->add_varchar(self::USER_COMPANY_NAME, true, null, $default_size);
        $table->add_varchar(self::USER_COMPANY_POSITION, true, null, $default_size);
        $table->add_varchar(self::USER_PHONE, true, null, $default_size);
        $table->add_varchar(self::USER_MOBILE, true, null, $default_size);
        $table->add_varchar(self::USER_FAX, true, null, $default_size);
        $table->add_varchar(self::USER_MAIL, true, null, $default_size);
        $table->add_varchar(self::USER_ADDRESS_STREET, true, null, $default_size);
        $table->add_varchar(self::USER_ADDRESS_CITY, true, null, $default_size);
        $table->add_varchar(self::USER_ADDRESS_ZIP, true, null, $default_size);
        $table->add_varchar(self::USER_ADDRESS_REGION, true, null, $default_size);
        $table->add_varchar(self::USER_ADDRESS_COUNTRY, true, null, $default_size);
        $table->add_varchar(self::USER_ADDRESS_FULL, true, null, 2048);
        $table->create();
    }
}
