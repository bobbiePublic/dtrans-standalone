<?php

namespace Dtrans\core\enums;

abstract class ArchivedStatusEnum {
    const ACTIVE = 'active';
    const ARCHIVED = 'archived';

    // important: add every valid value to this array
    const _values = array(self::ACTIVE, self::ARCHIVED);

    public static function parse(string $object): ?string {
        $object = trim($object);
        if(in_array($object, self::_values, true))
            return $object;
        else
            return null;
    }
}