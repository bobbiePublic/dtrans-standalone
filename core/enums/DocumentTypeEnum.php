<?php

namespace Dtrans\core\enums;

abstract class DocumentTypeEnum
{
    const UBL_DESPATCH_ADVICE = 'XML_UBL_DespatchAdvice';
    const UBL_RECEIPT_ADVICE = 'XML_UBL_ReceiptAdvice';
    const EINLIEFERSCHEIN_DESPATCH_ADVICE = 'XML_1LS_DespatchAdvice';
    const EINLIEFERSCHEIN_RECEIPT_ADVICE = 'XML_1LS_ReceiptAdvice';
    const PDF = 'PDF_plain';
    const PDF_HYBRID = 'PDF_hybrid';

    // important: add every valid value to this array
    const _values = array(self::UBL_DESPATCH_ADVICE, self::UBL_RECEIPT_ADVICE, self::EINLIEFERSCHEIN_DESPATCH_ADVICE, self::EINLIEFERSCHEIN_RECEIPT_ADVICE, self::PDF, self::PDF_HYBRID);
    const _mapping = array(
        self::UBL_DESPATCH_ADVICE => DtransFormatsEnum::UBL, self::UBL_RECEIPT_ADVICE => DtransFormatsEnum::UBL,
        self::EINLIEFERSCHEIN_DESPATCH_ADVICE => DtransFormatsEnum::EINLIEFERSCHEIN, self::EINLIEFERSCHEIN_RECEIPT_ADVICE => DtransFormatsEnum::EINLIEFERSCHEIN,
        self::PDF => DtransFormatsEnum::PDF, self::PDF_HYBRID => DtransFormatsEnum::PDF
    );

    public static function parse(string $object): ?string
    {
        $object = trim($object);
        if(in_array($object, self::_values, true))
            return $object;
        else
            return null;
    }

    public static function get_mapping(string $type) : string {
        if(array_key_exists($type, self::_mapping))
            return self::_mapping[$type];
        else return DtransFormatsEnum::UNKNOWN;
    }
}