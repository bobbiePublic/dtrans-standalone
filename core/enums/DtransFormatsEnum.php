<?php

namespace Dtrans\core\enums;

abstract class DtransFormatsEnum {
    const UBL = "ubl";
    const EINLIEFERSCHEIN = "1lieferschein";
    const UNECE = "unece";
    const ZUGPFERD = "zugferd";
    const ZUGPFERD2 = "zugferd2";
    const FACTURX = "facturx";
    const XRECHNUNG = "xrechnung";
    const ORDERX = "orderx";
    const OPENTRANS = "openTRANS";
    const PDF = "pdf";
    const UNKNOWN = "unknown";

    const _values = array(self::UBL, self::EINLIEFERSCHEIN, self::UNECE, self::ZUGPFERD, self::ZUGPFERD2, self::FACTURX, self::XRECHNUNG, self::ORDERX, self::OPENTRANS, self::PDF, self::UNKNOWN);

    public static function parse(?string $object): string {
        if(empty($object)) return self::UNKNOWN;
        $object = trim($object);
        if(in_array($object, self::_values, true))
            return $object;

        $object = strtolower($object);
        foreach (self::_values as $value) {
            if(strcmp($object, $value) === 0)
                return $value;
        }

        return self::UNKNOWN;
    }

    public static function get_dns_name(?string $object): ?string
    {
        $object = self::parse($object);
        if ($object === self::UNKNOWN || !in_array($object, self::_values))
            return null;

        return $object;
    }
}