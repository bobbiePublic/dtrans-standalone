<?php

namespace Dtrans\core\enums;

abstract class DtransRoutingResult {
    const SUCCESS = "success";
    const FALLBACK_MAIL = "fallback_mail";
    const FAILED = "failed";

    const _values = array(self::SUCCESS, self::FALLBACK_MAIL, self::FAILED);

    public static function parse(?string $object): ?string {
        if(empty($object)) return null;
        $object = trim($object);
        if(in_array($object, self::_values, true))
            return $object;

        $object = strtolower($object);
        foreach (self::_values as $value) {
            if(strcmp($object, $value) === 0)
                return $value;
        }

        return null;
    }
}