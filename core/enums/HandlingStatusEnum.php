<?php

namespace Dtrans\core\enums;

abstract class HandlingStatusEnum
{
    const DA_BY_DESPATCHER_OR_SELLER = 'daByDespatcherOrSeller';
    const DA_BY_DRIVER = 'daByDriver';
    const RA_BY_DRIVER = 'raByDriver';
    const RA_BY_RECEIVER = 'raByReceiver';
    const RA_BY_DRIVER_RECEIVER = 'raByDriverReceiver';
    const UNKNOWN = 'unknown';

    // important: add every valid value to this array
    const _values = array(self::UNKNOWN, self::DA_BY_DESPATCHER_OR_SELLER, self::DA_BY_DRIVER, self::RA_BY_DRIVER, self::RA_BY_RECEIVER, self::RA_BY_DRIVER_RECEIVER);

    // simple parse function to validate a state, mainly used for user submitted parameters
    public static function parse(string $object): ?string {
        switch (trim($object)) {
            case HandlingStatusEnum::DA_BY_DESPATCHER_OR_SELLER:
                return HandlingStatusEnum::DA_BY_DESPATCHER_OR_SELLER;
            case HandlingStatusEnum::DA_BY_DRIVER:                  return HandlingStatusEnum::DA_BY_DRIVER;
            case HandlingStatusEnum::RA_BY_DRIVER:                  return HandlingStatusEnum::RA_BY_DRIVER;
            case HandlingStatusEnum::RA_BY_RECEIVER:                return HandlingStatusEnum::RA_BY_RECEIVER;
            case HandlingStatusEnum::RA_BY_DRIVER_RECEIVER:         return HandlingStatusEnum::RA_BY_DRIVER_RECEIVER;
            case HandlingStatusEnum::UNKNOWN:
                return HandlingStatusEnum::UNKNOWN;
            default:                                                return null;
        }
    }

    public static function get_order(?string $object)  : int {
        if(!is_string($object)) return -1;
        switch (trim($object)) {
            case HandlingStatusEnum::DA_BY_DESPATCHER_OR_SELLER:              return 1;
            case HandlingStatusEnum::DA_BY_DRIVER:                  return 2;
            case HandlingStatusEnum::RA_BY_RECEIVER:
            case HandlingStatusEnum::RA_BY_DRIVER:                  return 3;
            case HandlingStatusEnum::RA_BY_DRIVER_RECEIVER:         return 4;
            case HandlingStatusEnum::UNKNOWN:
            default:                                                return 0;
        }
    }

    public static function evaluate_state(?string $current, ?string $next) : string {
        $current = self::parse($current);
        $next = self::parse($next);
        if(empty($current) && empty($next)) return self::UNKNOWN;
        if(empty($current)) return $next;

        // current state matches next state
        if(strcmp($next, $current) === 0) return $next;

        // special case RA_BY_DRIVER_RECEIVER
        if ((strcmp($current, self::RA_BY_DRIVER) === 0 && strcmp($next, self::RA_BY_RECEIVER) === 0) ||
            (strcmp($current, self::RA_BY_RECEIVER) === 0 && strcmp($next, self::RA_BY_DRIVER) === 0))
            return self::RA_BY_DRIVER_RECEIVER;

        // only move forwards never backwards
        if(self::get_order($next) <= self::get_order($current))
            return $current;

        return $next;
    }
}