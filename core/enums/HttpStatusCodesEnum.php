<?php

namespace Dtrans\core\enums;

abstract class HttpStatusCodesEnum {
    // SUCCESS
    const OK = 200;
    const CREATED = 201;

    // CLIENT ERROR
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const NOT_ALLOWED = 405;
    const CONFLICT = 409;
    const TOO_MANY_REQUESTS = 429;

    // SERVER ERROR
    const INTERNAL_SERVER_ERROR = 500;
    const NOT_IMPLEMENTED = 501;
    const SERVICE_UNAVAILABLE = 503;
}