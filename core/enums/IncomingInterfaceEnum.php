<?php

namespace Dtrans\core\enums;

abstract class IncomingInterfaceEnum {
    const DTRANS = 'dtrans';
    const API = 'api';
    const FILE_PROCESSOR = 'file_processor';
    const EMBEDDED_IN_PDF = 'embedded_in_pdf';

    // important: add every valid value to this array
    const _values = array(self::DTRANS, self::API, self::FILE_PROCESSOR, self::EMBEDDED_IN_PDF);
}