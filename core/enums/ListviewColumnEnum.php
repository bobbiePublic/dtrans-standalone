<?php

namespace Dtrans\core\enums;

enum ListviewColumnEnum: int {
    // PLACEHOLDER
    case NONE = 0;

    // document meta data
    case UUID = 1;

    case CHAIN_ID = 2;
    case DOCUMENT_TYPE = 3;

    case FILE_SIZE = 4;
    case CREATED_AT = 5;
    case UPDATED_AT = 6;
    case NOTE = 7;
    case DOCUMENT_PREDECESSOR = 8;
    case DOCUMENT_PDF_EMBEDDED_UUID = 9;

    case HANDLING_STATUS = 10;
    case ARCHIVED_STATUS = 11;
    case VERSION_STATUS = 12;

    case UBL_ID = 50;
    case UBL_DATE_DOCUMENT = 51;
    case UBL_DATE_SHIPPING_ESTIMATED_START = 52;
    case UBL_DATE_SHIPPING_ESTIMATED_END = 53;
    case UBL_DATE_SHIPPING_ACTUAL = 54;
    case UBL_PARTY_DESPATCH_SUPPLIER = 60;
    case UBL_PARTY_DELIVERY_CUSTOMER = 61;
    case UBL_PARTY_BUYER_CUSTOMER = 62;
    case UBL_PARTY_SELLER_SUPPLIER = 63;
    case UBL_PARTY_ORIGINATOR_CUSTOMER = 64;
    case UBL_PARTY_CARRIER_MAIN = 65;
    case UBL_PARTY_CARRIER_SUB = 66;

    case UBL_ITEM_DESCRIPTION = 70;
    case UBL_ITEM_WEIGHTS = 71;
    case UBL_CARRIER_LICENSE_PLATE = 72;
    case UBL_CARRIER_DRIVER_NAME = 73;
    case UBL_REFERENCES = 74;
    case UBL_DELIVERY_LOCATION = 75;
    case UBL_ALTERNATIVE_DELIVERY_LOCATION = 76;
}