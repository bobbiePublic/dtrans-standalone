<?php

namespace Dtrans\core\enums;

abstract class ParameterTypesEnum {
    const UNKNOWN = 0;

    const INTEGER = 1;
    const FLOAT = 2;
    const BOOLEAN = 3;
    const UUID = 4;
    const HANDLING_STATE = 5;
    const JSON_DOCUMENT_ATTRIBUTE = 6;
    const MAIL_ADDRESS = 7;
    const ARCHIVED_STATUS = 8;
    const FILTER_STRING = 9;
    const FILTER_TIMESTAMP = 10;
    const FILTER_TIMESTAMP_INTERVAL = 11;
    const VERSION_STATUS = 12;
    const DOCUMENT_TYPE = 13;
    const CHAIN_ID = 14;
    const FILTER_DATE = 15;
    const LISTVIEW_COLUMNS = 16;

}