<?php

namespace Dtrans\core\enums;

abstract class ProcessingReturnCodesEnum {
    // class used for return codes for all document processors

    const PROCESSED = 0;
    const DUPLICATE = 1;
    const ERROR = 2;
    const NOT_SUPPORTED = 3;
}