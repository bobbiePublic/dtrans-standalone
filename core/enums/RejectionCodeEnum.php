<?php

namespace Dtrans\core\enums;

abstract class RejectionCodeEnum {
    const UNKNOWN = 'unknown';
    const DTRANS_FORMAT_UNKNOWN = 'dtrans_format_unknown';
    const DTRANS_FORMAT_UNSUPPORTED = 'dtrans_format_unsupported';
    const DUPLICATE = 'duplicate';
    const ERROR_UBL = 'error_ubl';
    const ERROR_1LIEFERSCHEIN = 'error_1lieferschein';

    // important: add every valid value to this array
    const _values = array(self::UNKNOWN, self::DTRANS_FORMAT_UNKNOWN, self::DTRANS_FORMAT_UNSUPPORTED, self::DUPLICATE, self::ERROR_UBL, self::ERROR_1LIEFERSCHEIN);
}