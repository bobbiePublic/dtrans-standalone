<?php

namespace Dtrans\core\enums;

// why does php do not support enums below 8.X?
abstract class RequestMethodsEnum {
    const INVALID = 'INVALID';

    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';

    // resolves a string to a RequestMethod
    public static function parse($s) : string {
        if(!is_string($s)) return RequestMethodsEnum::INVALID;

        switch (strtoupper(trim($s))) {
            case RequestMethodsEnum::GET:     return RequestMethodsEnum::GET;
            case RequestMethodsEnum::POST:    return RequestMethodsEnum::POST;
            case RequestMethodsEnum::PUT:     return RequestMethodsEnum::PUT;
            case RequestMethodsEnum::DELETE:  return RequestMethodsEnum::DELETE;
            default:                          return RequestMethodsEnum::INVALID;
        }
    }
}