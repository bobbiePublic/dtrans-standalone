<?php

namespace Dtrans\core\enums;

abstract class SignatureReasonCodes {
    const DA_CREATION_DESPATCHER = 'daCreationDespatcher';
    const DA_CREATION_SELLER = 'daCreationSeller';
    const DA_CREATION_DRIVER = 'daCreationDriver';
    const RA_CREATION_DRIVER = 'raCreationDriver';
    const RA_CREATION_RECEIVER = 'raCreationReceiver';
    const RA_CREATION_RECEIVER_PAPER = 'raCreationReceiverPaper';
    const RA_CREATION_DRIVER_PAPER = 'raCreationDriverPaper';

    const STATE_ASSIGNMENTS = [
        self::DA_CREATION_DESPATCHER => HandlingStatusEnum::DA_BY_DESPATCHER_OR_SELLER,
        self::DA_CREATION_SELLER => HandlingStatusEnum::DA_BY_DESPATCHER_OR_SELLER,
        self::DA_CREATION_DRIVER => HandlingStatusEnum::DA_BY_DRIVER,
        self::RA_CREATION_DRIVER => HandlingStatusEnum::RA_BY_DRIVER,
        self::RA_CREATION_RECEIVER => HandlingStatusEnum::RA_BY_RECEIVER,
        self::RA_CREATION_RECEIVER_PAPER => HandlingStatusEnum::RA_BY_RECEIVER,
        self::RA_CREATION_DRIVER_PAPER => HandlingStatusEnum::RA_BY_DRIVER,
    ];

    public static function get_handling_state(?string $rc) : string
    {
        // null code
        if(empty($rc)) return HandlingStatusEnum::UNKNOWN;

        // matching case
        if(in_array($rc, self::STATE_ASSIGNMENTS)) return self::STATE_ASSIGNMENTS[$rc];

        // compare case insensitive
        $rc = trim(strtolower($rc));
        foreach (self::STATE_ASSIGNMENTS as $code => $state) {
            if(strcmp(strtolower($code), $rc) === 0)
                return $state;
        }

        return HandlingStatusEnum::UNKNOWN;
    }
}