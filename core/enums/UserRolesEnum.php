<?php

namespace Dtrans\core\enums;

abstract class UserRolesEnum {
    const UNDEFINED = null;
    const DRIVER = 'driver';
    const RECEIVER = 'receiver';
    const DESPATCHER = 'despatcher';
    const BOOKKEEPER = 'bookkeeper';

    // important: add every valid value to this array
    const _values = array(self::UNDEFINED, self::DRIVER, self::RECEIVER, self::DESPATCHER, self::BOOKKEEPER);

    public static function parse(string $object): ?string {
        $object = trim($object);
        if(in_array($object, self::_values, true))
            return $object;
        else
            return null;
    }
}