<?php

namespace Dtrans\core\enums;

abstract class VersionStatusEnum {
    const CURRENT = 'current';
    const PREVIOUS = 'previous';

    // important: add every valid value to this array
    const _values = array(self::CURRENT, self::PREVIOUS);

    public static function parse(string $object): ?string {
        switch (trim($object)) {
            case VersionStatusEnum::CURRENT:                return VersionStatusEnum::CURRENT;
            case VersionStatusEnum::PREVIOUS:               return VersionStatusEnum::PREVIOUS;
        }

        return null;
    }
}