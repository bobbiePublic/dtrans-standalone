<?php

namespace Dtrans\core\helpers;

use Dtrans\core\enums\DocumentTypeEnum;

abstract class DocumentReader {
    public static function read_document(string $file_name, string $format, bool $already_mapped = false): ?string
    {
        if (!$already_mapped)
            $format = DocumentTypeEnum::get_mapping($format);
        $file = __DIR__ . '/../../' . PATH_DOCUMENTS . $format . '/' . $file_name;
        if(file_exists($file))
            return file_get_contents($file);
        return null;
    }
}