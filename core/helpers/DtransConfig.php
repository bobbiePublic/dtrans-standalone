<?php

namespace Dtrans\core\helpers;

abstract class DtransConfig
{
    const CFG_GENERAL = 'general';
    const CFG_DTRANS = 'dtrans';
    const CFG_API = 'api';
    const CFG_PERMISSIONS = 'permissions';

    private static array $configuration = array();

    public static function load_configs()
    {
        if (!empty(self::$configuration)) return;
        self::load_config(self::CFG_GENERAL);
        self::load_config(self::CFG_API);
        self::load_config(self::CFG_DTRANS);
        self::load_config(self::CFG_PERMISSIONS);
    }

    const CONFIG_FILE_EXTENSION = '.ini';

    private static function load_config(string $filename): void
    {
        $dir = __DIR__ . '/../../' . 'config/';
        $file = parse_ini_file($dir . $filename . self::CONFIG_FILE_EXTENSION, true, INI_SCANNER_NORMAL);
        if ($file === false) {
            DtransLogger::info('Failed to load config file', ['filename' => $filename . self::CONFIG_FILE_EXTENSION]);
            self::$configuration[$filename] = array();
            return;
        }

        self::$configuration[$filename] = $file;
    }

    // if section is set to null this property will be searched in global scope
    // internal get function used by all external get functions
    private static function _get(string $file, ?string $section, string $property) /*?string|?array*/
    {
        if (!array_key_exists($file, self::$configuration)) {
            DtransLogger::warning('Configuration file is not loaded.', ['file' => $file, 'section' => $section, 'property' => $property]);
            return null;
        }

        // file is loaded. check if section exists
        $f = self::$configuration[$file];
        if (!is_null($section))
            $f = array_key_exists($section, $f) ? $f[$section] : null;

        // check if property exists
        if (!is_null($f) && array_key_exists($property, $f))
            return $f[$property];

        DtransLogger::info('Property not found in configuration file.', ['file' => $file, 'section' => $section, 'property' => $property]);
        return null;
    }

    public static function get_array(string $file, ?string $section = null, ?array $default = null): ?array
    {
        if (!array_key_exists($file, self::$configuration)) {
            DtransLogger::warning('Configuration file is not loaded.', ['file' => $file, 'section' => $section]);
            return $default;
        }

        $f = self::$configuration[$file];
        if (is_null($section))
            return $f;
        else
            return array_key_exists($section, $f) ? $f[$section] : $default;
    }

    public static function get_string(string $file, ?string $section, string $property, ?string $default = null): ?string
    {
        $value = self::_get($file, $section, $property);
        return is_null($value) || strcmp('null', strtolower($value)) === 0 ? $default : $value;
    }

    public static function get_int(string $file, ?string $section, string $property, ?int $default = null): ?int
    {
        $value = self::_get($file, $section, $property);
        return is_numeric($value) ? (int)$value : $default;
    }

    // technically this is a float too as in PHP float/double primitives are stored in platform dependent C double type
    // this is why there is no need for a get_float function
    public static function get_double(string $file, ?string $section, string $property, ?float $default = null): ?float
    {
        $value = self::_get($file, $section, $property);
        return is_numeric($value) ? doubleval($value) : $default;
    }

    public static function get_bool(string $file, ?string $section, string $property, ?bool $default = null): ?bool
    {
        $value = self::_get($file, $section, $property);
        if (is_null($value)) return $default;
        return !(empty($value) || $value == 0 || strcmp(strtolower($value), 'no') === 0 || strcmp(strtolower($value), 'false') === 0);
    }
}