<?php

namespace Dtrans\core\helpers;

abstract class DtransDNSHelper {
    // TODO add other formats and delivery methods here
    const DNS_SRV_DTRANS_HTTPS_UBL = '_dtrans_https_ubl._tcp';

    public static function check_dtrans_dns_entries($domain) : bool {
        $res = dns_get_record(self::DNS_SRV_DTRANS_HTTPS_UBL . '.' . $domain, DNS_SRV);
        return $res !== false;
    }

    // helper function to extract domain from url
    public static function clean_url(string $url) :string {
        $url = trim($url);

        // remove protocol if present
        $protocol_pos_end = strpos($url, "://");
        if ($protocol_pos_end !== false)
            $url = substr($url, $protocol_pos_end + 3);

        // delete path from domain
        return explode('/', $url)[0];
    }

    // helper function to extract domain from mail address
    public static function clean_mail(string $mail) : ?string {
        $m = explode('@', $mail);
        if(count($m) == 2) return $m[1];
        else return null;
    }
}