<?php

namespace Dtrans\core\helpers;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\FilterHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LogLevel;

abstract class DtransLogger
{
    protected static Logger $logger;

    const LOGGING_FORMAT_LINE = "[%datetime%] (%channel%) %level_name%: %message% %context%\n"; // . '%extra\n%; // ignored because it is never used anywhere in dtrans
    const LOGGING_FORMAT_LINE_DATE = "Y-m-d\TH:i:s.uP";

    const LOG_FILE_PERMISSIONS = 0664;

    public static function _initialize(string $channel): void
    {
        // directory where log files are stored
        $log_dir = __DIR__ . '/../../' . PATH_LOGS;

        // create the logger
        self::$logger = new Logger($channel);
        $formatter = new LineFormatter(self::LOGGING_FORMAT_LINE, self::LOGGING_FORMAT_LINE_DATE);
        $useDtransLogFormat = !DtransConfig::get_bool(DtransConfig::CFG_GENERAL, 'logging', 'use_monolog_msg_format', false);
        // full logger
        $level = DtransConfig::get_bool(DtransConfig::CFG_GENERAL, 'logging', 'suppress_debug_messages', false) ? LogLevel::INFO : LogLevel::DEBUG;
        $maxFiles = DtransConfig::get_int(DtransConfig::CFG_GENERAL, 'logging', 'max_age_days', 31);
        $handler = new RotatingFileHandler($log_dir . '/full.log', $maxFiles, $level, true, self::LOG_FILE_PERMISSIONS);

        if ($useDtransLogFormat)
            $handler->setFormatter($formatter);
        self::$logger->pushHandler($handler);

        // warning logger
        $handler = new StreamHandler($log_dir . '/warning.log', LogLevel::WARNING, true, self::LOG_FILE_PERMISSIONS);
        if ($useDtransLogFormat)
            $handler->setFormatter($formatter);
        // only warning
        $handler = new FilterHandler($handler, LogLevel::WARNING, LogLevel::WARNING);
        if ($useDtransLogFormat)
            $handler->setFormatter($formatter);
        self::$logger->pushHandler($handler);


        // error logger
        $handler = new StreamHandler($log_dir . '/error.log', LogLevel::ERROR, true, self::LOG_FILE_PERMISSIONS);
        if ($useDtransLogFormat)
            $handler->setFormatter($formatter);
        self::$logger->pushHandler($handler);
    }

    public static function debug(string $message, array $extra = []): void
    {
        if(empty($message) && empty($extra)) return;
        self::$logger->debug($message, $extra);
    }

    public static function info(string $message, array $extra = []): void
    {
        if(empty($message) && empty($extra)) return;
        self::$logger->info($message, $extra);
    }

    public static function notice(string $message, array $extra = []): void
    {
        if(empty($message) && empty($extra)) return;
        self::$logger->notice($message, $extra);
    }

    public static function warning(string $message, array $extra = []): void
    {
        if(empty($message) && empty($extra)) return;
        self::$logger->warning($message, $extra);
    }

    public static function error(string $message, array $extra = []): void
    {
        if(empty($message) && empty($extra)) return;
        self::$logger->error($message, $extra);
    }

    /* used for unexpected exceptions and crashes */
    public static function critical(string $message, array $extra = []): void
    {
        if(empty($message) && empty($extra)) return;
        self::$logger->critical($message , $extra);
    }
}