<?php

namespace Dtrans\core\helpers;

use JetBrains\PhpStorm\NoReturn;

abstract class ErrorHandler
{
    protected static ?string $instance_id = null;

    public static function set_php_handlers()
    {
        if (is_null(ErrorHandler::$instance_id)) {
            self::$instance_id = Random::string(6);
            DtransLogger::_initialize(self::$instance_id);
        }

        error_reporting(E_ALL);
        set_error_handler("\Dtrans\core\helpers\ErrorHandler::php_error_handler");
        set_exception_handler("\Dtrans\core\helpers\ErrorHandler::php_exception_handler");
    }

    // php error handler used to log php messages
    public static function php_error_handler ($severity, $message, $file, $line) :bool {
        if (!DtransConfig::get_bool(DtransConfig::CFG_GENERAL, 'logging', 'suppress_php_messages', false)) {
            // unescape message
            $message = htmlspecialchars_decode($message);
            $message = $message . ' at ' . $file . ':' . $line;
            switch ($severity) {
                case E_RECOVERABLE_ERROR:
                case E_USER_ERROR:
                case E_COMPILE_ERROR:
                case E_CORE_ERROR:
                case E_PARSE:
                case E_ERROR:
                    $prefix = 'PHP Error:';
                    break;
                case E_CORE_WARNING:
                case E_COMPILE_WARNING:
                case E_USER_WARNING:
                case E_WARNING:
                    $prefix = 'PHP Warning:';
                    break;
                case E_NOTICE:
                case E_USER_NOTICE:
                default:
                    $prefix = 'PHP Notice:';
                    break;
            }

            $message = $prefix . $message;
            DtransLogger::debug($message);
        }

        // show php notices/errors in response only in non production
        return IS_PRODUCTION;
    }

    // php exception handler which is used to generate crash reports & throw internal server error
    #[NoReturn] public static function php_exception_handler($exception): void
    {
        if (!IS_PRODUCTION)
            echo "\n" . $exception . "\n";

        self::crash($exception);
    }

    // external function which can be used to crash the instance
    #[NoReturn] public static function crash($exception): void
    {
        $crash_file = date("y-m-d-H:i") . '_' . self::$instance_id . '.crash';

        // crash report generation
        $crash_reports_enabled = DtransConfig::get_bool(DtransConfig::CFG_GENERAL, 'logging', 'write_crash_reports', true);
        if ($crash_reports_enabled) {
            ErrorHandler::generate_crash_report($crash_file, $exception);
            DtransLogger::critical('Crashed: Internal server error. Crash report was generated.', array("crash_report" => $crash_file));
        } else {
            DtransLogger::critical('Crashed: Internal server error. No crash report was generated as crash reports are disabled.');
        }

        // user feedback that something went wrong
        echo "Internal server error.\n";
        if ($crash_reports_enabled)
            echo "Crash dump was generated with id '" . $crash_file . "' Please report to service administrator!\n";

        http_response_code(500);
        exit();
    }

    // internal functions which is used to generate a crash report
    private static function generate_crash_report(string $crash_file, $exception) {
        $targetFile = __DIR__ . '/../../' . PATH_LOGS . '/crash/' . $crash_file;
        $body = file_get_contents('php://input');
        file_put_contents($targetFile,
            $exception . "\n\n" .
            '$_SERVER' . "\n" . var_export($_SERVER, true) . "\n\n" .
            '$_GET' . " = " . var_export($_GET, true) . "\n\n" .
            '$_POST' . " = " . var_export($_POST, true) . "\n\n" .
            '$_REQUEST' . " = " . var_export($_REQUEST, true) . "\n\n" .
            (empty($body) ? "Request-Body is empty." : "Request-Body:\n" . $body)
        );
        chmod($targetFile, 0775);
    }
}

// register handlers
ErrorHandler::set_php_handlers();