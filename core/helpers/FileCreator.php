<?php

namespace Dtrans\core\helpers;

abstract class FileCreator
{
    const BASE_DIR = __DIR__ . '/../../';

    public static function save_file(string $data, string $type, string $dir = ''): string
    {
        $time = gmdate("Y-m-d-H:i:s");
        $targetFile = tempnam(self::BASE_DIR . PATH_DOCUMENTS . $type . '/' . $dir, $time);
        file_put_contents($targetFile, $data);
        chmod($targetFile, 0755);
        return basename($targetFile);
    }

    public static function get_file_size(string $type, string $name): int
    {
        $targetFile = self::BASE_DIR . PATH_DOCUMENTS . $type . '/' . $name;
        $size = filesize($targetFile);
        return $size === false ? -1 : $size;
    }

    public static function delete_file(string $type, string $name): bool
    {
        $targetFile = self::BASE_DIR . PATH_DOCUMENTS . $type . '/' . $name;
        $size = filesize($targetFile);

        if ($size === false)
            return false;

        return unlink($targetFile);
    }
}