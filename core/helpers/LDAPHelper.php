<?php

namespace Dtrans\core\helpers;

use Dtrans\core\enums\HttpStatusCodesEnum;
use JetBrains\PhpStorm\NoReturn;

abstract class LDAPHelper
{

    /* helper function to get basic connection context */
    public static function get_host()
    {
        if (!DtransConfig::get_bool(DtransConfig::CFG_API, 'auth', 'enable', true))
            self::ldap_service_unavailable(false);

        $con = ldap_connect(DtransConfig::get_string(DtransConfig::CFG_API, 'ldap', 'host'));
        if ($con === false)
            self::ldap_service_unavailable();

        ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($con, LDAP_OPT_REFERRALS, 0);

        return $con;
    }

    /* helper function to get connection context as manager */
    public static function get_host_manager()
    {
        $lc = self::get_host();

        $manager_dn = DtransConfig::get_string(DtransConfig::CFG_API, 'ldap', 'bind_dn');
        $manager_pass = DtransConfig::get_string(DtransConfig::CFG_API, 'ldap', 'bind_password');

        if (!ldap_bind($lc, $manager_dn, $manager_pass))
            self::ldap_service_unavailable();

        // success
        return $lc;
    }

    /* helper function to map the scope to the corresponding php function */
    public static function search($lc, array $attributes, string $search_base, string $filter, string $scope)
    {
        $scope = strtolower(trim($scope));

        switch ($scope) {
            case 'base':
                return ldap_read($lc, $search_base, $filter, $attributes);
            case 'one':
                return ldap_list($lc, $search_base, $filter, $attributes);
            case 'sub':
            default:
                if (strcmp($scope, 'sub') !== 0)
                    DtransLogger::warning('Invalid scope used in ldap search. Using sub as fallback.', ['scope' => $scope, 'filter' => $filter, 'base' => $search_base]);
                return ldap_search($lc, $search_base, $filter, $attributes);
        }
    }

    #[NoReturn] private static function ldap_service_unavailable(bool $error = true): void
    {
        if ($error) {
            echo "Authentication service is not available. Please contact service administrator.\n";
            DtransLogger::critical("LDAP initial bind failed. Are given credentials correct and is LDAP service up and running? Right now LDAP login via API is not available.");
        } else {
            echo "Authentication is not available right now. Please try again later\n";
            DtransLogger::notice("LDAP Login is disabled in configuration. Login attempt was rejected.");
        }

        http_response_code(HttpStatusCodesEnum::SERVICE_UNAVAILABLE);
        exit();
    }
}