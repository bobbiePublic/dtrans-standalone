<?php

namespace Dtrans\core\helpers;

abstract class PathResolver
{
    public static function resolve(): ?string
    {
        $parsedUrl = parse_url($_SERVER['REQUEST_URI']);
        $path = trim($parsedUrl['path']) ?? null;

        // abort if path is faulty
        if (empty($path) || !is_string($path) || !str_starts_with($path, API_PREFIX))
            return null;

        // replace api prefix (only first occurrence)
        $pos = strpos($path, API_PREFIX);
        if ($pos !== false)
            $path = substr_replace($path, '', $pos, strlen(API_PREFIX));

        // remove optional slash at end of request url
        if (str_ends_with($path, '/'))
            $path = rtrim($path, '/');

        return $path;
    }
}