<?php

namespace Dtrans\core\helpers;

use Random\Engine\Secure;

abstract class Random {
    public static function string(int $length = 16, string $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') :string {
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++)
            $randomString .= $characters[rand(0, $charactersLength - 1)];

        return $randomString;
    }

    private static ?Secure $engine = null;

    public static function string_cryptographic(int $length): string
    {
        if (is_null(self::$engine))
            self::$engine = new Secure();

        // explanation: BASE64 = 6 bit/character while generate returns 8bit * INT_SIZE which is currently 64 bit
        // so we need to make sure the generated string is equal or longer than the required length
        $integerSizeBits = PHP_INT_SIZE * 8;
        $stringLengthBits = $length * 6;
        $runs = (int)ceil($stringLengthBits / $integerSizeBits);

        $string = '';
        for ($i = 0; $i < $runs; $i++)
            $string .= self::$engine->generate();

        $string = base64_encode($string);
        return substr($string, 0, $length);
    }
}