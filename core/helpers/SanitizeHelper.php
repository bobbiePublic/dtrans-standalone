<?php

namespace Dtrans\core\helpers;

use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\sanitization\SanitizeBoolean;
use Dtrans\core\sanitization\SanitizeEMail;
use Dtrans\core\sanitization\SanitizeEnumItem;
use Dtrans\core\sanitization\SanitizeFilterDate;
use Dtrans\core\sanitization\SanitizeFilterString;
use Dtrans\core\sanitization\SanitizeFilterTimestamp;
use Dtrans\core\sanitization\SanitizeFilterTimestampInterval;
use Dtrans\core\sanitization\SanitizeFloat;
use Dtrans\core\sanitization\SanitizeInteger;
use Dtrans\core\sanitization\SanitizeJsonDocumentAttribute;
use Dtrans\core\sanitization\SanitizeListviewColumns;
use Dtrans\core\sanitization\SanitizeProxy;
use Dtrans\core\sanitization\SanitizeStringWhitespaces;

abstract class SanitizeHelper
{
    private static ?array $sanitizers = null;

    public static function register_proxy($type, SanitizeProxy $proxy)
    {
        self::$sanitizers[$type] = $proxy;
    }

    private static function init_proxies()
    {
        self::$sanitizers = array();

        /* primitives */
        new SanitizeInteger();
        new SanitizeFloat();
        new SanitizeBoolean();
        new SanitizeEMail();
        new SanitizeFilterString();
        new SanitizeFilterTimestamp();
        new SanitizeFilterTimestampInterval();
        new SanitizeFilterDate();
        new SanitizeJsonDocumentAttribute();
        new SanitizeListviewColumns();

        /* enum */
        new SanitizeEnumItem(ParameterTypesEnum::HANDLING_STATE, ['Dtrans\core\enums\HandlingStatusEnum', 'parse']);
        new SanitizeEnumItem(ParameterTypesEnum::ARCHIVED_STATUS, ['Dtrans\core\enums\ArchivedStatusEnum', 'parse']);
        new SanitizeEnumItem(ParameterTypesEnum::VERSION_STATUS, ['Dtrans\core\enums\VersionStatusEnum', 'parse']);
        new SanitizeEnumItem(ParameterTypesEnum::DOCUMENT_TYPE, ['Dtrans\core\enums\DocumentTypeEnum', 'parse']);

        /* none */
        new SanitizeStringWhitespaces(ParameterTypesEnum::CHAIN_ID);
        new SanitizeStringWhitespaces(ParameterTypesEnum::UUID);
    }

    public static function sanitize($object, $default, $type, ?string $parameter_name = null)
    {
        if (empty(self::$sanitizers))
            self::init_proxies();

        // null types and arrays are not allowed
        if (is_null($object) || is_array($object)) return $default;

        if (array_key_exists($type, self::$sanitizers))
            return self::$sanitizers[$type]->sanitize($object, $default, $parameter_name);

        // this should never happen
        DtransLogger::warning("No sanitizer exists for parameter type.", ["type" => $type]);
        if (!is_null($parameter_name))
            UserFeedback::warning(ConstsStrings::CODE_API_PARAMETER_FAULTY_AND_IGNORED, $parameter_name);
        return $default;
    }
}