<?php

namespace Dtrans\core\helpers;

use Dtrans\core\database\abstraction\SQLExpression;

abstract class TimestampHelper
{
    public static function query_timestamp_as_iso8601(string $column): SQLExpression
    {
        return new SQLExpression("DATE_FORMAT(" . $column . ", \"%Y-%m-%dT%TZ\")");
    }

    public static function create_filter_timestamp(string $timestamp)
    {
        if (str_contains($timestamp, '#')) { // timespan mode
            $timespan = explode('#', $timestamp);
            if (count($timespan) != 2)
                return null; // something is invalid with the given timespan

            $from = self::create_single_utc(trim($timespan[0]), true);
            $to = self::create_single_utc(trim($timespan[1]), false);
        } else {
            // add some offset in both direction as the
            $from = self::create_single_utc(trim($timestamp), true, -1);
            $to = self::create_single_utc(trim($timestamp), false, +1);
        }

        if (is_null($from) || is_null($to))
            return null; // something wrong with one of the borders

        return strcmp($from, $to) === 0 ? $from : array($from, $to);
    }

    public static function create_single_utc(?string $timestamp, bool $lower = false, int $offset = 0): ?string
    {
        if (is_null($timestamp)) return null;

        $unix = $timestamp !== '' ? strtotime($timestamp) : ($lower ? 0 : time());
        $unix += $offset;
        if ($unix < 0)
            $unix = 0;
        return date_format(date_create('@' . $unix), 'c');
    }
}