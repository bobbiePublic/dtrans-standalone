<?php

namespace Dtrans\core\helpers;

use Dtrans\core\constants\ConstsStrings;

abstract class UserFeedback {
    private static array $errors = array();
    private static array $warnings = array();
    private static array $notices = array();

    public static function notice(int $code, ...$args) {
        if(array_key_exists($code, ConstsStrings::_codes)) {
            $message = ConstsStrings::format_code($code, $args);
            self::$notices[] = array($code, $message);
            DtransLogger::debug("user-notice '" . $code . "': '" . $message . "'");
        } else
            DtransLogger::warning(__FILE__ . ": Usage of unknown code '" . $code . "'");
    }

    public static function warning(int $code, ...$args) {
        if(array_key_exists($code, ConstsStrings::_codes)) {
            $message = ConstsStrings::format_code($code, $args);
            self::$warnings[] = array($code, $message);
            DtransLogger::debug("user-warning '" . $code . "': '" . $message . "'");
        } else
            DtransLogger::warning(__FILE__ . ": Usage of unknown code '" . $code . "'");
    }

    public static function error(int $code, ...$args) {
        if(array_key_exists($code, ConstsStrings::_codes)) {
            $message = ConstsStrings::format_code($code, $args);
            self::$errors[] = array($code, $message);
            DtransLogger::debug("user-error '" . $code . "': '" . $message . "'");
        } else
            DtransLogger::warning(__FILE__ . ": Usage of unknown code '" . $code . "'");
    }

    public static function notice_custom(string $msg, ...$args) {
        $msg = ConstsStrings::format_string($msg, $args);
        self::$notices[] = array(ConstsStrings::CODE_CUSTOM_MESSAGE, $msg);
        DtransLogger::debug("user-custom_notice '" . $msg . "'");
    }

    public static function warning_custom(string $msg, ...$args) {
        $msg = ConstsStrings::format_string($msg, $args);
        self::$warnings[] = array(ConstsStrings::CODE_CUSTOM_MESSAGE, $msg);
        DtransLogger::debug("user-custom_warning '" . $msg . "'");
    }

    public static function error_custom(string $msg, ...$args) {
        $msg = ConstsStrings::format_string($msg, $args);
        self::$errors[] = array(ConstsStrings::CODE_CUSTOM_MESSAGE, $msg);
        DtransLogger::debug("user-custom_error '" . $msg . "'");
    }

    public static function has_data() :bool {
        return !(empty(self::$errors) && empty(self::$warnings) && empty(self::$notices));
    }

    public static function flush() :?array {
        if(!self::has_data())
            return null;

        return array(
            'errors' => self::get_array(self::$errors),
            'warnings' => self::get_array(self::$warnings),
            'notices' => self::get_array(self::$notices),
        );
    }

    private static function get_array(array& $arr) :array {
        if(empty($arr)) return array();

        $array = array();
        foreach ($arr as $item) {
            $array['code'] = $item[0];
            $array['message'] = $item[1];
        }

        // flush array
        $arr = array();
        return $array;
    }
}