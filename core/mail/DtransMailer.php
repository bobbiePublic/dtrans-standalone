<?php

namespace Dtrans\core\mail;

use Dtrans\core\database\models\ModelDocumentsShared;
use Dtrans\core\helpers\DtransConfig;
use Dtrans\core\helpers\DtransLogger;
use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

abstract class DtransMailer
{

    public static function debug_mail_test($sql)
    {

        $data = [
            'ID' => 'dtrans://bobbie.de/1lieferschein/1200002376-1-0',
            'UUID' => '0fc3de93-7d12-4a40-8174-5ff877a8f774',
            'Type' => "1Lieferschein-ReceiptAdvice",
            'Zeitstempel (Dokument)' => "2023-03-21",
            'Zeitstempel (System)' => "2023-05-22 10:09:11.982651",
        ];

        DtransMailer::send_dtrans_fallback_mail($data, 'nikolas@bobbie.de', 'Nikolas Schöller');
        DtransMailer::send_dtrans_fallback_mail($data, 'night@bobbie.de', 'Night');
        DtransMailer::send_dtrans_fallback_mail($data, 'niklasr@bobbie.de', 'Niklas Reitz');
        DtransMailer::send_dtrans_fallback_mail($data, 'pascal@bobbie.de', 'Pascal');
    }

    public static function send_dtrans_fallback_mail(array $data, string $receiver_mail, string $receiver_name = '')
    {
        $reference = 'DTRANS fallback mail for ' . $receiver_mail;
        $uuid = $data["UUID"];
        $key = ModelDocumentsShared::create_document_key($uuid, $reference);
        if (empty($key)) {
            DtransLogger::debug($key);
            DtransLogger::error('Failed to create document share token for fallback mail. Sending fallback mail failed.', ['uuid' => $uuid, 'receiver_mail' => $receiver_mail, 'data' => $data]);
            return;
        }

        $subject = "1Lieferschein Systembenachrichtigung: Es liegt ein neues Dokument für Sie vor.";
        $bye_line = "Das 1Lieferschein System";
        $html_start = "<!DOCTYPE html>";
        $html_start .= '<head><title>1Lieferschein Systembenachrichtigung</title><style>';
        $html_start .= "* {font-family: 'Roboto';}";
        $html_start .= 'td:nth-child(1) {text-align: right;}';
        $html_start .= '</style></head><body>';
        $html_end = '</body></html>';

        // greeting
        $body_text = "[Benutze eine vereinfachte Ansicht, weil die HTML Darstellung bei Ihnen deaktiviert ist]" . "\n\n\n";
        $greeting = empty($receiver_name) ? "Sehr geehrte Damen und Herren," : "Sehr geehrte Frau/sehr geehrter Herr " . $receiver_name . ",";
        $body_text .= $greeting . "\n\n";
        $body_html = $html_start . '<p>' . $greeting . '</p>';

        // info
        $frontend_uri = DtransConfig::get_string(DtransConfig::CFG_DTRANS, "general", "frontend_uri");
        $link = $frontend_uri . '/shared' . '?doc=' . $key;
        $info = "es ist ein neues Dokument für Sie verfügbar. Dieses können Sie abrufen unter: ";
        $body_text .= $info . $link . "\n\n" . 'Weitere Informationen zur Einordnung:' . "\n";
        $body_html .= '<p>' . $info . "<a href=\"$link\">" . "1Lieferschein Dokument" . "</a>" . "<br>" . 'Weitere Informationen zur Einordnung:' . '</p>';

        $body_html .= '<table><thead></thead><tbody>';
        foreach ($data as $key => $value) {
            $body_text .= $key . ": " . $value . "\n";
            $body_html .= '<tr><td>' . $key . ":</td><td> " . $value . "</td></tr>";
        }

        $body_html .= '</tbody></table><p></p>';


        // bye
        $body_text .= "\n" . 'Mit freundlichen Grüßen' . "\n" . $bye_line . "\n\n\n";
        $body_html .= '<p>' . 'Mit freundlichen Grüßen' . '<br>' . $bye_line . '</p>';

        $contact_mail = DtransConfig::get_string(DtransConfig::CFG_DTRANS, 'owner', 'contact_mail');
        $disclaimer = array(
            "Diese Aktion wurde automatisch durchgeführt. Bitte anworten Sie nicht auf diese E-Mail.",
            'Wenn Sie glauben, dass es sich hierbei um einen Fehler handelt nehmen Sie bitte umgehend Kontakt mit uns auf.',
            empty($contact_mail) ? '' : ('Kontaktadresse: ' . $contact_mail),
            '',
            '1Lieferschein ist ein Produkt zum digitalisieren und automatisieren von Lieferketten.',
            'Für weitere Informationen besuchen Sie: ' . 'https://1lieferschein.com',
        );

        $body_html .= '<hr><div>';
        foreach ($disclaimer as $d) {
            $body_text .= $d . "\n";
            $body_html .= $d . '<br>';
        }
        $body_html .= '</div>';

        $body_html .= $html_end;

        self::send_mail($receiver_mail, $receiver_name, $subject, $body_html, $body_text);
    }

    public static function send_mail(string $receiver_mail, string $receiver_name, string $subject, string $body_html, string $body_plain, string $attachment_content = null, string $attachment_name = null): bool
    {
        if (!self::is_mail_allowed($receiver_mail)) {
            DtransLogger::info("Mail was not send, because receiver is blacklisted.", ['mail' => $receiver_mail, 'subject' => $subject]);
            return true;
        }

        $mail = new PHPMailer(true);

        try {
            // activate debug output
            if (!DtransConfig::get_bool(DtransConfig::CFG_GENERAL, 'logging', 'suppress_debug_messages', false))
                $mail->SMTPDebug = SMTP::DEBUG_OFF;

            $mail->isSMTP();
            $mail->Host = DtransConfig::get_string(DtransConfig::CFG_DTRANS, 'smtp', 'host', '');
            $mail->Port = DtransConfig::get_string(DtransConfig::CFG_DTRANS, 'smtp', 'port', '');

            $auth_user = DtransConfig::get_string(DtransConfig::CFG_DTRANS, 'smtp', 'user', '');
            $auth_pass = DtransConfig::get_string(DtransConfig::CFG_DTRANS, 'smtp', 'password', '');
            if (!empty($auth_user) && !empty($auth_pass)) {
                $mail->SMTPAuth = true;
                $mail->Username = $auth_user;
                $mail->Password = $auth_pass;

                // enable implicit TLS encryption if required
                $start_tls = DtransConfig::get_bool(DtransConfig::CFG_DTRANS, 'smtp', 'start_tls', false);
                $mail->SMTPSecure = $start_tls ? PHPMailer::ENCRYPTION_STARTTLS : PHPMailer::ENCRYPTION_SMTPS;
            }

            // add receivers
            $sender_mail = DtransConfig::get_string(DtransConfig::CFG_DTRANS, 'smtp', 'sender_mail', '');
            $sender_name = DtransConfig::get_string(DtransConfig::CFG_DTRANS, 'smtp', 'sender_name', '');
            $mail->setFrom($sender_mail, $sender_name);
            if (!$mail->addAddress($receiver_mail, $receiver_name)) {
                DtransLogger::error("Failed to send mail to invalid address.", ['receiver_mail' => $receiver_mail, 'receiver_name' => $receiver_name, 'subject' => $subject]);
                return false;
            }

            $outgoing_mailbox_bcc = DtransConfig::get_string(DtransConfig::CFG_DTRANS, 'smtp', 'bcc_mail');
            if (!empty($outgoing_mailbox_bcc))
                if (!$mail->addBCC($outgoing_mailbox_bcc))
                    DtransLogger::warning("Mailbox for outgoing mails is active, but the given mail address is invalid", ['mail' => $outgoing_mailbox_bcc]);

            // add attachments
            if (!empty($attachment_content) && !empty($attachment_name)) {
                $mail->addStringAttachment($attachment_content, $attachment_name);
            }
            // add content
            $mail->isHTML();
            $mail->CharSet = $mail::CHARSET_UTF8;
            $mail->Subject = $subject;
            $mail->Body = $body_html;
            $mail->AltBody = $body_plain;
            $mail->send();
            DtransLogger::info("Mail has been sent.", ['receiver_mail' => $receiver_mail, 'receiver_name' => $receiver_name, 'subject' => $subject]);
            return true;
        } catch (Exception $e) {
            DtransLogger::error("Failed to send mail. Exception was thrown.", ['receiver_mail' => $receiver_mail, 'receiver_name' => $receiver_name, 'subject' => $subject, 'error' => $mail->ErrorInfo, 'exception' => $e]);
            return false;
        }
    }

    const MAIL_BLACKLIST_LOCATION = __DIR__ . '/../../config/';

    public static function is_mail_allowed(string $mail): bool
    {
        $mail_lower = strtolower($mail);
        $mail_blacklist = DtransConfig::get_string(DtransConfig::CFG_DTRANS, 'general', 'file_mail_blacklist');

        // load list
        if (empty($mail_blacklist) || !file_exists(self::MAIL_BLACKLIST_LOCATION . $mail_blacklist)) {
            DtransLogger::warning('Mail blacklist file could not be loaded.', ['file' => $mail_blacklist]);
            return true;
        }

        if ($file = fopen(self::MAIL_BLACKLIST_LOCATION . $mail_blacklist, "r")) {
            while (!feof($file)) {
                $line = fgets($file);
                if (str_starts_with($line, '#'))
                    continue; // ignore comments
                if (str_starts_with($line, '=')) {
                    $expression = substr($line, 1, strlen($line) - 1);
                    if (preg_match($expression, $mail) !== 0)
                        return false;
                } else { // exact match
                    $match = trim(strtolower($line));
                    if (strcmp($match, $mail_lower) === 0)
                        return false;
                }
            }

            fclose($file);
        }

        return true;
    }
}
