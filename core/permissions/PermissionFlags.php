<?php

namespace Dtrans\core\permissions;

abstract class PermissionFlags
{
    const SECTION_DOCUMENTS_QUERY = 'documents_query.';
    const SECTION_DOCUMENTS_MODIFY = 'documents_modify.';
    const SECTION_DOCUMENTS_SHARE = 'documents_share.';
    const SECTION_OVERRIDES = 'overrides.';
    const SECTION_AUTH = 'auth.';

    // FLAG FOR using documents/list and documents/chain
    const ACCESS_METADATA_DOCUMENTS = self::SECTION_DOCUMENTS_QUERY . 'access_metadata_docs';
    // FLAG FOR retrieving documents from documents/get
    const ACCESS_CONTENT_DOCUMENTS = self::SECTION_DOCUMENTS_QUERY . 'access_content_docs';
    // FLAG FOR listing archived documents in documents/list and documents/chain
    const ACCESS_ARCHIVED_DOCUMENTS = self::SECTION_DOCUMENTS_QUERY . 'access_archived_docs';
    // FLAG FOR sending new documents over api to documents/new
    const SUBMIT_DOCUMENTS = self::SECTION_DOCUMENTS_MODIFY . 'submit_docs';
    // FLAG FOR archiving/unarchiving document chains
    const MODIFY_STATUS_DOCUMENTS = self::SECTION_DOCUMENTS_MODIFY . 'modify_status_docs';
    // FLAG TO GENERATE SHARE LINK
    const SHARE_DOCUMENTS = self::SECTION_DOCUMENTS_SHARE . 'share_docs';
    // FLAG TO REVOKE SHARING LINKS
    const UNSHARE_DOCUMENTS = self::SECTION_DOCUMENTS_SHARE . 'unshare_docs';

    const AUTH_USE_APP_DRIVER = self::SECTION_AUTH . 'use_app_as_driver';
    const AUTH_USE_APP_RECEIVER = self::SECTION_AUTH . 'use_app_as_receiver';
    const AUTH_USE_API = self::SECTION_AUTH . 'use_api';

    const IS_ADMIN = self::SECTION_OVERRIDES . 'is_admin';
}