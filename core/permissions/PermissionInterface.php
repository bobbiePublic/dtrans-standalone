<?php

namespace Dtrans\core\permissions;

use Dtrans\core\helpers\DtransConfig;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\LDAPHelper;

abstract class PermissionInterface
{
    public static function query_permission_flag(string $flag, ?string $username, ?string $ldap_dn): bool
    {
        $path = explode('.', $flag);

        if (count($path) == 2) {
            $flag_section = $path[0];
            $flag_name = $path[1];
        } else if (count($path) == 1) {
            $flag_section = null;
            $flag_name = $path[0];
        } else {
            DtransLogger::warning("Invalid permission flag queried.", ['flag' => $flag, 'user' => $username, 'ldap_dn' => $ldap_dn]);
            return false;
        }

        $filter = DtransConfig::get_string(DtransConfig::CFG_PERMISSIONS, $flag_section, $flag_name . '_filter');
        $scope = DtransConfig::get_string(DtransConfig::CFG_PERMISSIONS, $flag_section, $flag_name . '_scope');
        $base = DtransConfig::get_string(DtransConfig::CFG_PERMISSIONS, $flag_section, $flag_name . '_base');
        $match = DtransConfig::get_bool(DtransConfig::CFG_PERMISSIONS, $flag_section, $flag_name . '_match', false);
        if (empty($base))
            $base = DtransConfig::get_string(DtransConfig::CFG_API, 'ldap', 'search_base');
        if (empty($scope))
            $scope = DtransConfig::get_string(DtransConfig::CFG_API, 'ldap', 'search_scope');

        if (is_null($filter)) {
            DtransLogger::warning("Permission flag not found in file.", ['flag' => $flag, 'user' => $username, 'ldap_dn' => $ldap_dn]);
            return false;
        }

        // check for hard value
        $bool = self::filter_is_boolean($filter);
        if (!is_null($bool))
            return $bool;

        // escape variables and fill in. abort if variable used is not available
        if (!self::replace_placeholders($filter, $username, $ldap_dn))
            return false;
        if (!self::replace_placeholders($base, $username, $ldap_dn))
            return false;

        $lc = LDAPHelper::get_host_manager();
        if (!$lc) {
            DtransLogger::critical("Permission flag could not be checked as LDAP service is unavailable.", ['flag' => $flag, 'user' => $username, 'ldap_dn' => $ldap_dn]);
            return false;
        }

        $r = LDAPHelper::search($lc, ['dn'], $base, $filter, $scope);
        if ($r === false) {
            DtransLogger::warning("Permission flag query in LDAP service returned error flag. Maybe the query in permission file is invalid?", ['query' => $filter, 'flag' => $flag, 'user' => $username, 'ldap_dn' => $ldap_dn]);
            return false;
        }

        $entries = ldap_get_entries($lc, $r);
        if ($entries['count'] == 0) // failed
            return false;

        if ($match && (empty($ldap_dn) || !self::check_if_dn_in_result_set($entries, $ldap_dn)))
            return false;

        // success
        return true;
    }

    private static function replace_placeholders(string &$filter, ?string $username, ?string $ldap_dn): bool
    {
        if (str_contains($filter, '%user')) {
            if (is_null($username))
                return false;
            else {
                $username = ldap_escape($username, '', LDAP_ESCAPE_FILTER);
                $filter = str_replace('%user', $username, $filter);
            }
        }
        if (str_contains($filter, '%dn')) {
            if (is_null($ldap_dn))
                return false;
            else {
                $filter = str_replace('%dn', $ldap_dn, $filter);
            }
        }

        return true;
    }

    private static function check_if_dn_in_result_set(array $entries, string $dn): bool
    {
        $count = $entries['count'];
        for ($i = 0; $i < $count; $i++) {
            $result = $entries[$i];
            if (empty($result) || empty($result['dn']))
                continue;

            // found dn in query result
            if (strcmp($dn, $result['dn']) === 0)
                return true;
        }

        // failed to find dn in query result
        return false;
    }

    private static function filter_is_boolean(/*string|int|bool*/ $filter): ?bool
    {
        if ($filter === false || $filter === 0 || $filter === '' || strcmp(strtolower($filter), 'no') === 0 || strcmp(strtolower($filter), 'false') === 0)
            return false;
        if ($filter === true || $filter == 1 || strcmp(strtolower($filter), 'yes') === 0 || strcmp(strtolower($filter), 'true') === 0)
            return true;

        return null;
    }
}