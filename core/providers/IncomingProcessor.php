<?php

namespace Dtrans\core\providers;

use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\models\ModelIncoming;
use Dtrans\core\database\models\ModelRejected;
use Dtrans\core\enums\DtransFormatsEnum;
use Dtrans\core\enums\RejectionCodeEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\FileCreator;

abstract class IncomingProcessor
{
    // returns the saved filename if document is unique with the given format
    public static function create_file(string $data, string $origin_format, string $origin_interface, ?string $origin_user = null, ?string $reject_code = null, ?string $reject_reason = null): ?string
    {
        $duplicate = false;
        // validate format
        $format = DtransFormatsEnum::parse($origin_format);
        $format_unknown = strcmp($format, DtransFormatsEnum::UNKNOWN) === 0;

        // generate hash
        $hash = hash('SHA256', $data);
        $file_name = ModelIncoming::get_file_name_by_hash($hash, $format);

        // document is new so we need to save it
        if(empty($file_name))
            $file_name = FileCreator::save_file($data, $format);
        else
            $duplicate = true;

        $incoming_id = ModelIncoming::create_entry($file_name, $hash, $format, $origin_interface, self::get_user_ip(), $origin_user);
        if($format_unknown) {
            $reason = "The format '" . $origin_format . "' is not supported.";
            ModelRejected::create_entry($incoming_id, RejectionCodeEnum::DTRANS_FORMAT_UNKNOWN, $reason);
            $duplicate = true;
        } else if($duplicate) {
            $original_id = ModelDocuments::get_document_id_by_filename($file_name);
            $reason = "Hash matches with file " . $file_name;
            if(is_null($original_id))
                $reason .= ' but no reference document was found. Maybe the file contains errors and can not be processed.';

            ModelRejected::create_entry($incoming_id, RejectionCodeEnum::DUPLICATE, $reason, $original_id);
        } else if (!is_null($reject_code)) {
            ModelRejected::create_entry($incoming_id, $reject_code, $reject_reason);
            $duplicate = true;
        }

        return $duplicate ? null : $file_name;
    }

    public static function get_duplicate(string $data, string $origin_format): ?string
    {
        $format = DtransFormatsEnum::parse($origin_format);
        if (strcmp($format, DtransFormatsEnum::UNKNOWN) === 0) {
            DtransLogger::error('IncomingProcessor->is_duplicate() called with invalid origin_format!', ['origin_format' => $origin_format]);
            return null;
        }

        // generate hash
        $hash = hash('SHA256', $data);
        $file_name = ModelIncoming::get_file_name_by_hash($hash, $format);

        // document is new so we need to save it
        return empty($file_name) ? null : $file_name;
    }

    private static function get_user_ip(): ?string
    {
        // get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }

        $client = $_SERVER['HTTP_CLIENT_IP'] ?? null;
        $forward = $_SERVER['HTTP_X_FORWARDED_FOR'] ?? null;
        $remote  = $_SERVER['REMOTE_ADDR'] ?? null;

        if(filter_var($client, FILTER_VALIDATE_IP))
            return $client;
        else if(filter_var($forward, FILTER_VALIDATE_IP))
            return $forward;
        else
            return $remote;
    }

}