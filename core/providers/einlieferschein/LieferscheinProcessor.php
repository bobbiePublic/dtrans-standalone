<?php

namespace Dtrans\core\providers\einlieferschein;

use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\models\ModelDocumentsInsert;
use Dtrans\core\enums\DocumentTypeEnum;
use Dtrans\core\enums\DtransFormatsEnum;
use Dtrans\core\enums\ProcessingReturnCodesEnum;
use Dtrans\core\helpers\FileCreator;
use Dtrans\core\providers\ubl\extractors\DeliveryLocation;
use Dtrans\core\providers\ubl\extractors\DtransAddress;
use Dtrans\core\providers\ubl\extractors\PartyInfo;
use Dtrans\core\providers\ubl\UBLDataExtractor;
use Dtrans\core\providers\ubl\UBLProcessor;
use Dtrans\core\providers\ubl\UBLValidator;
use Dtrans\core\types\DtransDocument;

abstract class LieferscheinProcessor {
    public static function process(string $xml, string $file_name, int &$id_result = 0, string &$uuid_result = ''): int
    {
        $code = self::_process($xml, $file_name, $id_result, $uuid_result);

        $dir = __DIR__ . '/../../../';
        if ($code == ProcessingReturnCodesEnum::DUPLICATE) {
            if (file_exists($dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::EINLIEFERSCHEIN . '/' . $file_name))
                rename($dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::EINLIEFERSCHEIN . '/' . $file_name, $dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::EINLIEFERSCHEIN . '/' . SUB_DIR_DUPLICATE . '/' . $file_name);
        } else if ($code == ProcessingReturnCodesEnum::ERROR) {
            if (file_exists($dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::EINLIEFERSCHEIN . '/' . $file_name))
                rename($dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::EINLIEFERSCHEIN . '/' . $file_name, $dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::EINLIEFERSCHEIN . '/' . SUB_DIR_ERROR . '/' . $file_name);
        }

        return $code;
    }

    public static function validate(string $xml) : bool
    {
        if (!UBLValidator::validate_against_xsd($xml))
            return false;

        $object = UBLProcessor::parseToUBLObject($xml);
        if ($object === false)
            return false;

        if (!LieferscheinValidator::check_required_attributes($object))
            return false;

        return true;
    }

    private static function _process(string $xml, string $file_name, int &$id_result = 0, string &$uuid_result = ''): int
    {
        if (!self::validate($xml))
            return ProcessingReturnCodesEnum::ERROR;

        $object = UBLProcessor::parseToUBLObject($xml);
        if ($object === false)
            return ProcessingReturnCodesEnum::ERROR;

        // extract id
        $file_id = $object->getID();
        $uuid = $object->getUUID();
        $type = UBLValidator::is_despatch_advice($object) ? DocumentTypeEnum::EINLIEFERSCHEIN_DESPATCH_ADVICE : DocumentTypeEnum::EINLIEFERSCHEIN_RECEIPT_ADVICE;

        // check for predecessor document
        $previous_document_uuid = null;
        if (!empty($object->getSignature())) {
            $signature = $object->getSignature()[0];
            if(!empty($signature->getOriginalDocumentReference())) {
                $od = $signature->getOriginalDocumentReference();
                if(!empty($od->getID()->value()) && strtolower(trim($od->getID()->value())) == '1lieferschein_predecessor'
                    && !empty($od->getUUID()->value())) {
                        $previous_document_uuid = trim($od->getUUID()->value());
                }
            }
        }

        // check if there is a <RejectReason> within the document
        $rejected = false;
        if ($type == DocumentTypeEnum::EINLIEFERSCHEIN_RECEIPT_ADVICE) {
            foreach ($object->getReceiptLine() as $receipt_line) {
                if (!empty($receipt_line->getRejectReason())) {
                    $rejected = true;
                }
            }
        }

        // check if duplicate
        if (ModelDocuments::is_duplicate($uuid)) {
            $uuid_result = $uuid->value();
            return ProcessingReturnCodesEnum::DUPLICATE;
        }

        $file_size = FileCreator::get_file_size(DtransFormatsEnum::EINLIEFERSCHEIN, $file_name);
        $document = new DtransDocument($type, $file_id, $file_name, $file_size, $uuid);
        $document->set_previous_version_uuid($previous_document_uuid);
        $document->set_rejected($rejected);
        // analyse signature and adjust state
        $state = LieferscheinSignature::analyse_signature($object);
        $document->set_handling_status($state);
        // project reference
        $project = DeliveryLocation::get_reference($object);
        $document->setReference($project);

        UBLDataExtractor::add_additional_attributes($document, $object);
        $suc = ModelDocumentsInsert::insert_new_document($document);

        if ($suc) {
            $id_result = $document->get_internal_id();
            $uuid_result = $uuid->value();

            DtransAddress::extract_dtrans_recipients_ubl($object, $document->get_internal_id(), DtransFormatsEnum::EINLIEFERSCHEIN . '/' . $file_name, DtransFormatsEnum::EINLIEFERSCHEIN);
            return ProcessingReturnCodesEnum::PROCESSED;
        }

        return ProcessingReturnCodesEnum::ERROR;
    }
}