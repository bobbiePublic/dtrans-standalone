<?php

namespace Dtrans\core\providers\einlieferschein;

use Dtrans\core\enums\HandlingStatusEnum;
use Dtrans\core\enums\SignatureReasonCodes;
use Dtrans\core\helpers\DtransLogger;

abstract class LieferscheinSignature {
    const SIGNATURE_METHOD_IDENTIFIER = "1lieferschein-signature";

    public static function analyse_signature(/*?DespatchAdvice|?ReceiptAdvice*/ $object) :string {
        if(is_null($object) || empty($object->getSignature())) {
            return HandlingStatusEnum::UNKNOWN;
        }

        $signatures = $object->getSignature();
        if(sizeof($signatures) > 1)
            DtransLogger::info("Processed document '" . $object->getUUID() . "' contains multiple signature instances.");

        foreach ($signatures as $signature) {
            if(empty($signature)) continue;

            if(strcmp(trim(strtolower($signature->getSignatureMethod())), self::SIGNATURE_METHOD_IDENTIFIER) !== 0) {
                DtransLogger::info("Processed document '" . $object->getUUID() . "' contains a non 1lieferschein signature.");
                continue; // currently we are discussing if this signature method is required
            }

            if(empty($signature->getReasonCode()) || empty($signature->getReasonCode()->value())) {
                DtransLogger::info("Document '" . $object->getUUID() . "' signature object does not contain a reason code.");
                continue;
            }

            // extract reason code and match next state
            $rs = trim(strtolower($signature->getReasonCode()->value()));
            return SignatureReasonCodes::get_handling_state($rs);

            // get current document state and return next state if no current exists
            // REMOVED as check was moved to ModelDocuments
            /*$states = ModelDocuments::get_order_state($sql, $object->getID()->value());
            if(empty($states) || !array_key_exists(DB_ORDERS_HANDLING_STATUS, $states))
                return $next;

            $current = $states[DB_ORDERS_HANDLING_STATUS];
            return HandlingStatusEnum::evaluate_state($current, $next);*/
        }

        return HandlingStatusEnum::UNKNOWN;
    }
}