<?php

namespace Dtrans\core\providers\einlieferschein;

use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\types\DtransDocument;

abstract class LieferscheinValidator {
    public static function is_1lieferschein($object): bool
    {
        $a = $object->getCustomizationID();
        return (!empty($a) && strcmp(trim(strtolower($a->value())), "1lieferschein") === 0);
    }

    public static function check_required_attributes($object) : bool {
        if (is_null($object)) {
            DtransLogger::error("Something seriously went wrong while validating required attributes.");
            return false;
        }

        //$is_despatch = UBLValidator::is_despatch_advice($object);
        $accepted = true;

        $a = $object->getCustomizationID();
        if (empty($a)) {
            UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE, 'CustomizationID');
            $accepted = false;
        } else if (strcmp(trim(strtolower($a->value())), "1lieferschein") !== 0) {
            UserFeedback::warning(ConstsStrings::CODE_XML_1LIEFERSCHEIN_ATTRIBUTE_NOT_SET_TO_DEFAULT, 'CustomizationID', '1Lieferschein');
            $accepted = false;
        }

        $a = $object->getProfileID();
        if(empty($a)) {
            //UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE, 'ProfileID');
            //$accepted = false;
        } else {
            $a = trim(strtolower($a->value()));
            if (!in_array($a, ['default', 'da01'])) {
                UserFeedback::warning(ConstsStrings::CODE_XML_1LIEFERSCHEIN_ATTRIBUTE_NOT_SUPPORTED, 'ProfileID', $a);
                DtransLogger::notice("1Lieferschein defined ProfileID '" . $a . "' is not support.");
            }
        }

        $a = $object->getID();
        if (empty($a) || empty(trim($a))) {
            UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE, 'ID');
            $accepted = false;
        }
        $a = $object->getUUID();
        if (empty($a) || empty(trim($a))) {
            UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE, 'UUID');
            $accepted = false;
        } else if (strlen($a->value()) > DtransDocument::UUID_LENGTH) {
            UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_ATTRIBUTE_NOT_SUPPORTED, 'UUID', 'exceeds limit of ' . DtransDocument::UUID_LENGTH . ' characters');
            $accepted = false;
        }

        /*
        $a = $object->getIssueDate();
        if(empty($a)) {
            UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE, 'IssueDate');
            $accepted = false;
        }

        if($is_despatch) {
            $a = $object->getDespatchAdviceTypeCode();
            if(empty($a)) {
                UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE, 'DespatchAdviceTypeCode');
                $accepted = false;
            }
        }

        $a = $object->getOrderReference();
        if(empty($a)) {
            UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE, 'OrderReference');
            $accepted = false;
        } else {
            $has_one = false;
            foreach ($a as $ref) {
                $b = $ref->getCustomerReference();
                if (!empty($b)) $has_one = true;
            }

            if(!$has_one) {
                UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE, "CustomerReference' under 'OrderReference");
                $accepted = false;
            }
        }

        $a = $object->getShipment();
        if(empty($a)) {
            UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE, 'Shipment');
            $accepted = false;
        } else {
            $b = $a->getDelivery();
            if(empty($b)) {
                UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE, "Delivery' under 'Shipment");
                $accepted = false;
            }
        }

        // exactly one signature is required
        $a = $object->getSignature();
        if (empty($a)) {
            UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE, 'Signature');
            $accepted = false;
        } else {
            if(sizeof($a) > 1) {
                UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_SIGNATURE_MULTIPLE);
                $accepted = false;
            } else {
                $a = $a[0];
                $b = $a->getReasonCode();
                if (empty($b)) {
                    UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_MISSING_ATTRIBUTE, "ReasonCode' under 'Signature");
                    $accepted = false;
                } else if(empty($a->getSignatureMethod()) || strcmp(trim(strtolower($a->getSignatureMethod())), LieferscheinSignature::SIGNATURE_METHOD_IDENTIFIER) !== 0) {
                    UserFeedback::error(ConstsStrings::CODE_XML_1LIEFERSCHEIN_SIGNATURE_METHOD_UNSUPPORTED);
                    $accepted = false;
                } else if (strcmp(SignatureReasonCodes::get_handling_state($b->value()), HandlingStatusEnum::UNKNOWN) === 0)
                    UserFeedback::warning(ConstsStrings::CODE_XML_1LIEFERSCHEIN_ATTRIBUTE_NOT_SUPPORTED, 'Signature->ReasonCode', $b->value());
            }
        }*/

        return $accepted;
    }
}