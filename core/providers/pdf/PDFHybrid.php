<?php

namespace Dtrans\core\providers\pdf;

use Bobbie\UBL\Helper\Serializer;
use DOMDocument;
use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\tables\DBTableDocuments;
use Dtrans\core\enums\DtransFormatsEnum;
use Dtrans\core\enums\IncomingInterfaceEnum;
use Dtrans\core\enums\ProcessingReturnCodesEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\providers\einlieferschein\LieferscheinProcessor;
use Dtrans\core\providers\einlieferschein\LieferscheinValidator;
use Dtrans\core\providers\IncomingProcessor;
use Dtrans\core\providers\ubl\UBLProcessor;
use Dtrans\core\providers\ubl\UBLValidator;
use Error;
use Exception;
use Throwable;

abstract class PDFHybrid
{
    public static function processHybridPDF(string $pdf, $file_name, $uuid): ?array
    {
        try {
            $parser = new \Smalot\PdfParser\Parser();
            $pdfParsed = $parser->parseContent($pdf);

            /* if we ever need embedded file names here is code to read it from pdf header. Just in case */
            /*$file_spec = $pdfParsed->getObjectsByType('Filespec');
            foreach ($file_spec as $spec) {
                $specDetails = $spec->getDetails();
                $fileName = $specDetails['F'];
                // file size (optional)
                $fileLength = !empty($specDetails['EF']) && isset($specDetails['EF']['F']['Length']) ? $specDetails['EF']['F']['Length'] : null;
            }*/

            $embeddedFiles = $pdfParsed->getObjectsByType('EmbeddedFile');
            foreach ($embeddedFiles as $ef) {
                $detail = $ef->getDetails();
                $xmlString = $ef->getContent();

                $length = !empty($detail['Length']) ? $detail['Length'] : null;
                $subType = !empty($detail['Subtype']) ? $detail['Subtype'] : null;
                $rootNode = self::get_xml_root($xmlString);
                if (empty($rootNode)) {
                    DtransLogger::debug('Ignoring embedded file in PDF as XML check failed!', ['length' => $length, 'type' => $subType]);
                    continue;
                }

                // found valid xml file. Let's process it!
                $result = self::_processEmbeddedXML($xmlString, $rootNode);
                if (empty($result)) // failed
                    continue;

                return $result;
            }
        } catch (Error|Throwable $e) {
            DtransLogger::info('Exception occurred during PDF Hybrid check.', ['exception' => $e->__toString(), 'file_name' => $file_name, 'uuid' => $uuid]);
            return null;
        }

        return null;
    }

    private static function get_xml_root(string $file): ?string
    {
        try {
            $dom = new DOMDocument();
            $dom->loadXML($file);
            $root = $dom->documentElement;
            return !empty($root) && !empty($root->nodeName) ? $root->nodeName : null;
        } catch (Error|Throwable|Exception $t) {
            return null;
        }
    }

    private static function _processEmbeddedXML(string $xml, string $rootNodeName /* will be interesting when we support other xml formats than UBL */): ?array
    {
        $isUbl = UBLValidator::validate_against_xsd($xml);
        if (!$isUbl) {
            DtransLogger::debug('UBL XSD Validation failed for embedded xml. XML is not a valid UBL file.');
            return null;
        }

        $isEinLieferschein = false;
        try {
            $ublSerializer = new Serializer();
            $object = $ublSerializer->deSerializeXML($xml);
            if (empty($object))
                throw new Exception('Parsing to UBL object failed.');

            $isEinLieferschein = LieferscheinValidator::is_1lieferschein($object);
        } catch (Error|Throwable|Exception $e) {
            DtransLogger::debug('Exception occurred when trying to read UBL root node.', ['exception' => $e->__toString()]);
            return null;
        }

        $format = $isEinLieferschein ? DtransFormatsEnum::EINLIEFERSCHEIN : DtransFormatsEnum::UBL;
        $file_name = IncomingProcessor::create_file($xml, $format, IncomingInterfaceEnum::EMBEDDED_IN_PDF);
        if (empty($file_name)) { // file was already processed => file was duplicate (UUID match) or contains errors
            // find the duplicated file and process again to retrieve information what happened
            $file_name = IncomingProcessor::get_duplicate($xml, $format);
            $dir = __DIR__ . '/../../../';
            if (is_null($file_name)) {
                DtransLogger::debug('File was reported as duplicate but no duplicate file exist!', ['file' => $file_name]);
                return null;
            }
            $is_duplicate = file_exists($dir . PATH_DOCUMENTS . '/' . $format . '/' . SUB_DIR_DUPLICATE . '/' . $file_name);
            $is_faulty = file_exists($dir . PATH_DOCUMENTS . '/' . $format . '/' . SUB_DIR_ERROR . '/' . $file_name);

            // restore file as we need to process it again to retrieve the duplicated UUID for linking or create error message
            if ($is_duplicate)
                rename($dir . PATH_DOCUMENTS . '/' . $format . '/' . SUB_DIR_DUPLICATE . '/' . $file_name, $dir . PATH_DOCUMENTS . '/' . $format . '/' . $file_name);
            else if ($is_faulty)
                rename($dir . PATH_DOCUMENTS . '/' . $format . '/' . SUB_DIR_ERROR . '/' . $file_name, $dir . PATH_DOCUMENTS . '/' . $format . '/' . $file_name);
        }

        $doc_id = 0;
        $uuid = '';
        $returnCode = $isEinLieferschein ? LieferscheinProcessor::process($xml, $file_name, $doc_id, $uuid) : UBLProcessor::process($xml, $file_name, $doc_id, $uuid);

        switch ($returnCode) {
            case ProcessingReturnCodesEnum::PROCESSED;
                $doc = ModelDocuments::get_document_info($uuid);
                if (empty($doc)) {
                    DtransLogger::error('Embedded XML was processed, but could not be found!', ['uuid' => $uuid, 'return_code' => $returnCode]);
                    return array($doc_id, $uuid, null);
                }

                return array($doc_id, $uuid, $doc[DBTableDocuments::CHAIN_ID]);
            /* this happens when the document already exists but the hash mismatched */
            /* just look up the duplicated uuid and return */
            case ProcessingReturnCodesEnum::DUPLICATE;
                $original = ModelDocuments::get_document_info($uuid);
                if (empty($original)) {
                    DtransLogger::error('Embedded XML already exists but could not be found!', ['uuid' => $uuid, 'return_code' => $returnCode]);
                    return null;
                }
                $doc_id = $original[DBTableDocuments::ID];
                $doc_chain = $original[DBTableDocuments::CHAIN_ID];
                return array($doc_id, $uuid, $doc_chain);

            default:
                DtransLogger::notice('Embedded XML could not be processed as the Processor for the format run into an error. No idea what happened!', ['return_code' => $returnCode]);
                return null;
        }
    }
}