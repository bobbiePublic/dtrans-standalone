<?php

namespace Dtrans\core\providers\pdf;

use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\models\ModelDocumentsInsert;
use Dtrans\core\enums\DocumentTypeEnum;
use Dtrans\core\enums\DtransFormatsEnum;
use Dtrans\core\enums\ProcessingReturnCodesEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\FileCreator;
use Dtrans\core\types\DtransDocument;

abstract class PDFProcessor
{
    public static function process(string $xml, string $file_name): int
    {
        $code = self::_process($xml, $file_name);

        $dir = __DIR__ . '/../../../';
        if ($code == ProcessingReturnCodesEnum::DUPLICATE) {
            if (file_exists($dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::PDF . '/' . $file_name))
                rename($dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::PDF . '/' . $file_name, $dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::PDF . '/' . SUB_DIR_DUPLICATE . '/' . $file_name);
        } else if ($code == ProcessingReturnCodesEnum::ERROR) {
            if (file_exists($dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::PDF . '/' . $file_name))
                rename($dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::PDF . '/' . $file_name, $dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::PDF . '/' . SUB_DIR_ERROR . '/' . $file_name);
        }

        return $code;
    }

    private static function _process(string $file, string $file_name): int
    {
        // extract id and generate SHA256 hash for ubl
        $uuid = 'pdf@' . hash('sha256', $file);

        // check if duplicate
        if (ModelDocuments::is_duplicate($uuid))
            return ProcessingReturnCodesEnum::DUPLICATE;

        $file_size = FileCreator::get_file_size(DtransFormatsEnum::PDF, $file_name);
        $containing_file = PDFHybrid::processHybridPDF($file, $file_name, $uuid);
        $type = !empty($containing_file) ? DocumentTypeEnum::PDF_HYBRID : DocumentTypeEnum::PDF;
        $document = new DtransDocument($type, null, $file_name, $file_size, $uuid);
        if (!empty($containing_file)) { /* for hybrid pdf link with embedded file */
            $document->set_pdf_embedded_id($containing_file[0]);
            $document->set_pdf_embedded_uuid($containing_file[1]);
            $document->set_file_id($containing_file[2]);
            DtransLogger::info('Successfully linked hybrid pdf with embedded document.', ['pdf' => $uuid, 'embedded_document' => $containing_file[1]]);
        }

        $suc = ModelDocumentsInsert::insert_new_document($document);
        return $suc ? ProcessingReturnCodesEnum::PROCESSED : ProcessingReturnCodesEnum::ERROR;
    }
}