<?php

namespace Dtrans\core\providers\ubl;

use Dtrans\core\providers\ubl\extractors\DocumentReferences;
use Dtrans\core\providers\ubl\extractors\ItemLines;
use Dtrans\core\providers\ubl\extractors\PartyInfo;
use Dtrans\core\providers\ubl\extractors\ShipmentInfo;
use Dtrans\core\types\DtransDocument;
use UBL\DespatchAdvice;
use UBL\ReceiptAdvice;

abstract class UBLDataExtractor
{
    public static function add_additional_attributes(DtransDocument $document, DespatchAdvice|ReceiptAdvice|null $object): void
    {
        if (empty($object)) return;

        if (!empty($object->getIssueDate()))
            $document->set_date_document($object->getIssueDate()->format("Y-m-d"));

        ShipmentInfo::add_additional_attributes($document, $object);
        PartyInfo::add_additional_attributes($document, $object);
        ItemLines::add_additional_attributes($document, $object);
        DocumentReferences::add_additional_attributes($document, $object);
    }
}