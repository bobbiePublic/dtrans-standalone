<?php

namespace Dtrans\core\providers\ubl;

use Bobbie\UBL\Helper\Serializer;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\models\ModelDocumentsInsert;
use Dtrans\core\enums\DocumentTypeEnum;
use Dtrans\core\enums\DtransFormatsEnum;
use Dtrans\core\enums\ProcessingReturnCodesEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\FileCreator;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\providers\einlieferschein\LieferscheinValidator;
use Dtrans\core\providers\ubl\extractors\DeliveryLocation;
use Dtrans\core\providers\ubl\extractors\DtransAddress;
use Dtrans\core\types\DtransDocument;
use Error;
use Exception;
use Throwable;
use UBL\DespatchAdviceType;
use UBL\ReceiptAdviceType;

abstract class UBLProcessor {
    public static function process(string $xml, string $file_name, int &$id_result = 0, string &$uuid_result = ''): int
    {
        $code = self::_process($xml, $file_name, $id_result, $uuid_result);

        $dir = __DIR__ . '/../../../';
        if ($code == ProcessingReturnCodesEnum::DUPLICATE) {
            if (file_exists($dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::UBL . '/' . $file_name))
                rename($dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::UBL . '/' . $file_name, $dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::UBL . '/' . SUB_DIR_DUPLICATE . '/' . $file_name);
        } else if ($code == ProcessingReturnCodesEnum::ERROR) {
            if (file_exists($dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::UBL . '/' . $file_name))
                rename($dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::UBL . '/' . $file_name, $dir . PATH_DOCUMENTS . '/' . DtransFormatsEnum::UBL . '/' . SUB_DIR_ERROR . '/' . $file_name);
        }

        return $code;
    }

    public static function validate(string $xml): bool
    {
        // validate against XSD
        if (!UBLValidator::validate_against_xsd($xml))
            return false;

        // parse to object
        $object = self::parseToUBLObject($xml);
        if ($object === false)
            return false;

        // avoid passing 1lieferschein documents as ubl
        if (LieferscheinValidator::is_1lieferschein($object)) {
            UserFeedback::error(ConstsStrings::CODE_XML_UBL_IS_1LIEFERSCHEIN);
            return false;
        }

        return true;
    }

    public static function parseToUBLObject(string $xml) : DespatchAdviceType|ReceiptAdviceType|false
    {
        try {
            $ublSerializer = new Serializer();
            $object = $ublSerializer->deSerializeXML($xml);
            if (!is_null($object))
                return $object;
        } catch (Error|Throwable|Exception $e) {
            UserFeedback::error(ConstsStrings::CODE_UNKNOWN_ERROR_DOCUMENT_PROCESSING);
            DtransLogger::warning("Error occurred while processing ubl xml document.", ['$error' => $e]);
        }

        return false;
    }

    private static function _process(string $xml, string $file_name, int &$id_result = 0, string &$uuid_result = ''): int
    {
        if (!self::validate($xml))
            return ProcessingReturnCodesEnum::ERROR;

        // parse to object
        $object = UBLProcessor::parseToUBLObject($xml);
        if ($object === false)
            return ProcessingReturnCodesEnum::ERROR;

        // extract id and generate SHA256 hash for ubl
        $file_id = $object->getID();
        $uuid = $object->getUUID();
        if (empty($uuid)) $uuid = 'ubl@' . hash('sha256', $xml);
        $type = UBLValidator::is_despatch_advice($object) ? DocumentTypeEnum::UBL_DESPATCH_ADVICE : DocumentTypeEnum::UBL_RECEIPT_ADVICE;

        // check if duplicate
        if (ModelDocuments::is_duplicate($uuid)) {
            $uuid_result = $uuid->value();
            return ProcessingReturnCodesEnum::DUPLICATE;
        }

        $file_size = FileCreator::get_file_size(DtransFormatsEnum::UBL, $file_name);
        $document = new DtransDocument($type, $file_id, $file_name, $file_size, $uuid);
        // project reference
        $project = DeliveryLocation::get_reference($object);
        $document->setReference($project);

        UBLDataExtractor::add_additional_attributes($document, $object);
        $suc = ModelDocumentsInsert::insert_new_document($document);

        if ($suc) {
            $id_result = $document->get_internal_id();
            $uuid_result = $uuid->value();

            DtransAddress::extract_dtrans_recipients_ubl($object, $document->get_internal_id(), DtransFormatsEnum::UBL . '/' . $file_name, DtransFormatsEnum::UBL);
            return ProcessingReturnCodesEnum::PROCESSED;
        }

        return ProcessingReturnCodesEnum::ERROR;
    }
}