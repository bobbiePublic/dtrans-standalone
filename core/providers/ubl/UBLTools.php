<?php

namespace Dtrans\core\providers\ubl;

use Dtrans\core\api\ApiGeocoding;
use Dtrans\core\types\UBLLocation;
use UBL\cac\LocationCoordinateType;
use UBL\cac\LocationType;

abstract class UBLTools
{
    private static function ubl_address_to_string(?\UBL\cac\AddressType $address): ?string
    {
        if (empty($address))
            return null;

        $str = '';
        $lines = $address->getAddressLine();
        if (!empty($lines)) {
            $addr = array();
            foreach ($lines as $line)
                if (!empty($line) && !empty($line->getLine()) && !empty($line->getLine()->value()))
                    $addr[] = trim($line->getLine()->value());

            $str = implode(', ', $addr);
        }

        if (!empty($address->getStreetName()) && !empty($address->getStreetName()->value()))
            $str .= trim($address->getStreetName()->value());
        if (!empty($address->getAdditionalStreetName()) && !empty($address->getAdditionalStreetName()->value()))
            $str .= ' ' . trim($address->getAdditionalStreetName()->value());
        if (!empty($address->getBuildingNumber()) && !empty($address->getBuildingNumber()->value()))
            $str .= ' ' . trim($address->getBuildingNumber()->value()) . ',';
        if (!empty($address->getBuildingName()) && !empty($address->getBuildingName()->value()))
            $str .= ' ' . trim($address->getBuildingName()->value()) . ',';
        if (!empty($address->getPostalZone()) && !empty($address->getPostalZone()->value()))
            $str .= ' ' . trim($address->getPostalZone()->value());
        if (!empty($address->getCityName()) && !empty($address->getCityName()->value()))
            $str .= ' ' . trim($address->getCityName()->value());
        if (!empty($address->getCitySubdivisionName()) && !empty($address->getCitySubdivisionName()->value()))
            $str .= ' ' . trim($address->getCitySubdivisionName()->value());
        if (!empty($address->getCountry()) && !empty($address->getCountry()->getName()) && !empty($address->getCountry()->getName()->value()))
            $str .= ', ' . trim($address->getCountry()->getName()->value());
        if (!empty($address->getCitySubdivisionName()) && !empty($address->getCitySubdivisionName()->value()))
            $str .= ' ' . trim($address->getCitySubdivisionName()->value()) . ',';
        if (!empty($address->getRegion()) && !empty($address->getRegion()->value()))
            $str .= ' ' . trim($address->getRegion()->value());

        // cut last char if separator
        $str = trim($str);
        if (str_ends_with(',', $str))
            $str = substr($str, 0, strlen($str) - 1);
        if (str_starts_with(',', $str))
            $str = substr($str, 1);

        // only return if there is any value inside
        if (!empty(trim($str)))
            return trim($str);
        else
            return null;
    }

    public static function get_address_string(?\UBL\cac\AddressType $address, ?string &$address_source): ?string
    {
        if (empty($address))
            return null;

        $query = self::ubl_address_to_string($address);
        $address_source = $query;
        if (empty($query))
            return null;

        // return raw string representation if geo api is not available
        if (!ApiGeocoding::is_google_available())
            return $query;

        // send to api
        $result = ApiGeocoding::request($query);
        if (!empty($result) && !empty($result->getAddress()))
            return $result->getAddress();

        return $query;
    }

    // a helper function which trys to convert a UBL LocationCoordinateType instance to decimal representation
    // returns null if information is missing in the instance to represent a geo position
    // as UBL does not specify the coordinate format nor limit the value range this is always best effort and not very reliable
    private static function ubl_location_coordinate_to_decimal(?LocationCoordinateType $location): ?string
    {
        if (empty($location))
            return null;

        // coordinates are required
        if (empty($location->getLatitudeDegreesMeasure()) || empty($location->getLatitudeDegreesMeasure()->value()) ||
            empty($location->getLongitudeDegreesMeasure()) || empty($location->getLongitudeDegreesMeasure()->value()))
            return null;

        $latitude = $location->getLatitudeDegreesMeasure()->value();
        if (!empty($location->getLatitudeMinutesMeasure()) && !empty($location->getLatitudeMinutesMeasure()->value())) {
            $minutes = $location->getLatitudeMinutesMeasure()->value();
            $negative = $latitude < 0;
            $latitude = abs($latitude) + abs($minutes / 60.0);
            // restore sign
            if ($negative) $latitude *= -1;
        }
        if (!empty($location->getLatitudeDirectionCode()) && !empty($location->getLatitudeDirectionCode()->value())) {
            $direction = strtoupper(trim($location->getLatitudeDirectionCode()->value()));
            // if south then make sure latitude is negative
            if (($direction == 'S' || $direction == 'SOUTH') && $latitude > 0) {
                $latitude *= -1;
            }
        }

        if (abs($latitude) > 90) // invalid
            return null;

        $longitude = $location->getLongitudeDegreesMeasure()->value();
        if (!empty($location->getLongitudeMinutesMeasure()) && !empty($location->getLongitudeMinutesMeasure()->value())) {
            $minutes = $location->getLongitudeMinutesMeasure()->value();
            $negative = $longitude < 0;
            $longitude = abs($longitude) + abs($minutes / 60.0);
            // restore sign
            if ($negative) $longitude *= -1;
        }
        if (!empty($location->getLongitudeDirectionCode()) && !empty($location->getLongitudeDirectionCode()->value())) {
            $direction = strtoupper(trim($location->getLongitudeDirectionCode()->value()));
            // if south then make sure longitude is negative
            if (($direction == 'W' || $direction == 'WEST') && $longitude > 0) {
                $longitude *= -1;
            }
        }

        if (abs($longitude) > 180) // invalid
            return null;

        return $latitude . ',' . $longitude;
    }

    // trys to convert a UBL LocationCoordinateType to a string representation
    public static function get_location_coordinate_string(?LocationCoordinateType $location): ?string
    {
        return self::ubl_location_coordinate_to_decimal($location);
    }

    // try to get a location from a UBL LocationType instance
    public static function get_location_strings(?LocationType $location, ?string &$address_source): ?UBLLocation
    {
        if (empty($location))
            return null;

        // Location->Address
        $locationCoordinate = null;
        $addr = $location->getAddress();
        $address = self::get_address_string($addr, $address_source);
        // Location->Address->LocationCoordinate[1..n]
        if (!is_null($addr) && !empty($addr->getLocationCoordinate()))
            foreach ($addr->getLocationCoordinate() as $loc) {
                $locationCoordinate = self::get_location_coordinate_string($loc);
                if (!empty($locationCoordinate)) break; // stop after cord was found
            }
        // Fallback: Location->LocationCoordinate[1..n]
        if (empty($locationCoordinate) && !empty($location->getLocationCoordinate())) {
            foreach ($location->getLocationCoordinate() as $loc) {
                $locationCoordinate = self::get_location_coordinate_string($loc);
                if (!empty($locationCoordinate)) break; // stop after cord was found
            }
        }

        if (empty($address) && empty($locationCoordinate))
            return null;
        else {
            $ubl_location = new UBLLocation();
            $ubl_location->setLocationCoords($locationCoordinate);
            $ubl_location->setAddress($address);
            return $ubl_location;
        }
    }
}