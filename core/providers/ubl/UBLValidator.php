<?php

namespace Dtrans\core\providers\ubl;

use DOMDocument;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\ErrorHandler;
use Dtrans\core\helpers\UserFeedback;
use Error;
use Exception;
use Throwable;

abstract class UBLValidator
{
    public static function is_despatch_advice($object): bool
    {
        return strcmp(get_class($object), 'UBL\DespatchAdvice') === 0;
    }

    public static function is_receipt_advice($object): bool
    {
        return strcmp(get_class($object), 'UBL\ReceiptAdvice') === 0;
    }

    // custom error handler for tracking the xsd validation errors
    public static function custom_validator_error_handler ($severity, $message, $file, $line) :bool {
        // unescape message
        $message = htmlspecialchars_decode($message);

        // remove php source files
        $pos = strpos($message, 'DOMDocument::schemaValidate(): ');
        if ($pos !== false)
            $message = substr_replace($message, 'Error: ', $pos, strlen('DOMDocument::schemaValidate(): '));

        UserFeedback::error(ConstsStrings::CODE_XML_UBL_INVALID, $message);

        // hide php notices/errors
        return true;
    }

    public static function validate_against_xsd(string $xml, bool $reportToUser = true): bool
    {
        try {
            $dom = new DOMDocument();
            $dom->loadXML($xml);

            // replace global error handler with custom one while validating the xsd
            set_error_handler('\Dtrans\core\providers\ubl\UBLValidator::custom_validator_error_handler');

            $root = $dom->documentElement;
            if (is_null($root) || empty($root->nodeName)) { // for incorrect xml files
                if ($reportToUser)
                    UserFeedback::error(ConstsStrings::CODE_XML_UBL_INVALID, 'No root node found. Is document a well formed xml document?');
                DtransLogger::debug("XML could not be parsed for validation. XML structure is likely broken.");
                $suc = false;
            } else {
                $node = $root->nodeName;
                if (str_contains($node, ':')) {
                    $node = explode(':', $node);
                    $node = end($node);
                }

                if ($node == 'DespatchAdvice')
                    $suc = $dom->schemaValidate(__DIR__ . '/../../../' . XSD_PATH . XSD_DESPATCH_ADVICE);
                else if ($node == 'ReceiptAdvice')
                    $suc = $dom->schemaValidate(__DIR__ . '/../../../' . XSD_PATH . XSD_RECEIPT_ADVICE);
                else {
                    if ($reportToUser)
                        UserFeedback::error(ConstsStrings::CODE_XML_UBL_INVALID, 'Unknown root node in xml: ' . $root->nodeName);
                    DtransLogger::debug("Unknown XML root node in ubl xsd validation.", ['root' => $root->nodeName]);
                    $suc = false;
                }
            }

            ErrorHandler::set_php_handlers();
            if (!$suc)
                DtransLogger::debug("Validation against ubl xsd failed.");
            else
                DtransLogger::debug('Validation against ubl xsd successful!');

            return $suc;
        } catch (Error|Throwable|Exception $t) {
            DtransLogger::warning("Exception occurred while validating UBL document", ['exception' => $t]);
            return false;
        }
    }
}