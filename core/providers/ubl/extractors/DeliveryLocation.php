<?php

namespace Dtrans\core\providers\ubl\extractors;

use Dtrans\core\api\ApiGeocoding;
use Dtrans\core\database\models\ModelDocumentsReference;
use Dtrans\core\providers\ubl\UBLTools;
use Dtrans\core\types\UBLProjectReference;
use UBL\DespatchAdvice;
use UBL\ReceiptAdvice;

abstract class DeliveryLocation
{

    public static function get_reference(DespatchAdvice|ReceiptAdvice $doc): ?UBLProjectReference
    {
        $ref = self::find_existing_reference($doc);
        if (!empty($ref))
            return $ref;

        $ref = self::create_new_reference($doc);
        if (!empty($ref))
            return $ref;

        return null;
    }

    private static function find_existing_reference(DespatchAdvice|ReceiptAdvice $doc): ?UBLProjectReference
    {
        $sales_ids = array();
        $order_reference = $doc->getOrderReference();
        if (!empty($order_reference))
            foreach ($order_reference as $order_ref) {
                if (empty($order_ref)) continue;

                // store sales_ids
                if (!empty($order_ref->getSalesOrderID()) && !empty($order_ref->getSalesOrderID()->value()))
                    $sales_ids[] = $order_ref->getSalesOrderID()->value();
            }


        $shipment = $doc->getShipment();
        $delivery = !empty($shipment) ? $shipment->getDelivery() : null;

        // try to find address + location
        $address = null;
        $addressSource = null;
        $locationCoordinate = null;

        if (!empty($delivery)) {
            $delivery_location = $delivery->getDeliveryLocation();
            $alternative_delivery_location = $delivery->getAlternativeDeliveryLocation();
            $delivery_address = $delivery->getDeliveryAddress();

            // Shipment->Delivery->DeliveryAddress
            if (!empty($delivery_address)) { // Shipment->Delivery->DeliveryAddress + DeliveryAddress->Location
                $address = UBLTools::get_address_string($delivery_address, $addressSource);
                if (!empty($delivery_address->getLocationCoordinate()))
                    $locationCoordinate = $delivery_address->getLocationCoordinate();
            }

            // Fallback1: Shipment->Delivery->DeliveryLocation
            // Fallback2: Shipment->Delivery->AlternativeDeliveryLocation
            if (empty($address) && empty($locationCoordinate)) {
                $result = UBLTools::get_location_strings($delivery_location, $addressSource);
                if (is_null($result)) // Shipment->Delivery->AlternativeDeliveryLocation
                    $result = UBLTools::get_location_strings($alternative_delivery_location, $addressSource);
                if (!is_null($result)) {
                    $address = $result->getAddress();
                    $locationCoordinate = $result->getLocationCoords();
                }
            }

            // try to fix missing component of location
            if (((empty($address) && !empty($locationCoordinate)) || (!empty($address) && empty($locationCoordinate)))) {
                $query = empty($address) ? $locationCoordinate : $address;
                $location = ApiGeocoding::request($query);
                if (!empty($location)) {
                    $address = $location->getAddress();
                    $locationCoordinate = $location->getLocationCoords();
                }
            }
        }

        return ModelDocumentsReference::search_reference($sales_ids, $addressSource, $locationCoordinate, $address);
    }

    private static function create_new_reference(/*DespatchAdvice|ReceiptAdvice*/ $doc): ?UBLProjectReference
    {
        $order_reference = $doc->getOrderReference();
        $order_reference = !empty($order_reference) ? $order_reference[0] : null;

        $shipment = $doc->getShipment();
        $delivery = !empty($shipment) ? $shipment->getDelivery() : null;

        $salesID = !empty($order_reference) && !empty($order_reference->getSalesOrderID()) && !empty($order_reference->getSalesOrderID()->value()) ? $order_reference->getSalesOrderID()->value() : null;
        $name = NULL;
        if (!empty($order_reference) && !empty($order_reference->getCustomerReference()) && !empty($order_reference->getCustomerReference()->value()))
            $name = $order_reference->getCustomerReference()->value();
        else if (!empty($salesID))
            $name = 'P ' . $salesID;

        // try to find address + location
        $address = null;
        $addressSource = NULL;
        $locationCoordinate = null;

        if (!empty($delivery)) {
            $delivery_location = $delivery->getDeliveryLocation();
            $alternative_delivery_location = $delivery->getAlternativeDeliveryLocation();
            $delivery_address = $delivery->getDeliveryAddress();

            if (empty($name)) {
                if (!empty($delivery_location) && !empty($delivery_location->getName()) && !empty($delivery_location->getName()->value()))
                    $name = 'P ' . $delivery_location->getName()->value();
                else if (!empty($alternative_delivery_location) && !empty($alternative_delivery_location->getName()) && !empty($alternative_delivery_location->getName()->value()))
                    $name = 'P ' . $alternative_delivery_location->getName()->value();
            }

            // Shipment->Delivery->DeliveryAddress
            if (!empty($delivery_address)) { // Shipment->Delivery->DeliveryAddress + DeliveryAddress->Location
                $address = UBLTools::get_address_string($delivery_address, $addressSource);
                if (!empty($delivery_address->getLocationCoordinate()))
                    $locationCoordinate = $delivery_address->getLocationCoordinate();
            }

            // Fallback1: Shipment->Delivery->DeliveryLocation
            // Fallback2: Shipment->Delivery->AlternativeDeliveryLocation
            if (empty($address) && empty($locationCoordinate)) {
                $result = UBLTools::get_location_strings($delivery_location, $addressSource);
                if (is_null($result)) // Shipment->Delivery->AlternativeDeliveryLocation
                    $result = UBLTools::get_location_strings($alternative_delivery_location, $addressSource);
                if (!is_null($result)) {
                    $address = $result->getAddress();
                    $locationCoordinate = $result->getLocationCoords();
                }
            }

            // try to fix missing component of location
            if (((empty($address) && !empty($locationCoordinate)) || (!empty($address) && empty($locationCoordinate)))) {
                $query = empty($address) ? $locationCoordinate : $address;
                $location = ApiGeocoding::request($query);
                if (!empty($location)) {
                    $address = $location->getAddress();
                    $locationCoordinate = $location->getLocationCoords();
                }
            }
        }

        if (empty($name) && empty($salesID) && empty($address) && empty($locationCoordinate))
            return null; // no data exists to create new

        $ref = new UBLProjectReference();
        $ref->setName($name);
        $ref->setSalesID($salesID);
        $ref->setAddress($address);
        $ref->setLocationSource($addressSource);
        $ref->setLocationCoords($locationCoordinate);

        // create new reference with only name and salesID
        return $ref;
    }
}