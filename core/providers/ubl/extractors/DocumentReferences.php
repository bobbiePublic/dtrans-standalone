<?php

namespace Dtrans\core\providers\ubl\extractors;

use Dtrans\core\types\DtransDocument;
use UBL\cac\AdditionalDocumentReference;
use UBL\DespatchAdvice;
use UBL\ReceiptAdvice;

abstract class DocumentReferences {

    private static function get_document_references(?AdditionalDocumentReference $ref) : ?array{
        if(empty($ref)) return null;

        $reference = array();

        if(!empty($ref->getID() && !empty($ref->getID()->value())))
            $reference['id'] = trim($ref->getID()->value());
        if(!empty($ref->getUUID() && !empty($ref->getUUID()->value())))
            $reference['uuid'] = trim($ref->getUUID()->value());
        if(!empty($ref->getIssueDate()))
            $reference['issued'] = trim($ref->getIssueDate()->format("Y-m-d"));
        if(!empty($ref->getIssuerParty())) {
            $issuer = PartyInfo::get_party_string($ref->getIssuerParty());
            if(!empty($issuer))
                $reference['issuerParty'] = $issuer;
        }

        if(!empty($reference))
            return $reference;
        else return null;
    }

    public static function add_additional_attributes(DtransDocument $document, DespatchAdvice|ReceiptAdvice $object): void
    {
        $refs = array();
        foreach ($object->getAdditionalDocumentReference() as $ref) {
            $r = self::get_document_references($ref);
            if(!empty($r))
                $refs[] = $r;
        }

        if(!empty($refs)) {
            $json = json_encode($refs);
            $document->set_document_references($json);
        }
    }
}