<?php

namespace Dtrans\core\providers\ubl\extractors;

use Dtrans\core\database\abstraction\SQLTransaction;
use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\models\ModelSending;
use Dtrans\core\helpers\DtransDNSHelper;
use Dtrans\core\helpers\DtransLogger;
use UBL\cac\ContactType;
use UBL\cac\Party;

abstract class DtransAddress
{
    const DTRANS_CHANNEL_IDENTIFIER = ['dtrans', '1lieferschein'];

    private static function extract_ubl_contact(?ContactType $contact, array &$dtrans_receivers): void
    {
        if (is_null($contact)) return;

        //DtransLogger::debug('Found contact in document.', ['contact' => var_export($contact, true)]);

        // collect mails as fallback
        $mail = null;
        if (!is_null($contact->getElectronicMail()) && !empty($contact->getElectronicMail()->value()) && filter_var($contact->getElectronicMail()->value(), FILTER_VALIDATE_EMAIL))
            $mail = $contact->getElectronicMail()->value();

        $other = $contact->getOtherCommunication();
        if (is_null($other)) {
            //$dtrans_receivers[] = [$mail, null]; // add mail
            return;
        }

        foreach ($other as $communication) {
            // extract channel name
            if (!empty($communication->getChannelCode()) && !empty($communication->getChannelCode()->value()))
                $channel = $communication->getChannelCode()->value();
            else if (!empty($communication->getChannel()) && !empty($communication->getChannel()->value()))
                $channel = $communication->getChannel()->value();
            else
                continue; // no channel specified

            // if channel name is not dtrans related => ignore
            if (!in_array(trim(strtolower($channel)), self::DTRANS_CHANNEL_IDENTIFIER)) continue;
            $value = $communication->getValue();

            if(is_null($value) || empty($value->value())) continue;
            $dtrans_receivers[] = [$mail, $value->value()];
        }

        // no dtrans entry was found
        //$dtrans_receivers[] = [$mail, null];
    }

    private static function extract_ubl_party(?Party $party, array &$urls): void
    {
        if(is_null($party)) return;

        // collect website urls as fallback
        if(!is_null($party->getWebsiteURI()) && !is_null($party->getWebsiteURI()->value()) && filter_var($party->getWebsiteURI()->value(), FILTER_VALIDATE_URL))
            $urls[] = $party->getWebsiteURI()->value();
        else if(!is_null($party->getAdditionalWebSite())) {
            foreach ($party->getAdditionalWebSite() as $website) {
                if(is_null($website->getURI()) || is_null($website->getURI()->value())) continue;
                if(!filter_var($website->getURI()->value(), FILTER_VALIDATE_URL)) continue;
                $urls[] = $website->getURI()->value();
            }
        }
    }

    private static function process_receivers(int $document_id, string $file_id, array $potential_dtrans_receivers, array $potential_dtrans_domains, string $document_format): bool
    {
        $potential_dtrans_receivers = array_unique($potential_dtrans_receivers, SORT_REGULAR);

        $has_dtrans_entry =  false;
        $c = count($potential_dtrans_receivers);

        // queue dtrans links
        SQLTransaction::start();
        foreach ($potential_dtrans_receivers as $i => $r) {
            if (empty($r[1])) continue; // ignore contacts without dtrans url

            // create entry in sending table
            ModelSending::create_sending_entry($document_id, $file_id, $r[1], $r[0], $document_format);
            $has_dtrans_entry = true;
        }
        SQLTransaction::commit();


        // search in mail domains for dtrans server
        /* // currently we do not collect this mails as there are many out of context mails like manufacturer support
        foreach ($mails as $mail) {
            $domain = DtransDNSHelper::clean_mail($mail);
            if(!DtransDNSHelper::check_dtrans_dns_entries($domain)) continue;

            // create entry in sending table
            ModelSending::create_sending_entry($adapter, $document_id, $file_id, $domain, $mail);
            $has_dtrans_entry = true;
        }*/

        // last possibility to check for dtrans server in url domains
        // clean and collect url domains
        $c = count($potential_dtrans_domains);
        for($i = 0; $i < $c; $i++)
            $potential_dtrans_domains[$i] = DtransDNSHelper::clean_url($potential_dtrans_domains[$i]);
        // remove duplicate urls
        $domains = array_unique($potential_dtrans_domains, SORT_REGULAR);

        // search in url domains for dtrans server
        SQLTransaction::start();
        foreach ($domains as $domain) {
            if(!DtransDNSHelper::check_dtrans_dns_entries($domain)) continue;
            // create entry in sending table
            ModelSending::create_sending_entry($document_id, $file_id, $domain, null, $document_format);
            $has_dtrans_entry = true;
        }
        SQLTransaction::commit();

        // could not find dtrans server
        return $has_dtrans_entry;
    }

    public static function search_ubl_contact($document, array &$dtrans_receivers): void
    {
        // I have no idea why the values of XML Attributes end up here but this checks avoid a crash
        if (empty($document) || is_bool($document) || is_string($document))
            return;

        $class = get_class($document);
        $parent_class = get_parent_class($document);
        $target_type = 'UBL\\cac\\ContactType';
        if ($target_type == $parent_class) {
            self::extract_ubl_contact($document, $dtrans_receivers);
            return;
        }

        $namespace = 'UBL\\';
        if (!str_starts_with($class, $namespace))
            return; // only visit UBL types
        $namespace = 'UBL\\cbc\\';
        if (str_starts_with($class, $namespace))
            return; // only visit complex types and skip simple types
        $methods = get_class_methods($document);

        foreach ($methods as $method) {
            if (!str_starts_with($method, 'get'))
                continue; // only call getter

            $child = $document->$method();
            if (empty($child))
                continue; // null

            self::search_ubl_contact_next($child, $dtrans_receivers);
        }
    }

    // this function is for visiting all members in an array and ignoring empty children
    private static function search_ubl_contact_next($child, array &$dtrans_receivers)
    {
        if (empty($child))
            return; // null

        if (is_array($child)) {
            foreach ($child as $c)
                self::search_ubl_contact_next($c, $dtrans_receivers);
        } else
            self::search_ubl_contact($child, $dtrans_receivers);
    }

    public static function search_ubl_party_domains($document, array &$dtrans_domains)
    {
        if (empty($document))
            return;

        $class = get_class($document);
        $namespace = 'UBL\\';
        if (!str_starts_with($class, $namespace))
            return; // only visit UBL types
        $methods = get_class_methods($document);
        foreach ($methods as $method) {
            if (!str_starts_with($method, 'get'))
                continue; // only call getter
            if (!str_contains(strtolower($method), 'party'))
                continue; // not a party getter

            $child = $document->$method();
            if (empty($child) || is_array($child))
                continue; // null


            $class = get_class($child);
            $allowed_types = ['UBL\\cac\\SupplierParty', 'UBL\\cac\\CustomerParty'];
            if (in_array(substr($class, 0, strlen($namespace)), $allowed_types))
                return; // only visit UBL party types

            self::extract_ubl_party($child->getParty(), $dtrans_domains);
        }
    }


    public static function extract_dtrans_recipients_ubl($ubl, int $document_id, string $file_path, string $document_format): bool
    {
        // analyse ubl document type
        $potential_dtrans_receivers = array();
        self::search_ubl_contact($ubl, $potential_dtrans_receivers);
        $potential_dtrans_domains = array();
        self::search_ubl_party_domains($ubl, $potential_dtrans_domains);
        DtransLogger::debug('Searched document for dtrans receivers!',
            [
                'receivers' => var_export($potential_dtrans_receivers, true),
                'domains' => var_export($potential_dtrans_domains, true)
            ]
        );

        // process results
        $suc = self::process_receivers($document_id, $file_path, $potential_dtrans_receivers, $potential_dtrans_domains, $document_format);
        if ($suc)
            ModelDocuments::set_in_dtrans_queue_by_internal_id($document_id, true);
        else
            DtransLogger::info("Could not find any dtrans server in document.", ['document_id' => $document_id, 'file_path' => $file_path]);

        return $suc;
    }
}