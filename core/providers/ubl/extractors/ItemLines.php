<?php

namespace Dtrans\core\providers\ubl\extractors;

use Dtrans\core\types\DtransDocument;
use UBL\cac\DespatchLine;
use UBL\cac\Item;
use UBL\cac\ReceiptLine;
use UBL\DespatchAdvice;
use UBL\ReceiptAdvice;

abstract class ItemLines
{
    private static function get_item_description(?Item $item) : array {
        $des = array();
        if(empty($item)) return $des;

        $itemDescription = $item->getDescription();

        foreach($itemDescription as $description) {
            if(empty($description) || empty($description->value())) continue;

            $des[] = trim($description->value());
        }

        return $des;
    }

    private static function get_item_description_line(null|DespatchLine|ReceiptLine $line) : array
    {
        if(empty($line)) return array();
        return self::get_item_description($line->getItem());
    }

    private static function get_item_description_lines(array $lines) : ?string
    {
        if(empty($lines)) return null;

        $des = array();
        foreach($lines as $line) {
            if(empty($line)) continue;

            if($line instanceof DespatchLine) {
                $d = self::get_item_description_line($line);
                if(empty($d)) continue;
                $des = array_merge($des, $d);
            } else { // ReceiptLine can have multiple items
                $items = $line->getItem();
                foreach($items as $item) {
                    if(empty($item)) continue;

                    $d = self::get_item_description($item);
                    if(empty($d)) continue;
                    $des = array_merge($des, $d);
                }
            }
        }

        if(empty($des)) return null;
        return implode("$", $des);
    }

    private static function get_item_weight_lines(array $lines) : float
    {
        if(empty($lines)) return 0;

        $weight = 0;
        foreach($lines as $line) {
            if(empty($line)) continue;

            if($line instanceof DespatchLine)
                $weight += self::get_item_weight_despatch_line($line);
            else
                $weight += self::get_item_weight_receipt_line($line);
        }

        return $weight;
    }

    private static function get_item_weight_despatch_line(?DespatchLine $line) : float
    {
        if(empty($line)) return 0;
        $q = $line->getDeliveredQuantity();

        if(empty($q) || empty($q->value())) return 0;
        return $q->value();
    }

    private static function get_item_weight_receipt_line(?ReceiptLine $line) : float
    {
        if(empty($line)) return 0;

        $q = $line->getReceivedQuantity();

        if(empty($q) || empty($q->value())) return 0;
        return $q->value();
    }

    public static function add_additional_attributes(DtransDocument $document, DespatchAdvice|ReceiptAdvice $object): void {
        if($object instanceof DespatchAdvice) {
            $item_description = self::get_item_description_lines($object->getDespatchLine());
            $weight = self::get_item_weight_lines($object->getDespatchLine());
        } else {
            $item_description = self::get_item_description_lines($object->getReceiptLine());
            $weight = self::get_item_weight_lines($object->getReceiptLine());
        }

        // limit $item_description to 2048 characters
        if(!empty($item_description) && strlen($item_description) > 2048)
            $item_description = substr($item_description, 0, 2048);
        $document->setItemDescription($item_description);
        $document->setItemWeights($weight);
    }
}