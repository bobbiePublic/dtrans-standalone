<?php

namespace Dtrans\core\providers\ubl\extractors;

use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\types\DtransDocument;
use UBL\cac\ContactType;
use UBL\cac\CustomerPartyType;
use UBL\cac\PartyType;
use UBL\cac\SupplierPartyType;
use UBL\DespatchAdvice;
use UBL\ReceiptAdvice;

abstract class PartyInfo {
    private static function get_contact_string(?ContactType $contact) : ?string {
        if(empty($contact))
            return null;

        $n = $contact->getName();
        if(!empty($n) && !empty($n->value()))
            return $n->value();

        $n = $contact->getID();
        if(!empty($n) && !empty($n->value()))
            return $n->value();

        return null;
    }

    public static function get_party_string(?PartyType $party) : ?string {
        if(empty($party))
            return null;

        // 1. PartyLegalEntity[n]->RegistrationName
        $pn = $party->getPartyLegalEntity();
        if(!empty($pn)) {
            foreach ($pn as $partyLegalEntity) {
                if(!empty($partyLegalEntity)) {
                    $prn = $partyLegalEntity->getRegistrationName();
                    if(!empty($prn) && !empty($prn->value()))
                        return $prn->value();
                }
            }
        }

        // 2. PartyName[n]
        $pn = $party->getPartyName();
        if (!empty($pn)) {
            foreach ($pn as $name) {
                $n = $name->getName();
                if (!empty($n) && !empty($n->value()))
                    return $n->value();
            }
        }

        // 3. Contact->Name
        $pn = $party->getContact();
        if(!empty($pn) && !empty($pn->getName())) {
            $pnn = $pn->getName();
            if(!empty($pnn->value()))
                return $pnn->value();
        }

        // fallback: address string
        return PostalAddress::get_postal_address_string($party->getPostalAddress());
    }

    private static function get_customer_supplier_party_string(null|SupplierPartyType|CustomerPartyType $party) : ?string {
        if (empty($party))
            return null;

        $p = self::get_party_string($party->getParty());
        if(!empty($p))
            return $p;

        if($party instanceof SupplierPartyType) {
            $c = self::get_contact_string($party->getDespatchContact());
            if(!empty($c)) return $c;
            $c = self::get_contact_string($party->getSellerContact());
            if(!empty($c)) return $c;
            $c = self::get_contact_string($party->getAccountingContact());
            if(!empty($c)) return $c;
        } else if($party instanceof CustomerPartyType) {
            $c = self::get_contact_string($party->getDeliveryContact());
            if(!empty($c)) return $c;
            $c = self::get_contact_string($party->getBuyerContact());
            if(!empty($c)) return $c;
            $c = self::get_contact_string($party->getAccountingContact());
            if(!empty($c)) return $c;
        }

        return null;
    }

    public static function add_additional_attributes(DtransDocument $document, DespatchAdvice|ReceiptAdvice $object): void
    {
        $party = $object->getDespatchSupplierParty();
        $document->set_despatch_supplier_party(self::get_customer_supplier_party_string($party));

        $party = $object->getDeliveryCustomerParty();
        $document->set_deliver_customer_party(self::get_customer_supplier_party_string($party));

        $party = $object->getBuyerCustomerParty();
        $document->set_buyer_customer_party(self::get_customer_supplier_party_string($party));

        $party = $object->getSellerSupplierParty();
        $document->set_seller_supplier_party(self::get_customer_supplier_party_string($party));

        if($object instanceof DespatchAdvice) {
            $party = $object->getOriginatorCustomerParty();
            $document->set_originator_customer_party(self::get_customer_supplier_party_string($party));
        }
    }
}