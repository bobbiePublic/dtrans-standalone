<?php

namespace Dtrans\core\providers\ubl\extractors;

use Dtrans\core\providers\ubl\UBLTools;
use UBL\cac\AddressType;
use UBL\cac\LocationCoordinateType;
use UBL\cac\LocationType;

abstract class PostalAddress {
    public static function get_postal_address_string(?AddressType $postalAddress) : ?string {
        if(empty($postalAddress))
            return null;

        // 1. AddressLine[n]
        $al = $postalAddress->getAddressLine();
        if(!empty($al)) {
            $res = '';
            foreach ($al as $line) {
                if (!empty($line) && !empty($line->getLine()) && !empty($line->getLine()->value())) {
                    if(!empty($res))
                        $res .= ', ';
                    $res .= trim($line->getLine()->value());
                }
            }

            if(!empty($res))
                return $res;
        }

        // build address string
        $addr = '';

        // building name + floor + room
        if(!empty($postalAddress->getBuildingName()) && !empty($postalAddress->getBuildingName()->value())) {
            $addr .= trim($postalAddress->getBuildingName()->value());

            if(!empty($postalAddress->getFloor()) &&!empty($postalAddress->getFloor()->value()))
                $addr .= ' ' . trim($postalAddress->getFloor()->value());

            if(!empty($postalAddress->getRoom()) &&!empty($postalAddress->getRoom()->value()))
                $addr .= ' ' . trim($postalAddress->getRoom()->value());
        }

        // street name or additional street name + building number
        if(!empty($postalAddress->getStreetName()) && !empty($postalAddress->getStreetName()->value())) {
            if(!empty($addr))
                $addr .= ', ';

            $streetName = trim($postalAddress->getStreetName()->value());
            if(!empty($postalAddress->getBuildingNumber()) && !empty($postalAddress->getBuildingNumber()->value()))
                $streetName .= ' ' . trim($postalAddress->getBuildingNumber()->value());
            $addr .= trim($streetName);
        } else if(!empty($postalAddress->getAdditionalStreetName()) && !empty($postalAddress->getAdditionalStreetName()->value())) {
            $streetName = trim($postalAddress->getAdditionalStreetName()->value());
            if(!empty($postalAddress->getBuildingNumber()) && !empty($postalAddress->getBuildingNumber()->value()))
                $streetName .= ' ' . trim($postalAddress->getBuildingNumber()->value());
            $addr .= trim($streetName);
        }

        $city = '';
        if(!empty($postalAddress->getPostalZone()) && !empty($postalAddress->getPostalZone()->value())) {
            if(!empty($city))
                $city .= ' ';
            $city .= trim($postalAddress->getPostalZone()->value());
        }
        if(!empty($postalAddress->getCityName()) && !empty($postalAddress->getCityName()->value())) {
            if(!empty($city))
                $city .= ' ';
            $city = trim($postalAddress->getCityName()->value());
        }
        if(!empty($postalAddress->getCitySubdivisionName()) && !empty($postalAddress->getCitySubdivisionName()->value())) {
            if(!empty($city))
                $city .= ' ';
            $city .= trim($postalAddress->getCitySubdivisionName()->value());
        }
        if(!empty($city)) {
            if(!empty($addr))
                $addr .= ', ';
            $addr .= trim($city);
        }

        if(!empty($postalAddress->getDistrict()) && !empty($postalAddress->getDistrict()->value())) {
            if(!empty($addr))
                $addr .= ', ';
            $addr .= trim($postalAddress->getDistrict()->value());
        }

        if(!empty($postalAddress->getCountrySubentity()) && !empty($postalAddress->getCountrySubentity()->value())) {
            if(!empty($addr))
                $addr .= ', ';
            $addr .= trim($postalAddress->getCountrySubentity()->value());
        }

        if(!empty($postalAddress->getCountry()) && !empty($postalAddress->getCountry()->getName()) && !empty($postalAddress->getCountry()->getName()->value())) {
            if(!empty($addr))
                $addr .= ', ';
            $addr .= trim($postalAddress->getCountry()->getName()->value());
        }

        if(!empty($postalAddress->getRegion()) && !empty($postalAddress->getRegion()->value())) {
            if(!empty($addr))
                $addr .= ', ';
            $addr .= trim($postalAddress->getRegion()->value());
        }

        return $addr != '' ? $addr : null;
    }


    public static function get_location_coordinate_string(?LocationCoordinateType $coords) : ?string {
        return UBLTools::get_location_coordinate_string($coords);
    }

    public static function get_location_string(?LocationType $location): ?string
    {
        if (empty($location))
            return null;

        // Location->Address
        $addr = $location->getAddress();
        $address = PostalAddress::get_postal_address_string($addr);
        if(!empty($address)) return $address;

        // Location->LocationCoordinate[1..n]
        if (!empty($location->getLocationCoordinate())) {
            foreach ($location->getLocationCoordinate() as $loc) {
                $locationCoordinate = self::get_location_coordinate_string($loc);
                if (!empty($locationCoordinate))
                    return $locationCoordinate;
            }
        }

        // Location->Address->LocationCoordinate[1..n]
        if (!is_null($addr) && !empty($addr->getLocationCoordinate()))
            foreach ($addr->getLocationCoordinate() as $loc) {
                $locationCoordinate = self::get_location_coordinate_string($loc);
                if (!empty($locationCoordinate))
                    return $locationCoordinate;
            }

        return null;
    }
}