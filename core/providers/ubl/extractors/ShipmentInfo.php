<?php

namespace Dtrans\core\providers\ubl\extractors;

use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\types\DtransDocument;
use UBL\cac\Shipment;
use UBL\cac\ShipmentStage;
use UBL\DespatchAdvice;
use UBL\ReceiptAdvice;

abstract class ShipmentInfo
{

    private static function get_shipping_actual_string(?Shipment $s) : ?string {
        if (empty($s))
            return null;

        $d = $s->getDelivery();
        if(empty($d))
            return null;

        return empty($d->getActualDeliveryDate()) ? null : $d->getActualDeliveryDate()->format("Y-m-d");
    }

    private static function get_shipping_estimated_start_string(?Shipment $s): ?string
    {
        if (empty($s))
            return null;

        $d = $s->getDelivery();
        if(empty($d))
            return null;

        $eDP = $d->getEstimatedDeliveryPeriod();
        if(empty($eDP))
            return null;

        $sD = $eDP->getStartDate();
        if (!empty($sD))
            return $sD->format("Y-m-d");
        return null;
    }

    private static function get_shipping_estimated_end_string(?Shipment $s): ?string
    {
        if (empty($s))
            return null;

        $d = $s->getDelivery();
        if (empty($d))
            return null;

        $eDP = $d->getEstimatedDeliveryPeriod();
        if (empty($eDP))
            return null;

        $eD = $eDP->getEndDate();
        if (!empty($eD))
            return $eD->format("Y-m-d");
        else
            return null;
    }

    private static function get_license_plate_id_from_shipment_stage(?ShipmentStage $stage) : ?string
    {
        if(empty($stage)) return null;

        $means = $stage->getTransportMeans();
        if(empty($means)) return null;

        $road = $means->getRoadTransport();
        if(empty($road)) return null;

        $plate = $road->getLicensePlateID();
        if(!empty($plate) && !empty($plate->value())) return $plate->value();

        return null;
    }

    private static function get_license_plate_id_from_shipment(?Shipment $shipment) : ?string
    {
        if(empty($shipment)) return null;

        $stages = $shipment->getShipmentStage();
        if(empty($stages)) return null;

        $stage = $stages[array_key_last($stages)];
        if(empty($stage)) return null;

        $plate = self::get_license_plate_id_from_shipment_stage($stage);
        if(!empty($plate)) return $plate;

        return null;
    }

    private static function get_driver_name_from_shipment(?Shipment $shipment) : ?string
    {
        if(empty($shipment)) return null;

        $stages = $shipment->getShipmentStage();
        if(empty($stages)) return null;

        $stage = $stages[array_key_last($stages)];
        if(empty($stage)) return null;
        $drivers = $stage->getDriverPerson();
        if(empty($drivers)) return null;

        foreach ($drivers as $driver) {
            if(empty($driver->getFamilyName()->value()) && empty($driver->getFirstName()->value()))
                continue;
            $name = "";
            if(!empty($driver->getFirstName()->value()))
                $name = $driver->getFirstName()->value();
            if(!empty($driver->getMiddleName()->value()))
                $name .= ' ' . $driver->getMiddleName()->value();
            if(!empty($driver->getFamilyName()->value()))
                $name .= ' ' . $driver->getFamilyName()->value();
            else if(!empty($driver->getOtherName()->value()))
                $name .= ' ' . $driver->getOtherName()->value();
            if(!empty($driver->getNameSuffix()->value()))
                $name .= ' ' . $driver->getNameSuffix()->value();

            $name = trim($name);
            if(!empty($name))
                return $name;
        }

        return null;
    }

    private static function get_main_carrier_party(?Shipment $shipment) : ?string {
        if(empty($shipment)) return null;
        $delivery = $shipment->getDelivery();
        if(empty($delivery)) return null;
        $cp = $delivery->getCarrierParty();
        if(empty($cp)) return null;
        return PartyInfo::get_party_string($cp);
    }

    private static function get_sub_carrier_party(?Shipment $shipment) : ?string
    {
        if(empty($shipment)) return null;

        $stages = $shipment->getShipmentStage();
        if(empty($stages)) return null;

        $stage = $stages[array_key_last($stages)];
        if(empty($stage)) return null;

        $cps = $stage->getCarrierParty();
        if(empty($cps)) return null;

        foreach ($cps as $cp) {
            $ps = PartyInfo::get_party_string($cp);
            if(!empty($ps))
                return $ps;
        }

        return null;
    }

    private static function get_delivery_location(?Shipment $shipment) : ?string {
        if(empty($shipment)) return null;
        $delivery = $shipment->getDelivery();
        if(empty($delivery)) return null;
        $loc = $delivery->getDeliveryLocation();
        if(empty($loc)) return null;
        return PostalAddress::get_location_string($loc);
    }

    private static function get_alternative_delivery_location(?Shipment $shipment) : ?string {
        if(empty($shipment)) return null;
        $delivery = $shipment->getDelivery();
        if(empty($delivery)) return null;
        $loc = $delivery->getAlternativeDeliveryLocation();
        if(empty($loc)) return null;
        return PostalAddress::get_location_string($loc);
    }

    public static function add_additional_attributes(DtransDocument $document, DespatchAdvice|ReceiptAdvice $object): void {
        $shipment = $object->getShipment();
        $document->set_shipping_actual(self::get_shipping_actual_string($shipment));
        $document->set_shipping_estimated_start(self::get_shipping_estimated_start_string($shipment));
        $document->set_shipping_estimated_end(self::get_shipping_estimated_end_string($shipment));
        $document->setLicensePlate(self::get_license_plate_id_from_shipment($shipment));
        $document->setDriverName(self::get_driver_name_from_shipment($shipment));
        $document->set_main_carrier_party(self::get_main_carrier_party($shipment));
        $document->set_sub_carrier_party(self::get_sub_carrier_party($shipment));
        $document->set_delivery_location(self::get_delivery_location($shipment));
        $document->set_alternative_delivery_location(self::get_alternative_delivery_location($shipment));
    }
}