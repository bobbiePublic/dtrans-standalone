<?php

namespace Dtrans\core\sanitization;

use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\helpers\UserFeedback;

class SanitizeBoolean extends SanitizeProxy
{

    public function __construct()
    {
        parent::__construct(ParameterTypesEnum::BOOLEAN);
    }

    public function sanitize($object, $default, ?string $parameter_name = null) /* : mixed */
    {
        if (strcmp("true", strtolower(trim($object))) === 0)
            return true;
        else if (strcmp("false", strtolower(trim($object))) === 0)
            return false;
        else {
            if (!is_null($parameter_name))
                UserFeedback::warning(ConstsStrings::CODE_API_PARAMETER_FAULTY_AND_IGNORED, $parameter_name);
            return $default;
        }
    }
}