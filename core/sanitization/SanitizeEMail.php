<?php

namespace Dtrans\core\sanitization;

use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\helpers\UserFeedback;

class SanitizeEMail extends SanitizeProxy
{

    public function __construct()
    {
        parent::__construct(ParameterTypesEnum::MAIL_ADDRESS);
    }

    public function sanitize($object, $default, ?string $parameter_name = null) /* : mixed */
    {
        if (filter_var($object, FILTER_VALIDATE_EMAIL))
            return $object;
        else {
            if (!is_null($parameter_name))
                UserFeedback::warning(ConstsStrings::CODE_API_MAIL_FAULTY_AND_IGNORED, $parameter_name);
            return $default;
        }
    }
}