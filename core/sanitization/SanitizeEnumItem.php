<?php

namespace Dtrans\core\sanitization;

use Dtrans\core\constants\ConstsApi;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\UserFeedback;

class SanitizeEnumItem extends SanitizeProxy
{

    protected ?array $parse_function = null;

    public function __construct(string $type, array $parse_function)
    {
        parent::__construct($type);
        $this->parse_function = $parse_function;
    }

    public function sanitize($object, $default, ?string $parameter_name = null) /* : mixed */
    {
        $default = is_null($default) ? null : array($default);

        $fun = $this->parse_function;
        if (empty($fun)) {
            DtransLogger::warning('Sanitizer for enum item has no parse function provided.', ['SanitizerType' => $this->type]);
            return $default;
        }

        if (is_string($object)) {
            // single value
            if (!str_contains($object, ConstsApi::MULTI_VALUE_SEPARATOR)) {
                $value = $fun($object);
                if (is_null($value)) {
                    if (!is_null($parameter_name))
                        UserFeedback::warning(ConstsStrings::CODE_API_PARAMETER_FAULTY_AND_IGNORED, $parameter_name);
                    return $default;
                }
                return array($value);
            }

            $list = explode(ConstsApi::MULTI_VALUE_SEPARATOR, $object);
            $array = array();
            foreach ($list as $item) {
                $value = $fun($item);
                if (is_null($value))
                    continue;

                $array[] = $value;
            }

            return $array;
        } else
            return $default;
    }
}