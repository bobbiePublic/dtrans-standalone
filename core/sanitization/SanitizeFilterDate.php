<?php

namespace Dtrans\core\sanitization;

use DateTime;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\helpers\UserFeedback;
use Throwable;

class SanitizeFilterDate extends SanitizeProxy
{

    public function __construct()
    {
        parent::__construct(ParameterTypesEnum::FILTER_DATE);
    }

    public function sanitize($object, $default, ?string $parameter_name = null) /* : mixed */
    {
        $converted = null;
        try {
            $date = new DateTime($object);
            $converted = $date->format("Y-m-d");
        } catch (Throwable $e) {
        }

        if (empty($converted)) {
            if (!is_null($parameter_name))
                UserFeedback::warning(ConstsStrings::CODE_API_PARAMETER_FAULTY_AND_IGNORED, $parameter_name);
            return $default;
        } else
            return $converted;
    }
}