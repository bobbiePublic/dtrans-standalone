<?php

namespace Dtrans\core\sanitization;

use Dtrans\core\enums\ParameterTypesEnum;

class SanitizeFilterString extends SanitizeProxy
{

    public function __construct()
    {
        parent::__construct(ParameterTypesEnum::FILTER_STRING);
    }

    public function sanitize($object, $default, ?string $parameter_name = null) /* : mixed */
    {
        if (is_string($object))
            return trim($object);
        else
            return $default;
    }
}