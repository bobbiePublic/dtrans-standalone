<?php

namespace Dtrans\core\sanitization;

use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\helpers\TimestampHelper;
use Dtrans\core\helpers\UserFeedback;

class SanitizeFilterTimestamp extends SanitizeProxy
{

    public function __construct()
    {
        parent::__construct(ParameterTypesEnum::FILTER_TIMESTAMP);
    }

    public function sanitize($object, $default, ?string $parameter_name = null) /* : mixed */
    {
        $converted = TimestampHelper::create_single_utc($object);
        if (empty($converted)) {
            if (!is_null($parameter_name))
                UserFeedback::warning(ConstsStrings::CODE_API_PARAMETER_FAULTY_AND_IGNORED, $parameter_name);
            return $default;
        } else
            return $converted;
    }
}