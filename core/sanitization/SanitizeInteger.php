<?php

namespace Dtrans\core\sanitization;

use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\helpers\UserFeedback;

class SanitizeInteger extends SanitizeProxy
{

    public function __construct()
    {
        parent::__construct(ParameterTypesEnum::INTEGER);
    }

    public function sanitize($object, $default, ?string $parameter_name = null) /* : mixed */
    {
        if (is_numeric($object) && !str_contains($object, '.'))
            return (int)$object;
        else {
            if (!is_null($parameter_name))
                UserFeedback::warning(ConstsStrings::CODE_API_PARAMETER_FAULTY_AND_IGNORED, $parameter_name);
            return $default;
        }
    }
}