<?php

namespace Dtrans\core\sanitization;

use Dtrans\core\constants\ConstsJson;
use Dtrans\core\constants\ConstsStrings;
use Dtrans\core\enums\ListviewColumnEnum;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\helpers\UserFeedback;

class SanitizeJsonDocumentAttribute extends SanitizeProxy
{

    public function __construct()
    {
        parent::__construct(ParameterTypesEnum::JSON_DOCUMENT_ATTRIBUTE);
    }

    public function sanitize($object, $default, ?string $parameter_name = null) /* : mixed */
    {
        if (!is_string($object)) return $default;
        switch (trim(strtolower($object))) {
            case strtolower(ConstsJson::JSON_DOCUMENT_CHAIN_ID):
                return ConstsJson::JSON_DOCUMENT_CHAIN_ID;
            case strtolower(ConstsJson::JSON_DOCUMENT_UUID):
                return ConstsJson::JSON_DOCUMENT_UUID;
            case strtolower(ConstsJson::JSON_DOCUMENT_PREVIOUS_VERSION_UUID):
                return ConstsJson::JSON_DOCUMENT_PREVIOUS_VERSION_UUID;
            case strtolower(ConstsJson::JSON_DOCUMENT_VERSION_STATUS):
                return ConstsJson::JSON_DOCUMENT_VERSION_STATUS;
            case strtolower(ConstsJson::JSON_DOCUMENT_CREATED_AT):
                return ConstsJson::JSON_DOCUMENT_CREATED_AT;
            case strtolower(ConstsJson::JSON_DOCUMENT_UPDATED_AT):
                return ConstsJson::JSON_DOCUMENT_UPDATED_AT;
            case strtolower(ConstsJson::JSON_DOCUMENT_REFERENCES):
                return ConstsJson::JSON_DOCUMENT_REFERENCES;
            case strtolower(ConstsJson::JSON_DOCUMENT_DATE):
                return ConstsJson::JSON_DOCUMENT_DATE;
            case strtolower(ConstsJson::JSON_DOCUMENT_DATE_SHIPPING_ESTIMATED_START):
                return ConstsJson::JSON_DOCUMENT_DATE_SHIPPING_ESTIMATED_START;
            case strtolower(ConstsJson::JSON_DOCUMENT_DATE_SHIPPING_ESTIMATED_END):
                return ConstsJson::JSON_DOCUMENT_DATE_SHIPPING_ESTIMATED_END;
            case strtolower(ConstsJson::JSON_DOCUMENT_DATE_SHIPPING_ACTUAL):
                return ConstsJson::JSON_DOCUMENT_DATE_SHIPPING_ACTUAL;
            case strtolower(ConstsJson::JSON_DOCUMENT_DESPATCH_SUPPLIER_PARTY):
                return ConstsJson::JSON_DOCUMENT_DESPATCH_SUPPLIER_PARTY;
            case strtolower(ConstsJson::JSON_DOCUMENT_DELIVERY_CUSTOMER_PARTY):
                return ConstsJson::JSON_DOCUMENT_DELIVERY_CUSTOMER_PARTY;
            case strtolower(ConstsJson::JSON_DOCUMENT_BUYER_CUSTOMER_PARTY):
                return ConstsJson::JSON_DOCUMENT_BUYER_CUSTOMER_PARTY;
            case strtolower(ConstsJson::JSON_DOCUMENT_SELLER_SUPPLIER_PARTY):
                return ConstsJson::JSON_DOCUMENT_SELLER_SUPPLIER_PARTY;
            case strtolower(ConstsJson::JSON_DOCUMENT_ORIGINATOR_COSTUMER_PARTY):
                return ConstsJson::JSON_DOCUMENT_ORIGINATOR_COSTUMER_PARTY;
            case strtolower(ConstsJson::JSON_DOCUMENT_ARCHIVED_STATUS):
                return ConstsJson::JSON_DOCUMENT_ARCHIVED_STATUS;
            case strtolower(ConstsJson::JSON_DOCUMENT_HANDLING_STATUS):
                return ConstsJson::JSON_DOCUMENT_HANDLING_STATUS;

            default:
                if (!is_null($parameter_name))
                    UserFeedback::warning(ConstsStrings::CODE_API_PARAMETER_FAULTY_AND_IGNORED, $parameter_name);
                return $default;
        }
    }

    public static function is_column_active(?string $object, array $columns) : bool
    {
        if(empty($object))
            return false;

        switch (trim(strtolower($object))) {
            case strtolower(ConstsJson::JSON_DOCUMENT_UUID):
                return true;
            case strtolower(ConstsJson::JSON_DOCUMENT_CHAIN_ID):
                return in_array(ListviewColumnEnum::CHAIN_ID, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_PREVIOUS_VERSION_UUID):
                return in_array(ListviewColumnEnum::DOCUMENT_PREDECESSOR, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_VERSION_STATUS):
                return in_array(ListviewColumnEnum::VERSION_STATUS, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_CREATED_AT):
                return in_array(ListviewColumnEnum::CREATED_AT, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_UPDATED_AT):
                return in_array(ListviewColumnEnum::UPDATED_AT, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_REFERENCES):
                return in_array(ListviewColumnEnum::UBL_REFERENCES, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_DATE):
                return in_array(ListviewColumnEnum::UBL_DATE_DOCUMENT, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_DATE_SHIPPING_ESTIMATED_START):
                return in_array(ListviewColumnEnum::UBL_DATE_SHIPPING_ESTIMATED_START, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_DATE_SHIPPING_ESTIMATED_END):
                return in_array(ListviewColumnEnum::UBL_DATE_SHIPPING_ESTIMATED_END, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_DATE_SHIPPING_ACTUAL):
                return in_array(ListviewColumnEnum::UBL_DATE_SHIPPING_ACTUAL, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_DESPATCH_SUPPLIER_PARTY):
                return in_array(ListviewColumnEnum::UBL_PARTY_DESPATCH_SUPPLIER, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_DELIVERY_CUSTOMER_PARTY):
                return in_array(ListviewColumnEnum::UBL_PARTY_DELIVERY_CUSTOMER, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_BUYER_CUSTOMER_PARTY):
                return in_array(ListviewColumnEnum::UBL_PARTY_BUYER_CUSTOMER, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_SELLER_SUPPLIER_PARTY):
                return in_array(ListviewColumnEnum::UBL_PARTY_SELLER_SUPPLIER, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_ORIGINATOR_COSTUMER_PARTY):
                return in_array(ListviewColumnEnum::UBL_PARTY_ORIGINATOR_CUSTOMER, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_ARCHIVED_STATUS):
                return in_array(ListviewColumnEnum::ARCHIVED_STATUS, $columns);
            case strtolower(ConstsJson::JSON_DOCUMENT_HANDLING_STATUS):
                return in_array(ListviewColumnEnum::HANDLING_STATUS, $columns);
            default:
                return false;
        }
    }
}