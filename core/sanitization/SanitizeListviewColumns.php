<?php

namespace Dtrans\core\sanitization;

use Dtrans\core\constants\ConstsApi;
use Dtrans\core\enums\ListviewColumnEnum;
use Dtrans\core\enums\ParameterTypesEnum;

class SanitizeListviewColumns extends SanitizeProxy
{

    public function __construct()
    {
        parent::__construct(ParameterTypesEnum::LISTVIEW_COLUMNS);
    }

    public function sanitize($object, $default, ?string $parameter_name = null) : array
    {
        if(empty($object))
            return $default;
        $columns = array();

        $ts = trim($object);
        if($ts == "*") {
            foreach (ListviewColumnEnum::cases() as $case)
                $columns[] = $case;

            return $columns;
        }

        $c = explode(ConstsApi::MULTI_VALUE_SEPARATOR, $ts);

        foreach($c as $val) {
            $iv = intval($val);
            $value = ListviewColumnEnum::tryFrom($iv);
            if(empty($value) || in_array($value, $columns)) continue;
            $columns[] = $value;
        }

        if(empty($columns))
            return $default;
        else
            return $columns;
    }
}