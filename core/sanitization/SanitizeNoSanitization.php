<?php

namespace Dtrans\core\sanitization;

class SanitizeNoSanitization extends SanitizeProxy
{
    public function sanitize($object, $default, ?string $parameter_name = null) /* : mixed */
    {
        return $object;
    }
}