<?php

namespace Dtrans\core\sanitization;

use Dtrans\core\helpers\SanitizeHelper;

abstract class SanitizeProxy
{
    public string $type;

    public abstract function sanitize($object, $default, ?string $parameter_name = null);

    function __construct(string $type)
    {
        $this->type = $type;
        SanitizeHelper::register_proxy($type, $this);
    }
}