<?php

namespace Dtrans\core\sanitization;

class SanitizeStringWhitespaces extends SanitizeProxy
{
    public function sanitize($object, $default, ?string $parameter_name = null) /* : mixed */
    {
        if (is_string($object)) {
            $object = preg_replace('/\s*\R\s*/', "", trim($object));
            if (empty($object))
                return $default;

            return $object;
        } else
            return $default;
    }
}