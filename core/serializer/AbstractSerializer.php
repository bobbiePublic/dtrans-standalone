<?php

namespace Dtrans\core\serializer;

abstract class AbstractSerializer {
    public abstract function serialize($object): ?string;
    public abstract function deserialize($object);
}