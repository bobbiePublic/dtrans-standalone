<?php

namespace Dtrans\core\serializer;

use Dtrans\core\helpers\UserFeedback;

class JsonSerializer extends AbstractSerializer {

    public function serialize($object, bool $flush_reports = true): ?string
    {
        if(!is_array($object)) return null;
        header('Content-Type: ' . 'application/json; charset=UTF-8');

        // add user reporting to json
        if($flush_reports && UserFeedback::has_data())
            $object['_reporting'] = UserFeedback::flush();

        return json_encode($object);
    }

    public function deserialize($object)
    {
        return json_decode($object, true, 100);
    }

    public function flush_reports(): string
    {
        header('Content-Type: ' . 'application/json; charset=UTF-8');

        $object = [];

        // add user reporting to json
        if (UserFeedback::has_data())
            $object['_reporting'] = UserFeedback::flush();

        return json_encode($object);
    }
}