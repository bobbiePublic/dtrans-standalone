<?php

namespace Dtrans\core\types;

use Dtrans\core\enums\RequestMethodsEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\helpers\PathResolver;

class ApiRequest {
    private string $method;
    private string $path;
    private string $data;
    private array $get;

    public function __construct()
    {
        $this->path = PathResolver::resolve();
        $this->method = RequestMethodsEnum::parse($_SERVER['REQUEST_METHOD']);

        // parse core
        $d = file_get_contents('php://input');
        $this->data = is_string($d) ? $d : null;

        // get params
        $this->get = array();

        // yes we manually parse the query params as $_GET 'removes' some characters like +
        $query = $_SERVER['QUERY_STRING'];
        if (empty($query)) // no parameters provided
            return;

        $params = explode('&', $query);
        foreach ($params as $param) {
            $p = explode('=', $param);
            if (!str_contains($param, '=') || count($p) != 2) {
                DtransLogger::debug("Ignoring not parsable param.", ['param' => $param, 'query' => $query]);
                continue;
            }

            $key = strtolower($p[0]);
            $this->get[$key] = rawurldecode($p[1]);
        }

        // old way
        //foreach ($_GET as $key => $value)
        //$this->get[strtolower($key)] = $value;
    }

    public function request_method_is_valid() : bool {
        return strcmp($this->method, RequestMethodsEnum::INVALID) !== 0;
    }

    public function get_request_method() : string {
        return $this->method;
    }

    public function get_data() : string {
        return $this->data;
    }

    public function get_path() :?string {
        return $this->path;
    }

    public function has_get_param($name) : bool {
        return array_key_exists($name, $this->get);
    }

    public function get_param($name) : ?string {
        if(array_key_exists($name, $this->get))
            return $this->get[$name];
        else
            return null;
    }
}