<?php
namespace Dtrans\core\types;

use Dtrans\core\enums\ArchivedStatusEnum;
use Dtrans\core\enums\HandlingStatusEnum;

class DtransDocument
{
    const CHAIN_LENGTH = 128;
    const UUID_LENGTH = 96;
    const PARTY_STRING_LENGTH = 128;
    const ADDRESS_STRING_LENGTH = 128;

    private int $internal_id;
    private string $uuid;
    private string $type;
    private ?string $file_id;
    private string $file_name;
    private int $file_size;
    private bool $in_dtrans_queue = false;
    private string $handling_status = HandlingStatusEnum::DA_BY_DESPATCHER_OR_SELLER;
    private string $archived_status = ArchivedStatusEnum::ACTIVE;
    private ?string $created_at;
    private ?string $updated_at;

    /* UBL STUFF */
    private ?string $previous_version_uuid = null;
    private ?string $date_document = null;
    private ?string $document_references = null;
    private ?string $date_shipping_estimated_start = null;
    private ?string $date_shipping_estimated_end = null;
    private ?string $date_shipping_actual = null;
    private ?string $despatch_supplier_party = null;
    private ?string $delivery_customer_party = null;
    private ?string $buyer_customer_party = null;
    private ?string $seller_supplier_party = null;
    private ?string $originator_customer_party = null;
    private ?string $main_carrier_party = null;
    private ?string $sub_carrier_party = null;
    private ?string $driver_name = null;
    private ?string $delivery_location = null;
    private ?string $alternative_delivery_location = null;
    private bool $rejected = false;
    private ?string $licensePlate = null;
    private ?string $itemDescription = null;
    private ?float $itemWeights = null;
    private ?UBLProjectReference $reference = null;

    /* PDF STUFF  */
    private ?int $pdf_embedded_id = null;
    private ?string $pdf_embedded_uuid = null;

    public function __construct(string $type, ?string $file_id, string $file_name, int $file_size, string $uuid, ?string $created_at = null, ?string $updated_at = null, int $internal_id = -1)
    {
        $this->type = $type;
        $this->internal_id = $internal_id;
        $this->set_file_id($file_id);
        $this->file_name = $file_name;
        $this->file_size = $file_size;
        $this->set_uuid($uuid);
        $this->created_at = $created_at;
        $this->updated_at = $updated_at;
    }

    public function getDriverName(): ?string
    {
        return $this->driver_name;
    }

    public function setDriverName(?string $driverName): void
    {
        if(!empty($driverName))
            $driverName = substr($driverName, 0, 128);

        $this->driver_name = $driverName;
    }

    public function getLicensePlate(): ?string
    {
        return $this->licensePlate;
    }

    public function setLicensePlate(?string $licensePlate): void
    {
        if(!empty($licensePlate))
            $licensePlate = substr($licensePlate, 0, 32);

        $this->licensePlate = $licensePlate;
    }

    public function getItemDescription(): ?string
    {
        return $this->itemDescription;
    }

    public function setItemDescription(?string $itemDescription): void
    {
        $this->itemDescription = $itemDescription;
    }

    public function getItemWeights(): ?float
    {
        return $this->itemWeights;
    }

    public function setItemWeights(?float $itemAmount): void
    {
        $this->itemWeights = $itemAmount;
    }

    public function getReference(): ?UBLProjectReference
    {
        return $this->reference;
    }

    public function setReference(?UBLProjectReference $reference): void
    {
        $this->reference = $reference;
    }

    public function get_pdf_embedded_id(): ?int
    {
        return $this->pdf_embedded_id;
    }

    public function get_pdf_embedded_uuid(): ?string
    {
        return $this->pdf_embedded_uuid;
    }

    public function set_pdf_embedded_id(?int $id): void
    {
        $this->pdf_embedded_id = $id;
    }

    public function set_pdf_embedded_uuid(?string $uuid): void
    {
        $this->pdf_embedded_uuid = $uuid;
    }

    public function get_internal_id(): int
    {
        return $this->internal_id;
    }

    public function get_dtrans_id(): ?string
    {
        return $this->file_id;
    }

    public function set_file_id(?string $file_id): void
    {
        if(!empty($file_id))
            $file_id = substr($file_id, 0, self::CHAIN_LENGTH);

        $this->file_id = $file_id;
    }

    public function get_previous_version_uuid() : ?string {
        return $this->previous_version_uuid;
    }

    public function set_previous_version_uuid($previous_version_uuid) : void {
        if(!empty($previous_version_uuid))
            $previous_version_uuid = substr($previous_version_uuid, 0, self::UUID_LENGTH);

        $this->previous_version_uuid = $previous_version_uuid;
    }

    public function set_internal_id(int $id) : void {
        $this->internal_id = $id;
    }

    public function get_uuid() : string {
        return $this->uuid;
    }

    public function set_uuid($uuid) : void {
        if(!empty($uuid))
            $uuid = substr($uuid, 0, self::UUID_LENGTH);

        $this->uuid = $uuid;
    }

    public function get_dtrans_queue_status(): bool
    {
        return $this->in_dtrans_queue;
    }

    public function set_in_dtrans_queue($in_dtrans_queue): void
    {
        $this->in_dtrans_queue = $in_dtrans_queue;
    }

    public function get_handling_status() : string
    {
        return $this->handling_status;
    }

    public function set_handling_status($handling_status) : void
    {
        $this->handling_status = $handling_status;
    }

    public function get_archived_status() : string
    {
        return $this->archived_status;
    }

    public function set_archived_status($archived_status) : void
    {
        $this->archived_status = $archived_status;
    }

    public function get_created_at() : ?string
    {
        return $this->created_at;
    }

    public function get_updated_at() : ?string
    {
        return $this->updated_at;
    }

    public function set_update_at(?string $value)
    {
        $this->updated_at = $value;
    }

    public function get_type(): string
    {
        return $this->type;
    }

    public function get_file_name(): string
    {
        return $this->file_name;
    }

    public function get_file_size(): int
    {
        return $this->file_size;
    }

    public function get_document_references(): ?string
    {
        return $this->document_references;
    }

    public function set_document_references(?string $document_references): void
    {
        $this->document_references = $document_references;
    }

    public function get_despatch_supplier_party(): ?string
    {
        return $this->despatch_supplier_party;
    }

    public function set_despatch_supplier_party(?string $despatch_supplier_party): void
    {
        if(!empty($despatch_supplier_party))
            $despatch_supplier_party = substr($despatch_supplier_party, 0, self::PARTY_STRING_LENGTH);

        $this->despatch_supplier_party = $despatch_supplier_party;
    }

    public function get_delivery_location(): ?string
    {
        return $this->delivery_location;
    }

    public function set_delivery_location(?string $location): void
    {
        if(!empty($location))
            $location = substr($location, 0, self::ADDRESS_STRING_LENGTH);

        $this->delivery_location = $location;
    }

    public function get_alternative_delivery_location(): ?string
    {
        return $this->alternative_delivery_location;
    }

    public function set_alternative_delivery_location(?string $location): void
    {
        if(!empty($location))
            $location = substr($location, 0, self::ADDRESS_STRING_LENGTH);

        $this->alternative_delivery_location = $location;
    }


    public function get_delivery_customer_party(): ?string
    {
        return $this->delivery_customer_party;
    }

    public function set_deliver_customer_party(?string $delivery_customer_party): void
    {
        if(!empty($delivery_customer_party))
            $delivery_customer_party = substr($delivery_customer_party, 0, self::PARTY_STRING_LENGTH);

        $this->delivery_customer_party = $delivery_customer_party;
    }

    public function get_buyer_customer_party(): ?string
    {
        return $this->buyer_customer_party;
    }

    public function set_buyer_customer_party(?string $buyer_customer_party): void
    {
        if(!empty($buyer_customer_party))
            $buyer_customer_party = substr($buyer_customer_party, 0, self::PARTY_STRING_LENGTH);

        $this->buyer_customer_party = $buyer_customer_party;
    }

    public function get_seller_supplier_party(): ?string
    {
        return $this->seller_supplier_party;
    }

    public function set_seller_supplier_party(?string $seller_supplier_party): void
    {
        if(!empty($seller_supplier_party))
            $seller_supplier_party = substr($seller_supplier_party, 0, self::PARTY_STRING_LENGTH);

        $this->seller_supplier_party = $seller_supplier_party;
    }

    public function get_originator_customer_party(): ?string
    {
        return $this->originator_customer_party;
    }

    public function set_originator_customer_party(?string $originator_customer_party): void
    {
        if(!empty($originator_customer_party))
            $originator_customer_party = substr($originator_customer_party, 0, self::PARTY_STRING_LENGTH);

        $this->originator_customer_party = $originator_customer_party;
    }

    public function get_main_carrier_party(): ?string
    {
        return $this->main_carrier_party;
    }

    public function set_main_carrier_party(?string $party): void
    {
        if(!empty($party))
            $party = substr($party, 0, self::PARTY_STRING_LENGTH);

        $this->main_carrier_party = $party;
    }

    public function get_sub_carrier_party(): ?string
    {
        return $this->sub_carrier_party;
    }

    public function set_sub_carrier_party(?string $party): void
    {
        if(!empty($party))
            $party = substr($party, 0, self::PARTY_STRING_LENGTH);

        $this->sub_carrier_party = $party;
    }

    public function get_rejected(): bool
    {
        return $this->rejected;
    }

    public function set_rejected(bool $rejected) {
        $this->rejected = $rejected;
    }

    public function get_date_document() : ?string
    {
        return $this->date_document;
    }

    public function set_date_document(?string $value)
    {
        $this->date_document = $value;
    }

    public function get_shipping_estimated_start(): ?string
    {
        return $this->date_shipping_estimated_start;
    }

    public function set_shipping_estimated_start(?string $value): void
    {
        $this->date_shipping_estimated_start = $value;
    }

    public function get_shipping_estimated_end(): ?string
    {
        return $this->date_shipping_estimated_end;
    }

    public function set_shipping_estimated_end(?string $value): void
    {
        $this->date_shipping_estimated_end = $value;
    }

    public function get_shipping_actual() : ?string {
        return $this->date_shipping_actual;
    }

    public function set_shipping_actual(?string $value) : void {
        $this->date_shipping_actual = $value;
    }
}