<?php

namespace Dtrans\core\types;

class UBLLocation
{
    protected ?string $address = null;

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }


    protected ?string $locationCoords = null;

    public function getLocationCoords(): ?string
    {
        return $this->locationCoords;
    }

    public function setLocationCoords(?string $locationCoords): void
    {
        $this->locationCoords = $locationCoords;
    }
}