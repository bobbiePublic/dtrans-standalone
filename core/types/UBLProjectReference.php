<?php

namespace Dtrans\core\types;

class UBLProjectReference extends UBLLocation
{
    public function referenceAlreadyExists(): bool
    {
        return !is_null($this->existingReferenceID);
    }

    protected ?int $existingReferenceID = null;

    public function getExistingReferenceID(): ?int
    {
        return $this->existingReferenceID;
    }

    public function setExistingReferenceID(?int $existingReferenceID): void
    {
        $this->existingReferenceID = $existingReferenceID;
    }

    protected ?string $salesID = null;

    public function getSalesID(): ?string
    {
        return $this->salesID;
    }

    public function setSalesID(?string $salesID): void
    {
        $this->salesID = $salesID;
    }

    protected ?string $name = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    protected ?string $location_source = null;

    public function getLocationSource(): ?string
    {
        return $this->location_source;
    }

    public function setLocationSource(?string $location_source): void
    {
        $this->location_source = $location_source;
    }

}