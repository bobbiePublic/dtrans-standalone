<?php

namespace Dtrans\core\types;

class UserData
{
    // name / contact
    public ?string $firstName = null;
    public ?string $lastName = null;
    public ?string $phone = null;
    public ?string $mobile = null;
    public ?string $fax = null;

    public ?string $mail = null;

    // company info
    public ?string $companyName = null;
    public ?string $companyPosition = null;

    //ADDRESS
    public ?string $addressStreet = null;
    public ?string $addressCity = null;
    public ?string $addressZip = null;
    public ?string $addressRegion = null;
    public ?string $addressCountry = null;
    public ?string $addressFull = null;

    public ?string $dn = null;
    public ?string $sessionCreatedAt = null;
    public ?string $sessionExpiresAt = null;
}