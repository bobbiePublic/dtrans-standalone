#!/bin/bash
set -eu
# Note: This is executed in gitlab runner environment, i.e. in /home/gitlab-runner/bla/blub by gitlab-runner user
# gitlab will checkout recent code for us there, but we need it in correct location, so that requires some manual lifting
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DTRANS_DIR=$MY_DIR/../../
echo "expecting dtrans-standalone at $DTRANS_DIR"
cd $DTRANS_DIR

if [ $CI_COMMIT_REF_NAME != "master" ]; then
    echo "skipping install step for branch $CI_COMMIT_REF_NAME"
    exit 0
fi

/bin/bash deploy/deploy-stage.sh

if [[ -v UPSTREAM_COMMIT_MESSAGE && ! -z "${UPSTREAM_COMMIT_MESSAGE:-}" ]]; then
    MESSAGE=$UPSTREAM_COMMIT_MESSAGE
else
    MESSAGE=`git log -1 --pretty=%B`
fi
if [[ -v UPSTREAM_AUTHOR && ! -z "${UPSTREAM_AUTHOR:-}" ]]; then
    AUTHOR=$UPSTREAM_AUTHOR
else
    AUTHOR=`git log -1 --pretty=%an`
fi
if [[ -v UPSTREAM_AUTHOR_MAIL && ! -z "${UPSTREAM_AUTHOR_MAIL:-}" ]]; then
    AUTHORMAIL=$UPSTREAM_AUTHOR_MAIL
else
    AUTHORMAIL=`git log -1 --pretty=%ae`
fi

# preparing target git
mkdir -p ../dtrans-standalone.tmp/
cd ../dtrans-standalone.tmp/
GITTARGET="dtrans-standalone-prod-$CI_COMMIT_REF_NAME"
if [[ ! -d "$GITTARGET" ]]; then
    echo "Cloning prod git"
    git clone git@gitlab.com:bobbieDEU/dtrans-standalone-prod.git "$GITTARGET"
fi
cd "$GITTARGET"
echo "Dir:"
pwd -P
echo "Git fetch"
git fetch
echo "Git checkout"
git checkout $CI_COMMIT_REF_NAME
git reset --hard $CI_COMMIT_REF_NAME
echo "Git status"
git status

# copy new files into target git
cd ../..
echo "Rsync in "
pwd -P
rsync -a --stats --exclude README.md --exclude .git --del  dtrans-standalone/ "dtrans-standalone.tmp/$GITTARGET"
echo "git preparation"
cd "dtrans-standalone.tmp/$GITTARGET"
mv .gitignore-prod .gitignore
echo "echo git add etc"
pwd -P
git add -A
git add -f vendor/bobbie/module-ubl/schemas/generated/
git commit -m "$MESSAGE" --author "$AUTHOR \<$AUTHORMAIL\>"
git push
echo "Done"

exit 0
