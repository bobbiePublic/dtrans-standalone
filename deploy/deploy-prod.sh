#!/bin/bash
set -eu
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# shellcheck source=deploy/snippets/readenv.sh
. "$MY_DIR/snippets/readenv.sh"

echo "Updating git"
git fetch --all
git reset --hard "origin/master"

. ${DTRANS_DIR}deploy/snippets/update-env.sh
. ${DTRANS_DIR}deploy/snippets/update-db.sh

echo "done"
