#!/bin/bash
set -eu
MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DTRANS_DIR=$MY_DIR/../
cd $DTRANS_DIR

echo "composer update"
composer --no-progress --apcu-autoloader --optimize-autoloader --no-dev -n update

${DTRANS_DIR}deploy/generate-classes.sh

echo "done"
