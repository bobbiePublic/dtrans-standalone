#!/bin/bash
set -eu

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DTRANS_DIR=$MY_DIR/../
echo "expecting dtrans at $DTRANS_DIR"

echo "generate php classes from xsd"
MY_DIR="${DTRANS_DIR}vendor/bobbie/module-ubl"
. "$DTRANS_DIR"vendor/bobbie/module-ubl/deploy/post-composer.sh
