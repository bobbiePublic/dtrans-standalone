set -eu
umask 002
SNIP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DTRANS_DIR=$SNIP_DIR/../../
echo "expecting dtrans at $DTRANS_DIR"
cd "$DTRANS_DIR"

for i in /etc/dtrans/* ; do
    if [[ "$i" != /etc/dtrans/dkim-key ]] ; then
        echo "reading "$i
        source "$i"
    fi
done
