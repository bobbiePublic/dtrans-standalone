#!/bin/bash
set -eu
echo "Stop and disable router"
sudo systemctl stop dtrans.timer
sudo systemctl disable dtrans.timer
