echo "Setting local dtrans config"
#dtrans.ini
sed s/@@PATH_DKIM_PRIVATE_KEY@@/"${PATH_DKIM_PRIVATE_KEY//\//\\/}"/g ${DTRANS_DIR}config/dtrans.placeholder.ini > ${DTRANS_DIR}config/dtrans.ini.tmp

sed -i s/@@SELECTOR@@/"$DKIM_SELECTOR"/g ${DTRANS_DIR}config/dtrans.ini.tmp
sed -i s/@@SERVER_FQDN@@/"$SERVER_FQDN"/g ${DTRANS_DIR}config/dtrans.ini.tmp
sed -i s/@@SMTP_HOST@@/"$SMTP_HOST"/g ${DTRANS_DIR}config/dtrans.ini.tmp
sed -i s/@@SMTP_PORT@@/"$SMTP_PORT"/g ${DTRANS_DIR}config/dtrans.ini.tmp
sed -i s/@@SMTP_SENDER_MAIL@@/"$SMTP_SENDER_MAIL"/g ${DTRANS_DIR}config/dtrans.ini.tmp
sed -i s/@@SMTP_SENDER_NAME@@/"$SMTP_SENDER_NAME"/g ${DTRANS_DIR}config/dtrans.ini.tmp
sed s/@@FRONTEND_URI@@/"$(echo -n "$FRONTEND_URI" | sed -e 's/[\/&]/\\&/g')"/g ${DTRANS_DIR}config/dtrans.ini.tmp > ${DTRANS_DIR}config/dtrans.ini

#general.ini
sed  s/@@DB_NAME@@/"$DB_NAME"/g ${DTRANS_DIR}config/general.placeholder.ini > ${DTRANS_DIR}config/general.ini.tmp

sed -i s/@@DB_USERNAME@@/"$DB_USERNAME"/g ${DTRANS_DIR}config/general.ini.tmp

sed  s/@@DB_PASSWORD@@/"$DB_PASSWORD"/g ${DTRANS_DIR}config/general.ini.tmp > ${DTRANS_DIR}config/general.ini


#api.ini
sed  s/@@LDAP_HOST@@/"$LDAP_HOST"/g ${DTRANS_DIR}config/api.placeholder.ini > ${DTRANS_DIR}config/api.ini.tmp

sed -i s/@@LDAP_BIND_DN@@/"$LDAP_BIND_DN"/g ${DTRANS_DIR}config/api.ini.tmp
sed -i s/@@LDAP_BIND_PASSWORD@@/"$LDAP_BIND_PASSWORD"/g ${DTRANS_DIR}config/api.ini.tmp
sed -i s/@@LDAP_SEARCH_BASE@@/"$LDAP_SEARCH_BASE"/g ${DTRANS_DIR}config/api.ini.tmp

sed  s/@@LDAP_SEARCH_QUERY_FILTER@@/"$(printf %q $LDAP_SEARCH_QUERY_FILTER)"/g ${DTRANS_DIR}config/api.ini.tmp > ${DTRANS_DIR}config/api.ini

# php unit tests
sed -i s/@@FRONTEND_URI@@/"$(echo -n "$FRONTEND_URI" | sed -e 's/[\/&]/\\&/g')"/g ${DTRANS_DIR}tests/TestEnvironment.php

rm ${DTRANS_DIR}config/dtrans.ini.tmp
rm ${DTRANS_DIR}config/general.ini.tmp
rm ${DTRANS_DIR}config/api.ini.tmp
