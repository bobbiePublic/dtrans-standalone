#!/bin/bash
set -eu

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DTRANS_DIR=$MY_DIR/../
cd $DTRANS_DIR

php vendor/phpunit/phpunit/phpunit --colors --verbose tests