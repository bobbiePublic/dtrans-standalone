#!/bin/bash
set -eu
if [ "$EUID" -ne 0 ]
  then echo "Upgrade script can only run as root"
  exit
fi

SNIP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DTRANS_DIR=$SNIP_DIR/../
echo "expecting dtrans at $DTRANS_DIR"
cd "$DTRANS_DIR"

. deploy/snippets/stop-router.sh

echo "Saving files..."
rm -fr tmp
mkdir tmp
rm -rf documents/1lieferschein/error
rm -rf documents/1lieferschein/duplicate
mv -v documents/* tmp
rm -fr incoming/*
rm -fr logs/*

echo "Wiping database..."
. deploy/wipe-everything.sh

echo "Uploading previous documents..."
mv -v tmp/* incoming
php router/DtransFileProcessor.php

echo "Restoring folder structure..."
rm -rf incoming/*
rm -rf tmp
php router/DtransFileProcessor.php

echo "Done!"