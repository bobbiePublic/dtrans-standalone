#!/bin/bash
set -eu
if [ "$EUID" -ne 0 ]
  then echo "Wipe script can only run as root"
  exit
fi
SNIP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DTRANS_DIR=$SNIP_DIR/../
echo "expecting dtrans at $DTRANS_DIR"
cd "$DTRANS_DIR"

. deploy/snippets/stop-router.sh

echo "Cleaning files"
rm -rf incoming/*
rm -rf documents/*
rm -rf logs/*.log
rm -rf logs/*.crash

DB_NAME=$(grep DB_NAME < config.php | cut -d "'" -f 4)
DB_USERNAME=$(grep DB_USERNAME < config.php | cut -d "'" -f 4)
#DB_PASSWORD=$(grep DB_PASSWORD < config.php | cut -d "'" -f 4)

echo "KILL USER $DB_USERNAME"
#echo "KILL USER $DB_USERNAME" | mysql -u$DB_USERNAME -p$DB_PASSWORD
echo "KILL USER $DB_USERNAME" | mysql

echo "Cleaning database"
#mysql -u$DB_USERNAME -p$DB_PASSWORD -D $DB_NAME --execute="DROP DATABASE $DB_NAME; CREATE DATABASE $DB_NAME"
mysql -D $DB_NAME --execute="DROP DATABASE $DB_NAME; CREATE DATABASE $DB_NAME"
mysql --execute="RESET QUERY CACHE;"

. deploy/snippets/start-router.sh
