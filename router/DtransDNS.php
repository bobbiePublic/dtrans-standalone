<?php

namespace Dtrans\router;

use Dtrans\core\enums\DtransFormatsEnum;
use Dtrans\core\helpers\DtransLogger;

abstract class DtransDNS
{

    const DTRANS_DNS_RECORD_PREFIX = '_dtrans_https_';
    const DTRANS_DNS_RECORD_TYPE = 'SRV';

    public static function seek_options(string $host, string $format): array
    {
        $options = array();

        $dns_name = DtransFormatsEnum::get_dns_name($format);
        if (empty($dns_name)) {
            DtransLogger::warning('Unknown DTRANS format was queried in DNS lookup.', ['$format' => $format, '$host' => $host]);
            return $options;
        }

        $dns_name = self::DTRANS_DNS_RECORD_PREFIX . $dns_name . '._tcp.' . $host;

        if (!checkdnsrr($dns_name, self::DTRANS_DNS_RECORD_TYPE)) {
            DtransLogger::debug('DtransRouter: DNS entry not set for host.', ['$dns_name' => $dns_name, '$format' => $format]);
            return $options;
        }

        $records = dns_get_record($dns_name, DNS_SRV);
        if (empty($records)) {
            DtransLogger::debug('DtransRouter: No DNS entries found.', ['$dns_name' => $dns_name]);
            return $options;
        }

        // 'host' => '_dtrans_https_1lieferschein._tcp.dev.dtrans.bobbie.de', 'class' => 'IN', 'ttl' => 300, 'type' => 'SRV', 'pri' => 10, 'weight' => 20, 'port' => 443, 'target' => 'dev.dtrans.bobbie.de'
        foreach ($records as $record) {
            if (empty($record['type']) || $record['type'] != self::DTRANS_DNS_RECORD_TYPE)
                continue; // ignore non SRV types

            if (empty($record['target']) || empty($record['port']))
                continue;

            $target = $record['target'];
            $port = $record['port'];
            $pri = empty($record['pri']) ? 100 : $record['pri'];
            $weight = empty($record['weight']) ? 10 : $record['weight'];
            $options[] = array('target' => $target, 'port' => $port, 'priority' => $pri, 'weight' => $weight);
        }

        DtransLogger::debug('DtransRouter: Found DNS entries.', ['$dns_name' => $dns_name, '$options' => var_export($options, true)]);
        return $options;
    }
}