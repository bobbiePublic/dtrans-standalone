<?php

namespace Dtrans;

require_once __DIR__ . '/../config.php';

use Dtrans\core\enums\DtransFormatsEnum;
use Dtrans\core\enums\IncomingInterfaceEnum;
use Dtrans\core\enums\ProcessingReturnCodesEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\providers\einlieferschein\LieferscheinProcessor;
use Dtrans\core\providers\IncomingProcessor;
use Dtrans\core\providers\pdf\PDFProcessor;
use Dtrans\core\providers\ubl\UBLProcessor;

// process incoming documents
$incoming = __DIR__ . '/../' . PATH_FILE_PROCESSOR_INCOMING;
foreach (DtransFormatsEnum::_values as $format) {
    if(strcmp($format, DtransFormatsEnum::UNKNOWN) === 0) continue;

    $files = scandir($incoming . $format . '/');
    $files = array_diff($files, array('.', '..', '.keep'));
    $dir_src = $incoming . $format . '/';
    //$dir_des = __DIR__ . '/../' . PATH_DOCUMENTS . $format . '/';

    foreach ($files as $file_name) {
        DtransLogger::debug("DtransFileProcessor: Start processing of file.", ['file_name' => $file_name, 'format' => $format]);

        $xml = file_get_contents($dir_src . $file_name);
        $nfn = IncomingProcessor::create_file($xml, $format, IncomingInterfaceEnum::FILE_PROCESSOR);
        if(empty($nfn)) {
            DtransLogger::info("DtransFileProcessor: Skipping duplicate file hash.", ['file_name' => $file_name, 'format' => $format]);
            if(!unlink($dir_src . $file_name))
                DtransLogger::warning("DtransFileProcessor: Failed do delete duplicate file.", ['file_name' => $file_name, 'format' => $format]);
            continue;
        }

        switch ($format)
        {
            case DtransFormatsEnum::EINLIEFERSCHEIN:
                $rc = LieferscheinProcessor::process($xml, $nfn);
                break;

            case DtransFormatsEnum::UBL:
                $rc = UBLProcessor::process($xml, $nfn);
                break;

            case DtransFormatsEnum::PDF:
                PDFProcessor::process($xml, $file_name);
                break;

            default:
                $rc = ProcessingReturnCodesEnum::NOT_SUPPORTED;
                break;
        }

        switch ($rc) {
            case ProcessingReturnCodesEnum::PROCESSED:
                DtransLogger::info("DtransFileProcessor: File was processed and queued for sending.", ['incoming_file_name' => $file_name, 'new_file_name' => $nfn, 'format' => $format]);
                break;
            case ProcessingReturnCodesEnum::DUPLICATE:
                DtransLogger::info("DtransFileProcessor: File was duplicate and will be ignored.", ['incoming_file_name' => $file_name, 'new_file_name' => $nfn, 'format' => $format]);
                break;
            case ProcessingReturnCodesEnum::ERROR:
                DtransLogger::info("DtransFileProcessor: File was invalid and will be ignored.", ['incoming_file_name' => $file_name, 'new_file_name' => $nfn, 'format' => $format]);
                break;
            case ProcessingReturnCodesEnum::NOT_SUPPORTED:
                DtransLogger::info("DtransFileProcessor: File is not supported and will be ignored.", ['incoming_file_name' => $file_name, 'new_file_name' => $nfn, 'format' => $format]);
                break;
        }

        if(!unlink($dir_src . $file_name))
            DtransLogger::warning("DtransFileProcessor: Failed do delete processed file.", ['file_name' => $file_name, 'format' => $format]);
    }
}