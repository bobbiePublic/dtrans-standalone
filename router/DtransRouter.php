<?php

namespace Dtrans\router;

require_once __DIR__ . '/../config.php';

use Dtrans\core\database\models\ModelCacheErrorLog;
use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\models\ModelSend;
use Dtrans\core\database\models\ModelSending;
use Dtrans\core\database\tables\DBTableSendingQueue;
use Dtrans\core\enums\DtransFormatsEnum;
use Dtrans\core\enums\DtransRoutingResult;
use Dtrans\core\helpers\DocumentReader;
use Dtrans\core\helpers\DtransConfig;
use Dtrans\core\helpers\DtransLogger;

// just exit if router is disabled in config
if (!DtransConfig::get_bool(DtransConfig::CFG_DTRANS, 'router', 'enable', true))
    exit(0);


// make sure only one instance is running at a time via lock file
$lock_file = __DIR__ . '/router.lock';
$fp = fopen($lock_file, 'r+');
if ($fp === false) {
    $report = ModelCacheErrorLog::report_cron_error(__FILE__ . ': Failed to open lock file. Likely the permission/owner of the file is messed up.');
    exit($report ? 1 : 0);
}

if (!flock($fp, LOCK_EX | LOCK_NB)) {
    $report = ModelCacheErrorLog::report_cron_error(__FILE__ . ': Only one instance of the DtransRouter is allowed at the time! Looks like something is calling the router too often.');
    exit($report ? 1 : 0);
}

// remove lock on shutdown
register_shutdown_function(function () {
    global $fp;
    flock($fp, LOCK_UN);
    fclose($fp);
});

// query list of queue for sending
$result = ModelSending::get_sending_entries();
if (empty($result) || count($result) == 0) return;

$ids_failed = [];
$ids_completed = [];
$ids_completion_check_required = [];
foreach ($result as $entry) {
    // check if fallback mail should be used
    $attempt = (int)$entry[DBTableSendingQueue::ATTEMPT_COUNT];
    if ($attempt >= DtransConfig::get_int(DtransConfig::CFG_DTRANS, 'router', 'max_attempts', 1000)) {
        $mail = $entry[DBTableSendingQueue::FALLBACK_MAIL];

        // if no mail is available fail
        if (empty($mail)) {
            ModelSend::create_sending_result($entry[DBTableSendingQueue::DOCUMENT_ID], $entry[DBTableSendingQueue::DESTINATION],
                DtransRoutingResult::FAILED, 'Maximum attempt count exceeded and no fallback mail is available.');
            DtransLogger::notice('Fallback mail was NOT send as there is no fallback mail available.',
                [DBTableSendingQueue::DOCUMENT_ID => $entry[DBTableSendingQueue::DOCUMENT_ID],
                    DBTableSendingQueue::DESTINATION => $entry[DBTableSendingQueue::DESTINATION]]);
        } else {
            DtransSender::send_fallback_mail($entry);
            ModelSend::create_sending_result($entry[DBTableSendingQueue::DOCUMENT_ID], $entry[DBTableSendingQueue::DESTINATION],
                DtransRoutingResult::FALLBACK_MAIL, 'Maximum attempt count exceeded and fallback mail was send to ' . $entry[DBTableSendingQueue::FALLBACK_MAIL] . '.');
            DtransLogger::notice('Fallback mail was send.', [DBTableSendingQueue::DOCUMENT_ID => $entry[DBTableSendingQueue::DOCUMENT_ID],
                DBTableSendingQueue::DESTINATION => $entry[DBTableSendingQueue::DESTINATION], DBTableSendingQueue::FALLBACK_MAIL => $entry[DBTableSendingQueue::FALLBACK_MAIL]]);
        }

        // remove from sending table and queue up for completion check
        $ids_completed[] = $entry[DBTableSendingQueue::ID];
        if (!in_array($entry[DBTableSendingQueue::DOCUMENT_ID], $ids_completion_check_required))
            $ids_completion_check_required[] = $entry[DBTableSendingQueue::DOCUMENT_ID];

        continue;
    }

    // load file and continue
    $file = explode('/', $entry[DBTableSendingQueue::DOCUMENT_PATH]);
    $type = DtransFormatsEnum::parse($file[0]);
    if (empty($type)) {
        DtransLogger::error('Failed to send document: Document type was unknown.', [DBTableSendingQueue::DOCUMENT_ID => $entry[DBTableSendingQueue::DOCUMENT_ID],
            DBTableSendingQueue::DESTINATION => $entry[DBTableSendingQueue::DESTINATION], DBTableSendingQueue::FALLBACK_MAIL => $entry[DBTableSendingQueue::FALLBACK_MAIL]]);
    }
    $xml = DocumentReader::read_document($file[1], $type, true);
    if (empty($xml)) {
        DtransLogger::error('Failed to send document: Document file was not found.', [DBTableSendingQueue::DOCUMENT_ID => $entry[DBTableSendingQueue::DOCUMENT_ID],
            DBTableSendingQueue::DESTINATION => $entry[DBTableSendingQueue::DESTINATION], DBTableSendingQueue::FALLBACK_MAIL => $entry[DBTableSendingQueue::FALLBACK_MAIL]]);
    } else {
        $options = DtransDNS::seek_options($entry[DBTableSendingQueue::DESTINATION], $type);
        $signature = DtransSender::send_dtrans($entry, $xml, $options);

        // sending failed. Increment attempt count and try again in a few minutes
        if (empty($signature)) {
            DtransLogger::debug('Failed to send document: Dtrans transmission failed.', [DBTableSendingQueue::DOCUMENT_ID => $entry[DBTableSendingQueue::DOCUMENT_ID],
                DBTableSendingQueue::DESTINATION => $entry[DBTableSendingQueue::DESTINATION], DBTableSendingQueue::FALLBACK_MAIL => $entry[DBTableSendingQueue::FALLBACK_MAIL],
                DBTableSendingQueue::ATTEMPT_COUNT => $entry[DBTableSendingQueue::ATTEMPT_COUNT]]);
            $ids_failed[] = $entry[DBTableSendingQueue::ID];
            continue;
        }

        // save signature
        ModelSend::create_sending_result($entry[DBTableSendingQueue::DOCUMENT_ID], $entry[DBTableSendingQueue::DESTINATION],
            DtransRoutingResult::SUCCESS, $signature);
        DtransLogger::notice('Dtrans transmission successful.', [DBTableSendingQueue::DOCUMENT_ID => $entry[DBTableSendingQueue::DOCUMENT_ID],
            DBTableSendingQueue::DESTINATION => $entry[DBTableSendingQueue::DESTINATION], 'signature' => $signature]);
    }

    // completed transmission
    $ids_completed[] = $entry[DBTableSendingQueue::ID];
    if (!in_array($entry[DBTableSendingQueue::DOCUMENT_ID], $ids_completion_check_required))
        $ids_completion_check_required[] = $entry[DBTableSendingQueue::DOCUMENT_ID];
}

// increment attempt_count on every failed entry
ModelSending::increment_attempt_count($ids_failed);

// delete completed entries
ModelSending::delete_completed_entries($ids_completed);

// perform completion checks
// a sending process is completed if no more receivers are queued for this document
foreach ($ids_completion_check_required as $id) {
    if (!ModelSending::check_sending_document_completed($id)) continue;

    // set sending state to complete
    ModelDocuments::set_in_dtrans_queue_by_internal_id($id, false);
}