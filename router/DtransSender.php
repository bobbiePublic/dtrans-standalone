<?php

namespace Dtrans\router;

use Dtrans\core\database\models\ModelDocuments;
use Dtrans\core\database\tables\DBTable;
use Dtrans\core\database\tables\DBTableDocuments;
use Dtrans\core\database\tables\DBTableSendingQueue;
use Dtrans\core\enums\DtransFormatsEnum;
use Dtrans\core\helpers\DtransLogger;
use Dtrans\core\mail\DtransMailer;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

abstract class DtransSender
{
    public static function send_fallback_mail(array $entry): void
    {
        //$xml = file_get_contents(PATH_DOCUMENTS . $entry[DBTableSendingQueue::DOCUMENT_PATH]);
        $db_data = ModelDocuments::get_document_info_mail($entry[DBTableSendingQueue::DOCUMENT_ID]);

        if (!empty($db_data)) {
            $data = [
                'ID' => $db_data[DBTableDocuments::CHAIN_ID],
                'UUID' => $db_data[DBTableDocuments::UUID],
                'Typ' => $db_data[DBTableDocuments::TYPE],
                'Zeitstempel (Dokument)' => $db_data[DBTableDocuments::UBL_DATE_DOCUMENT],
                'Zeitstempel (System)' => $db_data[DBTable::CREATED_AT],
            ];
            //$file_name = $db_data[DBTableDocuments::FILE_NAME];

            // remove if date in document is missing
            if (empty($data['Zeitstempel (Dokument)']))
                unset($data['Zeitstempel (Dokument)']);
        }
        //$file_name = explode('/', $entry[DBTableSending::DOCUMENT_PATH])[1];

        DtransMailer::send_dtrans_fallback_mail((empty($data) ? [] : $data), $entry[DBTableSendingQueue::FALLBACK_MAIL]);
    }

    const DTRANS_ADDRESS_PREFIX = 'https://';
    const DTRANS_ADDRESS_POSTFIX = '/.well-known/dtrans';

    public static function send_dtrans(array $entry, string $xml, array $options): ?string
    {
        while (count($options) > 0) {
            $bestIndex = 0;
            $bestPriority = 1000;
            foreach ($options as $index => $option) {
                if ($option['priority'] < $bestPriority) {
                    $bestIndex = $index;
                    $bestPriority = $option['priority'];
                }
            }

            $signature = self::send_dtrans_to($entry, $xml, $options[$bestIndex]);
            if (!empty($signature))
                return $signature;

            array_splice($options, $bestIndex, 1);
        }

        return null;
    }

    private static function send_dtrans_to(array $entry, string $xml, array $option): ?string
    {
        // build post query
        $url = self::DTRANS_ADDRESS_PREFIX . $option['target'];
        if ($option['port'] != 443) // non default port
            $url .= ':' . $option['port'];
        $url .= self::DTRANS_ADDRESS_POSTFIX;

        $format = $entry[DBTableSendingQueue::DOCUMENT_FORMAT];

        $client = new Client();
        try {
            $response = $client->request('POST', $url, [
                'headers' => [
                    'Content-type' => self::get_mime_code($format),
                    'User-Agent' => 'DTRANS_ROUTER/1.0',
                ],
                'query' => [
                    'format' => $format
                ],
                'body' => $xml,
                // remove this line to manually resolve issues
                //'debug' => true
            ]);

            if ($response->getStatusCode() != 200 && $response->getStatusCode() != 201)
                return null;

            $signature = $response->getBody()->getContents();

            // failed
            if (empty($signature)) {
                DtransLogger::debug('Failed to send document via dtrans.', ['url' => $url, DBTableSendingQueue::DOCUMENT_ID => $entry[DBTableSendingQueue::DOCUMENT_ID]]);
                return null;
            }

            if (!str_contains(strtolower($signature), 'signature'))
                DtransLogger::notice('Could not find signature in dtrans response.', ['signature' => $signature, DBTableSendingQueue::DOCUMENT_ID => $entry[DBTableSendingQueue::DOCUMENT_ID]]);

            return $signature;
        } catch (GuzzleException $e) {
            DtransLogger::debug('Failed to send document via dtrans due to exception.', ['exception' => $e->getMessage(), 'url' => $url, DBTableSendingQueue::DOCUMENT_ID => $entry[DBTableSendingQueue::DOCUMENT_ID]]);
            return null;
        }
    }

    private static function get_mime_code(string $format): string
    {
        return match ($format) {
            DtransFormatsEnum::PDF => 'application/pdf',
            default => 'application/xml',
        };
    }
}