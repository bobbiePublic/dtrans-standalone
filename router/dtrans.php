<?php

namespace Dtrans;

require_once __DIR__ . '/../config.php';

use Dtrans\core\enums\DtransFormatsEnum;
use Dtrans\core\enums\HttpStatusCodesEnum;
use Dtrans\core\enums\IncomingInterfaceEnum;
use Dtrans\core\enums\ParameterTypesEnum;
use Dtrans\core\enums\RejectionCodeEnum;
use Dtrans\core\helpers\DtransConfig;
use Dtrans\core\helpers\SanitizeHelper;
use Dtrans\core\helpers\UserFeedback;
use Dtrans\core\providers\einlieferschein\LieferscheinProcessor;
use Dtrans\core\providers\IncomingProcessor;
use Dtrans\core\providers\pdf\PDFProcessor;
use Dtrans\core\providers\ubl\UBLProcessor;
use Dtrans\core\serializer\JsonSerializer;
use Dtrans\router\signature\DtransSigner;

$is_test = isset($_GET['test']) && SanitizeHelper::sanitize($_GET['test'], false, ParameterTypesEnum::BOOLEAN);

if (empty('php://input')) {
    echo "No file uploaded.\n";
    http_response_code(HttpStatusCodesEnum::BAD_REQUEST);
    exit();
}

// read input
$data = file_get_contents('php://input');

$format = $_GET['format'] ?? null;
if(is_null($format)) {
    echo "Required parameter 'format' is missing. Please resend request!\n";
    http_response_code(HttpStatusCodesEnum::BAD_REQUEST);
    exit();
}

// check if format is known
$f = DtransFormatsEnum::parse($format);
if(strcmp($f, DtransFormatsEnum::UNKNOWN) === 0) {
    echo "Parameter 'format' is of unsupported type.\n";
    if(!$is_test)
        IncomingProcessor::create_file($data, $format, IncomingInterfaceEnum::DTRANS);
    http_response_code(HttpStatusCodesEnum::BAD_REQUEST);
    exit();
}

if($is_test) {
    $success = true;
    switch($f) {
        case DtransFormatsEnum::EINLIEFERSCHEIN:
            $success = LieferscheinProcessor::validate($data);
            break;
        case DtransFormatsEnum::UBL:
            $success = UBLProcessor::validate($data);
            break;
    }
    if($success) {
        http_response_code(HttpStatusCodesEnum::OK);
        UserFeedback::notice_custom("Test successful.");
    } else {
        http_response_code(HttpStatusCodesEnum::BAD_REQUEST);
        UserFeedback::notice_custom("Test failed.");
    }

    // give feedback to user
    echo (new JsonSerializer())->flush_reports();
    exit();
}

// tell sender if format is not supported yet
if (!in_array($f, [DtransFormatsEnum::EINLIEFERSCHEIN, DtransFormatsEnum::UBL, DtransFormatsEnum::PDF])) {
    echo "Format '" . $f . "' is currently not implemented.\n";
    IncomingProcessor::create_file($data, $f, IncomingInterfaceEnum::DTRANS, null, RejectionCodeEnum::DTRANS_FORMAT_UNSUPPORTED, "Format not implemented: " . $f);
    http_response_code(HttpStatusCodesEnum::NOT_IMPLEMENTED);
    exit();
}

$file_name = IncomingProcessor::create_file($data, $f, IncomingInterfaceEnum::DTRANS);
if (!empty($file_name))  // only process if file is new
    switch ($f) {
        case DtransFormatsEnum::EINLIEFERSCHEIN:
            LieferscheinProcessor::process($data, $file_name);
            break;
        case DtransFormatsEnum::UBL:
            UBLProcessor::process($data, $file_name);
            break;
        case DtransFormatsEnum::PDF:
            PDFProcessor::process($data, $file_name);
            break;
        default:
            echo "Format '" . $f . "' is currently not implemented.\n";
            IncomingProcessor::create_file($data, $f, IncomingInterfaceEnum::DTRANS, null, RejectionCodeEnum::DTRANS_FORMAT_UNSUPPORTED, "Format not implemented: " . $f);
            http_response_code(HttpStatusCodesEnum::NOT_IMPLEMENTED);
            exit();
    }

// sign document
$selector = DtransConfig::get_string(DtransConfig::CFG_DTRANS, "general", "selector");
$server_fqdn = DtransConfig::get_string(DtransConfig::CFG_DTRANS, "general", "server_fqdn");
$dkim_private_path = DtransConfig::get_string(DtransConfig::CFG_DTRANS, "general", "file_dkim_private_key");
$signer = new DtransSigner($selector, $server_fqdn, $dkim_private_path);
$signed_data = $signer->get_signature($data);
echo($signed_data);
http_response_code(HttpStatusCodesEnum::OK);