<?php

namespace Dtrans\router\signature;

class DtransSigner {
    private string $private_key;
    private string $serverfqdn;
    private string $selector;

    public function __construct($selector, $serverfqdn, $path_private_key) {
        $this -> serverfqdn = $serverfqdn;
        $this -> selector = $selector;
        $this -> private_key = file_get_contents($path_private_key);
    }


    public function get_signature($message) {
        $timestamp = time();
        $data = 'T=' . $timestamp . ',S=' . $this -> serverfqdn . ',M=' . $message;

        // openssl_sign returns signature by reference
        $signed_data = '';

        $dkim_signature =
            'DKIM-Signature: ' .
                'v=1;'."\r\n\t" .
                'a=rsa-sha256;' . "\r\n\t" .
                's=' . $this -> selector . ';' . "\r\n\t" .
                'd=' . $this -> serverfqdn.';' . "\r\n\t" .
                'q=dns/txt;' . "\r\n\t" .
                't=' . $timestamp . ';' . "\r\n\t" .
                'b=';

        if (openssl_sign($data, $signed_data, $this -> private_key, OPENSSL_ALGO_SHA256)) {
            $dkim_signature .= rtrim(chunk_split(base64_encode($signed_data), 64, "\r\n\t")) . "\r\n";
        } else {
            trigger_error(sprintf('Could not sign core: %s', $data), E_USER_ERROR);
        }

        return $dkim_signature;
    }
}
