<?php
declare(strict_types=1);

// DEFINE TEST ENVIROMENT
const IS_DTRANS_TEST_PHP_UNIT_ENVIROMENT = TRUE;
require_once __DIR__ . '/../config.php';

const DTRANS_BASE_URL = "@@FRONTEND_URI@@";
const DTRANS_API_PATH = "/dtrans/v1/";
const SAMPLE_DOCUMENTS_PATH = __DIR__ . '/samples/';
const GUZZLE_OPTIONS = ['base_uri' => DTRANS_BASE_URL . DTRANS_API_PATH, 'http_errors' => false];

// test users
const ACCOUNT_ADMIN = array('lara.kraft@dtrans-testing.invalid', 'RstbGY2@zMiDxmk8OQOXrCc3');
const ACCOUNT_BOOKKEEPER = array('gerhard.freimann@dtrans-testing.invalid', 'Qah#%$H1#%4J3BCdEbxb*wHf');

// some more test constants
const API_LOGIN_TOKEN = 'auth/token/login';
const API_LOGOUT_TOKEN = 'auth/token/logout';
const API_DOCUMENTS_NEW_1LS = 'documents/new/1lieferschein';