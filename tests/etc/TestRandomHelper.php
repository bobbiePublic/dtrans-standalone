<?php

namespace Dtrans\tests\etc;

abstract class TestRandomHelper
{
    public static function generate_random_string(int $length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++)
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        return $randomString;
    }

    public static function generate_random_ID(int $length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
            if ($i == 3 || $i == 7)
                $randomString .= '-';
        }
        return $randomString;
    }

    public static function generate_random_UUIDv4($data = null)
    {
        if (empty($data))
            $data = random_bytes(16);

        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    public static function pseudo_randomize_ubl_document(string $document)
    {
        $document = str_replace('{{RANDOM:ID}}', 'dtrans://unit-test.invalid/1lieferschein/' . self::generate_random_ID(), $document);
        $document = str_replace('{{RANDOM:UUID}}', self::generate_random_UUIDv4(), $document);

        return $document;
    }
}