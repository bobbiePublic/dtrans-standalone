<?php

namespace Dtrans\tests\etc;
require_once __DIR__ . '/../TestEnvironment.php';

abstract class UrlValidator
{
    const TEST_SKIPPED_MESSAGE = "API path is not set up correctly in TestEnviroment.php";

    public static function is_api_setup(): bool
    {
        // check if url is valid
        $regex = "((https?|ftp)\:\/\/)?"; // SCHEME
        $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
        $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP
        $regex .= "(\:[0-9]{2,5})?"; // Port
        $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
        $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
        $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor
        return preg_match("/^$regex$/i", DTRANS_BASE_URL . DTRANS_API_PATH);
    }
}