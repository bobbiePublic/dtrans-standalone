<?php

namespace Dtrans\tests\unit;

// DEFINE TEST ENVIROMENT DO NOT REMOVE THIS LINE
require_once __DIR__ . '/../TestEnvironment.php';

use Dtrans\core\constants\ConstsLogin;
use Dtrans\tests\etc\TestRandomHelper;
use Dtrans\tests\etc\UrlValidator;
use GuzzleHttp;
use PHPUnit\Framework\TestCase;

final class EinLSDocumentsTest extends TestCase
{
    /* LOGIN CODE */
    private string $token = '';

    protected function setUp(): void
    {
        if (!UrlValidator::is_api_setup())
            $this->markTestSkipped(UrlValidator::TEST_SKIPPED_MESSAGE);

        $json = [ConstsLogin::LOGIN_JSON_USERNAME => ACCOUNT_BOOKKEEPER[0], ConstsLogin::LOGIN_JSON_PASSWORD => ACCOUNT_BOOKKEEPER[1]];
        $client = new GuzzleHttp\Client(GUZZLE_OPTIONS);

        $res = $client->post(API_LOGIN_TOKEN, [GuzzleHttp\RequestOptions::JSON => $json]);

        self::assertSame($res->getStatusCode(), 200);
        $body = json_decode($res->getBody()->getContents(), true);
        $token = $body[ConstsLogin::LOGIN_JSON_TOKEN];
        self::assertNotEmpty($token);
        self::assertSame(ConstsLogin::LOGIN_TOKEN_LENGTH, strlen($token));

        // save token for tests
        $this->token = $token;
    }

    protected function tearDown(): void
    {
        $client = new GuzzleHttp\Client(GUZZLE_OPTIONS);
        $res = $client->DELETE(API_LOGOUT_TOKEN, ['query' => [ConstsLogin::LOGIN_DELETE_TOKEN => $this->token]]);
        self::assertSame($res->getStatusCode(), 200);
    }

    /* DOCUMENT PROVIDERS */
    /*public static function allDocumentsProvider(): array
    {
        $xml = glob(SAMPLE_DOCUMENTS_PATH . '*.xml');
        $documents = array();
        foreach ($xml as $x) {
            $doc = file_get_contents($x);
            $doc = TestRandomHelper::pseudo_randomize_ubl_document($doc);

            $filename = explode('/', $x);
            $documents[end($filename)] = array($doc);
        }

        return $documents;
    } */


    public static function goodDocumentProvider(): array
    {
        $files = array('namespaces.xml', 'extreme_namespaces.xml', 'minimal.xml', 'postal.xml', 'embedded_files.xml', 'htmlcomments.xml', 'sample1.xml');
        $documents = array();

        foreach ($files as $file) {
            $doc = file_get_contents(SAMPLE_DOCUMENTS_PATH . $file);
            $doc = TestRandomHelper::pseudo_randomize_ubl_document($doc);
            $documents[$file] = array($doc);
        }

        return $documents;
    }

    /**
     * @test
     * @dataProvider goodDocumentProvider
     */
    public function testDocumentAccepted(string $document)
    {
        $client = new GuzzleHttp\Client(GUZZLE_OPTIONS);
        $res = $client->post(API_DOCUMENTS_NEW_1LS, [GuzzleHttp\RequestOptions::BODY => $document, 'headers' => ['Authorization' => 'Bearer ' . $this->token]]);

        self::assertSame($res->getStatusCode(), 201);
        $con = $res->getBody()->getContents();
        self::assertEmpty($con);
    }
}
