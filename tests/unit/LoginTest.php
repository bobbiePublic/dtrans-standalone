<?php

namespace Dtrans\tests\unit;

// DEFINE TEST ENVIROMENT DO NOT REMOVE THIS LINE
require_once __DIR__ . '/../TestEnvironment.php';

use Dtrans\core\constants\ConstsLogin;
use Dtrans\tests\etc\UrlValidator;
use GuzzleHttp;
use PHPUnit\Framework\TestCase;

final class LoginTest extends TestCase
{

    public function setUp(): void
    {
        if (!UrlValidator::is_api_setup())
            $this->markTestSkipped(UrlValidator::TEST_SKIPPED_MESSAGE);
    }

    /* SUCCESSFUL */
    public static function successfulLoginProvider(): array
    {
        return [
            'login admin' => ACCOUNT_ADMIN,
            'login bookkeeper' => ACCOUNT_BOOKKEEPER,
        ];
    }

    /**
     * @test
     * @dataProvider successfulLoginProvider
     */
    public function testSuccessfulLogin(string $username, string $password)
    {
        $json = [ConstsLogin::LOGIN_JSON_USERNAME => $username, ConstsLogin::LOGIN_JSON_PASSWORD => $password];
        $client = new GuzzleHttp\Client(GUZZLE_OPTIONS);

        $res = $client->post(API_LOGIN_TOKEN, [GuzzleHttp\RequestOptions::JSON => $json]);

        self::assertSame($res->getStatusCode(), 200);
        $body = json_decode($res->getBody()->getContents(), true);
        self::assertNotEmpty($body[ConstsLogin::LOGIN_JSON_TOKEN]);
        self::assertSame(ConstsLogin::LOGIN_TOKEN_LENGTH, strlen($body[ConstsLogin::LOGIN_JSON_TOKEN]));

        $res = $client->DELETE(API_LOGOUT_TOKEN, ['query' => [ConstsLogin::LOGIN_DELETE_TOKEN => $body[ConstsLogin::LOGIN_JSON_TOKEN]]]);
        self::assertSame($res->getStatusCode(), 200);
    }

    /* FAILED */
    public static function failedLoginProvider(): array
    {
        return [
            'invalid credentials' => ['dasddasa', 'ad23dh2gwz', 401],
            'no credentials' => [null, null, 400],
            'empty credentials' => ['', '', 401],
            'no password' => ['paul@fehler.de', null, 400],
            'no username' => [null, 'f3fg3fg37gfgf73g73', 400],
            'incorrect password #1' => [ACCOUNT_ADMIN[0], '4789fsfweu23f2F2a', 401],
            'incorrect password #2' => [ACCOUNT_ADMIN[0], '3h78rf3gfg387fg37fg3fg', 401],
            'incorrect password #3' => [ACCOUNT_BOOKKEEPER[0], '4289frh2fg28gf2gf27fg27fg28gf', 401],
            'incorrect password #4' => [ACCOUNT_BOOKKEEPER[0], 'hallo', 401],
        ];
    }

    /**
     * @test
     * @dataProvider failedLoginProvider
     */
    public function testFailedLogin(?string $username, ?string $password, int $expectedStatusCode)
    {
        $json = [];
        if (!is_null($username))
            $json[ConstsLogin::LOGIN_JSON_USERNAME] = $username;
        if (!is_null($password))
            $json[ConstsLogin::LOGIN_JSON_PASSWORD] = $password;

        $client = new GuzzleHttp\Client(GUZZLE_OPTIONS);
        $res = $client->post(API_LOGIN_TOKEN, [GuzzleHttp\RequestOptions::JSON => $json]);

        self::assertSame($res->getStatusCode(), $expectedStatusCode);
        self::assertEmpty($res->getBody()->getContents());
    }

    /** @test */
    public function testLoginWrongMethod()
    {
        $json = [ConstsLogin::LOGIN_JSON_USERNAME => ACCOUNT_BOOKKEEPER[0], ConstsLogin::LOGIN_JSON_PASSWORD => ACCOUNT_BOOKKEEPER[1]];

        $client = new GuzzleHttp\Client(GUZZLE_OPTIONS);
        $res = $client->delete(API_LOGIN_TOKEN, [GuzzleHttp\RequestOptions::JSON => $json]);

        self::assertSame($res->getStatusCode(), 405);
        self::assertEmpty($res->getBody()->getContents());
    }

    /** @test */
    public function testLoginInvalidMethod()
    {
        $json = [ConstsLogin::LOGIN_JSON_USERNAME => ACCOUNT_BOOKKEEPER[0], ConstsLogin::LOGIN_JSON_PASSWORD => ACCOUNT_BOOKKEEPER[1]];

        $client = new GuzzleHttp\Client(GUZZLE_OPTIONS);
        $res = $client->request('CAT', API_LOGIN_TOKEN, [GuzzleHttp\RequestOptions::JSON => $json]);

        self::assertSame($res->getStatusCode(), 405);
        self::assertEmpty($res->getBody()->getContents());
    }
}
